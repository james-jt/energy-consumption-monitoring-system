﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace EcmsServices
{
    public class EmailServiceSettings
    {
        public string Host { get; set; }
        public string Port{ get; set; }
        public SmtpDeliveryMethod DeliveryMethod { get; set; }
        public string SmtpUSername { get; set; }
        public string SmtpPassword { get; set; }
        public string SmtpSourceAddress { get; set; }
        public bool SslEnabled { get; set; }
        public bool UseDefaultCredentials { get; set; }
    }

    public class EmailService
    {
        public string userName;
        public string password;
        public bool useSsl;
        public string fromAddress;

        public EmailService(EmailServiceSettings settings)
        {
            //to be continued
        }

        public EmailService()
        {
            Client = new SmtpClient();
            Client.Host = ConfigurationManager.AppSettings["SmtpServer"] == null ? "loalhost" : ConfigurationManager.AppSettings["SmtpServer"];
            Client.Port = ConfigurationManager.AppSettings["SmtpPort"] == null ? 0 : Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            Client.DeliveryMethod = SmtpDeliveryMethod.Network;
            userName = ConfigurationManager.AppSettings["EmailUsername"] == null ? string.Empty : ConfigurationManager.AppSettings["EmailUsername"];
            password = ConfigurationManager.AppSettings["EmailPassword"] == null ? string.Empty : ConfigurationManager.AppSettings["EmailPassword"];
            fromAddress = ConfigurationManager.AppSettings["FromAddress"] == null ? string.Empty : ConfigurationManager.AppSettings["FromAddress"];
            useSsl = ConfigurationManager.AppSettings["UseSsl"] == null ? false : bool.Parse(ConfigurationManager.AppSettings["UseSsl"]);
            Client.Credentials = new System.Net.NetworkCredential(userName, password);
            Client.EnableSsl = useSsl;
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(customCertValidation);
            Client.UseDefaultCredentials = false;
        }

        public SmtpClient Client { get; set; }

        public async Task SendAsync(MailMessage message)
        {
            if (message.From == null || string.IsNullOrEmpty(message.From.Address))
                message.From = new MailAddress(fromAddress);
            await Task.Run(() => Client.SendAsync(message, new object()));
        }

        public async Task SendAsync(string destination, string subject, string body)
        {
            var message = new MailMessage(fromAddress, destination);
            message.Body = body;
            message.Subject = subject;
            var emailService = new EmailService();
            await Task.Run(() => Client.SendAsync(message, new object()));
        }

        private bool customCertValidation(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

    } 
}
