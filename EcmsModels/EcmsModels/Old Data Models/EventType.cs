namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EventType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        public int SeverityLevel { get; set; }

        [StringLength(128)]
        public string EventCategory { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public bool Disabled { get; set; }

        public short? EventCategory_EventCategoryId { get; set; }

        public byte? SeverityLevel_SeverityLevelId { get; set; }
    }
}
