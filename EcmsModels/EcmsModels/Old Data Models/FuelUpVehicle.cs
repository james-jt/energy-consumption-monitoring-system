namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelUpVehicle")]
    public partial class FuelUpVehicle
    {
        [Key]
        [Column(Order = 0)]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string DrawdownId { get; set; }

        public DateTime Date { get; set; }

        public int Quantity { get; set; }

        public int LevelBefore { get; set; }

        public int LevelAfter { get; set; }

        public int Mileage { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        public string FuelledBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Drawdown Drawdown { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
