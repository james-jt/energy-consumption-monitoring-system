namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectricityTokenIssue")]
    public partial class ElectricityTokenIssue
    {
        public string Id { get; set; }

        [Required]
        [StringLength(128)]
        public string ElectricityPurchaseId { get; set; }

        public DateTime DateIssued { get; set; }

        public virtual ElectricityPurchase ElectricityPurchas { get; set; }

        public virtual ElectricityRecharge ElectricityRecharge { get; set; }

        public virtual ElectricityRequest ElectricityRequest { get; set; }
    }
}
