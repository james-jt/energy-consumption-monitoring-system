namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FuelSource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FuelSource()
        {
            FuelDeliveries = new HashSet<FuelDelivery>();
            FuelInventories = new HashSet<FuelInventory>();
            FuelInventoryHistories = new HashSet<FuelInventoryHistory>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short Id { get; set; }

        public byte FuelSourceTypeId { get; set; }

        [StringLength(128)]
        [Index(IsUnique = true)]
        public string Description { get; set; }

        public byte StatusId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelDelivery> FuelDeliveries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelInventory> FuelInventories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelInventoryHistory> FuelInventoryHistories { get; set; }

        public virtual FuelSourceStatus FuelSourceStatu { get; set; }

        public virtual FuelSourceType FuelSourceType { get; set; }
    }
}
