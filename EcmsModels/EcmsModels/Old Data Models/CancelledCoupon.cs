﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.DataModels
{
    public partial class CancelledCoupon
    {
        public long Id { get; set; }
        public long BatchId { get; set; }
        public System.DateTime DateCancelled { get; set; }
        public string Reason { get; set; }
        public string CancelledBy { get; set; }
        public string Notes { get; set; }

        public virtual CouponsBatch CouponsBatch { get; set; }
    }
}
