namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectricityPurchases")]
    public partial class ElectricityPurchase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ElectricityPurchase()
        {
            ElectricityTokenIssues = new HashSet<ElectricityTokenIssue>();
        }

        public string Id { get; set; }

        public DateTime Date { get; set; }

        [StringLength(128)]
        public string ReceiptNumber { get; set; }

        [Required]
        [StringLength(128)]
        public string MeterNumber { get; set; }

        [StringLength(512)]
        public string CustomerName { get; set; }


        [Display(Name = "Meter Address")]
        public string MeterAddress { get; set; }

        [Required]
        [StringLength(32)]
        public string Token { get; set; }

        [StringLength(64)]
        public string Tariff { get; set; }

        [Required]
        public double? EnergyBought { get; set; }

        public double? TenderAmount { get; set; }

        public double? EnergyCharge { get; set; }

        public double? DebtCollected { get; set; }

        public double? ReLevy { get; set; }

        public double? VatRate { get; set; }

        public double? VatAmount { get; set; }

        [Required]
        public double? TotalPaid { get; set; }

        public double? DebtBalBf { get; set; }

        public double? DebtBalCf { get; set; }

        [StringLength(128)]
        public string VendorNumberAndName { get; set; }

        [StringLength(128)]
        public string VendorInfo1 { get; set; }

        [StringLength(128)]
        public string VendorInfo2 { get; set; }

        [StringLength(128)]
        public string VendorInfo3 { get; set; }

        [StringLength(128)]
        public string TariffIndex { get; set; }

        [StringLength(128)]
        public string SupplyGroupCode { get; set; }

        [StringLength(128)]
        public string KeyRevNumber { get; set; }

        public bool Authorised { get; set; }

        [StringLength(128)]
        public string BoughtBy { get; set; }

        public DateTime DateBought { get; set; }

        [StringLength(128)]
        public string AuthorisedBy { get; set; }

        public DateTime DateAuthorised { get; set; }
         
        [StringLength(128)]
        public string IssuedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityTokenIssue> ElectricityTokenIssues { get; set; }
    }
}
