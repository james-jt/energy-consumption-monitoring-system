namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Department()
        {
            Users = new HashSet<EcmsModels.WebModels.ApplicationUser>();
            CouponsBatches = new HashSet<CouponsBatch>();
            DepartmentOverseerAssignmentHistories = new HashSet<DepartmentOverseerAssignmentHistory>();
            Sites = new HashSet<Site>();
            SiteDepartmentAssignmentHistories = new HashSet<SiteDepartmentAssignmentHistory>();
            Vehicles = new HashSet<Vehicle>();
            VehicleDepartmentDeploymentHistories = new HashSet<VehicleDepartmentDeploymentHistory>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public byte ZoneId { get; set; }

        [StringLength(128)]
        public string OverseerUserId { get; set; }

        [StringLength(256)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [StringLength(32)]
        public string Type { get; set; }

        public bool? Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }
         
        public virtual Zone Zone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EcmsModels.WebModels.ApplicationUser> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CouponsBatch> CouponsBatches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DepartmentOverseerAssignmentHistory> DepartmentOverseerAssignmentHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Site> Sites { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteDepartmentAssignmentHistory> SiteDepartmentAssignmentHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleDepartmentDeploymentHistory> VehicleDepartmentDeploymentHistories { get; set; }
    }
}
