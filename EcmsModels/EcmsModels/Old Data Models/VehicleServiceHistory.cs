namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleServiceHistory")]
    public partial class VehicleServiceHistory
    {
        [Key]
        [Column(Order = 0)]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime DateServiced { get; set; }

        public int Mileage { get; set; }

        [StringLength(128)]
        public string JobCardNumber { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        public string ServicedBy { get; set; }

        [StringLength(128)]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
