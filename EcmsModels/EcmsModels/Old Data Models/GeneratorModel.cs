namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GeneratorModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GeneratorModel()
        {
            Generators = new HashSet<Generator>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [StringLength(128)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [StringLength(128)]
        public string EngineMake { get; set; }

        [StringLength(128)]
        public string EngineModel { get; set; }

        [StringLength(128)]
        public string GeneratorMake { get; set; }

        public int ServiceInterval { get; set; }

        public double GeneratorSize { get; set; }

        public int? NormalLifeSpan { get; set; }

        public double? NormalConsuptionRate { get; set; }

        public bool Disabled { get; set; }

        public byte FuelTypeId { get; set; }

        public virtual FuelType FuelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Generator> Generators { get; set; }
    }
}
