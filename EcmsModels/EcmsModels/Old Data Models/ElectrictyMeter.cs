namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ElectrictyMeter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ElectrictyMeter()
        {
            ElectricityLevelChecks = new HashSet<ElectricityLevelCheck>();
            ElectricityRecharges = new HashSet<ElectricityRecharge>();
            SiteElectricityMeterAssignmentHistories = new HashSet<SiteElectricityMeterAssignmentHistory>();
        }

        public string Id { get; set; }

        public int SiteId { get; set; }

        [StringLength(128)]
        public string AccountNumber { get; set; }

        public DateTime DateDeployed { get; set; }

        [StringLength(128)]
        public string DeployedBy { get; set; }

        [StringLength(32)]
        public string Type { get; set; }

        public bool Disabled { get; set; }

        public byte MeterStatusId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityLevelCheck> ElectricityLevelChecks { get; set; }

        public virtual ElectricityMeterStatus ElectricityMeterStatu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityRecharge> ElectricityRecharges { get; set; }

        public virtual Site Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteElectricityMeterAssignmentHistory> SiteElectricityMeterAssignmentHistories { get; set; }
    }
}
