namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelInventory")]
    public partial class FuelInventory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FuelSourceId { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte FuelTypeId { get; set; }

        [Range(1, Double.PositiveInfinity, ErrorMessage = "Capacity cannot be 0 or negative.")]
        public int Capacity { get; set; }

        [Range(0, Double.PositiveInfinity, ErrorMessage = "Current quantity cannot be negative.")]
        public int CurrentQuantity { get; set; }

        [Range(0, Double.PositiveInfinity, ErrorMessage = "Reorder Level cannot be negative.")]
        public int ReorderLevel { get; set; }

        public bool ReorderTriggered { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelSource FuelSource { get; set; }

        public virtual FuelType FuelType { get; set; }
    }
}
