namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VehicleTrip
    { 
        [Key]
        [Column(Order = 0)]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime DateRecorded { get; set; }

        public double OpeningMileage { get; set; }

        public string OpeningMileageImage { get; set; }

        public double ClosingMileage { get; set; }

        public string ClosingMileageImage { get; set; }

        [StringLength(128)]
        public string Origin { get; set; }

        [StringLength(128)]
        public string Destination { get; set; }

        [StringLength(256)]
        public string Purpose { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [StringLength(128)]
        public string LoggedBy { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
