namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FuelDelivery
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FuelSourceId { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte FuelTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateDelivered { get; set; }

        [Range(1, Double.PositiveInfinity, ErrorMessage = "Quantity cannot be 0 or negative.")]
        public int Quantity { get; set; }

        public int QuantityBefore { get; set; }

        [StringLength(128)]
        public string OrderNumber { get; set; }

        [StringLength(128)]
        public string ReceivedBy { get; set; }

        [StringLength(128)]
        public string LoggedBy { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelSource FuelSource { get; set; }

        public virtual FuelType FuelType { get; set; }
    }
}
