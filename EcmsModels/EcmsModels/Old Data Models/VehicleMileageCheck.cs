namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VehicleMileageCheck
    {
        [Key]
        [Column(Order = 0)]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime DateChecked { get; set; }

        public double Mileage { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
