namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Site
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Site()
        {
            ElectrictyMeters = new HashSet<ElectrictyMeter>();
            GeneratorDeploymentHistories = new HashSet<GeneratorDeploymentHistory>();
            Generators = new HashSet<Generator>();
            SiteDepartmentAssignmentHistories = new HashSet<SiteDepartmentAssignmentHistory>();
            SiteElectricityMeterAssignmentHistories = new HashSet<SiteElectricityMeterAssignmentHistory>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int DepartmentId { get; set; }

        [StringLength(256)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [StringLength(256)]
        public string Location { get; set; }

        public bool CommercialPowerAvailable { get; set; }

        [StringLength(32)]
        public string Latitude { get; set; }

        [StringLength(32)]
        public string Longitude { get; set; }

        public byte CommercialPowerStatusId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual CommercialPowerStatus CommercialPowerStatu { get; set; }

        public virtual Department Department { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectrictyMeter> ElectrictyMeters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GeneratorDeploymentHistory> GeneratorDeploymentHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Generator> Generators { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteDepartmentAssignmentHistory> SiteDepartmentAssignmentHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteElectricityMeterAssignmentHistory> SiteElectricityMeterAssignmentHistories { get; set; }
    }
}
