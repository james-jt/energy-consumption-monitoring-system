﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.DataModels
{
    public partial class UsedCoupon
    {
        public long Id { get; set; }
        public long BatchId { get; set; }
        public string RequestId { get; set; }
        public System.DateTime DateRedeemed { get; set; }
        public string RedemptionPoint { get; set; }
        public string RedeemedBy { get; set; }
        public string Notes { get; set; }

        public virtual CouponsBatch CouponsBatch { get; set; }
    }
}
