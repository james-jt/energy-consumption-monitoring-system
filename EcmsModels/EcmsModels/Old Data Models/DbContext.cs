namespace EcmsModels.DataModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Microsoft.AspNet.Identity.EntityFramework;
    using EcmsModels.WebModels;

    public partial class EcmsDbContext : IdentityDbContext<ApplicationUser>
    {
        public EcmsDbContext()
            : base("name=EcmsConnection")
        {

        }

        public static EcmsDbContext Create()
        {
            return new EcmsDbContext();
        }

        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        //public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        //public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }

        public virtual DbSet<CancelledCoupon> CancelledCoupon { get; set; }
        public virtual DbSet<CommercialPowerStatus> CommercialPowerStatus { get; set; }
        public virtual DbSet<CouponArchive> CouponArchive { get; set; }
        public virtual DbSet<CouponBatchStatus> CouponBatchStatus { get; set; }
        public virtual DbSet<CouponIssuer> CouponIssuer { get; set; }
        public virtual DbSet<CouponsBatch> CouponsBatch { get; set; }
        public virtual DbSet<CouponsBatchTransfer> CouponsBatchTransfer { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<DepartmentOverseerAssignmentHistory> DepartmentOverseerAssignmentHistory { get; set; }
        public virtual DbSet<DepartmentSubSectionAssignmentHistory> DepartmentSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<Drawdown> Drawdown { get; set; }
        public virtual DbSet<ElectricityLevelCheck> ElectricityLevelCheck { get; set; }
        public virtual DbSet<ElectricityMeterStatus> ElectricityMeterStatus { get; set; }
        public virtual DbSet<ElectricityPurchases> ElectricityPurchases { get; set; }
        public virtual DbSet<ElectricityRecharge> ElectricityRecharge { get; set; }
        public virtual DbSet<ElectricityRequest> ElectricityRequest { get; set; }
        public virtual DbSet<ElectricityTokenIssue> ElectricityTokenIssue { get; set; }
        public virtual DbSet<ElectricityTokenIssueViewModel> ElectricityTokenIssueViewModel { get; set; }
        public virtual DbSet<ElectrictyMeter> ElectrictyMeter { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventCategory> EventCategory { get; set; }
        public virtual DbSet<EventType> EventType { get; set; }
        public virtual DbSet<FuelDelivery> FuelDelivery { get; set; }
        public virtual DbSet<FuelInventory> FuelInventory { get; set; }
        public virtual DbSet<FuelInventoryHistory> FuelInventoryHistory { get; set; }
        public virtual DbSet<FuelRequest> FuelRequest { get; set; }
        public virtual DbSet<FuelSource> FuelSource { get; set; }
        public virtual DbSet<FuelSourceStatus> FuelSourceStatus { get; set; }
        public virtual DbSet<FuelSourceType> FuelSourceType { get; set; }
        public virtual DbSet<FuelType> FuelType { get; set; }
        public virtual DbSet<FuelUpGenerator> FuelUpGenerator { get; set; }
        public virtual DbSet<FuelUpVehicle> FuelUpVehicle { get; set; }
        public virtual DbSet<Generator> Generator { get; set; }
        public virtual DbSet<GeneratorDeploymentHistory> GeneratorDeploymentHistory { get; set; }
        public virtual DbSet<GeneratorModel> GeneratorModel { get; set; }
        public virtual DbSet<GeneratorServiceHistory> GeneratorServiceHistory { get; set; }
        public virtual DbSet<GeneratorStatus> GeneratorStatus { get; set; }
        public virtual DbSet<GenFuelLevelCheck> GenFuelLevelCheck { get; set; }
        public virtual DbSet<GenHoursRunCheck> GenHoursRunCheck { get; set; }
        public virtual DbSet<MaintenanceCentre> MaintenanceCentre { get; set; }
        public virtual DbSet<MaintenanceCentreOverseerAssignmentHistory> MaintenanceCentreOverseerAssignmentHistory { get; set; }
        public virtual DbSet<MaintenanceCentreSubZoneAssignmentHistory> MaintenanceCentreSubZoneAssignmentHistory { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<RegionOverseerAssignmentHistory> RegionOverseerAssignmentHistory { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<SectionOverseerAssignmentHistory> SectionOverseerAssignmentHistory { get; set; }
        public virtual DbSet<SectionSubSectionAssignmentHistory> SectionSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<SeverityLevel> SeverityLevel { get; set; }
        public virtual DbSet<Site> Site { get; set; }
        public virtual DbSet<SiteDepartmentAssignmentHistory> SiteDepartmentAssignmentHistory { get; set; }
        public virtual DbSet<SiteElectricityMeterAssignmentHistory> SiteElectricityMeterAssignmentHistory { get; set; }
        public virtual DbSet<SubSection> SubSection { get; set; }
        public virtual DbSet<SubSectionOverseerAssignmentHistory> SubSectionOverseerAssignmentHistory { get; set; }
        public virtual DbSet<SubSectionRegionAssignmentHistory> SubSectionRegionAssignmentHistory { get; set; }
        public virtual DbSet<SubZone> SubZone { get; set; }
        public virtual DbSet<SubZoneOverseerAssignmentHistory> SubZoneOverseerAssignmentHistory { get; set; }
        public virtual DbSet<SubZoneSubSectionAssignmentHistory> SubZoneSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<UsedCoupon> UsedCoupon { get; set; }
        public virtual DbSet<UserDepartmentAssignmentHistory> UserDepartmentAssignmentHistory { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleBodyType> VehicleBodyType { get; set; }
        public virtual DbSet<VehicleDepartmentDeploymentHistory> VehicleDepartmentDeploymentHistory { get; set; }
        public virtual DbSet<VehicleFuelLevelCheck> VehicleFuelLevelCheck { get; set; }
        public virtual DbSet<VehicleMake> VehicleMake { get; set; }
        public virtual DbSet<VehicleMileageCheck> VehicleMileageCheck { get; set; }
        public virtual DbSet<VehicleModel> VehicleModel { get; set; }
        public virtual DbSet<VehicleServiceHistory> VehicleServiceHistory { get; set; }
        public virtual DbSet<VehicleStatus> VehicleStatus { get; set; }
        public virtual DbSet<VehicleTrip> VehicleTrip { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.DepartmentOverseerAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.ElectricityRequest)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.RequestedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.FuelRequest)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.RequestedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.UserDepartmentAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.MaintenanceCentreOverseerAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.RegionOverseerAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.SectionOverseerAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.SubSectionOverseerAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.SubZoneOverseerAssignmentHistory)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CommercialPowerStatus>()
                .HasMany(e => e.Site)
                .WithRequired(e => e.CommercialPowerStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponBatchStatus>()
                .HasMany(e => e.CouponsBatch)
                .WithRequired(e => e.CouponBatchStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponIssuer>()
                .HasMany(e => e.CouponsBatch)
                .WithRequired(e => e.CouponIssuer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.CancelledCoupon)
                .WithRequired(e => e.CouponsBatch)
                .HasForeignKey(e => new { e.BatchId, e.BatchSize, e.CouponIssuerId, e.FuelTypeId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.CouponsBatch1)
                .WithRequired(e => e.CouponsBatch2)
                .HasForeignKey(e => new { e.ParentBatchId, e.BatchSize, e.CouponIssuerId, e.FuelTypeId });

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.CouponsBatchTransfer)
                .WithRequired(e => e.CouponsBatch)
                .HasForeignKey(e => new { e.BatchId, e.BatchSize, e.CouponIssuerId, e.FuelTypeId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.UsedCoupon)
                .WithRequired(e => e.CouponsBatch)
                .HasForeignKey(e => new { e.BatchId, e.BatchSize, e.CouponIssuerId, e.FuelTypeId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.DepartmentOverseerAssignmentHistory)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Section)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.DepartmentSubSectionAssignmentHistory)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Drawdown>()
                .HasMany(e => e.FuelUpGenerator)
                .WithRequired(e => e.Drawdown)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Drawdown>()
                .HasMany(e => e.FuelUpVehicle)
                .WithRequired(e => e.Drawdown)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectricityMeterStatus>()
                .HasMany(e => e.ElectrictyMeter)
                .WithRequired(e => e.ElectricityMeterStatus)
                .HasForeignKey(e => e.MeterStatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectricityPurchases>()
                .HasMany(e => e.ElectricityTokenIssue)
                .WithRequired(e => e.ElectricityPurchases)
                .HasForeignKey(e => e.ElectricityPurchaseId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectricityRequest>()
                .HasOptional(e => e.ElectricityTokenIssue)
                .WithRequired(e => e.ElectricityRequest);

            modelBuilder.Entity<ElectricityTokenIssue>()
                .HasOptional(e => e.ElectricityRecharge)
                .WithRequired(e => e.ElectricityTokenIssue);

            modelBuilder.Entity<ElectrictyMeter>()
                .HasMany(e => e.ElectricityLevelCheck)
                .WithRequired(e => e.ElectrictyMeter)
                .HasForeignKey(e => e.MeterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectrictyMeter>()
                .HasMany(e => e.ElectricityRecharge)
                .WithRequired(e => e.ElectrictyMeter)
                .HasForeignKey(e => e.MeterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectrictyMeter>()
                .HasMany(e => e.SiteElectricityMeterAssignmentHistory)
                .WithRequired(e => e.ElectrictyMeter)
                .HasForeignKey(e => e.MeterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelRequest>()
                .HasOptional(e => e.Drawdown)
                .WithRequired(e => e.FuelRequest);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelDelivery)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelInventory)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelInventoryHistory)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSourceStatus>()
                .HasMany(e => e.FuelSource)
                .WithRequired(e => e.FuelSourceStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSourceType>()
                .HasMany(e => e.FuelSource)
                .WithRequired(e => e.FuelSourceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.CouponsBatch)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.FuelDelivery)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.FuelInventory)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.FuelInventoryHistory)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.GeneratorModel)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.FuelUpGenerator)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GeneratorDeploymentHistory)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GeneratorServiceHistory)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GenFuelLevelCheck)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GenHoursRunCheck)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GeneratorModel>()
                .HasMany(e => e.Generator)
                .WithRequired(e => e.GeneratorModel)
                .HasForeignKey(e => e.ModelId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GeneratorStatus>()
                .HasMany(e => e.Generator)
                .WithRequired(e => e.GeneratorStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.MaintenanceCentreOverseerAssignmentHistory)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.Site)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.MaintenanceCentreSubZoneAssignmentHistory)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.SiteDepartmentAssignmentHistory)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.RegionOverseerAssignmentHistory)
                .WithRequired(e => e.Region)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.SubSection)
                .WithRequired(e => e.Region)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.SubSectionRegionAssignmentHistory)
                .WithRequired(e => e.Region)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.DepartmentSubSectionAssignmentHistory)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.SectionOverseerAssignmentHistory)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.SubSection)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.SectionSubSectionAssignmentHistory)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.ElectrictyMeter)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.Generator)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.GeneratorDeploymentHistory)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.SiteDepartmentAssignmentHistory)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.SiteElectricityMeterAssignmentHistory)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.AspNetUsers)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.CouponsBatch)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SectionSubSectionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubSectionOverseerAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubSectionRegionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubZone)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubZoneSubSectionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.VehicleDepartmentDeploymentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.MaintenanceCentre)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.MaintenanceCentreSubZoneAssignmentHistory)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.SubZoneOverseerAssignmentHistory)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.SubZoneSubSectionAssignmentHistory)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.FuelUpVehicle)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleDepartmentDeploymentHistory)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleFuelLevelCheck)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleMileageCheck)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleServiceHistory)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleTrip)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleBodyType>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.VehicleBodyType)
                .HasForeignKey(e => e.BodyTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleMake>()
                .HasMany(e => e.VehicleModel)
                .WithRequired(e => e.VehicleMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleModel>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.VehicleModel)
                .HasForeignKey(e => e.ModelId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleStatus>()
                .HasMany(e => e.Vehicle)
                .WithRequired(e => e.VehicleStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);
        }
    }
}
