namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ElectricityRequest
    {
        public string Id { get; set; }

        public int SiteId { get; set; }

        public DateTime DateRequested { get; set; }

        [Required]
        [StringLength(128)]
        public string RequestedBy { get; set; }

        [StringLength(512)]
        public string RequestorComment { get; set; }

        public int Units { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        public DateTime? DateActioned { get; set; }

        [StringLength(128)]
        public string ActionedBy { get; set; }

        public bool Authorised { get; set; }

        [StringLength(512)]
        public string ActionerComment { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual EcmsModels.WebModels.ApplicationUser Requestor { get; set; }

        public virtual ElectricityTokenIssue ElectricityTokenIssue { get; set; }
    }
}
