﻿using EcmsModels.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.DataModels
{ 
    public partial class CustomConfig
    {
        public static void DbSeed(EcmsModels.DataModels.EcmsDbContext context)
        { 
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var petrol = new FuelType() { Description = "Petrol" };
            var diesel = new FuelType() { Description = "Diesel" };
            context.FuelTypes.AddOrUpdate(petrol, diesel);

            var warehouse = new FuelSourceType() { Description = "NetOne Warehouse" };
            var tpGarage = new FuelSourceType() { Description = "Third Party Garage" };
            var coupons = new FuelSourceType() { Description = "Coupons" };
            context.FuelSourceTypes.AddOrUpdate(warehouse, tpGarage, coupons);

            var singleCabTruck = new VehicleBodyType() { Description = "Single-cab Truck" };
            var clubCabTruck = new VehicleBodyType() { Description = "Club-cab Truck" };
            var doubleCabTruck = new VehicleBodyType() { Description = "Double-cab Truck" };
            var hatchback = new VehicleBodyType() { Description = "Hatchback" };
            var sedan = new VehicleBodyType() { Description = "Sedan" };
            var van = new VehicleBodyType() { Description = "Van" };
            var minibus = new VehicleBodyType() { Description = "Mini Bus" };
            var bus = new VehicleBodyType() { Description = "Bus" };
            var suv = new VehicleBodyType() { Description = "SUV" };
            var coupe = new VehicleBodyType() { Description = "Coupe" };
            context.VehicleBodyTypes.AddOrUpdate(singleCabTruck, clubCabTruck, doubleCabTruck, hatchback, sedan, van, minibus, bus, suv, coupe);

            var mazda = new VehicleMake() { Description = "Mazda" };
            var nissan = new VehicleMake() { Description = "Nissan" };
            var toyota = new VehicleMake() { Description = "Toyota" };
            var mahindra = new VehicleMake() { Description = "Mahindra" };
            var foton = new VehicleMake() { Description = "Foton" };
            var isuzu = new VehicleMake() { Description = "Isuzu" };
            var benz = new VehicleMake() { Description = "Mercedes Benz" };
            var volvo = new VehicleMake() { Description = "Volvo" };
            var audi = new VehicleMake() { Description = "Audi" };
            var datsun = new VehicleMake() { Description = "Datsun" };
            var suzuki = new VehicleMake() { Description = "Suzuki" };
            var vw = new VehicleMake() { Description = "VW" };
            var hyundai = new VehicleMake() { Description = "Hyundai" };
            var honda = new VehicleMake() { Description = "Honda" };
            context.VehicleMakes.AddOrUpdate(
                mazda,
                nissan,
                toyota,
                mahindra,
                foton,
                isuzu,
                benz,
                volvo,
                audi,
                datsun,
                suzuki,
                vw,
                hyundai,
                honda
                );


            var commercialPowerStatus = new CommercialPowerStatus() { Description = "No Connection" };
            var commercialPowerStatus1 = new CommercialPowerStatus() { Description = "Connected" };
            var commercialPowerStatus2 = new CommercialPowerStatus() { Description = "Faulty" };
            context.CommercialPowerStatus.AddOrUpdate(commercialPowerStatus, commercialPowerStatus1, commercialPowerStatus2);

            var vehicleStatus = new VehicleStatus() { Description = "Out of Order" };
            var vehicleStatus1 = new VehicleStatus() { Description = "Decommissioned" };
            var vehicleStatus2 = new VehicleStatus() { Description = "Intact" };
            context.VehicleStatus.AddOrUpdate(vehicleStatus, vehicleStatus1, vehicleStatus2);

            var generatorStatus = new GeneratorStatus() { Description = "Out of Order" };
            var generatorStatus1 = new GeneratorStatus() { Description = "Decommissioned" };
            var generatorStatus2 = new GeneratorStatus() { Description = "Intact" };
            context.GeneratorStatus.AddOrUpdate(generatorStatus, generatorStatus1, generatorStatus2);

            var meterStatus = new ElectricityMeterStatus() { Description = "Out of Order" };
            var meterStatus1 = new ElectricityMeterStatus() { Description = "Decommissioned" };
            var meterStatus2 = new ElectricityMeterStatus() { Description = "Intact" };
            context.ElectricityMeterStatus.AddOrUpdate(meterStatus, meterStatus1, meterStatus2);

            var fuelSourceStatus = new FuelSourceStatus() { Description = "Out of Order" };
            var fuelSourceStatus1 = new FuelSourceStatus() { Description = "Decommissioned" };
            var fuelSourceStatus2 = new FuelSourceStatus() { Description = "Intact" };
            context.FuelSourceStatus.AddOrUpdate(fuelSourceStatus, fuelSourceStatus1, fuelSourceStatus2);

            context.SaveChanges();

            var cleveland = new FuelSource() { Description = "Cleveland" };
            var byoDepot = new FuelSource() { Description = "Bulawayo Depot" };
            var clevelandStatus = context.FuelSourceStatus.Where(s => s.Description == "Intact").FirstOrDefault();
            var clevelandSourceType = context.FuelSourceTypes.Where(s => s.Description == "NetOne Warehouse").FirstOrDefault();
            byoDepot.FuelSourceStatu = cleveland.FuelSourceStatu = clevelandStatus;
            byoDepot.StatusId = cleveland.StatusId = clevelandStatus.Id;
            byoDepot.FuelSourceType = cleveland.FuelSourceType = clevelandSourceType;
            byoDepot.FuelSourceTypeId = cleveland.FuelSourceTypeId = clevelandSourceType.Id;
            if (clevelandStatus.FuelSources.Where(f => f.Description == cleveland.Description).Count() == 0)
                clevelandStatus.FuelSources.Add(cleveland);
            if (clevelandStatus.FuelSources.Where(f => f.Description == byoDepot.Description).Count() == 0)
                clevelandStatus.FuelSources.Add(byoDepot);

            if (clevelandSourceType.FuelSources.Where(f => f.Description == cleveland.Description).Count() == 0)
                clevelandSourceType.FuelSources.Add(cleveland);
            if (clevelandSourceType.FuelSources.Where(f => f.Description == byoDepot.Description).Count() == 0)
                clevelandSourceType.FuelSources.Add(byoDepot);


            var puma = new FuelSource() { Description = "Puma" };
            var redan = new FuelSource() { Description = "Redan" };
            var pumaStatus = context.FuelSourceStatus.Where(s => s.Description == "Intact").FirstOrDefault();
            var pumaSourceType = context.FuelSourceTypes.Where(s => s.Description == "Third Party Garage").FirstOrDefault();
            redan.FuelSourceStatu = puma.FuelSourceStatu = pumaStatus;
            redan.StatusId = puma.StatusId = pumaStatus.Id;
            redan.FuelSourceType = puma.FuelSourceType = pumaSourceType;
            redan.FuelSourceTypeId = puma.FuelSourceTypeId = pumaSourceType.Id;

            if (pumaStatus.FuelSources.Where(f => f.Description == puma.Description).Count() == 0)
                pumaStatus.FuelSources.Add(puma);
            if (pumaStatus.FuelSources.Where(f => f.Description == redan.Description).Count() == 0)
                pumaStatus.FuelSources.Add(redan);

            if (pumaSourceType.FuelSources.Where(f => f.Description == puma.Description).Count() == 0)
                pumaSourceType.FuelSources.Add(puma);
            if (pumaSourceType.FuelSources.Where(f => f.Description == redan.Description).Count() == 0)
                pumaSourceType.FuelSources.Add(redan);

            var coupns = new FuelSource() { Description = "Coupons" };
            var coupnsStatus = context.FuelSourceStatus.Where(s => s.Description == "Intact").FirstOrDefault();
            var coupnsSourceType = context.FuelSourceTypes.Where(s => s.Description == "Coupons").FirstOrDefault();
            coupns.FuelSourceStatu = coupnsStatus;
            coupns.StatusId = coupnsStatus.Id;
            coupns.FuelSourceType = coupnsSourceType;
            coupns.FuelSourceTypeId = coupnsSourceType.Id;

            if (coupnsStatus.FuelSources.Where(f => f.Description == coupns.Description).Count() == 0)
                coupnsStatus.FuelSources.Add(coupns);

            if (coupnsSourceType.FuelSources.Where(f => f.Description == coupns.Description).Count() == 0)
                coupnsSourceType.FuelSources.Add(coupns);

            context.FuelSources.AddOrUpdate(cleveland, byoDepot, puma, redan, coupns);
            context.SaveChanges();

            var NP200 = new VehicleModel() { Description = "NP200" };
            var makeNissan = context.VehicleMakes.Where(m => m.Description == "Nissan").FirstOrDefault();
            NP200.VehicleMake = makeNissan;
            NP200.VehicleMakeId = makeNissan.Id;
            NP200.YearOfManufacture = DateTime.Now;
            if (makeNissan.VehicleModels.Where(m => m.Description == NP200.Description).Count() == 0)
                makeNissan.VehicleModels.Add(NP200);

            var hilux = new VehicleModel() { Description = "Hilux" };
            var makeToyota = context.VehicleMakes.Where(m => m.Description == "Toyota").FirstOrDefault();
            hilux.VehicleMake = makeToyota;
            hilux.VehicleMakeId = makeToyota.Id;
            hilux.YearOfManufacture = DateTime.Now;
            if (makeToyota.VehicleModels.Where(m => m.Description == hilux.Description).Count() == 0)
                makeToyota.VehicleModels.Add(hilux);

            context.VehicleModels.AddOrUpdate(NP200, hilux);
            context.SaveChanges();

            var dpt = context.Departments.FirstOrDefault();
            var statusNissan = context.VehicleStatus.Where(s => s.Description == "Intact").FirstOrDefault();

            var vehicleNissan = new Vehicle() { Id = "ABP 0220" };
            var modelNP200 = context.VehicleModels.Where(m => m.Description == "NP200").FirstOrDefault();
            var bodyNissan = context.VehicleBodyTypes.Where(b => b.Description == "Single-cab Truck").FirstOrDefault();
            var fuelNissan = context.FuelTypes.Where(f => f.Description == "Petrol").FirstOrDefault();
            vehicleNissan.VehicleBodyType = bodyNissan;
            vehicleNissan.BodyTypeId = bodyNissan.Id;
            vehicleNissan.VehicleModel = modelNP200;
            vehicleNissan.ModelId = modelNP200.Id;
            vehicleNissan.FuelType = fuelNissan;
            vehicleNissan.FuelTypeId = fuelNissan.Id;
            vehicleNissan.Department = dpt;
            vehicleNissan.DepartmentId = dpt.Id;
            vehicleNissan.EngineCapacity = 1400;
            vehicleNissan.FuelTankCapacity = 50;
            vehicleNissan.StartMileage = 200;
            vehicleNissan.VehicleStatu = statusNissan;
            vehicleNissan.StatusId = statusNissan.Id;
            vehicleNissan.YearFirstUsedAsNew = DateTime.Now;
            if (modelNP200.Vehicles.Where(v => v.Id == vehicleNissan.Id).Count() == 0)
                modelNP200.Vehicles.Add(vehicleNissan);
            if (bodyNissan.Vehicles.Where(v => v.Id == vehicleNissan.Id).Count() == 0)
                bodyNissan.Vehicles.Add(vehicleNissan);
            if (fuelNissan.Vehicles.Where(v => v.Id == vehicleNissan.Id).Count() == 0)
                fuelNissan.Vehicles.Add(vehicleNissan);
            if (statusNissan.Vehicles.Where(v => v.Id == vehicleNissan.Id).Count() == 0)
                statusNissan.Vehicles.Add(vehicleNissan);
            if (dpt.Vehicles.Where(v => v.Id == vehicleNissan.Id).Count() == 0)
                dpt.Vehicles.Add(vehicleNissan);


            var vehicleToyota = new Vehicle() { Id = "ADR 3055" };
            var modelHilux = context.VehicleModels.Where(m => m.Description == "Hilux").FirstOrDefault();
            var bodyToyota = context.VehicleBodyTypes.Where(b => b.Description == "double-cab Truck").FirstOrDefault();
            var fuelToyota = context.FuelTypes.Where(f => f.Description == "Diesel").FirstOrDefault();
            vehicleToyota.VehicleBodyType = bodyToyota;
            vehicleToyota.BodyTypeId = bodyToyota.Id;
            vehicleToyota.VehicleModel = hilux;
            vehicleToyota.ModelId = hilux.Id;
            vehicleToyota.FuelType = fuelToyota;
            vehicleToyota.FuelTypeId = fuelToyota.Id;
            vehicleToyota.Department = dpt;
            vehicleToyota.DepartmentId = dpt.Id;
            vehicleToyota.EngineCapacity = 2500;
            vehicleToyota.FuelTankCapacity = 80;
            vehicleToyota.StartMileage = 1357;
            vehicleToyota.VehicleStatu = statusNissan;
            vehicleToyota.StatusId = statusNissan.Id;
            vehicleToyota.YearFirstUsedAsNew = DateTime.Now;
            if (modelHilux.Vehicles.Where(v => v.Id == vehicleToyota.Id).Count() == 0)
                modelHilux.Vehicles.Add(vehicleToyota);
            if (bodyToyota.Vehicles.Where(v => v.Id == vehicleToyota.Id).Count() == 0)
                bodyToyota.Vehicles.Add(vehicleToyota);
            if (fuelToyota.Vehicles.Where(v => v.Id == vehicleToyota.Id).Count() == 0)
                fuelToyota.Vehicles.Add(vehicleToyota);
            if (statusNissan.Vehicles.Where(v => v.Id == vehicleToyota.Id).Count() == 0)
                statusNissan.Vehicles.Add(vehicleToyota);
            if (dpt.Vehicles.Where(v => v.Id == vehicleToyota.Id).Count() == 0)
                dpt.Vehicles.Add(vehicleToyota);

            context.Vehicles.AddOrUpdate(vehicleNissan, vehicleToyota);
            context.SaveChanges();

            var powerStatus = context.CommercialPowerStatus.Where(s => s.Description == "Connected").FirstOrDefault();
            var electricityMeterStatus = context.ElectricityMeterStatus.Where(s => s.Description == "Intact").FirstOrDefault();

            var girlsHighSite = new Site() { Name = "Girl's High Harare" };
            girlsHighSite.CommercialPowerStatu = powerStatus;
            girlsHighSite.CommercialPowerStatusId = powerStatus.Id;
            girlsHighSite.CommercialPowerAvailable = true;
            girlsHighSite.Department = dpt;
            girlsHighSite.DepartmentId = dpt.Id;
            girlsHighSite.Latitude = "102.2568";
            girlsHighSite.Longitude = "47.5698";
            girlsHighSite.Location = "Harare Girl's High School";
            girlsHighSite.Region = "Harare";
            girlsHighSite.ZoneNumber = 1;
            if (powerStatus.Sites.Where(s => s.Name == girlsHighSite.Name).Count() == 0)
                powerStatus.Sites.Add(girlsHighSite);

            var nustSite = new Site() { Name = "NUST Bulawayo" };
            nustSite.CommercialPowerStatu = powerStatus;
            nustSite.CommercialPowerStatusId = powerStatus.Id;
            nustSite.CommercialPowerAvailable = true;
            nustSite.Department = dpt;
            nustSite.DepartmentId = dpt.Id;
            nustSite.Latitude = "142.4245";
            nustSite.Longitude = "76.2097";
            nustSite.Location = "NUST Grounds, Behind Ceremonial Hall";
            nustSite.Region = "Bulawayo";
            nustSite.ZoneNumber = 2;
            if (powerStatus.Sites.Where(s => s.Name == nustSite.Name).Count() == 0)
                powerStatus.Sites.Add(nustSite);

            context.Sites.AddOrUpdate(girlsHighSite, nustSite);
            context.SaveChanges();

            var girlsHighMeter = new ElectrictyMeter() { Id = "12345678901" };
            girlsHighMeter.AccountNumber = "012345678901";
            girlsHighMeter.DateDeployed = DateTime.Now;
            girlsHighMeter.DeployedBy = dpt.OverseerUserId;
            girlsHighMeter.Site = girlsHighSite;
            girlsHighMeter.SiteId = girlsHighSite.Id;
            girlsHighMeter.ElectricityMeterStatu = electricityMeterStatus;
            girlsHighMeter.MeterStatusId = electricityMeterStatus.Id;
            girlsHighMeter.Type = "Prepaid";
            if (electricityMeterStatus.ElectrictyMeters.Where(s => s.Id == girlsHighMeter.Id).Count() == 0)
                electricityMeterStatus.ElectrictyMeters.Add(girlsHighMeter);
            if (girlsHighSite.ElectrictyMeters.Where(m => m.Id == girlsHighMeter.Id).Count() == 0)
                girlsHighSite.ElectrictyMeters.Add(girlsHighMeter);

            var nustMeter = new ElectrictyMeter() { Id = "10987654321" };
            nustMeter.AccountNumber = "010987654321";
            nustMeter.DateDeployed = DateTime.Now;
            nustMeter.DeployedBy = dpt.OverseerUserId;
            nustMeter.Site = nustSite;
            nustMeter.SiteId = nustSite.Id;
            nustMeter.ElectricityMeterStatu = electricityMeterStatus;
            nustMeter.MeterStatusId = electricityMeterStatus.Id;
            nustMeter.Type = "Prepaid";
            if (electricityMeterStatus.ElectrictyMeters.Where(s => s.Id == nustMeter.Id).Count() == 0)
                electricityMeterStatus.ElectrictyMeters.Add(nustMeter);
            if (nustSite.ElectrictyMeters.Where(m => m.Id == nustMeter.Id).Count() == 0)
                nustSite.ElectrictyMeters.Add(nustMeter);

            context.ElectrictyMeters.AddOrUpdate(girlsHighMeter, nustMeter);
            context.SaveChanges();

            var genStatus = context.GeneratorStatus.Where(s => s.Description == "Intact").FirstOrDefault();

            var perkinsModel = new GeneratorModel() { Name = "Huawei-Perkins 20" };
            perkinsModel.EngineMake = "Perkins";
            perkinsModel.EngineModel = "KD4567TY";
            perkinsModel.FuelType = diesel;
            perkinsModel.FuelTypeId = diesel.Id;
            perkinsModel.NormalConsuptionRate = 23;
            perkinsModel.NormalLifeSpan = 10;
            perkinsModel.ServiceInterval = 5000;
            if (diesel.GeneratorModels.Where(m => m.Name == perkinsModel.Name).Count() == 0)
                diesel.GeneratorModels.Add(perkinsModel);

            var cumminsModel = new GeneratorModel() { Name = "Cummins-Cummins 17.5" };
            cumminsModel.EngineMake = "Cummins";
            cumminsModel.EngineModel = "XZP1278";
            cumminsModel.FuelType = diesel;
            cumminsModel.FuelTypeId = diesel.Id;
            cumminsModel.NormalConsuptionRate = 15;
            cumminsModel.NormalLifeSpan = 15;
            cumminsModel.ServiceInterval = 10000;
            if (diesel.GeneratorModels.Where(m => m.Name == cumminsModel.Name).Count() == 0)
                diesel.GeneratorModels.Add(cumminsModel);

            context.GeneratorModels.AddOrUpdate(perkinsModel, cumminsModel);
            context.SaveChanges();

            var girlsHighGenerator = new Generator() { Id = "GN71134R01060A" };
            var girlsHighGeneratorModel = context.GeneratorModels.Where(m => m.Name == perkinsModel.Name).FirstOrDefault();
            girlsHighGenerator.FuelCapacity = 1000;
            girlsHighGenerator.GeneratorModel = girlsHighGeneratorModel;
            girlsHighGenerator.ModelId = girlsHighGeneratorModel.Id;
            girlsHighGenerator.GeneratorStatu = genStatus;
            girlsHighGenerator.GeneratorStatusId = genStatus.Id;
            girlsHighGenerator.Site = girlsHighSite;
            girlsHighGenerator.SiteId = girlsHighSite.Id;
            girlsHighGenerator.Supplier = "Local Dealer";
            girlsHighGenerator.YearFirstUsedAsNew = DateTime.Now;
            if (girlsHighGeneratorModel.Generators.Where(g => g.Id == girlsHighGenerator.Id).Count() == 0)
                girlsHighGeneratorModel.Generators.Add(girlsHighGenerator);
            if (genStatus.Generators.Where(g => g.Id == girlsHighGenerator.Id).Count() == 0)
                genStatus.Generators.Add(girlsHighGenerator);
            if (girlsHighSite.Generators.Where(g => g.Id == girlsHighGenerator.Id).Count() == 0)
                girlsHighSite.Generators.Add(girlsHighGenerator);

            var girlsHighGeneratorDeploymentHistory = new GeneratorDeploymentHistory();
            var changedBy = context.Departments.FirstOrDefault().OverseerUserId;
            var dateChanged = DateTime.Today;
            dateChanged = dateChanged.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
            girlsHighGeneratorDeploymentHistory.SiteId = girlsHighGenerator.SiteId;
            girlsHighGeneratorDeploymentHistory.GeneratorId = girlsHighGenerator.Id;
            girlsHighGeneratorDeploymentHistory.DateDeployed = dateChanged;
            girlsHighGeneratorDeploymentHistory.DeployedBy = changedBy;
            context.GeneratorDeploymentHistory.Add(girlsHighGeneratorDeploymentHistory);

            var nustGenerator = new Generator() { Id = "C22D5TL1411200052" };
            var nustGeneratorModel = context.GeneratorModels.Where(m => m.Name == cumminsModel.Name).FirstOrDefault();
            nustGenerator.FuelCapacity = 500;
            nustGenerator.GeneratorModel = nustGeneratorModel;
            nustGenerator.ModelId = nustGeneratorModel.Id;
            nustGenerator.GeneratorStatu = genStatus;
            nustGenerator.GeneratorStatusId = genStatus.Id;
            nustGenerator.Site = nustSite;
            nustGenerator.SiteId = nustSite.Id;
            nustGenerator.Supplier = "Foreign Dealer";
            nustGenerator.YearFirstUsedAsNew = DateTime.Now;
            if (nustGeneratorModel.Generators.Where(g => g.Id == nustGenerator.Id).Count() == 0)
                nustGeneratorModel.Generators.Add(nustGenerator);
            if (genStatus.Generators.Where(g => g.Id == nustGenerator.Id).Count() == 0)
                genStatus.Generators.Add(nustGenerator);
            if (nustSite.Generators.Where(g => g.Id == nustGenerator.Id).Count() == 0)
                nustSite.Generators.Add(nustGenerator);

            var nustGeneratorDeploymentHistory = new GeneratorDeploymentHistory();
            nustGeneratorDeploymentHistory.SiteId = nustGenerator.SiteId;
            nustGeneratorDeploymentHistory.GeneratorId = nustGenerator.Id;
            nustGeneratorDeploymentHistory.DateDeployed = dateChanged;
            nustGeneratorDeploymentHistory.DeployedBy = changedBy;
            context.GeneratorDeploymentHistory.Add(nustGeneratorDeploymentHistory);

            context.Generators.AddOrUpdate(girlsHighGenerator, nustGenerator);
            context.SaveChanges();

        }

    }
}
