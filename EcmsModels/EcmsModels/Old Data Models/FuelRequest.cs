namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FuelRequest
    {
        public string Id { get; set; }

        public DateTime DateRequested { get; set; }

        [Required]
        [StringLength(128)]
        public string RequestedBy { get; set; }

        public DateTime DateActioned { get; set; }

        [Required]
        [StringLength(128)]
        public string FuelType { get; set; }
         
        public int Quantity { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        public string ActionedBy { get; set; }

        public bool Authorised { get; set; }

        [StringLength(512)]
        public string RequestorComment { get; set; }

        [StringLength(512)]
        public string ActionerComment { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual EcmsModels.WebModels.ApplicationUser Requestor { get; set; }

        public virtual Drawdown Drawdown { get; set; }
    }
}
