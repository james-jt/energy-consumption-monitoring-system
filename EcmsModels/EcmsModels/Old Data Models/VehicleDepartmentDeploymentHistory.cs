namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleDepartmentDeploymentHistory")]
    public partial class VehicleDepartmentDeploymentHistory
    {
        [Key]
        [Column(Order = 0)]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartmentId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateDeployed { get; set; }

        [StringLength(128)]
        public string DeployedBy { get; set; }

        public DateTime DateRecalled { get; set; }

        [StringLength(128)]
        public string RecalledBy { get; set; }

        public virtual Department Department { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
