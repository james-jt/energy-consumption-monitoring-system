namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelInventoryHistory")]
    public partial class FuelInventoryHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FuelSourceId { get; set; }

        [Key]
        [Column(Order = 1)]
        public byte FuelTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Year { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Period { get; set; }

        public DateTime OpeningDate { get; set; }

        public int OpeningQuantity { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelType FuelType { get; set; }

        public virtual FuelSource FuelSource { get; set; }
    }
}
