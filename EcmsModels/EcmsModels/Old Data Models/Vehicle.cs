namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Vehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicle()
        {
            FuelUpVehicles = new HashSet<FuelUpVehicle>();
            VehicleDepartmentDeploymentHistories = new HashSet<VehicleDepartmentDeploymentHistory>();
            VehicleFuelLevelChecks = new HashSet<VehicleFuelLevelCheck>();
            VehicleMileageChecks = new HashSet<VehicleMileageCheck>();
            VehicleServiceHistories = new HashSet<VehicleServiceHistory>();
            VehicleTrips = new HashSet<VehicleTrip>();
        }

        public string Id { get; set; }

        public int DepartmentId { get; set; }

        public short ModelId { get; set; }

        public byte BodyTypeId { get; set; }

        public byte FuelTypeId { get; set; }

        public byte StatusId { get; set; }

        public DateTime YearFirstUsedAsNew { get; set; }

        public double EngineCapacity { get; set; }

        public short FuelTankCapacity { get; set; }

        public int StartMileage { get; set; }

        public virtual Department Department { get; set; }

        public virtual FuelType FuelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpVehicle> FuelUpVehicles { get; set; }

        public virtual VehicleBodyType VehicleBodyType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleDepartmentDeploymentHistory> VehicleDepartmentDeploymentHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleFuelLevelCheck> VehicleFuelLevelChecks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleMileageCheck> VehicleMileageChecks { get; set; }

        public virtual VehicleModel VehicleModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleServiceHistory> VehicleServiceHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleTrip> VehicleTrips { get; set; }

        public virtual VehicleStatus VehicleStatu { get; set; }
    }
}
