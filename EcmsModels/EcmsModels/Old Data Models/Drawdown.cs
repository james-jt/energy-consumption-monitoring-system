namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Drawdown
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Drawdown()
        {
            FuelUpGenerators = new HashSet<FuelUpGenerator>();
            FuelUpVehicles = new HashSet<FuelUpVehicle>();
        }

        public string Id { get; set; }

        public short FuelSourceId { get; set; }

        public DateTime Date { get; set; }

        public int AvailableQuantity { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelRequest FuelRequest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpGenerator> FuelUpGenerators { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpVehicle> FuelUpVehicles { get; set; }
    }
}
