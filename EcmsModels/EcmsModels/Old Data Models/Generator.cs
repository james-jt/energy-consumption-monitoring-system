namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Generator
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Generator()
        {
            FuelUpGenerators = new HashSet<FuelUpGenerator>();
            GeneratorDeploymentHistories = new HashSet<GeneratorDeploymentHistory>();
            GeneratorServiceHistories = new HashSet<GeneratorServiceHistory>();
            GenFuelLevelChecks = new HashSet<GenFuelLevelCheck>();
            GenHoursRunChecks = new HashSet<GenHoursRunCheck>();
        }

        public string Id { get; set; }

        public int SiteId { get; set; }

        [StringLength(256)]
        public string Supplier { get; set; }

        public DateTime YearFirstUsedAsNew { get; set; }

        public int HoursRun { get; set; }

        public int FuelCapacity { get; set; }

        public byte GeneratorStatusId { get; set; }

        public byte ModelId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpGenerator> FuelUpGenerators { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GeneratorDeploymentHistory> GeneratorDeploymentHistories { get; set; }

        public virtual GeneratorModel GeneratorModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GeneratorServiceHistory> GeneratorServiceHistories { get; set; }

        public virtual GeneratorStatus GeneratorStatu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenFuelLevelCheck> GenFuelLevelChecks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenHoursRunCheck> GenHoursRunChecks { get; set; }

        public virtual Site Site { get; set; }
    }
}
