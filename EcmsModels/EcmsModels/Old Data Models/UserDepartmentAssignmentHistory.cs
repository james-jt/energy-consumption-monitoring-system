namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserDepartmentAssignmentHistory")]
    public partial class UserDepartmentAssignmentHistory
    {
        [Key]
        [Column(Order = 0)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short DepartmentId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateAssigned { get; set; }

        [StringLength(128)]
        public string AssignedBy { get; set; }

        public DateTime DateRecalled { get; set; }

        [StringLength(128)] 
        public string RecalledBy { get; set; }

        public virtual EcmsModels.WebModels.ApplicationUser User { get; set; }
    }
}
