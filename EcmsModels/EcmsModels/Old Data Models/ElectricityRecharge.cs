namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ElectricityRecharge
    {
        public string Id { get; set; }

        [Required]
        [StringLength(128)]
        public string MeterId { get; set; }

        public DateTime Date { get; set; }

        public int UnitsBefore { get; set; }

        public int UnitsAfter { get; set; }

        [StringLength(128)]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual ElectrictyMeter ElectrictyMeter { get; set; }

        public virtual ElectricityTokenIssue ElectricityTokenIssue { get; set; }
    }
}
