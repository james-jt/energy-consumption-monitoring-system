namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CouponsBatch
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CouponsBatch()
        {
            ChildBatches = new HashSet<CouponsBatch>();
            CancelledCoupons = new HashSet<CancelledCoupon>();
            UsedCoupons = new HashSet<UsedCoupon>();
        }

        public long Id { get; set; }

        public long ParentBatchId { get; set; }

        public int DepartmentId { get; set; }

        public short BookSize { get; set; }

        public byte FuelTypeId { get; set; }

        public short Denomination { get; set; }

        public int BatchSize { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime ExpiryDate { get; set; }

        [StringLength(128)]
        public string Issuer { get; set; }

        [StringLength(128)]
        public string CreatedBy { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        public byte StatusId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual CouponBatchStatus Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CouponsBatch> ChildBatches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CancelledCoupon> CancelledCoupons { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsedCoupon> UsedCoupons { get; set; }

        public virtual CouponsBatch ParentBatch { get; set; }

        public virtual CouponsBatchTransfer CouponsBatchTransfer { get; set; }

        public virtual Department Department { get; set; }

        public virtual FuelType FuelType { get; set; }
    }
}
