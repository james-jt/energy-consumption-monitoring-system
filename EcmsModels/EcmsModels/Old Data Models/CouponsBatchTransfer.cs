namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CouponsBatchTransfer
    {
        [Key]
        public long BatchId { get; set; }

        public DateTime DateTransferred { get; set; }

        public short SenderDepartmentId { get; set; }

        public short ReceiverDepartmentId { get; set; }

        public DateTime DateReceived { get; set; }

        [StringLength(128)]
        public string TransferredBy { get; set; }

        [StringLength(128)]
        public string ReceivedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual CouponsBatch CouponsBatch { get; set; }
    }
}
