namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Event
    {
        public string EventId { get; set; }

        public DateTime EventDate { get; set; }

        [StringLength(128)]
        public string GeneratorId { get; set; }

        [StringLength(128)]
        public string SiteId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(128)]
        public string LoggedBy { get; set; }
    }
}
