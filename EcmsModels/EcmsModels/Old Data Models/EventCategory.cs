namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EventCategory
    {
        public short Id { get; set; }

        [StringLength(32)]
        public string Description { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public bool Disabled { get; set; }
    }
}
