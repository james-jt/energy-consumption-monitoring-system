namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelUPdate3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ElectricityTokenIssueViewModel",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ElectricityPurchaseId = c.String(),
                        TokenIssueConfirmed = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ReceiptNumber = c.String(maxLength: 128),
                        MeterNumber = c.String(nullable: false, maxLength: 128),
                        CustomerName = c.String(maxLength: 512),
                        MeterAddress = c.String(),
                        Token = c.String(nullable: false, maxLength: 32),
                        Tariff = c.String(maxLength: 64),
                        EnergyBought = c.Double(nullable: false),
                        TenderAmount = c.Double(),
                        EnergyCharge = c.Double(),
                        DebtCollected = c.Double(),
                        ReLevy = c.Double(),
                        VatRate = c.Double(),
                        VatAmount = c.Double(),
                        TotalPaid = c.Double(nullable: false),
                        DebtBalBf = c.Double(),
                        DebtBalCf = c.Double(),
                        VendorNumberAndName = c.String(maxLength: 128),
                        VendorInfo1 = c.String(maxLength: 128),
                        VendorInfo2 = c.String(maxLength: 128),
                        VendorInfo3 = c.String(maxLength: 128),
                        TariffIndex = c.String(maxLength: 128),
                        SupplyGroupCode = c.String(maxLength: 128),
                        KeyRevNumber = c.String(maxLength: 128),
                        Authorised = c.Boolean(nullable: false),
                        BoughtBy = c.String(maxLength: 128),
                        DateBought = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 128),
                        DateAuthorised = c.DateTime(nullable: false),
                        IssuedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ElectricityTokenIssueViewModel");
        }
    }
}
