namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedtoenumw : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" }, "dbo.FuelInventoryHistory");
            DropIndex("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            DropPrimaryKey("dbo.FuelInventoryHistory");
            AlterColumn("dbo.FuelInventoryAdjustment", "Year", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventoryAdjustment", "Period", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventoryHistory", "Year", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventoryHistory", "Period", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.FuelInventoryHistory", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            CreateIndex("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            AddForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" }, "dbo.FuelInventoryHistory", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" }, "dbo.FuelInventoryHistory");
            DropIndex("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            DropPrimaryKey("dbo.FuelInventoryHistory");
            AlterColumn("dbo.FuelInventoryHistory", "Period", c => c.Short(nullable: false));
            AlterColumn("dbo.FuelInventoryHistory", "Year", c => c.Short(nullable: false));
            AlterColumn("dbo.FuelInventoryAdjustment", "Period", c => c.Short(nullable: false));
            AlterColumn("dbo.FuelInventoryAdjustment", "Year", c => c.Short(nullable: false));
            AddPrimaryKey("dbo.FuelInventoryHistory", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            CreateIndex("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            AddForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" }, "dbo.FuelInventoryHistory", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
        }
    }
}
