namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iveforgotenjlf : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ElectrictyMeter", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ElectrictyMeter", "Type", c => c.String(maxLength: 32));
        }
    }
}
