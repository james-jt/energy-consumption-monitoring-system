namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedType : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.MaintenanceCentre", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MaintenanceCentre", "Type", c => c.String(maxLength: 32));
        }
    }
}
