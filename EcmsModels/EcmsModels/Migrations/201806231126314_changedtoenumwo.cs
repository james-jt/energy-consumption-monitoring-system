namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedtoenumwo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory");
            AddForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory", new[] { "FuelSourceId", "FuelTypeId" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory");
            AddForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory", new[] { "FuelSourceId", "FuelTypeId" });
        }
    }
}
