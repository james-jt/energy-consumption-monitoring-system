namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fieldsForFuelRequest : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FuelRequest", "DepartmentId", c => c.Int(nullable: false));
            AddColumn("dbo.FuelRequest", "SectionId", c => c.Int(nullable: false));
            AddColumn("dbo.FuelRequest", "SubSectionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FuelRequest", "SubSectionId");
            DropColumn("dbo.FuelRequest", "SectionId");
            DropColumn("dbo.FuelRequest", "DepartmentId");
        }
    }
}
