namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelUPdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ElectricityPurchases", "MeterNumber", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.ElectricityPurchases", "Token", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.ElectricityPurchases", "EnergyBought", c => c.Double(nullable: false));
            AlterColumn("dbo.ElectricityPurchases", "TotalPaid", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ElectricityPurchases", "TotalPaid", c => c.Double());
            AlterColumn("dbo.ElectricityPurchases", "EnergyBought", c => c.Double());
            AlterColumn("dbo.ElectricityPurchases", "Token", c => c.String(maxLength: 32));
            AlterColumn("dbo.ElectricityPurchases", "MeterNumber", c => c.String(maxLength: 128));
        }
    }
}
