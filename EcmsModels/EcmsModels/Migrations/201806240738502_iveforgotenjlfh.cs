namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iveforgotenjlfh : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Landlord",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Address = c.String(maxLength: 128),
                        Email = c.String(),
                        Phone = c.String(),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Province",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.SiteSegment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Site", "SegmentId", c => c.Int(nullable: false));
            AddColumn("dbo.Site", "ProvinceId", c => c.Int(nullable: false));
            AddColumn("dbo.Site", "LandlordId", c => c.Int(nullable: false));
            CreateIndex("dbo.Site", "SegmentId");
            CreateIndex("dbo.Site", "ProvinceId");
            CreateIndex("dbo.Site", "LandlordId");
            AddForeignKey("dbo.Site", "LandlordId", "dbo.Landlord", "Id");
            AddForeignKey("dbo.Site", "ProvinceId", "dbo.Province", "Id");
            AddForeignKey("dbo.Site", "SegmentId", "dbo.SiteSegment", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Site", "SegmentId", "dbo.SiteSegment");
            DropForeignKey("dbo.Site", "ProvinceId", "dbo.Province");
            DropForeignKey("dbo.Site", "LandlordId", "dbo.Landlord");
            DropIndex("dbo.Province", new[] { "Name" });
            DropIndex("dbo.Landlord", new[] { "Name" });
            DropIndex("dbo.Site", new[] { "LandlordId" });
            DropIndex("dbo.Site", new[] { "ProvinceId" });
            DropIndex("dbo.Site", new[] { "SegmentId" });
            DropColumn("dbo.Site", "LandlordId");
            DropColumn("dbo.Site", "ProvinceId");
            DropColumn("dbo.Site", "SegmentId");
            DropTable("dbo.SiteSegment");
            DropTable("dbo.Province");
            DropTable("dbo.Landlord");
        }
    }
}
