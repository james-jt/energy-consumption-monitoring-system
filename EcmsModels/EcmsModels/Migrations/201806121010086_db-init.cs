namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbinit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CancelledCoupon",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BatchId = c.Long(nullable: false),
                        BatchSize = c.Int(nullable: false),
                        CouponIssuerId = c.Byte(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        DateCancelled = c.DateTime(nullable: false),
                        Reason = c.String(maxLength: 512),
                        CancelledBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponsBatch", t => new { t.BatchId, t.BatchSize, t.CouponIssuerId, t.FuelTypeId })
                .Index(t => new { t.BatchId, t.BatchSize, t.CouponIssuerId, t.FuelTypeId });
            
            CreateTable(
                "dbo.CouponsBatch",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        BatchSize = c.Int(nullable: false),
                        CouponIssuerId = c.Byte(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        ParentBatchId = c.Long(nullable: false),
                        SubSectionId = c.Int(nullable: false),
                        BookSize = c.Short(nullable: false),
                        Denomination = c.Short(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 128),
                        Description = c.String(maxLength: 128),
                        StatusId = c.Byte(nullable: false),
                        Notes = c.String(maxLength: 512),
                        ParentBatch_Id = c.Long(nullable: false),
                        ParentBatch_BatchSize = c.Int(nullable: false),
                        ParentBatch_CouponIssuerId = c.Byte(nullable: false),
                        ParentBatch_FuelTypeId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Id, t.BatchSize, t.CouponIssuerId, t.FuelTypeId })
                .ForeignKey("dbo.CouponsBatch", t => new { t.ParentBatch_Id, t.ParentBatch_BatchSize, t.ParentBatch_CouponIssuerId, t.ParentBatch_FuelTypeId })
                .ForeignKey("dbo.CouponBatchStatus", t => t.StatusId)
                .ForeignKey("dbo.CouponIssuer", t => t.CouponIssuerId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .Index(t => t.CouponIssuerId)
                .Index(t => t.FuelTypeId)
                .Index(t => t.SubSectionId)
                .Index(t => t.StatusId)
                .Index(t => new { t.ParentBatch_Id, t.ParentBatch_BatchSize, t.ParentBatch_CouponIssuerId, t.ParentBatch_FuelTypeId });
            
            CreateTable(
                "dbo.CouponBatchStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CouponIssuer",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.CouponsBatchTransfer",
                c => new
                    {
                        BatchId = c.Long(nullable: false),
                        BatchSize = c.Int(nullable: false),
                        CouponIssuerId = c.Byte(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        DateTransferred = c.DateTime(nullable: false),
                        SenderDepartmentId = c.Short(nullable: false),
                        ReceiverDepartmentId = c.Short(nullable: false),
                        DateReceived = c.DateTime(nullable: false),
                        TransferredBy = c.String(maxLength: 128),
                        ReceivedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.BatchId)
                .ForeignKey("dbo.CouponsBatch", t => new { t.BatchId, t.BatchSize, t.CouponIssuerId, t.FuelTypeId })
                .Index(t => new { t.BatchId, t.BatchSize, t.CouponIssuerId, t.FuelTypeId });
            
            CreateTable(
                "dbo.FuelType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.FuelDelivery",
                c => new
                    {
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        DateDelivered = c.DateTime(nullable: false),
                        Quantity = c.Int(nullable: false),
                        QuantityBefore = c.Int(nullable: false),
                        OrderNumber = c.String(maxLength: 128),
                        ReceivedBy = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.FuelSourceId, t.FuelTypeId, t.DateDelivered })
                .ForeignKey("dbo.FuelSource", t => t.FuelSourceId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.FuelSourceId)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.FuelSource",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        FuelSourceTypeId = c.Byte(nullable: false),
                        Name = c.String(maxLength: 128),
                        StatusId = c.Byte(nullable: false),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FuelSourceStatus", t => t.StatusId)
                .ForeignKey("dbo.FuelSourceType", t => t.FuelSourceTypeId)
                .Index(t => t.FuelSourceTypeId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.FuelInventory",
                c => new
                    {
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        Capacity = c.Int(nullable: false),
                        CurrentQuantity = c.Int(nullable: false),
                        ReorderLevel = c.Int(nullable: false),
                        ReorderTriggered = c.Boolean(nullable: false),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.FuelSourceId, t.FuelTypeId })
                .ForeignKey("dbo.FuelSource", t => t.FuelSourceId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.FuelSourceId)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.FuelInventoryHistory",
                c => new
                    {
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        Year = c.Short(nullable: false),
                        Period = c.Short(nullable: false),
                        OpeningDate = c.DateTime(nullable: false),
                        OpeningQuantity = c.Int(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.FuelSourceId, t.FuelTypeId, t.Year, t.Period })
                .ForeignKey("dbo.FuelSource", t => t.FuelSourceId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.FuelSourceId)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.FuelSourceStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FuelSourceType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.GeneratorModel",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(maxLength: 128),
                        EngineMake = c.String(maxLength: 128),
                        EngineModel = c.String(maxLength: 128),
                        GeneratorMake = c.String(maxLength: 128),
                        ServiceInterval = c.Int(nullable: false),
                        GeneratorSize = c.Double(nullable: false),
                        NormalLifeSpan = c.Int(),
                        NormalConsuptionRate = c.Double(),
                        Disabled = c.Boolean(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.Generator",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SiteId = c.Int(nullable: false),
                        Supplier = c.String(maxLength: 256),
                        YearFirstUsedAsNew = c.DateTime(nullable: false),
                        HoursRun = c.Int(nullable: false),
                        FuelCapacity = c.Int(nullable: false),
                        GeneratorStatusId = c.Byte(nullable: false),
                        ModelId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .ForeignKey("dbo.GeneratorStatus", t => t.GeneratorStatusId)
                .ForeignKey("dbo.GeneratorModel", t => t.ModelId)
                .Index(t => t.SiteId)
                .Index(t => t.GeneratorStatusId)
                .Index(t => t.ModelId);
            
            CreateTable(
                "dbo.GeneratorDeploymentHistory",
                c => new
                    {
                        SiteId = c.Int(nullable: false),
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateDeployed = c.DateTime(nullable: false),
                        DeployedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SiteId, t.GeneratorId, t.DateDeployed })
                .ForeignKey("dbo.Site", t => t.SiteId)
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.SiteId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.Site",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaintenanceCentreId = c.Int(nullable: false),
                        Name = c.String(maxLength: 256),
                        Location = c.String(maxLength: 256),
                        CommercialPowerAvailable = c.Boolean(nullable: false),
                        Latitude = c.String(maxLength: 32),
                        Longitude = c.String(maxLength: 32),
                        CommercialPowerStatusId = c.Byte(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CommercialPowerStatus", t => t.CommercialPowerStatusId)
                .ForeignKey("dbo.MaintenanceCentre", t => t.MaintenanceCentreId)
                .Index(t => t.MaintenanceCentreId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.CommercialPowerStatusId);
            
            CreateTable(
                "dbo.CommercialPowerStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ElectrictyMeter",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SiteId = c.Int(nullable: false),
                        AccountNumber = c.String(maxLength: 128),
                        DateDeployed = c.DateTime(nullable: false),
                        DeployedBy = c.String(maxLength: 128),
                        Type = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        MeterStatusId = c.Byte(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ElectricityMeterStatus", t => t.MeterStatusId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.MeterStatusId);
            
            CreateTable(
                "dbo.ElectricityLevelCheck",
                c => new
                    {
                        MeterId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Units = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.MeterId, t.DateChecked })
                .ForeignKey("dbo.ElectrictyMeter", t => t.MeterId)
                .Index(t => t.MeterId);
            
            CreateTable(
                "dbo.ElectricityMeterStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ElectricityRecharge",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        MeterId = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        UnitsBefore = c.Int(nullable: false),
                        UnitsAfter = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ElectricityTokenIssue", t => t.Id)
                .ForeignKey("dbo.ElectrictyMeter", t => t.MeterId)
                .Index(t => t.Id)
                .Index(t => t.MeterId);
            
            CreateTable(
                "dbo.ElectricityTokenIssue",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ElectricityPurchaseId = c.String(nullable: false, maxLength: 128),
                        DateIssued = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ElectricityPurchases", t => t.ElectricityPurchaseId)
                .ForeignKey("dbo.ElectricityRequest", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.ElectricityPurchaseId);
            
            CreateTable(
                "dbo.ElectricityPurchases",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        ReceiptNumber = c.String(maxLength: 128),
                        MeterNumber = c.String(nullable: false, maxLength: 128),
                        CustomerName = c.String(maxLength: 512),
                        Token = c.String(nullable: false, maxLength: 32),
                        Tariff = c.String(maxLength: 64),
                        EnergyBought = c.Double(nullable: false),
                        TenderAmount = c.Double(),
                        EnergyCharge = c.Double(),
                        DebtCollected = c.Double(),
                        ReLevy = c.Double(),
                        VatRate = c.Double(),
                        VatAmount = c.Double(),
                        TotalPaid = c.Double(nullable: false),
                        DebtBalBf = c.Double(),
                        DebtBalCf = c.Double(),
                        VendorNumberAndName = c.String(maxLength: 128),
                        VendorInfo1 = c.String(maxLength: 128),
                        VendorInfo2 = c.String(maxLength: 128),
                        VendorInfo3 = c.String(maxLength: 128),
                        TariffIndex = c.String(maxLength: 128),
                        SupplyGroupCode = c.String(maxLength: 128),
                        KeyRevNumber = c.String(maxLength: 128),
                        BoughtBy = c.String(maxLength: 128),
                        AuthorisedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        MeterAddress = c.String(maxLength: 256),
                        Authorised = c.Boolean(nullable: false),
                        DateBought = c.DateTime(nullable: false),
                        DateAuthorised = c.DateTime(nullable: false),
                        IssuedBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ElectricityRequest",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SiteId = c.Int(nullable: false),
                        DateRequested = c.DateTime(nullable: false),
                        RequestedBy = c.String(nullable: false, maxLength: 128),
                        RequestorComment = c.String(maxLength: 512),
                        Units = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        DateActioned = c.DateTime(),
                        ActionedBy = c.String(maxLength: 128),
                        Authorised = c.Boolean(nullable: false),
                        ActionerComment = c.String(maxLength: 512),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.RequestedBy)
                .Index(t => t.RequestedBy);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FullName = c.String(),
                        SubSectionId = c.Int(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .Index(t => t.SubSectionId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DepartmentOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DepartmentId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.DepartmentId, t.DateAssigned })
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.DepartmentSubSectionAssignmentHistory",
                c => new
                    {
                        DepartmentId = c.Int(nullable: false),
                        SectionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.DepartmentId, t.SectionId, t.DateAssigned })
                .ForeignKey("dbo.Section", t => t.SectionId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.DepartmentId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.Section",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                        DepartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.SectionOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        SectionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.SectionId, t.DateAssigned })
                .ForeignKey("dbo.Section", t => t.SectionId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.SectionSubSectionAssignmentHistory",
                c => new
                    {
                        SectionId = c.Int(nullable: false),
                        SubSectionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SectionId, t.SubSectionId, t.DateAssigned })
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .ForeignKey("dbo.Section", t => t.SectionId)
                .Index(t => t.SectionId)
                .Index(t => t.SubSectionId);
            
            CreateTable(
                "dbo.SubSection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                        SectionId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.Section", t => t.SectionId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.SectionId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.RegionOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RegionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RegionId, t.DateAssigned })
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.SubSectionRegionAssignmentHistory",
                c => new
                    {
                        RegionId = c.Int(nullable: false),
                        SubSectionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.RegionId, t.SubSectionId, t.DateAssigned })
                .ForeignKey("dbo.Region", t => t.RegionId)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .Index(t => t.RegionId)
                .Index(t => t.SubSectionId);
            
            CreateTable(
                "dbo.SubSectionOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        SubSectionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.SubSectionId, t.DateAssigned })
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SubSectionId);
            
            CreateTable(
                "dbo.SubZone",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                        ZoneId = c.Int(nullable: false),
                        SubSectionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.SubSectionId);
            
            CreateTable(
                "dbo.MaintenanceCentre",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Type = c.String(maxLength: 32),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                        SubZoneId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubZone", t => t.SubZoneId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.SubZoneId);
            
            CreateTable(
                "dbo.MaintenanceCentreOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        MaintenanceCentreId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.MaintenanceCentreId, t.DateAssigned })
                .ForeignKey("dbo.MaintenanceCentre", t => t.MaintenanceCentreId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.MaintenanceCentreId);
            
            CreateTable(
                "dbo.MaintenanceCentreSubZoneAssignmentHistory",
                c => new
                    {
                        MaintenanceCentreId = c.Int(nullable: false),
                        SubZoneId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.MaintenanceCentreId, t.SubZoneId, t.DateAssigned })
                .ForeignKey("dbo.MaintenanceCentre", t => t.MaintenanceCentreId)
                .ForeignKey("dbo.SubZone", t => t.SubZoneId)
                .Index(t => t.MaintenanceCentreId)
                .Index(t => t.SubZoneId);
            
            CreateTable(
                "dbo.SiteDepartmentAssignmentHistory",
                c => new
                    {
                        SiteId = c.Int(nullable: false),
                        MaintenanceCentreId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SiteId, t.MaintenanceCentreId, t.DateAssigned })
                .ForeignKey("dbo.MaintenanceCentre", t => t.MaintenanceCentreId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.MaintenanceCentreId);
            
            CreateTable(
                "dbo.SubZoneOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        SubZoneId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.SubZoneId, t.DateAssigned })
                .ForeignKey("dbo.SubZone", t => t.SubZoneId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SubZoneId);
            
            CreateTable(
                "dbo.SubZoneSubSectionAssignmentHistory",
                c => new
                    {
                        SubSectionId = c.Int(nullable: false),
                        SubZoneId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SubSectionId, t.SubZoneId, t.DateAssigned })
                .ForeignKey("dbo.SubZone", t => t.SubZoneId)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .Index(t => t.SubSectionId)
                .Index(t => t.SubZoneId);
            
            CreateTable(
                "dbo.UserSubSectionAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        SubSectionId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.SubSectionId, t.DateAssigned })
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SubSectionId);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SubSectionId = c.Int(nullable: false),
                        ModelId = c.Short(nullable: false),
                        BodyTypeId = c.Byte(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        StatusId = c.Byte(nullable: false),
                        YearFirstUsedAsNew = c.DateTime(nullable: false),
                        EngineCapacity = c.Double(nullable: false),
                        FuelTankCapacity = c.Short(nullable: false),
                        StartMileage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VehicleBodyType", t => t.BodyTypeId)
                .ForeignKey("dbo.VehicleModel", t => t.ModelId)
                .ForeignKey("dbo.VehicleStatus", t => t.StatusId)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.SubSectionId)
                .Index(t => t.ModelId)
                .Index(t => t.BodyTypeId)
                .Index(t => t.FuelTypeId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.VehicleBodyType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VehicleSubSectionDeploymentHistory",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        SubSectionId = c.Int(nullable: false),
                        DateDeployed = c.DateTime(nullable: false),
                        DeployedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.SubSectionId, t.DateDeployed })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .ForeignKey("dbo.SubSection", t => t.SubSectionId)
                .Index(t => t.VehicleId)
                .Index(t => t.SubSectionId);
            
            CreateTable(
                "dbo.VehicleFuelLevelCheck",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Quantity = c.Int(),
                        Level = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateChecked })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.FuelUpVehicle",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DrawdownId = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Quantity = c.Int(nullable: false),
                        LevelBefore = c.Int(nullable: false),
                        LevelAfter = c.Int(nullable: false),
                        Mileage = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        FuelledBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DrawdownId })
                .ForeignKey("dbo.Drawdown", t => t.DrawdownId)
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId)
                .Index(t => t.DrawdownId);
            
            CreateTable(
                "dbo.Drawdown",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FuelSourceId = c.Short(nullable: false),
                        Date = c.DateTime(nullable: false),
                        AvailableQuantity = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FuelRequest", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.FuelRequest",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DateRequested = c.DateTime(nullable: false),
                        RequestedBy = c.String(nullable: false, maxLength: 128),
                        DateActioned = c.DateTime(nullable: false),
                        FuelType = c.String(nullable: false, maxLength: 128),
                        Quantity = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        ActionedBy = c.String(maxLength: 128),
                        Authorised = c.Boolean(nullable: false),
                        RequestorComment = c.String(maxLength: 512),
                        ActionerComment = c.String(maxLength: 512),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.RequestedBy)
                .Index(t => t.RequestedBy);
            
            CreateTable(
                "dbo.FuelUpGenerator",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DrawdownId = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Quantity = c.Int(nullable: false),
                        LevelBefore = c.Int(nullable: false),
                        LevelAfter = c.Int(nullable: false),
                        HoursRun = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        FuelledBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DrawdownId })
                .ForeignKey("dbo.Drawdown", t => t.DrawdownId)
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId)
                .Index(t => t.DrawdownId);
            
            CreateTable(
                "dbo.VehicleMileageCheck",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Mileage = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateChecked })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.VehicleModel",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        VehicleMakeId = c.Short(nullable: false),
                        YearOfManufacture = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 64),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VehicleMake", t => t.VehicleMakeId)
                .Index(t => t.VehicleMakeId)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.VehicleMake",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Name = c.String(maxLength: 64),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.VehicleServiceHistory",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateServiced = c.DateTime(nullable: false),
                        Mileage = c.Int(nullable: false),
                        JobCardNumber = c.String(maxLength: 128),
                        DocumentNumber = c.String(maxLength: 128),
                        ServicedBy = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateServiced })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.VehicleStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VehicleTrip",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateRecorded = c.DateTime(nullable: false),
                        OpeningMileage = c.Double(nullable: false),
                        OpeningMileageImage = c.String(),
                        ClosingMileage = c.Double(nullable: false),
                        ClosingMileageImage = c.String(),
                        Origin = c.String(maxLength: 128),
                        Destination = c.String(maxLength: 128),
                        Purpose = c.String(maxLength: 256),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        LoggedBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateRecorded })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SiteElectricityMeterAssignmentHistory",
                c => new
                    {
                        SiteId = c.Int(nullable: false),
                        MeterId = c.String(nullable: false, maxLength: 128),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SiteId, t.MeterId, t.DateAssigned })
                .ForeignKey("dbo.ElectrictyMeter", t => t.MeterId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.MeterId);
            
            CreateTable(
                "dbo.GeneratorServiceHistory",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateServiced = c.DateTime(nullable: false),
                        HoursRun = c.Int(nullable: false),
                        JobCardNumber = c.String(maxLength: 128),
                        DocumentNumber = c.String(maxLength: 128),
                        ServicedBy = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DateServiced })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.GeneratorStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GenFuelLevelCheck",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Quantity = c.Double(),
                        Level = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DateChecked })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.GenHoursRunCheck",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        HoursRun = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DateChecked })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.UsedCoupon",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BatchId = c.Long(nullable: false),
                        BatchSize = c.Int(nullable: false),
                        CouponIssuerId = c.Byte(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        RequestId = c.String(maxLength: 128),
                        DateRedeemed = c.DateTime(nullable: false),
                        RedemptionPoint = c.String(maxLength: 256),
                        RedeemedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponsBatch", t => new { t.BatchId, t.BatchSize, t.CouponIssuerId, t.FuelTypeId })
                .Index(t => new { t.BatchId, t.BatchSize, t.CouponIssuerId, t.FuelTypeId });
            
            CreateTable(
                "dbo.CouponArchive",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        DateActioned = c.DateTime(nullable: false),
                        BatchId = c.Long(nullable: false),
                        RequestId = c.String(maxLength: 128),
                        RedemptionPoint = c.String(maxLength: 256),
                        RedeemedBy = c.String(maxLength: 128),
                        ReasonCancelled = c.String(nullable: false, maxLength: 512),
                        CancelledBy = c.String(nullable: false, maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        FuelTypeId = c.Byte(nullable: false),
                        SubSectionId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 128),
                        ExpiryDate = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Denomination = c.Short(nullable: false),
                        CouponIssuerId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.Id, t.DateActioned });
            
            CreateTable(
                "dbo.ElectricityTokenIssueViewModel",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ElectricityPurchaseId = c.String(maxLength: 128),
                        TokenIssueConfirmed = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        ReceiptNumber = c.String(maxLength: 128),
                        MeterNumber = c.String(nullable: false, maxLength: 128),
                        CustomerName = c.String(maxLength: 512),
                        MeterAddress = c.String(maxLength: 256),
                        Token = c.String(nullable: false, maxLength: 32),
                        Tariff = c.String(maxLength: 64),
                        EnergyBought = c.Double(nullable: false),
                        TenderAmount = c.Double(),
                        EnergyCharge = c.Double(),
                        DebtCollected = c.Double(),
                        ReLevy = c.Double(),
                        VatRate = c.Double(),
                        VatAmount = c.Double(),
                        TotalPaid = c.Double(nullable: false),
                        DebtBalBf = c.Double(),
                        DebtBalCf = c.Double(),
                        VendorNumberAndName = c.String(maxLength: 128),
                        VendorInfo1 = c.String(maxLength: 128),
                        VendorInfo2 = c.String(maxLength: 128),
                        VendorInfo3 = c.String(maxLength: 128),
                        TariffIndex = c.String(maxLength: 128),
                        SupplyGroupCode = c.String(maxLength: 128),
                        KeyRevNumber = c.String(maxLength: 128),
                        Authorised = c.Boolean(nullable: false),
                        BoughtBy = c.String(maxLength: 128),
                        DateBought = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 128),
                        DateAuthorised = c.DateTime(nullable: false),
                        IssuedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        EventId = c.String(nullable: false, maxLength: 128),
                        EventDate = c.DateTime(nullable: false),
                        GeneratorId = c.String(maxLength: 128),
                        SiteId = c.String(maxLength: 128),
                        Disabled = c.Boolean(nullable: false),
                        LoggedBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.EventCategory",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventType",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SeverityLevel = c.Int(nullable: false),
                        EventCategory = c.String(maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                        EventCategory_EventCategoryId = c.Short(),
                        SeverityLevel_SeverityLevelId = c.Byte(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SeverityLevel",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Description = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.UsedCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch");
            DropForeignKey("dbo.Vehicle", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.GeneratorModel", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.Generator", "ModelId", "dbo.GeneratorModel");
            DropForeignKey("dbo.GenHoursRunCheck", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.GenFuelLevelCheck", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.Generator", "GeneratorStatusId", "dbo.GeneratorStatus");
            DropForeignKey("dbo.GeneratorServiceHistory", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.FuelUpGenerator", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.GeneratorDeploymentHistory", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.SiteElectricityMeterAssignmentHistory", "SiteId", "dbo.Site");
            DropForeignKey("dbo.SiteDepartmentAssignmentHistory", "SiteId", "dbo.Site");
            DropForeignKey("dbo.Generator", "SiteId", "dbo.Site");
            DropForeignKey("dbo.GeneratorDeploymentHistory", "SiteId", "dbo.Site");
            DropForeignKey("dbo.ElectrictyMeter", "SiteId", "dbo.Site");
            DropForeignKey("dbo.SiteElectricityMeterAssignmentHistory", "MeterId", "dbo.ElectrictyMeter");
            DropForeignKey("dbo.ElectricityRecharge", "MeterId", "dbo.ElectrictyMeter");
            DropForeignKey("dbo.UserSubSectionAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubZoneOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubSectionOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SectionOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RegionOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.MaintenanceCentreOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FuelRequest", "RequestedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.ElectricityRequest", "RequestedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.DepartmentOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Section", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.DepartmentSubSectionAssignmentHistory", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.SubSection", "SectionId", "dbo.Section");
            DropForeignKey("dbo.SectionSubSectionAssignmentHistory", "SectionId", "dbo.Section");
            DropForeignKey("dbo.VehicleSubSectionDeploymentHistory", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.Vehicle", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.VehicleTrip", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "StatusId", "dbo.VehicleStatus");
            DropForeignKey("dbo.VehicleServiceHistory", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "ModelId", "dbo.VehicleModel");
            DropForeignKey("dbo.VehicleModel", "VehicleMakeId", "dbo.VehicleMake");
            DropForeignKey("dbo.VehicleMileageCheck", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.FuelUpVehicle", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.FuelUpVehicle", "DrawdownId", "dbo.Drawdown");
            DropForeignKey("dbo.FuelUpGenerator", "DrawdownId", "dbo.Drawdown");
            DropForeignKey("dbo.Drawdown", "Id", "dbo.FuelRequest");
            DropForeignKey("dbo.VehicleFuelLevelCheck", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.VehicleSubSectionDeploymentHistory", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "BodyTypeId", "dbo.VehicleBodyType");
            DropForeignKey("dbo.UserSubSectionAssignmentHistory", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.AspNetUsers", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SubZoneSubSectionAssignmentHistory", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SubZone", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SubZoneSubSectionAssignmentHistory", "SubZoneId", "dbo.SubZone");
            DropForeignKey("dbo.SubZoneOverseerAssignmentHistory", "SubZoneId", "dbo.SubZone");
            DropForeignKey("dbo.MaintenanceCentreSubZoneAssignmentHistory", "SubZoneId", "dbo.SubZone");
            DropForeignKey("dbo.MaintenanceCentre", "SubZoneId", "dbo.SubZone");
            DropForeignKey("dbo.Site", "MaintenanceCentreId", "dbo.MaintenanceCentre");
            DropForeignKey("dbo.SiteDepartmentAssignmentHistory", "MaintenanceCentreId", "dbo.MaintenanceCentre");
            DropForeignKey("dbo.MaintenanceCentreSubZoneAssignmentHistory", "MaintenanceCentreId", "dbo.MaintenanceCentre");
            DropForeignKey("dbo.MaintenanceCentreOverseerAssignmentHistory", "MaintenanceCentreId", "dbo.MaintenanceCentre");
            DropForeignKey("dbo.SubSectionRegionAssignmentHistory", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SubSectionOverseerAssignmentHistory", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SectionSubSectionAssignmentHistory", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SubSection", "RegionId", "dbo.Region");
            DropForeignKey("dbo.SubSectionRegionAssignmentHistory", "RegionId", "dbo.Region");
            DropForeignKey("dbo.RegionOverseerAssignmentHistory", "RegionId", "dbo.Region");
            DropForeignKey("dbo.CouponsBatch", "SubSectionId", "dbo.SubSection");
            DropForeignKey("dbo.SectionOverseerAssignmentHistory", "SectionId", "dbo.Section");
            DropForeignKey("dbo.DepartmentSubSectionAssignmentHistory", "SectionId", "dbo.Section");
            DropForeignKey("dbo.DepartmentOverseerAssignmentHistory", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ElectricityTokenIssue", "Id", "dbo.ElectricityRequest");
            DropForeignKey("dbo.ElectricityRecharge", "Id", "dbo.ElectricityTokenIssue");
            DropForeignKey("dbo.ElectricityTokenIssue", "ElectricityPurchaseId", "dbo.ElectricityPurchases");
            DropForeignKey("dbo.ElectrictyMeter", "MeterStatusId", "dbo.ElectricityMeterStatus");
            DropForeignKey("dbo.ElectricityLevelCheck", "MeterId", "dbo.ElectrictyMeter");
            DropForeignKey("dbo.Site", "CommercialPowerStatusId", "dbo.CommercialPowerStatus");
            DropForeignKey("dbo.FuelInventoryHistory", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.FuelInventory", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.FuelDelivery", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.FuelSource", "FuelSourceTypeId", "dbo.FuelSourceType");
            DropForeignKey("dbo.FuelSource", "StatusId", "dbo.FuelSourceStatus");
            DropForeignKey("dbo.FuelInventoryHistory", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.FuelInventory", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.FuelDelivery", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.CouponsBatch", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.CouponsBatchTransfer", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatch", "CouponIssuerId", "dbo.CouponIssuer");
            DropForeignKey("dbo.CouponsBatch", "StatusId", "dbo.CouponBatchStatus");
            DropForeignKey("dbo.CouponsBatch", new[] { "ParentBatch_Id", "ParentBatch_BatchSize", "ParentBatch_CouponIssuerId", "ParentBatch_FuelTypeId" }, "dbo.CouponsBatch");
            DropForeignKey("dbo.CancelledCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.UsedCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            DropIndex("dbo.GenHoursRunCheck", new[] { "GeneratorId" });
            DropIndex("dbo.GenFuelLevelCheck", new[] { "GeneratorId" });
            DropIndex("dbo.GeneratorServiceHistory", new[] { "GeneratorId" });
            DropIndex("dbo.SiteElectricityMeterAssignmentHistory", new[] { "MeterId" });
            DropIndex("dbo.SiteElectricityMeterAssignmentHistory", new[] { "SiteId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.VehicleTrip", new[] { "VehicleId" });
            DropIndex("dbo.VehicleServiceHistory", new[] { "VehicleId" });
            DropIndex("dbo.VehicleMake", new[] { "Name" });
            DropIndex("dbo.VehicleModel", new[] { "Name" });
            DropIndex("dbo.VehicleModel", new[] { "VehicleMakeId" });
            DropIndex("dbo.VehicleMileageCheck", new[] { "VehicleId" });
            DropIndex("dbo.FuelUpGenerator", new[] { "DrawdownId" });
            DropIndex("dbo.FuelUpGenerator", new[] { "GeneratorId" });
            DropIndex("dbo.FuelRequest", new[] { "RequestedBy" });
            DropIndex("dbo.Drawdown", new[] { "Id" });
            DropIndex("dbo.FuelUpVehicle", new[] { "DrawdownId" });
            DropIndex("dbo.FuelUpVehicle", new[] { "VehicleId" });
            DropIndex("dbo.VehicleFuelLevelCheck", new[] { "VehicleId" });
            DropIndex("dbo.VehicleSubSectionDeploymentHistory", new[] { "SubSectionId" });
            DropIndex("dbo.VehicleSubSectionDeploymentHistory", new[] { "VehicleId" });
            DropIndex("dbo.Vehicle", new[] { "StatusId" });
            DropIndex("dbo.Vehicle", new[] { "FuelTypeId" });
            DropIndex("dbo.Vehicle", new[] { "BodyTypeId" });
            DropIndex("dbo.Vehicle", new[] { "ModelId" });
            DropIndex("dbo.Vehicle", new[] { "SubSectionId" });
            DropIndex("dbo.UserSubSectionAssignmentHistory", new[] { "SubSectionId" });
            DropIndex("dbo.UserSubSectionAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.SubZoneSubSectionAssignmentHistory", new[] { "SubZoneId" });
            DropIndex("dbo.SubZoneSubSectionAssignmentHistory", new[] { "SubSectionId" });
            DropIndex("dbo.SubZoneOverseerAssignmentHistory", new[] { "SubZoneId" });
            DropIndex("dbo.SubZoneOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.SiteDepartmentAssignmentHistory", new[] { "MaintenanceCentreId" });
            DropIndex("dbo.SiteDepartmentAssignmentHistory", new[] { "SiteId" });
            DropIndex("dbo.MaintenanceCentreSubZoneAssignmentHistory", new[] { "SubZoneId" });
            DropIndex("dbo.MaintenanceCentreSubZoneAssignmentHistory", new[] { "MaintenanceCentreId" });
            DropIndex("dbo.MaintenanceCentreOverseerAssignmentHistory", new[] { "MaintenanceCentreId" });
            DropIndex("dbo.MaintenanceCentreOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.MaintenanceCentre", new[] { "SubZoneId" });
            DropIndex("dbo.MaintenanceCentre", new[] { "Name" });
            DropIndex("dbo.SubZone", new[] { "SubSectionId" });
            DropIndex("dbo.SubZone", new[] { "Name" });
            DropIndex("dbo.SubSectionOverseerAssignmentHistory", new[] { "SubSectionId" });
            DropIndex("dbo.SubSectionOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.SubSectionRegionAssignmentHistory", new[] { "SubSectionId" });
            DropIndex("dbo.SubSectionRegionAssignmentHistory", new[] { "RegionId" });
            DropIndex("dbo.RegionOverseerAssignmentHistory", new[] { "RegionId" });
            DropIndex("dbo.RegionOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.Region", new[] { "Name" });
            DropIndex("dbo.SubSection", new[] { "RegionId" });
            DropIndex("dbo.SubSection", new[] { "SectionId" });
            DropIndex("dbo.SubSection", new[] { "Name" });
            DropIndex("dbo.SectionSubSectionAssignmentHistory", new[] { "SubSectionId" });
            DropIndex("dbo.SectionSubSectionAssignmentHistory", new[] { "SectionId" });
            DropIndex("dbo.SectionOverseerAssignmentHistory", new[] { "SectionId" });
            DropIndex("dbo.SectionOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.Section", new[] { "DepartmentId" });
            DropIndex("dbo.Section", new[] { "Name" });
            DropIndex("dbo.DepartmentSubSectionAssignmentHistory", new[] { "SectionId" });
            DropIndex("dbo.DepartmentSubSectionAssignmentHistory", new[] { "DepartmentId" });
            DropIndex("dbo.Department", new[] { "Name" });
            DropIndex("dbo.DepartmentOverseerAssignmentHistory", new[] { "DepartmentId" });
            DropIndex("dbo.DepartmentOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "SubSectionId" });
            DropIndex("dbo.ElectricityRequest", new[] { "RequestedBy" });
            DropIndex("dbo.ElectricityTokenIssue", new[] { "ElectricityPurchaseId" });
            DropIndex("dbo.ElectricityTokenIssue", new[] { "Id" });
            DropIndex("dbo.ElectricityRecharge", new[] { "MeterId" });
            DropIndex("dbo.ElectricityRecharge", new[] { "Id" });
            DropIndex("dbo.ElectricityLevelCheck", new[] { "MeterId" });
            DropIndex("dbo.ElectrictyMeter", new[] { "MeterStatusId" });
            DropIndex("dbo.ElectrictyMeter", new[] { "SiteId" });
            DropIndex("dbo.Site", new[] { "CommercialPowerStatusId" });
            DropIndex("dbo.Site", new[] { "Name" });
            DropIndex("dbo.Site", new[] { "MaintenanceCentreId" });
            DropIndex("dbo.GeneratorDeploymentHistory", new[] { "GeneratorId" });
            DropIndex("dbo.GeneratorDeploymentHistory", new[] { "SiteId" });
            DropIndex("dbo.Generator", new[] { "ModelId" });
            DropIndex("dbo.Generator", new[] { "GeneratorStatusId" });
            DropIndex("dbo.Generator", new[] { "SiteId" });
            DropIndex("dbo.GeneratorModel", new[] { "FuelTypeId" });
            DropIndex("dbo.GeneratorModel", new[] { "Name" });
            DropIndex("dbo.FuelSourceType", new[] { "Name" });
            DropIndex("dbo.FuelInventoryHistory", new[] { "FuelTypeId" });
            DropIndex("dbo.FuelInventoryHistory", new[] { "FuelSourceId" });
            DropIndex("dbo.FuelInventory", new[] { "FuelTypeId" });
            DropIndex("dbo.FuelInventory", new[] { "FuelSourceId" });
            DropIndex("dbo.FuelSource", new[] { "StatusId" });
            DropIndex("dbo.FuelSource", new[] { "Name" });
            DropIndex("dbo.FuelSource", new[] { "FuelSourceTypeId" });
            DropIndex("dbo.FuelDelivery", new[] { "FuelTypeId" });
            DropIndex("dbo.FuelDelivery", new[] { "FuelSourceId" });
            DropIndex("dbo.FuelType", new[] { "Name" });
            DropIndex("dbo.CouponsBatchTransfer", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            DropIndex("dbo.CouponIssuer", new[] { "Name" });
            DropIndex("dbo.CouponsBatch", new[] { "ParentBatch_Id", "ParentBatch_BatchSize", "ParentBatch_CouponIssuerId", "ParentBatch_FuelTypeId" });
            DropIndex("dbo.CouponsBatch", new[] { "StatusId" });
            DropIndex("dbo.CouponsBatch", new[] { "SubSectionId" });
            DropIndex("dbo.CouponsBatch", new[] { "FuelTypeId" });
            DropIndex("dbo.CouponsBatch", new[] { "CouponIssuerId" });
            DropIndex("dbo.CancelledCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            DropTable("dbo.SeverityLevel");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.EventType");
            DropTable("dbo.EventCategory");
            DropTable("dbo.Event");
            DropTable("dbo.ElectricityTokenIssueViewModel");
            DropTable("dbo.CouponArchive");
            DropTable("dbo.UsedCoupon");
            DropTable("dbo.GenHoursRunCheck");
            DropTable("dbo.GenFuelLevelCheck");
            DropTable("dbo.GeneratorStatus");
            DropTable("dbo.GeneratorServiceHistory");
            DropTable("dbo.SiteElectricityMeterAssignmentHistory");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.VehicleTrip");
            DropTable("dbo.VehicleStatus");
            DropTable("dbo.VehicleServiceHistory");
            DropTable("dbo.VehicleMake");
            DropTable("dbo.VehicleModel");
            DropTable("dbo.VehicleMileageCheck");
            DropTable("dbo.FuelUpGenerator");
            DropTable("dbo.FuelRequest");
            DropTable("dbo.Drawdown");
            DropTable("dbo.FuelUpVehicle");
            DropTable("dbo.VehicleFuelLevelCheck");
            DropTable("dbo.VehicleSubSectionDeploymentHistory");
            DropTable("dbo.VehicleBodyType");
            DropTable("dbo.Vehicle");
            DropTable("dbo.UserSubSectionAssignmentHistory");
            DropTable("dbo.SubZoneSubSectionAssignmentHistory");
            DropTable("dbo.SubZoneOverseerAssignmentHistory");
            DropTable("dbo.SiteDepartmentAssignmentHistory");
            DropTable("dbo.MaintenanceCentreSubZoneAssignmentHistory");
            DropTable("dbo.MaintenanceCentreOverseerAssignmentHistory");
            DropTable("dbo.MaintenanceCentre");
            DropTable("dbo.SubZone");
            DropTable("dbo.SubSectionOverseerAssignmentHistory");
            DropTable("dbo.SubSectionRegionAssignmentHistory");
            DropTable("dbo.RegionOverseerAssignmentHistory");
            DropTable("dbo.Region");
            DropTable("dbo.SubSection");
            DropTable("dbo.SectionSubSectionAssignmentHistory");
            DropTable("dbo.SectionOverseerAssignmentHistory");
            DropTable("dbo.Section");
            DropTable("dbo.DepartmentSubSectionAssignmentHistory");
            DropTable("dbo.Department");
            DropTable("dbo.DepartmentOverseerAssignmentHistory");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ElectricityRequest");
            DropTable("dbo.ElectricityPurchases");
            DropTable("dbo.ElectricityTokenIssue");
            DropTable("dbo.ElectricityRecharge");
            DropTable("dbo.ElectricityMeterStatus");
            DropTable("dbo.ElectricityLevelCheck");
            DropTable("dbo.ElectrictyMeter");
            DropTable("dbo.CommercialPowerStatus");
            DropTable("dbo.Site");
            DropTable("dbo.GeneratorDeploymentHistory");
            DropTable("dbo.Generator");
            DropTable("dbo.GeneratorModel");
            DropTable("dbo.FuelSourceType");
            DropTable("dbo.FuelSourceStatus");
            DropTable("dbo.FuelInventoryHistory");
            DropTable("dbo.FuelInventory");
            DropTable("dbo.FuelSource");
            DropTable("dbo.FuelDelivery");
            DropTable("dbo.FuelType");
            DropTable("dbo.CouponsBatchTransfer");
            DropTable("dbo.CouponIssuer");
            DropTable("dbo.CouponBatchStatus");
            DropTable("dbo.CouponsBatch");
            DropTable("dbo.CancelledCoupon");
        }
    }
}
