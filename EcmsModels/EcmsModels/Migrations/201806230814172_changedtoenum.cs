namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedtoenum : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FuelInventoryAdjustment", "AdjustmentType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FuelInventoryAdjustment", "AdjustmentType", c => c.String(maxLength: 128));
        }
    }
}
