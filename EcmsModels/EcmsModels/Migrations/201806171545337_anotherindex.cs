namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anotherindex : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleBodyType", "Name", c => c.String(maxLength: 32));
            CreateIndex("dbo.VehicleBodyType", "Name", unique: true);
            DropColumn("dbo.VehicleBodyType", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VehicleBodyType", "Description", c => c.String(maxLength: 32));
            DropIndex("dbo.VehicleBodyType", new[] { "Name" });
            DropColumn("dbo.VehicleBodyType", "Name");
        }
    }
}
