namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adjustmentsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FuelInventoryAdjustment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        Year = c.Short(nullable: false),
                        Period = c.Short(nullable: false),
                        AdjustmentType = c.String(maxLength: 128),
                        AdjustmentQuantity = c.Int(nullable: false),
                        AdjustmentReason = c.String(maxLength: 256),
                        InitiatedBy = c.String(maxLength: 128),
                        DateInitiated = c.DateTime(nullable: false),
                        Actioned = c.Boolean(nullable: false),
                        ActionedBy = c.String(maxLength: 128),
                        DateActioned = c.DateTime(nullable: false),
                        Notes = c.String(maxLength: 512),
                        Initiator_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Initiator_Id)
                .ForeignKey("dbo.FuelInventoryHistory", t => new { t.FuelSourceId, t.FuelTypeId, t.Year, t.Period })
                .ForeignKey("dbo.FuelInventory", t => new { t.FuelSourceId, t.FuelTypeId })
                .Index(t => new { t.FuelSourceId, t.FuelTypeId, t.Year, t.Period })
                .Index(t => t.Initiator_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory");
            DropForeignKey("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" }, "dbo.FuelInventoryHistory");
            DropForeignKey("dbo.FuelInventoryAdjustment", "Initiator_Id", "dbo.AspNetUsers");
            DropIndex("dbo.FuelInventoryAdjustment", new[] { "Initiator_Id" });
            DropIndex("dbo.FuelInventoryAdjustment", new[] { "FuelSourceId", "FuelTypeId", "Year", "Period" });
            DropTable("dbo.FuelInventoryAdjustment");
        }
    }
}
