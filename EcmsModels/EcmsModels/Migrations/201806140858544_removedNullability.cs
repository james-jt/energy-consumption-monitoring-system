namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedNullability : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Department", "Disabled", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Section", "Disabled", c => c.Boolean(nullable: false));
            AlterColumn("dbo.SubSection", "Disabled", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Region", "Disabled", c => c.Boolean(nullable: false));
            AlterColumn("dbo.SubZone", "Disabled", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MaintenanceCentre", "Disabled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MaintenanceCentre", "Disabled", c => c.Boolean());
            AlterColumn("dbo.SubZone", "Disabled", c => c.Boolean());
            AlterColumn("dbo.Region", "Disabled", c => c.Boolean());
            AlterColumn("dbo.SubSection", "Disabled", c => c.Boolean());
            AlterColumn("dbo.Section", "Disabled", c => c.Boolean());
            AlterColumn("dbo.Department", "Disabled", c => c.Boolean());
        }
    }
}
