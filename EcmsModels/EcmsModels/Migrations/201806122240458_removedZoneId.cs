namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedZoneId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.SubZone", "ZoneId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubZone", "ZoneId", c => c.Int(nullable: false));
        }
    }
}
