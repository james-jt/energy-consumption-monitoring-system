namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emu : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GeneratorReadingsView",
                c => new
                    {
                        Site = c.String(nullable: false, maxLength: 128),
                        LoggedBy = c.String(),
                        MaintenanceCentre = c.String(),
                        GenHoursRun = c.Double(nullable: false),
                        GenFuelLevel = c.Double(nullable: false),
                        GenHoursRunImage = c.String(),
                        GenFuelLevelImage = c.String(),
                    })
                .PrimaryKey(t => t.Site);
            
            CreateTable(
                "dbo.VehicleReadingsView",
                c => new
                    {
                        VehicleRegistrationNumber = c.String(nullable: false, maxLength: 128),
                        LoggedBy = c.String(),
                        Mileage = c.Double(nullable: false),
                        FuelLevel = c.Double(nullable: false),
                        MileageImage = c.String(),
                        FuelLevelImage = c.String(),
                    })
                .PrimaryKey(t => t.VehicleRegistrationNumber);
            
            AddColumn("dbo.CouponsBatch", "IsArchivable", c => c.Boolean(nullable: false));
            DropColumn("dbo.CouponArchive", "FuelTypeId");
            DropColumn("dbo.CouponArchive", "SubSectionId");
            DropColumn("dbo.CouponArchive", "CreatedBy");
            DropColumn("dbo.CouponArchive", "ExpiryDate");
            DropColumn("dbo.CouponArchive", "DateCreated");
            DropColumn("dbo.CouponArchive", "Denomination");
            DropColumn("dbo.CouponArchive", "CouponIssuerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CouponArchive", "CouponIssuerId", c => c.Byte(nullable: false));
            AddColumn("dbo.CouponArchive", "Denomination", c => c.Short(nullable: false));
            AddColumn("dbo.CouponArchive", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.CouponArchive", "ExpiryDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.CouponArchive", "CreatedBy", c => c.String(maxLength: 128));
            AddColumn("dbo.CouponArchive", "SubSectionId", c => c.Int(nullable: false));
            AddColumn("dbo.CouponArchive", "FuelTypeId", c => c.Byte(nullable: false));
            DropColumn("dbo.CouponsBatch", "IsArchivable");
            DropTable("dbo.VehicleReadingsView");
            DropTable("dbo.GeneratorReadingsView");
        }
    }
}
