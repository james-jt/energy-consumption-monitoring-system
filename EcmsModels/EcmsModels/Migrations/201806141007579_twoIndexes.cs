namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class twoIndexes : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Section", new[] { "Name" });
            DropIndex("dbo.Section", new[] { "DepartmentId" });
            CreateIndex("dbo.Section", new[] { "Name", "DepartmentId" }, unique: true, name: "IX_SectionNameAndDepartmentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Section", "IX_SectionNameAndDepartmentId");
            CreateIndex("dbo.Section", "DepartmentId");
            CreateIndex("dbo.Section", "Name", unique: true);
        }
    }
}
