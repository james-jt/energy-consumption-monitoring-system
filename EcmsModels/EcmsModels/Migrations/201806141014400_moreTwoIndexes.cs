namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moreTwoIndexes : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SubSection", new[] { "Name" });
            DropIndex("dbo.SubSection", new[] { "SectionId" });
            DropIndex("dbo.SubZone", new[] { "Name" });
            DropIndex("dbo.SubZone", new[] { "SubSectionId" });
            DropIndex("dbo.MaintenanceCentre", new[] { "Name" });
            DropIndex("dbo.MaintenanceCentre", new[] { "SubZoneId" });
            CreateIndex("dbo.SubSection", new[] { "Name", "SectionId" }, unique: true, name: "IX_SubSectionNameAndSectionId");
            CreateIndex("dbo.SubZone", new[] { "Name", "SubSectionId" }, unique: true, name: "IX_SubZoneNameAndSubSectionId");
            CreateIndex("dbo.MaintenanceCentre", new[] { "Name", "SubZoneId" }, unique: true, name: "IX_MaintenanceCentreNameAndSubZoneId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MaintenanceCentre", "IX_MaintenanceCentreNameAndSubZoneId");
            DropIndex("dbo.SubZone", "IX_SubZoneNameAndSubSectionId");
            DropIndex("dbo.SubSection", "IX_SubSectionNameAndSectionId");
            CreateIndex("dbo.MaintenanceCentre", "SubZoneId");
            CreateIndex("dbo.MaintenanceCentre", "Name", unique: true);
            CreateIndex("dbo.SubZone", "SubSectionId");
            CreateIndex("dbo.SubZone", "Name", unique: true);
            CreateIndex("dbo.SubSection", "SectionId");
            CreateIndex("dbo.SubSection", "Name", unique: true);
        }
    }
}
