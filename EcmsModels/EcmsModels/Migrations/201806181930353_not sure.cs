namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class notsure : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponsBatchTransfer", "SenderSubSectionId", c => c.Short(nullable: false));
            AddColumn("dbo.CouponsBatchTransfer", "ReceiverSubSectionId", c => c.Short(nullable: false));
            DropColumn("dbo.CouponsBatchTransfer", "SenderDepartmentId");
            DropColumn("dbo.CouponsBatchTransfer", "ReceiverDepartmentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CouponsBatchTransfer", "ReceiverDepartmentId", c => c.Short(nullable: false));
            AddColumn("dbo.CouponsBatchTransfer", "SenderDepartmentId", c => c.Short(nullable: false));
            DropColumn("dbo.CouponsBatchTransfer", "ReceiverSubSectionId");
            DropColumn("dbo.CouponsBatchTransfer", "SenderSubSectionId");
        }
    }
}
