namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelUPdate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ElectricityPurchases", "MeterAddress", c => c.String());
            AddColumn("dbo.ElectricityPurchases", "Authorised", c => c.Boolean(nullable: false));
            AddColumn("dbo.ElectricityPurchases", "DateBought", c => c.DateTime(nullable: false));
            AddColumn("dbo.ElectricityPurchases", "DateAuthorised", c => c.DateTime(nullable: false));
            AddColumn("dbo.ElectricityPurchases", "IssuedBy", c => c.String(maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ElectricityPurchases", "IssuedBy");
            DropColumn("dbo.ElectricityPurchases", "DateAuthorised");
            DropColumn("dbo.ElectricityPurchases", "DateBought");
            DropColumn("dbo.ElectricityPurchases", "Authorised");
            DropColumn("dbo.ElectricityPurchases", "MeterAddress");
        }
    }
}
