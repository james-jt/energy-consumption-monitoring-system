namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iveforgoten : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FuelInventoryAdjustment", "Approved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FuelInventoryAdjustment", "Approved");
        }
    }
}
