namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iveforgotenj : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleServiceHistory", "FuelLevel", c => c.Double(nullable: false));
            AddColumn("dbo.GeneratorServiceHistory", "FuelLevel", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelDelivery", "Quantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelDelivery", "QuantityBefore", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelInventory", "Capacity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelInventory", "CurrentQuantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelInventory", "ReorderLevel", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelInventoryAdjustment", "AdjustmentQuantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "Quantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "LevelBefore", c => c.Single(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "LevelAfter", c => c.Single(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "HoursRun", c => c.Double(nullable: false));
            AlterColumn("dbo.Drawdown", "AvailableQuantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelRequest", "Quantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "Quantity", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "LevelBefore", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "LevelAfter", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "Mileage", c => c.Double(nullable: false));
            AlterColumn("dbo.VehicleServiceHistory", "Mileage", c => c.Double(nullable: false));
            AlterColumn("dbo.VehicleServiceHistory", "ServicedBy", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.VehicleTrip", "Origin", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.VehicleTrip", "Destination", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.VehicleTrip", "Purpose", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.GeneratorServiceHistory", "HoursRun", c => c.Double(nullable: false));
            AlterColumn("dbo.FuelInventoryHistory", "ClosingQuantity", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FuelInventoryHistory", "ClosingQuantity", c => c.Int(nullable: false));
            AlterColumn("dbo.GeneratorServiceHistory", "HoursRun", c => c.Int(nullable: false));
            AlterColumn("dbo.VehicleTrip", "Purpose", c => c.String(maxLength: 256));
            AlterColumn("dbo.VehicleTrip", "Destination", c => c.String(maxLength: 128));
            AlterColumn("dbo.VehicleTrip", "Origin", c => c.String(maxLength: 128));
            AlterColumn("dbo.VehicleServiceHistory", "ServicedBy", c => c.String(maxLength: 128));
            AlterColumn("dbo.VehicleServiceHistory", "Mileage", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "Mileage", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "LevelAfter", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "LevelBefore", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpVehicle", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelRequest", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Drawdown", "AvailableQuantity", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "HoursRun", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "LevelAfter", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "LevelBefore", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelUpGenerator", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventoryAdjustment", "AdjustmentQuantity", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventory", "ReorderLevel", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventory", "CurrentQuantity", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelInventory", "Capacity", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelDelivery", "QuantityBefore", c => c.Int(nullable: false));
            AlterColumn("dbo.FuelDelivery", "Quantity", c => c.Int(nullable: false));
            DropColumn("dbo.GeneratorServiceHistory", "FuelLevel");
            DropColumn("dbo.VehicleServiceHistory", "FuelLevel");
        }
    }
}
