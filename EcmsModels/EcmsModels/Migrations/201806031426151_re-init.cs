namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reinit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CommercialPowerStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.Site",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentId = c.Int(nullable: false),
                        ZoneNumber = c.Short(nullable: false),
                        Name = c.String(maxLength: 256),
                        Region = c.String(maxLength: 256),
                        Location = c.String(maxLength: 256),
                        CommercialPowerAvailable = c.Boolean(nullable: false),
                        Latitude = c.String(maxLength: 32),
                        Longitude = c.String(maxLength: 32),
                        CommercialPowerStatusId = c.Byte(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .ForeignKey("dbo.CommercialPowerStatus", t => t.CommercialPowerStatusId)
                .Index(t => t.DepartmentId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.CommercialPowerStatusId);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        OverseerUserId = c.String(maxLength: 128),
                        Type = c.String(maxLength: 32),
                        Disabled = c.Boolean(),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.CouponsBatch",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentBatchId = c.Long(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        BookSize = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        Denomination = c.Short(nullable: false),
                        BatchSize = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        Issuer = c.String(maxLength: 128),
                        CreatedBy = c.String(maxLength: 128),
                        Description = c.String(maxLength: 128),
                        StatusId = c.Byte(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponsBatch", t => t.ParentBatchId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .ForeignKey("dbo.CouponBatchStatus", t => t.StatusId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.ParentBatchId)
                .Index(t => t.DepartmentId)
                .Index(t => t.FuelTypeId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.CancelledCoupon",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BatchId = c.Long(nullable: false),
                        DateCancelled = c.DateTime(nullable: false),
                        Reason = c.String(),
                        CancelledBy = c.String(),
                        Notes = c.String(),
                        CouponsBatch_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponsBatch", t => t.CouponsBatch_Id)
                .Index(t => t.CouponsBatch_Id);
            
            CreateTable(
                "dbo.CouponsBatchTransfer",
                c => new
                    {
                        BatchId = c.Long(nullable: false),
                        DateTransferred = c.DateTime(nullable: false),
                        SenderDepartmentId = c.Short(nullable: false),
                        ReceiverDepartmentId = c.Short(nullable: false),
                        DateReceived = c.DateTime(nullable: false),
                        TransferredBy = c.String(maxLength: 128),
                        ReceivedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.BatchId)
                .ForeignKey("dbo.CouponsBatch", t => t.BatchId)
                .Index(t => t.BatchId);
            
            CreateTable(
                "dbo.FuelType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.FuelDelivery",
                c => new
                    {
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        DateDelivered = c.DateTime(nullable: false),
                        Quantity = c.Int(nullable: false),
                        QuantityBefore = c.Int(nullable: false),
                        OrderNumber = c.String(maxLength: 128),
                        ReceivedBy = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.FuelSourceId, t.FuelTypeId, t.DateDelivered })
                .ForeignKey("dbo.FuelSource", t => t.FuelSourceId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.FuelSourceId)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.FuelSource",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        FuelSourceTypeId = c.Byte(nullable: false),
                        Description = c.String(maxLength: 128),
                        StatusId = c.Byte(nullable: false),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FuelSourceStatus", t => t.StatusId)
                .ForeignKey("dbo.FuelSourceType", t => t.FuelSourceTypeId)
                .Index(t => t.FuelSourceTypeId)
                .Index(t => t.Description, unique: true)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.FuelInventory",
                c => new
                    {
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        Capacity = c.Int(nullable: false),
                        CurrentQuantity = c.Int(nullable: false),
                        ReorderLevel = c.Int(nullable: false),
                        ReorderTriggered = c.Boolean(nullable: false),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.FuelSourceId, t.FuelTypeId })
                .ForeignKey("dbo.FuelSource", t => t.FuelSourceId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.FuelSourceId)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.FuelInventoryHistory",
                c => new
                    {
                        FuelSourceId = c.Short(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        Year = c.Short(nullable: false),
                        Period = c.Short(nullable: false),
                        OpeningDate = c.DateTime(nullable: false),
                        OpeningQuantity = c.Int(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.FuelSourceId, t.FuelTypeId, t.Year, t.Period })
                .ForeignKey("dbo.FuelSource", t => t.FuelSourceId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.FuelSourceId)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.FuelSourceStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.FuelSourceType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.GeneratorModel",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(maxLength: 128),
                        EngineMake = c.String(maxLength: 128),
                        EngineModel = c.String(maxLength: 128),
                        GeneratorMake = c.String(maxLength: 128),
                        ServiceInterval = c.Int(nullable: false),
                        GeneratorSize = c.Double(nullable: false),
                        NormalLifeSpan = c.Int(),
                        NormalConsuptionRate = c.Double(),
                        Disabled = c.Boolean(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.FuelTypeId);
            
            CreateTable(
                "dbo.Generator",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SiteId = c.Int(nullable: false),
                        Supplier = c.String(maxLength: 256),
                        YearFirstUsedAsNew = c.DateTime(nullable: false),
                        HoursRun = c.Int(nullable: false),
                        FuelCapacity = c.Int(nullable: false),
                        GeneratorStatusId = c.Byte(nullable: false),
                        ModelId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeneratorStatus", t => t.GeneratorStatusId)
                .ForeignKey("dbo.GeneratorModel", t => t.ModelId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.GeneratorStatusId)
                .Index(t => t.ModelId);
            
            CreateTable(
                "dbo.FuelUpGenerator",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DrawdownId = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Quantity = c.Int(nullable: false),
                        LevelBefore = c.Int(nullable: false),
                        LevelAfter = c.Int(nullable: false),
                        HoursRun = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        FuelledBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DrawdownId })
                .ForeignKey("dbo.Drawdown", t => t.DrawdownId)
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId)
                .Index(t => t.DrawdownId);
            
            CreateTable(
                "dbo.Drawdown",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FuelSourceId = c.Short(nullable: false),
                        Date = c.DateTime(nullable: false),
                        AvailableQuantity = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FuelRequest", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.FuelRequest",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DateRequested = c.DateTime(nullable: false),
                        RequestedBy = c.String(nullable: false, maxLength: 128),
                        DateActioned = c.DateTime(nullable: false),
                        FuelType = c.String(nullable: false, maxLength: 128),
                        Quantity = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        ActionedBy = c.String(maxLength: 128),
                        Authorised = c.Boolean(nullable: false),
                        RequestorComment = c.String(maxLength: 512),
                        ActionerComment = c.String(maxLength: 512),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.RequestedBy)
                .Index(t => t.RequestedBy);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DepartmentId = c.String(),
                        FullName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Department_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Department", t => t.Department_Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Department_Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DepartmentOverseerAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DepartmentId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.DepartmentId, t.DateAssigned })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.UserId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.ElectricityRequest",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SiteId = c.Int(nullable: false),
                        DateRequested = c.DateTime(nullable: false),
                        RequestedBy = c.String(nullable: false, maxLength: 128),
                        RequestorComment = c.String(maxLength: 512),
                        Units = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        DateActioned = c.DateTime(),
                        ActionedBy = c.String(maxLength: 128),
                        Authorised = c.Boolean(nullable: false),
                        ActionerComment = c.String(maxLength: 512),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.RequestedBy)
                .Index(t => t.RequestedBy);
            
            CreateTable(
                "dbo.ElectricityTokenIssue",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ElectricityPurchaseId = c.String(nullable: false, maxLength: 128),
                        DateIssued = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ElectricityPurchases", t => t.ElectricityPurchaseId)
                .ForeignKey("dbo.ElectricityRequest", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.ElectricityPurchaseId);
            
            CreateTable(
                "dbo.ElectricityPurchases",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        ReceiptNumber = c.String(maxLength: 128),
                        MeterNumber = c.String(maxLength: 128),
                        CustomerName = c.String(maxLength: 512),
                        Token = c.String(maxLength: 32),
                        Tariff = c.String(maxLength: 64),
                        EnergyBought = c.Double(),
                        TenderAmount = c.Double(),
                        EnergyCharge = c.Double(),
                        DebtCollected = c.Double(),
                        ReLevy = c.Double(),
                        VatRate = c.Double(),
                        VatAmount = c.Double(),
                        TotalPaid = c.Double(),
                        DebtBalBf = c.Double(),
                        DebtBalCf = c.Double(),
                        VendorNumberAndName = c.String(maxLength: 128),
                        VendorInfo1 = c.String(maxLength: 128),
                        VendorInfo2 = c.String(maxLength: 128),
                        VendorInfo3 = c.String(maxLength: 128),
                        TariffIndex = c.String(maxLength: 128),
                        SupplyGroupCode = c.String(maxLength: 128),
                        KeyRevNumber = c.String(maxLength: 128),
                        BoughtBy = c.String(maxLength: 128),
                        AuthorisedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ElectricityRecharge",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        MeterId = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        UnitsBefore = c.Int(nullable: false),
                        UnitsAfter = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ElectrictyMeter", t => t.MeterId)
                .ForeignKey("dbo.ElectricityTokenIssue", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.MeterId);
            
            CreateTable(
                "dbo.ElectrictyMeter",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SiteId = c.Int(nullable: false),
                        AccountNumber = c.String(maxLength: 128),
                        DateDeployed = c.DateTime(nullable: false),
                        DeployedBy = c.String(maxLength: 128),
                        Type = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        MeterStatusId = c.Byte(nullable: false),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ElectricityMeterStatus", t => t.MeterStatusId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.MeterStatusId);
            
            CreateTable(
                "dbo.ElectricityLevelCheck",
                c => new
                    {
                        MeterId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Units = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.MeterId, t.DateChecked })
                .ForeignKey("dbo.ElectrictyMeter", t => t.MeterId)
                .Index(t => t.MeterId);
            
            CreateTable(
                "dbo.ElectricityMeterStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.SiteElectricityMeterAssignmentHistory",
                c => new
                    {
                        SiteId = c.Int(nullable: false),
                        MeterId = c.String(nullable: false, maxLength: 128),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SiteId, t.MeterId, t.DateAssigned })
                .ForeignKey("dbo.ElectrictyMeter", t => t.MeterId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.MeterId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserDepartmentAssignmentHistory",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DepartmentId = c.Short(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.DepartmentId, t.DateAssigned })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.FuelUpVehicle",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DrawdownId = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Quantity = c.Int(nullable: false),
                        LevelBefore = c.Int(nullable: false),
                        LevelAfter = c.Int(nullable: false),
                        Mileage = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        FuelledBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DrawdownId })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .ForeignKey("dbo.Drawdown", t => t.DrawdownId)
                .Index(t => t.VehicleId)
                .Index(t => t.DrawdownId);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DepartmentId = c.Int(nullable: false),
                        ModelId = c.Short(nullable: false),
                        BodyTypeId = c.Byte(nullable: false),
                        FuelTypeId = c.Byte(nullable: false),
                        StatusId = c.Byte(nullable: false),
                        YearFirstUsedAsNew = c.DateTime(nullable: false),
                        EngineCapacity = c.Double(nullable: false),
                        FuelTankCapacity = c.Short(nullable: false),
                        StartMileage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VehicleBodyType", t => t.BodyTypeId)
                .ForeignKey("dbo.VehicleModel", t => t.ModelId)
                .ForeignKey("dbo.VehicleStatus", t => t.StatusId)
                .ForeignKey("dbo.FuelType", t => t.FuelTypeId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.DepartmentId)
                .Index(t => t.ModelId)
                .Index(t => t.BodyTypeId)
                .Index(t => t.FuelTypeId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.VehicleBodyType",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.VehicleDepartmentDeploymentHistory",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DepartmentId = c.Int(nullable: false),
                        DateDeployed = c.DateTime(nullable: false),
                        DeployedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DepartmentId, t.DateDeployed })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .Index(t => t.VehicleId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.VehicleFuelLevelCheck",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Quantity = c.Int(),
                        Level = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateChecked })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.VehicleMileageCheck",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Mileage = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateChecked })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.VehicleModel",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        VehicleMakeId = c.Short(nullable: false),
                        YearOfManufacture = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 64),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VehicleMake", t => t.VehicleMakeId)
                .Index(t => t.VehicleMakeId)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.VehicleMake",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Description = c.String(maxLength: 64),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.VehicleServiceHistory",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateServiced = c.DateTime(nullable: false),
                        Mileage = c.Int(nullable: false),
                        JobCardNumber = c.String(maxLength: 128),
                        DocumentNumber = c.String(maxLength: 128),
                        ServicedBy = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateServiced })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.VehicleStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.VehicleTrip",
                c => new
                    {
                        VehicleId = c.String(nullable: false, maxLength: 128),
                        DateRecorded = c.DateTime(nullable: false),
                        OpeningMileage = c.Double(nullable: false),
                        OpeningMileageImage = c.String(),
                        ClosingMileage = c.Double(nullable: false),
                        ClosingMileageImage = c.String(),
                        Origin = c.String(maxLength: 128),
                        Destination = c.String(maxLength: 128),
                        Purpose = c.String(maxLength: 256),
                        DocumentNumber = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        LoggedBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.VehicleId, t.DateRecorded })
                .ForeignKey("dbo.Vehicle", t => t.VehicleId)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.GeneratorDeploymentHistory",
                c => new
                    {
                        SiteId = c.Int(nullable: false),
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateDeployed = c.DateTime(nullable: false),
                        DeployedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SiteId, t.GeneratorId, t.DateDeployed })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.GeneratorServiceHistory",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateServiced = c.DateTime(nullable: false),
                        HoursRun = c.Int(nullable: false),
                        JobCardNumber = c.String(maxLength: 128),
                        DocumentNumber = c.String(maxLength: 128),
                        ServicedBy = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DateServiced })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.GeneratorStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.GenFuelLevelCheck",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        Quantity = c.Double(),
                        Level = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DateChecked })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.GenHoursRunCheck",
                c => new
                    {
                        GeneratorId = c.String(nullable: false, maxLength: 128),
                        DateChecked = c.DateTime(nullable: false),
                        HoursRun = c.Double(nullable: false),
                        DocumentNumber = c.String(maxLength: 128),
                        LoggedBy = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => new { t.GeneratorId, t.DateChecked })
                .ForeignKey("dbo.Generator", t => t.GeneratorId)
                .Index(t => t.GeneratorId);
            
            CreateTable(
                "dbo.CouponBatchStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Disabled = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            CreateTable(
                "dbo.UsedCoupon",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BatchId = c.Long(nullable: false),
                        RequestId = c.String(),
                        DateRedeemed = c.DateTime(nullable: false),
                        RedemptionPoint = c.String(),
                        RedeemedBy = c.String(),
                        Notes = c.String(),
                        CouponsBatch_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CouponsBatch", t => t.CouponsBatch_Id)
                .Index(t => t.CouponsBatch_Id);
            
            CreateTable(
                "dbo.SiteDepartmentAssignmentHistory",
                c => new
                    {
                        SiteId = c.Int(nullable: false),
                        DepartmentId = c.Int(nullable: false),
                        DateAssigned = c.DateTime(nullable: false),
                        AssignedBy = c.String(maxLength: 128),
                        DateRecalled = c.DateTime(nullable: false),
                        RecalledBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.SiteId, t.DepartmentId, t.DateAssigned })
                .ForeignKey("dbo.Department", t => t.DepartmentId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.EventCategory",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Description = c.String(maxLength: 32),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        EventId = c.String(nullable: false, maxLength: 128),
                        EventDate = c.DateTime(nullable: false),
                        GeneratorId = c.String(maxLength: 128),
                        SiteId = c.String(maxLength: 128),
                        Disabled = c.Boolean(nullable: false),
                        LoggedBy = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.EventType",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SeverityLevel = c.Int(nullable: false),
                        EventCategory = c.String(maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                        EventCategory_EventCategoryId = c.Short(),
                        SeverityLevel_SeverityLevelId = c.Byte(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SeverityLevel",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Description = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        Disabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Site", "CommercialPowerStatusId", "dbo.CommercialPowerStatus");
            DropForeignKey("dbo.SiteElectricityMeterAssignmentHistory", "SiteId", "dbo.Site");
            DropForeignKey("dbo.SiteDepartmentAssignmentHistory", "SiteId", "dbo.Site");
            DropForeignKey("dbo.Generator", "SiteId", "dbo.Site");
            DropForeignKey("dbo.GeneratorDeploymentHistory", "SiteId", "dbo.Site");
            DropForeignKey("dbo.ElectrictyMeter", "SiteId", "dbo.Site");
            DropForeignKey("dbo.Vehicle", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.VehicleDepartmentDeploymentHistory", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.AspNetUsers", "Department_Id", "dbo.Department");
            DropForeignKey("dbo.Site", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.SiteDepartmentAssignmentHistory", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.DepartmentOverseerAssignmentHistory", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.CouponsBatch", "DepartmentId", "dbo.Department");
            DropForeignKey("dbo.UsedCoupon", "CouponsBatch_Id", "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatch", "StatusId", "dbo.CouponBatchStatus");
            DropForeignKey("dbo.Vehicle", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.GeneratorModel", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.Generator", "ModelId", "dbo.GeneratorModel");
            DropForeignKey("dbo.GenHoursRunCheck", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.GenFuelLevelCheck", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.Generator", "GeneratorStatusId", "dbo.GeneratorStatus");
            DropForeignKey("dbo.GeneratorServiceHistory", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.GeneratorDeploymentHistory", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.FuelUpGenerator", "GeneratorId", "dbo.Generator");
            DropForeignKey("dbo.FuelUpVehicle", "DrawdownId", "dbo.Drawdown");
            DropForeignKey("dbo.VehicleTrip", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "StatusId", "dbo.VehicleStatus");
            DropForeignKey("dbo.VehicleServiceHistory", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "ModelId", "dbo.VehicleModel");
            DropForeignKey("dbo.VehicleModel", "VehicleMakeId", "dbo.VehicleMake");
            DropForeignKey("dbo.VehicleMileageCheck", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.VehicleFuelLevelCheck", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.VehicleDepartmentDeploymentHistory", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.Vehicle", "BodyTypeId", "dbo.VehicleBodyType");
            DropForeignKey("dbo.FuelUpVehicle", "VehicleId", "dbo.Vehicle");
            DropForeignKey("dbo.FuelUpGenerator", "DrawdownId", "dbo.Drawdown");
            DropForeignKey("dbo.UserDepartmentAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FuelRequest", "RequestedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.ElectricityRequest", "RequestedBy", "dbo.AspNetUsers");
            DropForeignKey("dbo.ElectricityTokenIssue", "Id", "dbo.ElectricityRequest");
            DropForeignKey("dbo.ElectricityRecharge", "Id", "dbo.ElectricityTokenIssue");
            DropForeignKey("dbo.SiteElectricityMeterAssignmentHistory", "MeterId", "dbo.ElectrictyMeter");
            DropForeignKey("dbo.ElectricityRecharge", "MeterId", "dbo.ElectrictyMeter");
            DropForeignKey("dbo.ElectrictyMeter", "MeterStatusId", "dbo.ElectricityMeterStatus");
            DropForeignKey("dbo.ElectricityLevelCheck", "MeterId", "dbo.ElectrictyMeter");
            DropForeignKey("dbo.ElectricityTokenIssue", "ElectricityPurchaseId", "dbo.ElectricityPurchases");
            DropForeignKey("dbo.DepartmentOverseerAssignmentHistory", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Drawdown", "Id", "dbo.FuelRequest");
            DropForeignKey("dbo.FuelInventoryHistory", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.FuelInventory", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.FuelDelivery", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.FuelSource", "FuelSourceTypeId", "dbo.FuelSourceType");
            DropForeignKey("dbo.FuelSource", "StatusId", "dbo.FuelSourceStatus");
            DropForeignKey("dbo.FuelInventoryHistory", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.FuelInventory", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.FuelDelivery", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.CouponsBatch", "FuelTypeId", "dbo.FuelType");
            DropForeignKey("dbo.CouponsBatchTransfer", "BatchId", "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatch", "ParentBatchId", "dbo.CouponsBatch");
            DropForeignKey("dbo.CancelledCoupon", "CouponsBatch_Id", "dbo.CouponsBatch");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.SiteDepartmentAssignmentHistory", new[] { "DepartmentId" });
            DropIndex("dbo.SiteDepartmentAssignmentHistory", new[] { "SiteId" });
            DropIndex("dbo.UsedCoupon", new[] { "CouponsBatch_Id" });
            DropIndex("dbo.CouponBatchStatus", new[] { "Description" });
            DropIndex("dbo.GenHoursRunCheck", new[] { "GeneratorId" });
            DropIndex("dbo.GenFuelLevelCheck", new[] { "GeneratorId" });
            DropIndex("dbo.GeneratorStatus", new[] { "Description" });
            DropIndex("dbo.GeneratorServiceHistory", new[] { "GeneratorId" });
            DropIndex("dbo.GeneratorDeploymentHistory", new[] { "GeneratorId" });
            DropIndex("dbo.GeneratorDeploymentHistory", new[] { "SiteId" });
            DropIndex("dbo.VehicleTrip", new[] { "VehicleId" });
            DropIndex("dbo.VehicleStatus", new[] { "Description" });
            DropIndex("dbo.VehicleServiceHistory", new[] { "VehicleId" });
            DropIndex("dbo.VehicleMake", new[] { "Description" });
            DropIndex("dbo.VehicleModel", new[] { "Description" });
            DropIndex("dbo.VehicleModel", new[] { "VehicleMakeId" });
            DropIndex("dbo.VehicleMileageCheck", new[] { "VehicleId" });
            DropIndex("dbo.VehicleFuelLevelCheck", new[] { "VehicleId" });
            DropIndex("dbo.VehicleDepartmentDeploymentHistory", new[] { "DepartmentId" });
            DropIndex("dbo.VehicleDepartmentDeploymentHistory", new[] { "VehicleId" });
            DropIndex("dbo.VehicleBodyType", new[] { "Description" });
            DropIndex("dbo.Vehicle", new[] { "StatusId" });
            DropIndex("dbo.Vehicle", new[] { "FuelTypeId" });
            DropIndex("dbo.Vehicle", new[] { "BodyTypeId" });
            DropIndex("dbo.Vehicle", new[] { "ModelId" });
            DropIndex("dbo.Vehicle", new[] { "DepartmentId" });
            DropIndex("dbo.FuelUpVehicle", new[] { "DrawdownId" });
            DropIndex("dbo.FuelUpVehicle", new[] { "VehicleId" });
            DropIndex("dbo.UserDepartmentAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.SiteElectricityMeterAssignmentHistory", new[] { "MeterId" });
            DropIndex("dbo.SiteElectricityMeterAssignmentHistory", new[] { "SiteId" });
            DropIndex("dbo.ElectricityMeterStatus", new[] { "Description" });
            DropIndex("dbo.ElectricityLevelCheck", new[] { "MeterId" });
            DropIndex("dbo.ElectrictyMeter", new[] { "MeterStatusId" });
            DropIndex("dbo.ElectrictyMeter", new[] { "SiteId" });
            DropIndex("dbo.ElectricityRecharge", new[] { "MeterId" });
            DropIndex("dbo.ElectricityRecharge", new[] { "Id" });
            DropIndex("dbo.ElectricityTokenIssue", new[] { "ElectricityPurchaseId" });
            DropIndex("dbo.ElectricityTokenIssue", new[] { "Id" });
            DropIndex("dbo.ElectricityRequest", new[] { "RequestedBy" });
            DropIndex("dbo.DepartmentOverseerAssignmentHistory", new[] { "DepartmentId" });
            DropIndex("dbo.DepartmentOverseerAssignmentHistory", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Department_Id" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.FuelRequest", new[] { "RequestedBy" });
            DropIndex("dbo.Drawdown", new[] { "Id" });
            DropIndex("dbo.FuelUpGenerator", new[] { "DrawdownId" });
            DropIndex("dbo.FuelUpGenerator", new[] { "GeneratorId" });
            DropIndex("dbo.Generator", new[] { "ModelId" });
            DropIndex("dbo.Generator", new[] { "GeneratorStatusId" });
            DropIndex("dbo.Generator", new[] { "SiteId" });
            DropIndex("dbo.GeneratorModel", new[] { "FuelTypeId" });
            DropIndex("dbo.GeneratorModel", new[] { "Name" });
            DropIndex("dbo.FuelSourceType", new[] { "Description" });
            DropIndex("dbo.FuelSourceStatus", new[] { "Description" });
            DropIndex("dbo.FuelInventoryHistory", new[] { "FuelTypeId" });
            DropIndex("dbo.FuelInventoryHistory", new[] { "FuelSourceId" });
            DropIndex("dbo.FuelInventory", new[] { "FuelTypeId" });
            DropIndex("dbo.FuelInventory", new[] { "FuelSourceId" });
            DropIndex("dbo.FuelSource", new[] { "StatusId" });
            DropIndex("dbo.FuelSource", new[] { "Description" });
            DropIndex("dbo.FuelSource", new[] { "FuelSourceTypeId" });
            DropIndex("dbo.FuelDelivery", new[] { "FuelTypeId" });
            DropIndex("dbo.FuelDelivery", new[] { "FuelSourceId" });
            DropIndex("dbo.FuelType", new[] { "Description" });
            DropIndex("dbo.CouponsBatchTransfer", new[] { "BatchId" });
            DropIndex("dbo.CancelledCoupon", new[] { "CouponsBatch_Id" });
            DropIndex("dbo.CouponsBatch", new[] { "StatusId" });
            DropIndex("dbo.CouponsBatch", new[] { "FuelTypeId" });
            DropIndex("dbo.CouponsBatch", new[] { "DepartmentId" });
            DropIndex("dbo.CouponsBatch", new[] { "ParentBatchId" });
            DropIndex("dbo.Department", new[] { "Name" });
            DropIndex("dbo.Site", new[] { "CommercialPowerStatusId" });
            DropIndex("dbo.Site", new[] { "Name" });
            DropIndex("dbo.Site", new[] { "DepartmentId" });
            DropIndex("dbo.CommercialPowerStatus", new[] { "Description" });
            DropTable("dbo.SeverityLevel");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.EventType");
            DropTable("dbo.Event");
            DropTable("dbo.EventCategory");
            DropTable("dbo.SiteDepartmentAssignmentHistory");
            DropTable("dbo.UsedCoupon");
            DropTable("dbo.CouponBatchStatus");
            DropTable("dbo.GenHoursRunCheck");
            DropTable("dbo.GenFuelLevelCheck");
            DropTable("dbo.GeneratorStatus");
            DropTable("dbo.GeneratorServiceHistory");
            DropTable("dbo.GeneratorDeploymentHistory");
            DropTable("dbo.VehicleTrip");
            DropTable("dbo.VehicleStatus");
            DropTable("dbo.VehicleServiceHistory");
            DropTable("dbo.VehicleMake");
            DropTable("dbo.VehicleModel");
            DropTable("dbo.VehicleMileageCheck");
            DropTable("dbo.VehicleFuelLevelCheck");
            DropTable("dbo.VehicleDepartmentDeploymentHistory");
            DropTable("dbo.VehicleBodyType");
            DropTable("dbo.Vehicle");
            DropTable("dbo.FuelUpVehicle");
            DropTable("dbo.UserDepartmentAssignmentHistory");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.SiteElectricityMeterAssignmentHistory");
            DropTable("dbo.ElectricityMeterStatus");
            DropTable("dbo.ElectricityLevelCheck");
            DropTable("dbo.ElectrictyMeter");
            DropTable("dbo.ElectricityRecharge");
            DropTable("dbo.ElectricityPurchases");
            DropTable("dbo.ElectricityTokenIssue");
            DropTable("dbo.ElectricityRequest");
            DropTable("dbo.DepartmentOverseerAssignmentHistory");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.FuelRequest");
            DropTable("dbo.Drawdown");
            DropTable("dbo.FuelUpGenerator");
            DropTable("dbo.Generator");
            DropTable("dbo.GeneratorModel");
            DropTable("dbo.FuelSourceType");
            DropTable("dbo.FuelSourceStatus");
            DropTable("dbo.FuelInventoryHistory");
            DropTable("dbo.FuelInventory");
            DropTable("dbo.FuelSource");
            DropTable("dbo.FuelDelivery");
            DropTable("dbo.FuelType");
            DropTable("dbo.CouponsBatchTransfer");
            DropTable("dbo.CancelledCoupon");
            DropTable("dbo.CouponsBatch");
            DropTable("dbo.Department");
            DropTable("dbo.Site");
            DropTable("dbo.CommercialPowerStatus");
        }
    }
}
