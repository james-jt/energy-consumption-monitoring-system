namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emunl : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CancelledCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatch", new[] { "ParentBatch_Id", "ParentBatch_BatchSize", "ParentBatch_CouponIssuerId", "ParentBatch_FuelTypeId" }, "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatchTransfer", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch");
            DropForeignKey("dbo.UsedCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch");
            DropIndex("dbo.CancelledCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            DropIndex("dbo.CouponsBatch", new[] { "ParentBatch_Id", "ParentBatch_BatchSize", "ParentBatch_CouponIssuerId", "ParentBatch_FuelTypeId" });
            DropIndex("dbo.CouponsBatchTransfer", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            DropIndex("dbo.UsedCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            DropColumn("dbo.CouponsBatch", "ParentBatchId");
            RenameColumn(table: "dbo.CouponsBatch", name: "ParentBatch_Id", newName: "ParentBatchId");
            DropPrimaryKey("dbo.CancelledCoupon");
            DropPrimaryKey("dbo.CouponsBatch");
            DropPrimaryKey("dbo.CouponsBatchTransfer");
            DropPrimaryKey("dbo.UsedCoupon");
            DropPrimaryKey("dbo.CouponArchive");
            AddColumn("dbo.CouponArchive", "CouponId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.CouponArchive", "DateArchived", c => c.DateTime(nullable: false));
            AlterColumn("dbo.CancelledCoupon", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.CancelledCoupon", "BatchId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.CouponsBatch", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.CouponsBatch", "ParentBatchId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.CouponsBatch", "ParentBatchId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.CouponsBatchTransfer", "BatchId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.UsedCoupon", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.UsedCoupon", "BatchId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.CouponArchive", "BatchId", c => c.String());
            AddPrimaryKey("dbo.CancelledCoupon", "Id");
            AddPrimaryKey("dbo.CouponsBatch", "Id");
            AddPrimaryKey("dbo.CouponsBatchTransfer", new[] { "BatchId", "DateTransferred" });
            AddPrimaryKey("dbo.UsedCoupon", "Id");
            AddPrimaryKey("dbo.CouponArchive", new[] { "CouponId", "DateActioned" });
            CreateIndex("dbo.CancelledCoupon", "BatchId");
            CreateIndex("dbo.CouponsBatch", "ParentBatchId");
            CreateIndex("dbo.CouponsBatchTransfer", "BatchId");
            CreateIndex("dbo.UsedCoupon", "BatchId");
            AddForeignKey("dbo.CancelledCoupon", "BatchId", "dbo.CouponsBatch", "Id");
            AddForeignKey("dbo.CouponsBatch", "ParentBatchId", "dbo.CouponsBatch", "Id");
            AddForeignKey("dbo.CouponsBatchTransfer", "BatchId", "dbo.CouponsBatch", "Id");
            AddForeignKey("dbo.UsedCoupon", "BatchId", "dbo.CouponsBatch", "Id");
            DropColumn("dbo.CancelledCoupon", "BatchSize");
            DropColumn("dbo.CancelledCoupon", "CouponIssuerId");
            DropColumn("dbo.CancelledCoupon", "FuelTypeId");
            DropColumn("dbo.CouponsBatch", "ParentBatch_BatchSize");
            DropColumn("dbo.CouponsBatch", "ParentBatch_CouponIssuerId");
            DropColumn("dbo.CouponsBatch", "ParentBatch_FuelTypeId");
            DropColumn("dbo.CouponsBatchTransfer", "BatchSize");
            DropColumn("dbo.CouponsBatchTransfer", "CouponIssuerId");
            DropColumn("dbo.CouponsBatchTransfer", "FuelTypeId");
            DropColumn("dbo.UsedCoupon", "BatchSize");
            DropColumn("dbo.UsedCoupon", "CouponIssuerId");
            DropColumn("dbo.UsedCoupon", "FuelTypeId");
            DropColumn("dbo.CouponArchive", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CouponArchive", "Id", c => c.Long(nullable: false));
            AddColumn("dbo.UsedCoupon", "FuelTypeId", c => c.Byte(nullable: false));
            AddColumn("dbo.UsedCoupon", "CouponIssuerId", c => c.Byte(nullable: false));
            AddColumn("dbo.UsedCoupon", "BatchSize", c => c.Int(nullable: false));
            AddColumn("dbo.CouponsBatchTransfer", "FuelTypeId", c => c.Byte(nullable: false));
            AddColumn("dbo.CouponsBatchTransfer", "CouponIssuerId", c => c.Byte(nullable: false));
            AddColumn("dbo.CouponsBatchTransfer", "BatchSize", c => c.Int(nullable: false));
            AddColumn("dbo.CouponsBatch", "ParentBatch_FuelTypeId", c => c.Byte(nullable: false));
            AddColumn("dbo.CouponsBatch", "ParentBatch_CouponIssuerId", c => c.Byte(nullable: false));
            AddColumn("dbo.CouponsBatch", "ParentBatch_BatchSize", c => c.Int(nullable: false));
            AddColumn("dbo.CancelledCoupon", "FuelTypeId", c => c.Byte(nullable: false));
            AddColumn("dbo.CancelledCoupon", "CouponIssuerId", c => c.Byte(nullable: false));
            AddColumn("dbo.CancelledCoupon", "BatchSize", c => c.Int(nullable: false));
            DropForeignKey("dbo.UsedCoupon", "BatchId", "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatchTransfer", "BatchId", "dbo.CouponsBatch");
            DropForeignKey("dbo.CouponsBatch", "ParentBatchId", "dbo.CouponsBatch");
            DropForeignKey("dbo.CancelledCoupon", "BatchId", "dbo.CouponsBatch");
            DropIndex("dbo.UsedCoupon", new[] { "BatchId" });
            DropIndex("dbo.CouponsBatchTransfer", new[] { "BatchId" });
            DropIndex("dbo.CouponsBatch", new[] { "ParentBatchId" });
            DropIndex("dbo.CancelledCoupon", new[] { "BatchId" });
            DropPrimaryKey("dbo.CouponArchive");
            DropPrimaryKey("dbo.UsedCoupon");
            DropPrimaryKey("dbo.CouponsBatchTransfer");
            DropPrimaryKey("dbo.CouponsBatch");
            DropPrimaryKey("dbo.CancelledCoupon");
            AlterColumn("dbo.CouponArchive", "BatchId", c => c.Long(nullable: false));
            AlterColumn("dbo.UsedCoupon", "BatchId", c => c.Long(nullable: false));
            AlterColumn("dbo.UsedCoupon", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.CouponsBatchTransfer", "BatchId", c => c.Long(nullable: false));
            AlterColumn("dbo.CouponsBatch", "ParentBatchId", c => c.Long(nullable: false));
            AlterColumn("dbo.CouponsBatch", "ParentBatchId", c => c.Long(nullable: false));
            AlterColumn("dbo.CouponsBatch", "Id", c => c.Long(nullable: false));
            AlterColumn("dbo.CancelledCoupon", "BatchId", c => c.Long(nullable: false));
            AlterColumn("dbo.CancelledCoupon", "Id", c => c.Long(nullable: false, identity: true));
            DropColumn("dbo.CouponArchive", "DateArchived");
            DropColumn("dbo.CouponArchive", "CouponId");
            AddPrimaryKey("dbo.CouponArchive", new[] { "Id", "DateActioned" });
            AddPrimaryKey("dbo.UsedCoupon", "Id");
            AddPrimaryKey("dbo.CouponsBatchTransfer", "BatchId");
            AddPrimaryKey("dbo.CouponsBatch", new[] { "Id", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            AddPrimaryKey("dbo.CancelledCoupon", "Id");
            RenameColumn(table: "dbo.CouponsBatch", name: "ParentBatchId", newName: "ParentBatch_Id");
            AddColumn("dbo.CouponsBatch", "ParentBatchId", c => c.Long(nullable: false));
            CreateIndex("dbo.UsedCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            CreateIndex("dbo.CouponsBatchTransfer", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            CreateIndex("dbo.CouponsBatch", new[] { "ParentBatch_Id", "ParentBatch_BatchSize", "ParentBatch_CouponIssuerId", "ParentBatch_FuelTypeId" });
            CreateIndex("dbo.CancelledCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            AddForeignKey("dbo.UsedCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch", new[] { "Id", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            AddForeignKey("dbo.CouponsBatchTransfer", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch", new[] { "Id", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            AddForeignKey("dbo.CouponsBatch", new[] { "ParentBatch_Id", "ParentBatch_BatchSize", "ParentBatch_CouponIssuerId", "ParentBatch_FuelTypeId" }, "dbo.CouponsBatch", new[] { "Id", "BatchSize", "CouponIssuerId", "FuelTypeId" });
            AddForeignKey("dbo.CancelledCoupon", new[] { "BatchId", "BatchSize", "CouponIssuerId", "FuelTypeId" }, "dbo.CouponsBatch", new[] { "Id", "BatchSize", "CouponIssuerId", "FuelTypeId" });
        }
    }
}
