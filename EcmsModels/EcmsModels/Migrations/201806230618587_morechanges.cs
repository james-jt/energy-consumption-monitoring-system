namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class morechanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FuelInventoryHistory", "ClosingDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.FuelInventoryHistory", "ClosingQuantity", c => c.Int(nullable: false));
            AddColumn("dbo.ElectricityRequest", "DepartmentId", c => c.Int(nullable: false));
            AddColumn("dbo.ElectricityRequest", "SectionId", c => c.Int(nullable: false));
            AddColumn("dbo.ElectricityRequest", "SubSectionId", c => c.Int(nullable: false));
            DropColumn("dbo.FuelInventoryHistory", "OpeningDate");
            DropColumn("dbo.FuelInventoryHistory", "OpeningQuantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FuelInventoryHistory", "OpeningQuantity", c => c.Int(nullable: false));
            AddColumn("dbo.FuelInventoryHistory", "OpeningDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.ElectricityRequest", "SubSectionId");
            DropColumn("dbo.ElectricityRequest", "SectionId");
            DropColumn("dbo.ElectricityRequest", "DepartmentId");
            DropColumn("dbo.FuelInventoryHistory", "ClosingQuantity");
            DropColumn("dbo.FuelInventoryHistory", "ClosingDate");
        }
    }
}
