namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredNames : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Department", new[] { "Name" });
            DropIndex("dbo.Section", "IX_SectionNameAndDepartmentId");
            DropIndex("dbo.SubSection", "IX_SubSectionNameAndSectionId");
            DropIndex("dbo.SubZone", "IX_SubZoneNameAndSubSectionId");
            DropIndex("dbo.MaintenanceCentre", "IX_MaintenanceCentreNameAndSubZoneId");
            AlterColumn("dbo.Department", "Name", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.Section", "Name", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.SubSection", "Name", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.SubZone", "Name", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.MaintenanceCentre", "Name", c => c.String(nullable: false, maxLength: 256));
            CreateIndex("dbo.Department", "Name", unique: true);
            CreateIndex("dbo.Section", new[] { "Name", "DepartmentId" }, unique: true, name: "IX_SectionNameAndDepartmentId");
            CreateIndex("dbo.SubSection", new[] { "Name", "SectionId" }, unique: true, name: "IX_SubSectionNameAndSectionId");
            CreateIndex("dbo.SubZone", new[] { "Name", "SubSectionId" }, unique: true, name: "IX_SubZoneNameAndSubSectionId");
            CreateIndex("dbo.MaintenanceCentre", new[] { "Name", "SubZoneId" }, unique: true, name: "IX_MaintenanceCentreNameAndSubZoneId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MaintenanceCentre", "IX_MaintenanceCentreNameAndSubZoneId");
            DropIndex("dbo.SubZone", "IX_SubZoneNameAndSubSectionId");
            DropIndex("dbo.SubSection", "IX_SubSectionNameAndSectionId");
            DropIndex("dbo.Section", "IX_SectionNameAndDepartmentId");
            DropIndex("dbo.Department", new[] { "Name" });
            AlterColumn("dbo.MaintenanceCentre", "Name", c => c.String(maxLength: 256));
            AlterColumn("dbo.SubZone", "Name", c => c.String(maxLength: 256));
            AlterColumn("dbo.SubSection", "Name", c => c.String(maxLength: 256));
            AlterColumn("dbo.Section", "Name", c => c.String(maxLength: 256));
            AlterColumn("dbo.Department", "Name", c => c.String(maxLength: 256));
            CreateIndex("dbo.MaintenanceCentre", new[] { "Name", "SubZoneId" }, unique: true, name: "IX_MaintenanceCentreNameAndSubZoneId");
            CreateIndex("dbo.SubZone", new[] { "Name", "SubSectionId" }, unique: true, name: "IX_SubZoneNameAndSubSectionId");
            CreateIndex("dbo.SubSection", new[] { "Name", "SectionId" }, unique: true, name: "IX_SubSectionNameAndSectionId");
            CreateIndex("dbo.Section", new[] { "Name", "DepartmentId" }, unique: true, name: "IX_SectionNameAndDepartmentId");
            CreateIndex("dbo.Department", "Name", unique: true);
        }
    }
}
