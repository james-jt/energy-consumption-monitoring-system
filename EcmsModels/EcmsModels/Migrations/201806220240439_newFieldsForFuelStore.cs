namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newFieldsForFuelStore : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FuelSource", "ContactPerson", c => c.String(maxLength: 128));
            AddColumn("dbo.FuelSource", "Email", c => c.String(maxLength: 128));
            AddColumn("dbo.FuelSource", "CellNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FuelSource", "CellNumber");
            DropColumn("dbo.FuelSource", "Email");
            DropColumn("dbo.FuelSource", "ContactPerson");
        }
    }
}
