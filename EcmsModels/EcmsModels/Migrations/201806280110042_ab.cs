namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ab : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CouponsBatch", "IsInTransit", c => c.Boolean(nullable: false));
            AlterColumn("dbo.CouponsBatchTransfer", "SenderSubSectionId", c => c.Int(nullable: false));
            AlterColumn("dbo.CouponsBatchTransfer", "ReceiverSubSectionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CouponsBatchTransfer", "ReceiverSubSectionId", c => c.Short(nullable: false));
            AlterColumn("dbo.CouponsBatchTransfer", "SenderSubSectionId", c => c.Short(nullable: false));
            DropColumn("dbo.CouponsBatch", "IsInTransit");
        }
    }
}
