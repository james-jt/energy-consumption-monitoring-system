namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class abc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FuelRequest", "FuelTypeId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Drawdown", "FuelSourceId");
            CreateIndex("dbo.FuelRequest", "FuelTypeId");
            AddForeignKey("dbo.FuelRequest", "FuelTypeId", "dbo.FuelType", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Drawdown", "FuelSourceId", "dbo.FuelSource", "Id");
            DropColumn("dbo.FuelRequest", "FuelType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FuelRequest", "FuelType", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.Drawdown", "FuelSourceId", "dbo.FuelSource");
            DropForeignKey("dbo.FuelRequest", "FuelTypeId", "dbo.FuelType");
            DropIndex("dbo.FuelRequest", new[] { "FuelTypeId" });
            DropIndex("dbo.Drawdown", new[] { "FuelSourceId" });
            DropColumn("dbo.FuelRequest", "FuelTypeId");
        }
    }
}
