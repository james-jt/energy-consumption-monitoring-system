namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedZoneAndRegion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Zone",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        RegionId = c.Byte(nullable: false),
                        Name = c.String(maxLength: 64),
                        OverseerUserId = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        Overseer_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Overseer_Id)
                .ForeignKey("dbo.Region", t => t.RegionId)
                .Index(t => t.RegionId)
                .Index(t => t.Overseer_Id);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Byte(nullable: false, identity: true),
                        Name = c.String(maxLength: 64),
                        OverseerUserId = c.String(maxLength: 128),
                        Notes = c.String(maxLength: 512),
                        Overseer_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Overseer_Id)
                .Index(t => t.Overseer_Id);
            
            AddColumn("dbo.Department", "ZoneId", c => c.Byte(nullable: false));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "ElectricityPurchaseId", c => c.String(nullable: false));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "MeterNumber", c => c.String(maxLength: 128));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "Token", c => c.String(maxLength: 32));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "EnergyBought", c => c.Double());
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "TotalPaid", c => c.Double());
            CreateIndex("dbo.Department", "ZoneId");
            AddForeignKey("dbo.Department", "ZoneId", "dbo.Zone", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Zone", "RegionId", "dbo.Region");
            DropForeignKey("dbo.Region", "Overseer_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Zone", "Overseer_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Department", "ZoneId", "dbo.Zone");
            DropIndex("dbo.Region", new[] { "Overseer_Id" });
            DropIndex("dbo.Zone", new[] { "Overseer_Id" });
            DropIndex("dbo.Zone", new[] { "RegionId" });
            DropIndex("dbo.Department", new[] { "ZoneId" });
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "TotalPaid", c => c.Double(nullable: false));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "EnergyBought", c => c.Double(nullable: false));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "Token", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "MeterNumber", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.ElectricityTokenIssueViewModel", "ElectricityPurchaseId", c => c.String());
            DropColumn("dbo.Department", "ZoneId");
            DropTable("dbo.Region");
            DropTable("dbo.Zone");
        }
    }
}
