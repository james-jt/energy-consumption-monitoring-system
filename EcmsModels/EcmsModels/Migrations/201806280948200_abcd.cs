namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class abcd : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.FuelDelivery", new[] { "FuelTypeId" });
            AddColumn("dbo.Drawdown", "InventoryItem_FuelSourceId", c => c.Short());
            AddColumn("dbo.Drawdown", "InventoryItem_FuelTypeId", c => c.Byte());
            CreateIndex("dbo.FuelDelivery", new[] { "FuelSourceId", "FuelTypeId" });
            CreateIndex("dbo.Drawdown", new[] { "InventoryItem_FuelSourceId", "InventoryItem_FuelTypeId" });
            AddForeignKey("dbo.Drawdown", new[] { "InventoryItem_FuelSourceId", "InventoryItem_FuelTypeId" }, "dbo.FuelInventory", new[] { "FuelSourceId", "FuelTypeId" });
            AddForeignKey("dbo.FuelDelivery", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory", new[] { "FuelSourceId", "FuelTypeId" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FuelDelivery", new[] { "FuelSourceId", "FuelTypeId" }, "dbo.FuelInventory");
            DropForeignKey("dbo.Drawdown", new[] { "InventoryItem_FuelSourceId", "InventoryItem_FuelTypeId" }, "dbo.FuelInventory");
            DropIndex("dbo.Drawdown", new[] { "InventoryItem_FuelSourceId", "InventoryItem_FuelTypeId" });
            DropIndex("dbo.FuelDelivery", new[] { "FuelSourceId", "FuelTypeId" });
            DropColumn("dbo.Drawdown", "InventoryItem_FuelTypeId");
            DropColumn("dbo.Drawdown", "InventoryItem_FuelSourceId");
            CreateIndex("dbo.FuelDelivery", "FuelTypeId");
        }
    }
}
