namespace EcmsModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class auditTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditLog",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        ActionerId = c.String(nullable: false, maxLength: 128),
                        EntityType = c.String(nullable: false, maxLength: 256),
                        EntityId = c.String(maxLength: 128),
                        ActionType = c.String(),
                        DateActioned = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AuditLog");
        }
    }
}
