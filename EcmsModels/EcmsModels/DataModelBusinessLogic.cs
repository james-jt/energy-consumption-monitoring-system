﻿using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using EcmsModels.WebModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EcmsModels.DataModels
{
    public partial class Region
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public Region GetByName(string name)
        {
            return context.Region.Where(e => e.Name == name).FirstOrDefault();
        }

        public Region GetById(int id)
        {
            return context.Region.Find(id);
        }

        public static Region GetDefault()
        {
            return new Region().GetByName(DefaultValues.DefaultDbEntityName);
        }

        public static Region CreateDefault()
        {
            var defaultRegion = new Region()
            {
                Name = DefaultValues.DefaultDbEntityName,
                Notes = "Default Region",
            };
            return defaultRegion;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().Region.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }
    }

    public partial class Department
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public Department GetByName(string name)
        {
            return context.Department.Where(e => e.Name == name).FirstOrDefault();
        }

        public Department GetById(int id)
        {
            return context.Department.Find(id);
        }

        public static Department GetDefault()
        {
            return new Department().GetByName(DefaultValues.DefaultDbEntityName);
        }

        public static Department CreateDefault()
        {
            var defaultDepartment = new Department().GetByName(DefaultValues.DefaultDbEntityName);
            if (defaultDepartment == null)
            {
                defaultDepartment = new Department()
                {
                    Name = DefaultValues.DefaultDbEntityName,
                    Notes = $"Default department",
                };
            }
            return defaultDepartment;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().Department.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }

        public ApplicationUser GetOverseer()
        {
            return context.Users.Find(this.OverseerUserId);
        }

        public List<ApplicationUser> GetUsers()
        {
            var users = new List<ApplicationUser>();
            Sections.ToList().ForEach(e =>
            {
                e.SubSections.ToList().ForEach(s =>
                {
                    users.AddRange(s.Users);
                });
            });
            return users;
        }

    }

    public partial class Section
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public Section GetByName(int departmentId, string name)
        {
            return context.Section.FirstOrDefault(e => e.DepartmentId == departmentId && e.Name == name);
        }

        public Section GetById(int id)
        {
            return context.Section.Find(id);
        }

        public static Section GetDefault(Department department)
        {
            return new Section().GetByName(department.Id, DefaultValues.DefaultDbEntityName);
        }

        public static Section CreateDefault(Department department)
        {
            var defaultSection = new Section().GetByName(department.Id, DefaultValues.DefaultDbEntityName);
            if (defaultSection == null)
            {
                defaultSection = new Section()
                {
                    Name = DefaultValues.DefaultDbEntityName,
                    Notes = $"Default Section in the {department.Name} department",
                    Department = department,
                    DepartmentId = department.Id,
                    OverseerUserId = department.OverseerUserId
                };
                department.Sections.Add(defaultSection);
            }
            return defaultSection;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().Section.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }

        public ApplicationUser GetOverseer()
        {
            return context.Users.Find(this.OverseerUserId);
        }

        public List<ApplicationUser> GetUsers()
        {
            var users = new List<ApplicationUser>();
            SubSections.ToList().ForEach(s =>
            {
                users.AddRange(s.Users);
            });
            return users;
        }

    }

    public partial class SubSection
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public SubSection GetByName(int SectionId, string name)
        {
            return context.SubSection.Where(e => e.SectionId == SectionId && e.Name == name).FirstOrDefault();
        }

        public SubSection GetById(int id)
        {
            return context.SubSection.Find(id);
        }

        public static SubSection GetDefault(Section section)
        {
            return new SubSection().GetByName(section.Id, DefaultValues.DefaultDbEntityName);
        }

        public static SubSection CreateDefault(Section section)
        {
            var defaultSubSection = new SubSection().GetByName(section.Id, DefaultValues.DefaultDbEntityName);
            if (defaultSubSection == null)
            {
                defaultSubSection = new SubSection()
                {
                    Name = DefaultValues.DefaultDbEntityName,
                    Notes = $"Default Sub-Section for {section.Name} section in the {section.Department.Name} department",
                    Section = section,
                    SectionId = section.Id,
                    OverseerUserId = section.OverseerUserId,
                    RegionId = Region.GetDefault().Id
                };
                section.SubSections.Add(defaultSubSection);
            }
            return defaultSubSection;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().SubSection.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }

        public ApplicationUser GetOverseer()
        {
            return context.Users.Find(this.OverseerUserId);
        }

        public List<ApplicationUser> GetUsers()
        {
            return Users.ToList();
        }

    }

    public partial class SubZone
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public SubZone GetByName(int ZoneId, string name)
        { 
            return context.SubZone.Where(e => e.SubSectionId == ZoneId && e.Name == name).FirstOrDefault();
        }

        public SubZone GetById(int id)
        {
            return context.SubZone.Find(id);
        }

        public static SubZone GetDefault(SubSection zone)
        {
            return new SubZone().GetByName(zone.Id, DefaultValues.DefaultDbEntityName);
        }

        public static SubZone CreateDefault(SubSection zone)
        {
            var defaultSubZone = new SubZone().GetByName(zone.Id, DefaultValues.DefaultDbEntityName);
            if (defaultSubZone == null)
            {
                defaultSubZone = new SubZone()
                {
                    Name = DefaultValues.DefaultDbEntityName,
                    Notes = $"Default Sub-Zone for {zone.Name} sub section in the {zone.Section.Name} section of the {zone.Section.Department.Name} department",
                    SubSection = zone,
                    SubSectionId = zone.Id,
                    OverseerUserId = zone.OverseerUserId
                };
                zone.SubZones.Add(defaultSubZone);
            }
            return defaultSubZone;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().SubZone.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }

        public ApplicationUser GetOverseer()
        {
            return context.Users.Find(this.OverseerUserId);
        }

    }

    public partial class MaintenanceCentre
    { 
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public MaintenanceCentre GetByName(int subZoneId, string name)
        {
            return context.MaintenanceCentre.Where(e => e.SubZoneId == subZoneId && e.Name == name).FirstOrDefault();
        }

        public MaintenanceCentre GetById(int id)
        {
            return context.MaintenanceCentre.Find(id);
        }

        public static MaintenanceCentre GetDefault(SubZone subZone)
        {
            return new MaintenanceCentre().GetByName(subZone.Id, DefaultValues.DefaultDbEntityName);
        }

        public static MaintenanceCentre CreateDefault(SubZone subZone)
        {
            var defaultMaintenanceCentre = new MaintenanceCentre().GetByName(subZone.Id, DefaultValues.DefaultDbEntityName);
            if (defaultMaintenanceCentre == null)
            {
                defaultMaintenanceCentre = new MaintenanceCentre()
                {
                    Name = DefaultValues.DefaultDbEntityName,
                    Notes = $"Default Maintenance Centre for the {subZone.Name} sub-zone in {subZone.SubSection.Name} sub section",
                    SubZone = subZone,
                    SubZoneId = subZone.Id,
                    OverseerUserId = subZone.OverseerUserId
                };
                subZone.MaintenanceCentres.Add(defaultMaintenanceCentre);
            }
            return defaultMaintenanceCentre;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().MaintenanceCentre.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }

        public ApplicationUser GetOverseer()
        {
            return context.Users.Find(this.OverseerUserId);
        }

    }

    public partial class Site
    { 
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public Site GetByName(int maintenanceCentreId, string name)
        { 
            return context.Site.FirstOrDefault(e => e.MaintenanceCentreId == maintenanceCentreId && e.Name == name);
        }

        public Site GetById(int id)
        {
            return context.Site.Find(id);
        }

        public static Site GetDefault(MaintenanceCentre maintenanceCentre)
        {
            return new Site().GetByName(maintenanceCentre.Id, DefaultValues.DefaultDbEntityName);
        }

        public static Site CreateDefault(MaintenanceCentre maintenanceCentre)
        {
            var defaultSite = new Site().GetByName(maintenanceCentre.Id, DefaultValues.DefaultDbEntityName);
            if (defaultSite == null)
            {
                defaultSite = new Site()
                {
                    Name = DefaultValues.DefaultDbEntityName,
                    Notes = $"Default Site for {maintenanceCentre.Name} Maintenance Centre for the {maintenanceCentre.SubZone.Name} sub-zone in {maintenanceCentre.SubZone.SubSection.Name} sub section",
                    MaintenanceCentre = maintenanceCentre,
                    MaintenanceCentreId = maintenanceCentre.Id,
                 };
                maintenanceCentre.Sites.Add(defaultSite);
            }
            return defaultSite;
        }

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().Site.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Id.ToString()
                });
            });
            return items;
        }
    }

    public partial class Roles
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public static List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            new EcmsDbContext().Roles.ToList().ForEach(e =>
            {
                items.Add(new SelectListItem()
                {
                    Text = e.Name,
                    Value = e.Name.ToString()
                });
            });
            return items;
        }
    }

    public partial class FuelInventory
    {
        EcmsDbContext context = new EcmsDbContext();
    }
}
