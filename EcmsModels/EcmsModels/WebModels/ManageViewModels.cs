﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace EcmsModels.WebModels
{
    public class IndexViewModel
    { 
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Section { get; set; }
        public string SubSection { get; set; }
        //public string SubZone { get; set; }
        //public string MaintenanceCentre { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
        public string UserId { get; set; }
    }
     
    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class ChangeEmailViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class ChangeSubSectionViewModel
    { 
        [Required]
        [Display(Name = "Sub Section")]
        public string SubSectionId { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class ChangeSectionViewModel
    {
        [Required]
        [Display(Name = "Department")]
        public string DepartmentId { get; set; }

        [Required]
        [Display(Name = "Section")]
        public string SectionId { get; set; }

        [Required]
        [Display(Name = "Sub Section")]
        public string SubSectionId { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class ChangeDepartmentViewModel
    { 
        [Required]
        [Display(Name = "Department")]
        public string DepartmentId { get; set; }

        [Required]
        [Display(Name = "Section")]
        public string SectionId { get; set; }

        [Required]
        [Display(Name = "Sub Section")]
        public string SubSectionId { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class ChangeFullNameViewModel
    {
        [Required]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }


    public class ChangeUsernameViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }


    public class ChangeRoleViewModel
    {
        [Required] 
        [Display(Name = "New Role")]
        public string Role { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}