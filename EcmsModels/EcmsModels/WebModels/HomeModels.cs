﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.WebModels
{
    public class InfoModel
    {
        [Required]
        [Display(Name = "Message Title")]
        public string Title { get; set; }
         
        [Required]
        [Display(Name = "Message Body")]
        public string Body { get; set; }

    }
}
