﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using EcmsModels.DataModels;
using System.Collections.Generic;

namespace EcmsModels.WebModels
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public partial class ApplicationUser : IdentityUser 
    {
        public string FullName { get; set; }
        public int SubSectionId { get; set; }
        public virtual SubSection SubSection { get; set; }

        public ApplicationUser()
            : base()
        {
            DepartmentOverseerAssignmentHistory = new HashSet<DepartmentOverseerAssignmentHistory>();
            ElectricityRequests = new HashSet<ElectricityRequest>();
            FuelRequests = new HashSet<FuelRequest>();
            UserSubSectionAssignmentHistory = new HashSet<UserSubSectionAssignmentHistory>();
            MaintenanceCentreOverseerAssignmentHistory = new HashSet<MaintenanceCentreOverseerAssignmentHistory>();
            RegionOverseerAssignmentHistory = new HashSet<RegionOverseerAssignmentHistory>();
            SectionOverseerAssignmentHistory = new HashSet<SectionOverseerAssignmentHistory>();
            SubSectionOverseerAssignmentHistory = new HashSet<SubSectionOverseerAssignmentHistory>();
            SubZoneOverseerAssignmentHistory = new HashSet<SubZoneOverseerAssignmentHistory>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;

        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenitcationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenitcationType);
            // Add custom user claims here
            return userIdentity;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DepartmentOverseerAssignmentHistory> DepartmentOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityRequest> ElectricityRequests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelRequest> FuelRequests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserSubSectionAssignmentHistory> UserSubSectionAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MaintenanceCentreOverseerAssignmentHistory> MaintenanceCentreOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RegionOverseerAssignmentHistory> RegionOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SectionOverseerAssignmentHistory> SectionOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubSectionOverseerAssignmentHistory> SubSectionOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubZoneOverseerAssignmentHistory> SubZoneOverseerAssignmentHistory { get; set; }

    }
}