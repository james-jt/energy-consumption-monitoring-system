﻿using EcmsModels.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.WebModels
{
    public class VehicleReadingsView : VehicleReadingsViewModel
    {
        [Key]
        public override string VehicleRegistrationNumber { get; set; }

    }
}
