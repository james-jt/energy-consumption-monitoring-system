﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.WebModels
{
    [NotMapped]
    public class ElectricityTokenIssueViewModel
    {
        [Required]
        public string ElectricityPurchaseId { get; set; }

        public bool TokenIssueConfirmed { get; set; }
         
        public string Id { get; set; }

        public DateTime Date { get; set; }

        [StringLength(128)]
        public string ReceiptNumber { get; set; }

        [StringLength(128)]
        public string MeterNumber { get; set; }

        [StringLength(512)]
        public string CustomerName { get; set; }


        [Display(Name = "Meter Address")]
        public string MeterAddress { get; set; }

        [StringLength(32)]
        public string Token { get; set; }

        [StringLength(64)]
        public string Tariff { get; set; }

        public double? EnergyBought { get; set; }

        public double? TenderAmount { get; set; }

        public double? EnergyCharge { get; set; }

        public double? DebtCollected { get; set; }

        public double? ReLevy { get; set; }

        public double? VatRate { get; set; }

        public double? VatAmount { get; set; }

        public double? TotalPaid { get; set; }

        public double? DebtBalBf { get; set; }

        public double? DebtBalCf { get; set; }

        [StringLength(128)]
        public string VendorNumberAndName { get; set; }

        [StringLength(128)]
        public string VendorInfo1 { get; set; }

        [StringLength(128)]
        public string VendorInfo2 { get; set; }

        [StringLength(128)]
        public string VendorInfo3 { get; set; }

        [StringLength(128)]
        public string TariffIndex { get; set; }

        [StringLength(128)]
        public string SupplyGroupCode { get; set; }

        [StringLength(128)]
        public string KeyRevNumber { get; set; }

        public bool Authorised { get; set; }

        [StringLength(128)]
        public string BoughtBy { get; set; }

        public DateTime DateBought { get; set; }

        [StringLength(128)]
        public string AuthorisedBy { get; set; }

        public DateTime DateAuthorised { get; set; }

        [StringLength(128)]
        public string IssuedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

    }
}
