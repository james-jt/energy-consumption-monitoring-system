﻿using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.WebModels
{
    public partial class ApplicationUser
    {
        [NotMapped]
        EcmsDbContext context = new EcmsDbContext();

        public ApplicationUser GetByUsername(string name)
        {
            return context.Users.FirstOrDefault(e => e.UserName == name);
        }

        public static ApplicationUser GetDefault()
        {
            return new ApplicationUser().GetByUsername(DefaultValues.DefaultAdministratorEmail);
        }

        public static ApplicationUser CreateDefault(UserManager<ApplicationUser> UserManager)
        {
            var defaultAdmin = ApplicationUser.GetDefault();
            if (defaultAdmin == null)
            {
                defaultAdmin = new ApplicationUser();
                defaultAdmin.FullName = DefaultValues.DefaultAdministratorName;
                defaultAdmin.UserName = DefaultValues.DefaultAdministratorEmail;
                defaultAdmin.Email = DefaultValues.DefaultAdministratorEmail;
                defaultAdmin.EmailConfirmed = true;
                defaultAdmin.LockoutEnabled = true;
                defaultAdmin.PhoneNumber = DefaultValues.DefaultAdministratorPhoneNumber;
                defaultAdmin.SubSectionId = SubSection.GetDefault(Section.GetDefault(Department.GetDefault())).Id;
                string defaultAdminPassword = DefaultValues.DefaultAdministratorPassword;
                var createUserResult = UserManager.Create(defaultAdmin, defaultAdminPassword);
                if (!createUserResult.Succeeded)
                {
                    throw new Exception("Default identity user creation failed. Please review database initialisation method where the default user is created.");
                }
            }
            return defaultAdmin;
        }

        public Department GetDepartment()
        {
            return this.SubSection.Section.Department;
        }

        public Section GetSection()
        {
            return this.SubSection.Section;
        }

        //public SubZone GetSubZone()
        //{
        //    return context.SubZone.FirstOrDefault(e => e.SubSectionId == this.SubSectionId);
        //}

        //public MaintenanceCentre GetMaintenanceCentre()
        //{
        //    return context.MaintenanceCentre.FirstOrDefault(e => e.SubZone.SubSectionId == this.SubSectionId);
        //}

    }

}
