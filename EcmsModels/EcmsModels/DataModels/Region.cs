namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Region")]
    public partial class Region
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Region()
        {
            RegionOverseerAssignmentHistory = new HashSet<RegionOverseerAssignmentHistory>();
            SubSections = new HashSet<SubSection>();
            SubSectionRegionAssignmentHistory = new HashSet<SubSectionRegionAssignmentHistory>();
        }

        [Display(Name = "Region Number")]
        public int Id { get; set; }

        [StringLength(256)]
        [Index(IsUnique = true)]
        [Display(Name = "Region")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Overseer")]
        public string OverseerUserId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RegionOverseerAssignmentHistory> RegionOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubSection> SubSections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubSectionRegionAssignmentHistory> SubSectionRegionAssignmentHistory { get; set; }
    }
}
