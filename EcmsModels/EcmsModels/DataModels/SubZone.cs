namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubZone")]
    public partial class SubZone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubZone()
        {
            MaintenanceCentres = new HashSet<MaintenanceCentre>();
            MaintenanceCentreSubZoneAssignmentHistory = new HashSet<MaintenanceCentreSubZoneAssignmentHistory>();
            SubZoneOverseerAssignmentHistory = new HashSet<SubZoneOverseerAssignmentHistory>();
            SubZoneSubSectionAssignmentHistory = new HashSet<SubZoneSubSectionAssignmentHistory>();
        }

        [Display(Name = "Sub-Zone Number")]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        [Index("IX_SubZoneNameAndSubSectionId", 1, IsUnique = true)]
        [Display(Name = "Sub Zone")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Overseer")]
        public string OverseerUserId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [Required]
        [Index("IX_SubZoneNameAndSubSectionId", 2, IsUnique = true)]
        [Display(Name = "Sub Section")]
        public int SubSectionId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MaintenanceCentre> MaintenanceCentres { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MaintenanceCentreSubZoneAssignmentHistory> MaintenanceCentreSubZoneAssignmentHistory { get; set; }

        public virtual SubSection SubSection { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubZoneOverseerAssignmentHistory> SubZoneOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubZoneSubSectionAssignmentHistory> SubZoneSubSectionAssignmentHistory { get; set; }
    }
}
