namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SeverityLevel")]
    public partial class SeverityLevel
    {
        public byte Id { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public bool Disabled { get; set; }
    }
}
