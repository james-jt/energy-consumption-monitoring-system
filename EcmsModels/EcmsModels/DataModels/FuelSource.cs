namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelSource")]
    public partial class FuelSource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FuelSource()
        {
            FuelDeliveries = new HashSet<FuelDelivery>();
            FuelInventory = new HashSet<FuelInventory>();
            FuelInventoryHistory = new HashSet<FuelInventoryHistory>();
        }

        [Display(Name = "Fuel Store Number")]
        public short Id { get; set; }

        [Display(Name = "Fuel Store Type")]
        public byte FuelSourceTypeId { get; set; }

        [StringLength(128)]
        [Index(IsUnique = true)]
        [Display(Name = "Fuel Store")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Contact Person")]
        public string ContactPerson { get; set; }
         
        [StringLength(128)]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Cellphone Number")]
        public string CellNumber { get; set; }

        [Display(Name = "Status")]
        public byte StatusId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelDelivery> FuelDeliveries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Drawdown> FuelDrawdowns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelInventory> FuelInventory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelInventoryHistory> FuelInventoryHistory { get; set; }

        public virtual FuelSourceStatus FuelSourceStatus { get; set; }

        public virtual FuelSourceType FuelSourceType { get; set; }
    }
}
