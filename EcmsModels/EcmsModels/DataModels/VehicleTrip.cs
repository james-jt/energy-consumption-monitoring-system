namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleTrip")]
    public partial class VehicleTrip
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Vehicle")]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Recorded")]
        public DateTime DateRecorded { get; set; }

        [Required]
        [Display(Name = "Opening Mileage")]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Opening Mileage. Please enter a value greater than 0.")]
        public double OpeningMileage { get; set; }

        [Display(Name = "Opening Mileage Image")]
        [FileExtensions(ErrorMessage = "Please upload an image file.")]
        public string OpeningMileageImage { get; set; }

        [Required]
        [Display(Name = "Closing Mileage")]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Closing Mileage. Please enter a value greater than 0.")]
        public double ClosingMileage { get; set; }

        [Display(Name = "Closing Mileage Image")]
        [FileExtensions(ErrorMessage = "Please upload an image file.")]
        public string ClosingMileageImage { get; set; }

        [StringLength(128)]
        [Required]
        public string Origin { get; set; }

        [StringLength(128)]
        [Required]
        public string Destination { get; set; }

        [StringLength(256)]
        [Required]
        public string Purpose { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
