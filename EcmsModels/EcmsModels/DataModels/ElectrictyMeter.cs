namespace EcmsModels.DataModels
{
    using EcmsModels.ViewModels.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectrictyMeter")]
    public partial class ElectrictyMeter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ElectrictyMeter()
        {
            ElectricityLevelChecks = new HashSet<ElectricityLevelCheck>();
            ElectricityRecharges = new HashSet<ElectricityRecharge>();
            SiteElectricityMeterAssignmentHistory = new HashSet<SiteElectricityMeterAssignmentHistory>();
        }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Meter Number")]
        public string Id { get; set; }

        [Display(Name = "Site")]
        public int SiteId { get; set; }

        [StringLength(128)]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Date Deployed")]
        public DateTime DateDeployed { get; set; }

        [StringLength(128)]
        [Display(Name = "Deployed By")]
        public string DeployedBy { get; set; }

        public MeterTypes Type { get; set; }

        public bool Disabled { get; set; }

        [Display(Name = "Meter Status")]
        public byte MeterStatusId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityLevelCheck> ElectricityLevelChecks { get; set; }

        public virtual ElectricityMeterStatus ElectricityMeterStatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityRecharge> ElectricityRecharges { get; set; }

        public virtual Site Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteElectricityMeterAssignmentHistory> SiteElectricityMeterAssignmentHistory { get; set; }
    }
}
