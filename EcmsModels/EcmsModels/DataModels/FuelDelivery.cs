namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelDelivery")]
    public partial class FuelDelivery
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Fuel Store")]
        public short FuelSourceId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Fuel Type")]
        public byte FuelTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        [Display(Name = "Date Delivered")]
        public DateTime DateDelivered { get; set; }

        [Required]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Quantity. Please enter a value greater than 0.")]
        public double Quantity { get; set; }

        [Display(Name = "Quantity Before")]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid value for Quantity Before. Please enter a value greater than 0.")]
        public double QuantityBefore { get; set; }

        [StringLength(128)]
        [Display(Name = "Order Number")]
        public string OrderNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Received By")]
        public string ReceivedBy { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelSource FuelSource { get; set; }

        public virtual FuelType FuelType { get; set; }
    }
}
