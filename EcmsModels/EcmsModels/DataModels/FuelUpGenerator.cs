namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelUpGenerator")]
    public partial class FuelUpGenerator
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Generator")]
        public string GeneratorId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Drawdown Number")]
        public string DrawdownId { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Quantity. Please enter a value greater than 0.")]
        public double Quantity { get; set; }

        [Required]
        [Display(Name = "Level Before")]
        [Range(1, float.MaxValue, ErrorMessage = "Invalid Level Before. Please enter a value greater than 0.")]
        public float LevelBefore { get; set; }

        [Required]
        [Display(Name = "Level After")]
        [Range(0, float.MaxValue, ErrorMessage = "Invalid Level. Please enter a value between 0 and 1.")]
        public float LevelAfter { get; set; }

        [Required]
        [Display(Name = "Hours Run")]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Hours Run. Please enter a value greater than 0.")]
        public double HoursRun { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Fuelled By")]
        public string FuelledBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Drawdown Drawdown { get; set; }

        public virtual Generator Generator { get; set; }
    }
}
