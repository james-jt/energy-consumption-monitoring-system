namespace EcmsModels.DataModels
{
    using EcmsModels.ViewModels.Enums;
    using EcmsModels.WebModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelInventoryAdjustment")]
    public partial class FuelInventoryAdjustment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Adjustment")]
        public int Id { get; set; }

        [Display(Name = "Fuel Store")]
        public short FuelSourceId { get; set; }

        [Display(Name = "Fuel Type")]
        public byte FuelTypeId { get; set; }

        public int Year { get; set; }

        public int Period { get; set; }

        [Display(Name = "Type")]
        public InventoryAdjustmentTypes AdjustmentType { get; set; }

        [Display(Name = "Quantity")]
        [Range(0.0, Double.PositiveInfinity, ErrorMessage = "Enter the positive quantity and indicate the direction of adjustment in the Adjustment Type field.")]
        public double AdjustmentQuantity { get; set; }

        [Display(Name = "Reason")]
        [StringLength(256)]
        public string AdjustmentReason { get; set; }

        [Display(Name = "Initiated By")]
        [StringLength(128)]
        public string InitiatedBy { get; set; }

        [Display(Name = "Date Initiated")]
        public DateTime DateInitiated { get; set; }

        [Display(Name = "Actioned")]
        public bool Actioned { get; set; }

        [Display(Name = "Approved")]
        public bool Approved { get; set; }

        [Display(Name = "Actioned By")]
        [StringLength(128)]
        public string ActionedBy { get; set; }

        [Display(Name = "Date Actioned")]
        public DateTime DateActioned { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }
         
        public virtual ApplicationUser Initiator { get; set; }

        public virtual FuelInventory InventoryItem { get; set; }
         
        public virtual FuelInventoryHistory InventoryHistoryItem { get; set; }

    }
}
