namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SiteElectricityMeterAssignmentHistory")]
    public partial class SiteElectricityMeterAssignmentHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Site")]
        public int SiteId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Meter Number")]
        public string MeterId { get; set; }

        [Key]
        [Column(Order = 2)]
        [Display(Name = "Date Assigned")]
        public DateTime DateAssigned { get; set; }

        [StringLength(128)]
        [Display(Name = "Assigned By")]
        public string AssignedBy { get; set; }

        [Display(Name = "Date Recalled")]
        public DateTime DateRecalled { get; set; }

        [StringLength(128)]
        [Display(Name = "Recalled By")]
        public string RecalledBy { get; set; }

        public virtual ElectrictyMeter ElectrictyMeter { get; set; }

        public virtual Site Site { get; set; }
    }
}
