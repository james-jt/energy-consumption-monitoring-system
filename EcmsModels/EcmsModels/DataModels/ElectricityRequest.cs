namespace EcmsModels.DataModels
{
    using EcmsModels.WebModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectricityRequest")]
    public partial class ElectricityRequest
    {
        public string Id { get; set; }

        [Display(Name = "Site")]
        public int SiteId { get; set; }

        [Display(Name = "Date Requested")]
        public DateTime DateRequested { get; set; }

        [StringLength(128)]
        [Display(Name = "Requested By")]
        public string RequestedBy { get; set; }

        [Display(Name = "Department")]
        public int DepartmentId { get; set; }

        [Display(Name = "Section")]
        public int SectionId { get; set; }

        [Display(Name = "Sub Section")]
        public int SubSectionId { get; set; }

        [StringLength(512)]
        [Display(Name = "Requestor Comment")]
        public string RequestorComment { get; set; }

        public int Units { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [Display(Name = "Date Actioned")]
        public DateTime? DateActioned { get; set; }

        [StringLength(128)]
        [Display(Name = "Actioned By")]
        public string ActionedBy { get; set; }

        public bool Authorised { get; set; }

        [StringLength(512)]
        [Display(Name = "Actioner Comment")]
        public string ActionerComment { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual ApplicationUser Requestor { get; set; }

        public virtual ElectricityTokenIssue ElectricityTokenIssue { get; set; }
    }
}
