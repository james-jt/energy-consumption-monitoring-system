namespace EcmsModels.DataModels
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Microsoft.AspNet.Identity.EntityFramework;
    using EcmsModels.WebModels;

    public partial class EcmsDbContext : IdentityDbContext<ApplicationUser>
    {
        public EcmsDbContext()
            : base("name=EcmsConnection")
        {

        }

        public static EcmsDbContext Create()
        {
            return new EcmsDbContext();
        }

        public virtual DbSet<CancelledCoupon> CancelledCoupon { get; set; }
        public virtual DbSet<CommercialPowerStatus> CommercialPowerStatus { get; set; }
        public virtual DbSet<CouponArchive> CouponArchive { get; set; }
        public virtual DbSet<CouponBatchStatus> CouponBatchStatus { get; set; }
        public virtual DbSet<CouponIssuer> CouponIssuer { get; set; }
        public virtual DbSet<CouponsBatch> CouponsBatch { get; set; }
        public virtual DbSet<CouponsBatchTransfer> CouponsBatchTransfer { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<DepartmentOverseerAssignmentHistory> DepartmentOverseerAssignmentHistory { get; set; }
        public virtual DbSet<DepartmentSubSectionAssignmentHistory> DepartmentSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<Drawdown> Drawdown { get; set; }
        public virtual DbSet<ElectricityLevelCheck> ElectricityLevelCheck { get; set; }
        public virtual DbSet<ElectricityMeterStatus> ElectricityMeterStatus { get; set; }
        public virtual DbSet<ElectricityPurchases> ElectricityPurchase { get; set; }
        public virtual DbSet<ElectricityRecharge> ElectricityRecharge { get; set; }
        public virtual DbSet<ElectricityRequest> ElectricityRequest { get; set; }
        public virtual DbSet<ElectricityTokenIssue> ElectricityTokenIssue { get; set; }
        public virtual DbSet<ElectricityTokenIssueViewModel> ElectricityTokenIssueViewModel { get; set; }
        public virtual DbSet<ElectrictyMeter> ElectrictyMeter { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventCategory> EventCategory { get; set; }
        public virtual DbSet<EventType> EventType { get; set; }
        public virtual DbSet<FuelDelivery> FuelDelivery { get; set; }
        public virtual DbSet<FuelInventory> FuelInventory { get; set; }
        public virtual DbSet<FuelInventoryHistory> FuelInventoryHistory { get; set; }
        public virtual DbSet<FuelRequest> FuelRequest { get; set; }
        public virtual DbSet<FuelSource> FuelSource { get; set; }
        public virtual DbSet<FuelSourceStatus> FuelSourceStatus { get; set; }
        public virtual DbSet<FuelSourceType> FuelSourceType { get; set; }
        public virtual DbSet<FuelType> FuelType { get; set; }
        public virtual DbSet<FuelUpGenerator> FuelUpGenerator { get; set; }
        public virtual DbSet<FuelUpVehicle> FuelUpVehicle { get; set; }
        public virtual DbSet<Generator> Generator { get; set; }
        public virtual DbSet<GeneratorDeploymentHistory> GeneratorDeploymentHistory { get; set; }
        public virtual DbSet<GeneratorModel> GeneratorModel { get; set; }
        public virtual DbSet<GeneratorServiceHistory> GeneratorServiceHistory { get; set; }
        public virtual DbSet<GeneratorStatus> GeneratorStatus { get; set; }
        public virtual DbSet<GenFuelLevelCheck> GenFuelLevelCheck { get; set; }
        public virtual DbSet<GenHoursRunCheck> GenHoursRunCheck { get; set; }
        public virtual DbSet<MaintenanceCentre> MaintenanceCentre { get; set; }
        public virtual DbSet<MaintenanceCentreOverseerAssignmentHistory> MaintenanceCentreOverseerAssignmentHistory { get; set; }
        public virtual DbSet<MaintenanceCentreSubZoneAssignmentHistory> MaintenanceCentreSubZoneAssignmentHistory { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<RegionOverseerAssignmentHistory> RegionOverseerAssignmentHistory { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<SectionOverseerAssignmentHistory> SectionOverseerAssignmentHistory { get; set; }
        public virtual DbSet<SectionSubSectionAssignmentHistory> SectionSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<SeverityLevel> SeverityLevel { get; set; }
        public virtual DbSet<Site> Site { get; set; }
        public virtual DbSet<SiteDepartmentAssignmentHistory> SiteDepartmentAssignmentHistory { get; set; }
        public virtual DbSet<SiteElectricityMeterAssignmentHistory> SiteElectricityMeterAssignmentHistory { get; set; }
        public virtual DbSet<SubSection> SubSection { get; set; }
        public virtual DbSet<SubSectionOverseerAssignmentHistory> SubSectionOverseerAssignmentHistory { get; set; }
        public virtual DbSet<SubSectionRegionAssignmentHistory> SubSectionRegionAssignmentHistory { get; set; }
        public virtual DbSet<SubZone> SubZone { get; set; }
        public virtual DbSet<SubZoneOverseerAssignmentHistory> SubZoneOverseerAssignmentHistory { get; set; }
        public virtual DbSet<SubZoneSubSectionAssignmentHistory> SubZoneSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<UsedCoupon> UsedCoupon { get; set; }
        public virtual DbSet<UserSubSectionAssignmentHistory> UserSubSectionAssignmentHistory { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleBodyType> VehicleBodyType { get; set; }
        public virtual DbSet<VehicleSubSectionDeploymentHistory> VehicleSubSectionDeploymentHistory { get; set; }
        public virtual DbSet<VehicleFuelLevelCheck> VehicleFuelLevelCheck { get; set; }
        public virtual DbSet<VehicleMake> VehicleMake { get; set; }
        public virtual DbSet<VehicleMileageCheck> VehicleMileageCheck { get; set; }
        public virtual DbSet<VehicleModel> VehicleModel { get; set; }
        public virtual DbSet<VehicleServiceHistory> VehicleServiceHistory { get; set; }
        public virtual DbSet<VehicleStatus> VehicleStatus { get; set; }
        public virtual DbSet<VehicleTrip> VehicleTrip { get; set; }
        public virtual DbSet<AuditLog> AuditLog { get; set; }
        public virtual DbSet<FuelInventoryAdjustment> InventoryAdjustment { get; set; }
        public virtual DbSet<SiteSegment> Segment { get; set; }
        public virtual DbSet<Province> Province { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.DepartmentOverseerAssignmentHistory)
                .WithRequired(e => e.Overseer)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ElectricityRequests)
                .WithRequired(e => e.Requestor)
                .HasForeignKey(e => e.RequestedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.FuelRequests)
                .WithRequired(e => e.Requestor)
                .HasForeignKey(e => e.RequestedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.UserSubSectionAssignmentHistory)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.MaintenanceCentreOverseerAssignmentHistory)
                .WithRequired(e => e.Overseer)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.RegionOverseerAssignmentHistory)
                .WithRequired(e => e.Overseer)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.SectionOverseerAssignmentHistory)
                .WithRequired(e => e.Overseer)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.SubSectionOverseerAssignmentHistory)
                .WithRequired(e => e.Overseer)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.SubZoneOverseerAssignmentHistory)
                .WithRequired(e => e.Overseer)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CommercialPowerStatus>()
                .HasMany(e => e.Sites)
                .WithRequired(e => e.CommercialPowerStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponBatchStatus>()
                .HasMany(e => e.CouponsBatches)
                .WithRequired(e => e.CouponBatchStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponIssuer>()
                .HasMany(e => e.CouponsBatches)
                .WithRequired(e => e.CouponIssuer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.CancelledCoupons)
                .WithRequired(e => e.CouponsBatch)
                .HasForeignKey(e => new { e.BatchId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.ChildBatches)
                .WithRequired(e => e.ParentBatch)
                .HasForeignKey(e => new { e.ParentBatchId });

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.CouponsBatchTransfers)
                .WithRequired(e => e.CouponsBatch)
                .HasForeignKey(e => new { e.BatchId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CouponsBatch>()
                .HasMany(e => e.UsedCoupons)
                .WithRequired(e => e.CouponsBatch)
                .HasForeignKey(e => new { e.BatchId })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.DepartmentOverseerAssignmentHistory)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.Sections)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasMany(e => e.DepartmentSubSectionAssignmentHistory)
                .WithRequired(e => e.Department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Drawdown>()
                .HasMany(e => e.GeneratorFuelUps)
                .WithRequired(e => e.Drawdown)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Drawdown>()
                .HasMany(e => e.VehicleFuelUps)
                .WithRequired(e => e.Drawdown)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectricityMeterStatus>()
                .HasMany(e => e.ElectrictyMeters)
                .WithRequired(e => e.ElectricityMeterStatus)
                .HasForeignKey(e => e.MeterStatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectricityPurchases>()
                .HasMany(e => e.ElectricityTokenIssue)
                .WithRequired(e => e.ElectricityPurchases)
                .HasForeignKey(e => e.ElectricityPurchaseId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectricityRequest>()
                .HasOptional(e => e.ElectricityTokenIssue)
                .WithRequired(e => e.ElectricityRequest);

            modelBuilder.Entity<ElectricityTokenIssue>()
                .HasOptional(e => e.ElectricityRecharge)
                .WithRequired(e => e.ElectricityTokenIssue);

            modelBuilder.Entity<ElectrictyMeter>()
                .HasMany(e => e.ElectricityLevelChecks)
                .WithRequired(e => e.ElectrictyMeter)
                .HasForeignKey(e => e.MeterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectrictyMeter>()
                .HasMany(e => e.ElectricityRecharges)
                .WithRequired(e => e.ElectrictyMeter)
                .HasForeignKey(e => e.MeterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ElectrictyMeter>()
                .HasMany(e => e.SiteElectricityMeterAssignmentHistory)
                .WithRequired(e => e.ElectrictyMeter)
                .HasForeignKey(e => e.MeterId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelInventoryHistory>()
                .HasMany(e => e.Adjustments)
                .WithRequired(e => e.InventoryHistoryItem)
                .HasForeignKey(e => new { e.FuelSourceId, e.FuelTypeId, e.Year, e.Period })
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<FuelInventory>()
            //    .HasMany(e => e.Adjustments)
            //    .WithRequired(e => e.InventoryItem)
            //    .HasForeignKey(e => new { e.FuelSourceId, e.FuelTypeId })
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<FuelInventory>()
            //    .HasMany(e => e.Drawdowns)
            //    .WithRequired(e => e.InventoryItem)
            //    .HasForeignKey(e => new { e.FuelSourceId, e.FuelTypeId })
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelRequest>()
                .HasOptional(e => e.Drawdown)
                .WithRequired(e => e.FuelRequest);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelDeliveries)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelDrawdowns)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelInventory)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSource>()
                .HasMany(e => e.FuelInventoryHistory)
                .WithRequired(e => e.FuelSource)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSourceStatus>()
                .HasMany(e => e.FuelSources)
                .WithRequired(e => e.FuelSourceStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelSourceType>()
                .HasMany(e => e.FuelSources)
                .WithRequired(e => e.FuelSourceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.CouponsBatches)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.FuelDeliveries)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.FuelInventory)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.FuelInventoryHistory)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.GeneratorModels)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FuelType>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.FuelType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GeneratorFuelUps)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GeneratorDeploymentHistory)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GeneratorServiceHistory)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GenFuelLevelChecks)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Generator>()
                .HasMany(e => e.GenHoursRunChecks)
                .WithRequired(e => e.Generator)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GeneratorModel>()
                .HasMany(e => e.Generators)
                .WithRequired(e => e.GeneratorModel)
                .HasForeignKey(e => e.ModelId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GeneratorStatus>()
                .HasMany(e => e.Generators)
                .WithRequired(e => e.GeneratorStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Landlord>()
                .HasMany(e => e.Sites)
                .WithRequired(e => e.Landlord)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.MaintenanceCentreOverseerAssignmentHistory)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.Sites)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.MaintenanceCentreSubZoneAssignmentHistory)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MaintenanceCentre>()
                .HasMany(e => e.SiteDepartmentAssignmentHistory)
                .WithRequired(e => e.MaintenanceCentre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Province>()
                .HasMany(e => e.Sites)
                .WithRequired(e => e.Province)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.RegionOverseerAssignmentHistory)
                .WithRequired(e => e.Region)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.SubSections)
                .WithRequired(e => e.Region)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Region>()
                .HasMany(e => e.SubSectionRegionAssignmentHistory)
                .WithRequired(e => e.Region)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.DepartmentSubSectionAssignmentHistory)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.SectionOverseerAssignmentHistory)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.SubSections)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasMany(e => e.SectionSubSectionAssignmentHistory)
                .WithRequired(e => e.Section)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.ElectrictyMeters)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.Generators)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.GeneratorDeploymentHistory)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.SiteDepartmentAssignmentHistory)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Site>()
                .HasMany(e => e.SiteElectricityMeterAssignmentHistory)
                .WithRequired(e => e.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SiteSegment>()
                .HasMany(e => e.Sites)
                .WithRequired(e => e.Segment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.CouponsBatches)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SectionSubSectionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubSectionOverseerAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubSectionRegionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubZones)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.SubZoneSubSectionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.UserSubSectionAssignmentHistory)
                .WithRequired(e => e.SubSection)
                .HasForeignKey(e => e.SubSectionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubSection>()
                .HasMany(e => e.VehicleSubSectionDeploymentHistory)
                .WithRequired(e => e.SubSection)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.MaintenanceCentres)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.MaintenanceCentreSubZoneAssignmentHistory)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.SubZoneOverseerAssignmentHistory)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SubZone>()
                .HasMany(e => e.SubZoneSubSectionAssignmentHistory)
                .WithRequired(e => e.SubZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleFuelUps)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleDepartmentDeploymentHistory)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleFuelLevelChecks)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleMileageChecks)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleServiceHistory)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vehicle>()
                .HasMany(e => e.VehicleTrips)
                .WithRequired(e => e.Vehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleBodyType>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.VehicleBodyType)
                .HasForeignKey(e => e.BodyTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleMake>()
                .HasMany(e => e.VehicleModels)
                .WithRequired(e => e.VehicleMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleModel>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.VehicleModel)
                .HasForeignKey(e => e.ModelId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleStatus>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.VehicleStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);
        }

        public System.Data.Entity.DbSet<EcmsModels.DataModels.Landlord> Landlord { get; set; }

        public System.Data.Entity.DbSet<EcmsModels.WebModels.VehicleReadingsView> VehicleReadingsViews { get; set; }

        public System.Data.Entity.DbSet<EcmsModels.WebModels.GeneratorReadingsView> GeneratorReadingsViews { get; set; }
    }
}
