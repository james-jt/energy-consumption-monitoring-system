namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
     
    [Table("CouponsBatchArchive")]
    public partial class CouponsBatchArchive
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CouponsBatchArchive()
        {
            CancelledCoupons = new HashSet<CancelledCoupon>();
            ChildBatches = new HashSet<CouponsBatch>();
            CouponsBatchTransfers = new HashSet<CouponsBatchTransfer>();
            UsedCoupons = new HashSet<UsedCoupon>();
        }

        [Key]
        [Column(Order = 0)]
        [Display(Name = "Batch Number")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        [Key]
        [Display(Name = "Date Archived")]
        public DateTime DateArchived { get; set; }

        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid Batch Size. Please enter a value greater than 0.")]
        [Display(Name = "Batch Size")]
        public int BatchSize { get; set; }

        [Display(Name = "Issuer")]
        public byte CouponIssuerId { get; set; }

        [Display(Name = "Fuel Type")]
        public byte FuelTypeId { get; set; }

        [Display(Name = "Parent Batch")]
        public string ParentBatchId { get; set; }

        [Display(Name = "Sub Section")]
        public int SubSectionId { get; set; }

        [Display(Name = "Book Size")]
        [Range(1, Int16.MaxValue, ErrorMessage = "Invalid Book Size. Please enter a value greater than 0.")]
        public short BookSize { get; set; }

        [Display(Name = "Denomination")]
        [Range(1, Int16.MaxValue, ErrorMessage = "Invalid Denomination. Please enter a value greater than 0.")]
        public short Denomination { get; set; }

        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Expiry Date")]
        public DateTime ExpiryDate { get; set; }

        [StringLength(128)]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [StringLength(128)]
        public string Description { get; set; }

        [Display(Name = "Status")]
        public byte StatusId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CancelledCoupon> CancelledCoupons { get; set; }

        public virtual CouponBatchStatus CouponBatchStatus { get; set; }

        public virtual CouponIssuer CouponIssuer { get; set; }

        public virtual SubSection SubSection { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CouponsBatch> ChildBatches { get; set; }

        public virtual CouponsBatch ParentBatch { get; set; }

        public virtual FuelType FuelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CouponsBatchTransfer> CouponsBatchTransfers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsedCoupon> UsedCoupons { get; set; }
    }
}
