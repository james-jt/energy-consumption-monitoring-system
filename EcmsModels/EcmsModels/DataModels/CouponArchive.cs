namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CouponArchive")]
    public partial class CouponArchive
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Coupon Number")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CouponId { get; set; }
         
        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Actioned")]
        public DateTime DateActioned { get; set; }

        [Display(Name = "Date Archived")]
        public DateTime DateArchived { get; set; }

        [Display(Name = "Batch Number")]
        public string BatchId { get; set; }

        [StringLength(128)]
        [Display(Name = "Request Number")]
        public string RequestId { get; set; }

        [StringLength(256)]
        [Display(Name = "Redemption Point")]
        public string RedemptionPoint { get; set; }

        [StringLength(128)]
        [Display(Name = "Redeemed By")]
        public string RedeemedBy { get; set; }

        [Required]
        [StringLength(512)]
        [Display(Name = "Reason for Cancellation")]
        public string ReasonCancelled { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Cancelled By")]
        public string CancelledBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }
    }
}
