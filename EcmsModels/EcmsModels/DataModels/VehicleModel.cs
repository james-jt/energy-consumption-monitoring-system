namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleModel")]
    public partial class VehicleModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VehicleModel()
        {
            Vehicles = new HashSet<Vehicle>();
        }

        [Display(Name = "Vehicle Model Number")]
        public short Id { get; set; }

        [Display(Name = "Make")]
        public short VehicleMakeId { get; set; }

        [Display(Name = "Year of Manufacture")]
        public DateTime YearOfManufacture { get; set; }

        [StringLength(64)]
        [Index(IsUnique = true)]
        [Display(Name = "Model")]
        public string Name { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public bool Disabled { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }

        public virtual VehicleMake VehicleMake { get; set; }
    }
}
