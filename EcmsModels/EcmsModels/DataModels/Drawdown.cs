namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Drawdown")]
    public partial class Drawdown
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Drawdown()
        {
            GeneratorFuelUps = new HashSet<FuelUpGenerator>();
            VehicleFuelUps = new HashSet<FuelUpVehicle>();
        }

        [Display(Name = "Drawdown Number")]
        public string Id { get; set; }

        [Display(Name = "Fuel Store")]
        public short FuelSourceId { get; set; }

        public DateTime Date { get; set; }

        [Display(Name = "Available Quantity")]
        public double AvailableQuantity { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelRequest FuelRequest { get; set; }

        public virtual FuelSource FuelSource { get; set; }
         
        public virtual FuelInventory InventoryItem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpGenerator> GeneratorFuelUps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpVehicle> VehicleFuelUps { get; set; }


    }
}
