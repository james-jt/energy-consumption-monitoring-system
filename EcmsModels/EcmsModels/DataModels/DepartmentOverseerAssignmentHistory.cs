namespace EcmsModels.DataModels
{
    using EcmsModels.WebModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DepartmentOverseerAssignmentHistory")]
    public partial class DepartmentOverseerAssignmentHistory
    {
        [Key]
        [Column(Order = 0)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartmentId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateAssigned { get; set; }

        [StringLength(128)]
        public string AssignedBy { get; set; }

        public DateTime DateRecalled { get; set; }

        [StringLength(128)]
        public string RecalledBy { get; set; }

        public virtual ApplicationUser Overseer { get; set; }

        public virtual Department Department { get; set; }
    }
}
