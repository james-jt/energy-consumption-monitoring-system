namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Site")]
    public partial class Site
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Site()
        {
            ElectrictyMeters = new HashSet<ElectrictyMeter>();
            Generators = new HashSet<Generator>();
            GeneratorDeploymentHistory = new HashSet<GeneratorDeploymentHistory>();
            SiteDepartmentAssignmentHistory = new HashSet<SiteDepartmentAssignmentHistory>();
            SiteElectricityMeterAssignmentHistory = new HashSet<SiteElectricityMeterAssignmentHistory>();
        }

        [Display(Name = "Site Number")]
        public int Id { get; set; }

        [Display(Name = "Maintenance Centre")]
        public int MaintenanceCentreId { get; set; }

        [StringLength(256)]
        [Index(IsUnique = true)]
        [Display(Name = "Site")]
        public string Name { get; set; }

        [StringLength(256)]
        public string Location { get; set; }

        [Display(Name = "Commercial Power Available")]
        public bool CommercialPowerAvailable { get; set; }

        [StringLength(32)]
        public string Latitude { get; set; }

        [StringLength(32)]
        public string Longitude { get; set; }

        [Display(Name = "Power Status")]
        public byte CommercialPowerStatusId { get; set; }

        [Display(Name = "Segment")]
        public int SegmentId { get; set; }

        [Display(Name = "Province")]
        public int ProvinceId { get; set; }
         
        [Display(Name = "Landlord")]
        public int LandlordId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual CommercialPowerStatus CommercialPowerStatus { get; set; }

        public virtual SiteSegment Segment { get; set; }

        public virtual Province Province { get; set; }

        public virtual Landlord Landlord { get; set; }
         
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectrictyMeter> ElectrictyMeters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Generator> Generators { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GeneratorDeploymentHistory> GeneratorDeploymentHistory { get; set; }

        public virtual MaintenanceCentre MaintenanceCentre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteDepartmentAssignmentHistory> SiteDepartmentAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteElectricityMeterAssignmentHistory> SiteElectricityMeterAssignmentHistory { get; set; }
    
}
}
