namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ElectricityPurchases
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ElectricityPurchases()
        {
            ElectricityTokenIssue = new HashSet<ElectricityTokenIssue>();
        }

        [Display(Name = "Request Number")]
        public string Id { get; set; }

        public DateTime Date { get; set; }

        [StringLength(128)]
        [Display(Name = "Receipt Number")]
        public string ReceiptNumber { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Meter Number")]
        public string MeterNumber { get; set; }

        [StringLength(512)]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Required]
        [StringLength(32)]
        public string Token { get; set; }

        [StringLength(64)]
        public string Tariff { get; set; }

        [Display(Name = "Energy Bought")]
        public double EnergyBought { get; set; }

        [Display(Name = "Tender Amount")]
        public double? TenderAmount { get; set; }

        [Display(Name = "Energy Charge")]
        public double? EnergyCharge { get; set; }

        [Display(Name = "Debt Collected")]
        public double? DebtCollected { get; set; }

        [Display(Name = "RE Levy")]
        public double? ReLevy { get; set; }

        [Display(Name = "VAT Rate")]
        public double? VatRate { get; set; }

        [Display(Name = "VAT Amount")]
        public double? VatAmount { get; set; }

        [Display(Name = "Total Paid")]
        public double TotalPaid { get; set; }

        [Display(Name = "Debt Balance b/f")]
        public double? DebtBalBf { get; set; }

        [Display(Name = "Debt Balance c/f")]
        public double? DebtBalCf { get; set; }

        [StringLength(128)]
        [Display(Name = "Vendor Number and Name")]
        public string VendorNumberAndName { get; set; }

        [StringLength(128)]
        [Display(Name = "Vendor Info")]
        public string VendorInfo1 { get; set; }

        [StringLength(128)]
        [Display(Name = "Vendor Info")]
        public string VendorInfo2 { get; set; }

        [StringLength(128)]
        [Display(Name = "Vendor Info")]
        public string VendorInfo3 { get; set; }

        [StringLength(128)]
        public string TariffIndex { get; set; }

        [StringLength(128)]
        [Display(Name = "Supply Group Code")]
        public string SupplyGroupCode { get; set; }

        [StringLength(128)]
        [Display(Name = "Kety Rev Number")]
        public string KeyRevNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Bought By")]
        public string BoughtBy { get; set; }

        [StringLength(128)]
        [Display(Name = "Authorised By")]
        public string AuthorisedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [StringLength(256)]
        [Display(Name = "Meter Address")]
        public string MeterAddress { get; set; }

        public bool Authorised { get; set; }

        [Display(Name = "Date Bought")]
        public DateTime DateBought { get; set; }

        [Display(Name = "Date Authorised")]
        public DateTime DateAuthorised { get; set; }

        [StringLength(128)]
        [Display(Name = "Issued By")]
        public string IssuedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ElectricityTokenIssue> ElectricityTokenIssue { get; set; }
    }
}
