namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CouponsBatchTransfer")]
    public partial class CouponsBatchTransfer
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Batch Number")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BatchId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Transferred")]
        public DateTime DateTransferred { get; set; }

        [Display(Name = "Sender Sub-Section")]
        public int SenderSubSectionId { get; set; }

        [Display(Name = "Receiver Sub-Section")]
        public int ReceiverSubSectionId { get; set; }

        [Display(Name = "Date Received")]
        public DateTime DateReceived { get; set; }

        [StringLength(128)]
        [Display(Name = "Transferred By")]
        public string TransferredBy { get; set; }

        [StringLength(128)]
        [Display(Name = "Received By")]
        public string ReceivedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual CouponsBatch CouponsBatch { get; set; }
    }
}
