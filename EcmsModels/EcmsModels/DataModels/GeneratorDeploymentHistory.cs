namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GeneratorDeploymentHistory")]
    public partial class GeneratorDeploymentHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Site")]
        public int SiteId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Generator")]
        public string GeneratorId { get; set; }

        [Key]
        [Column(Order = 2)]
        [Display(Name = "Date Deployed")]
        public DateTime DateDeployed { get; set; }

        [StringLength(128)]
        [Display(Name = "Deployed By")]
        public string DeployedBy { get; set; }

        [Display(Name = "Date Recalled")]
        public DateTime DateRecalled { get; set; }

        [StringLength(128)]
        [Display(Name = "Recalled By")]
        public string RecalledBy { get; set; }

        public virtual Generator Generator { get; set; }

        public virtual Site Site { get; set; }
    }
}
