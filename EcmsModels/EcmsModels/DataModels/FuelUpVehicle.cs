namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelUpVehicle")]
    public partial class FuelUpVehicle
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Vehicle Reg Number")]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Drawdown Number")]
        public string DrawdownId { get; set; }

        public DateTime Date { get; set; }

        [Required]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Quantity. Please enter a value greater than 0.")]
        public double Quantity { get; set; }

        [Required]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Level. Please enter a value between 0 and 1.")]
        [Display(Name = "Level Before")]
        public double LevelBefore { get; set; }

        [Required]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Level. Please enter a value between 0 and 1.")]
        [Display(Name = "Level After")]
        public double LevelAfter { get; set; }

        [Required]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Quantity. Please enter a value greater than 0.")]
        public double Mileage { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Fuelled By")]
        public string FuelledBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Drawdown Drawdown { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
