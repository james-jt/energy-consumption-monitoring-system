namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Generator")]
    public partial class Generator
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Generator()
        {
            GeneratorFuelUps = new HashSet<FuelUpGenerator>();
            GeneratorDeploymentHistory = new HashSet<GeneratorDeploymentHistory>();
            GeneratorServiceHistory = new HashSet<GeneratorServiceHistory>();
            GenFuelLevelChecks = new HashSet<GenFuelLevelCheck>();
            GenHoursRunChecks = new HashSet<GenHoursRunCheck>();
        }

        [Display(Name = "Generator Serial Number")]
        public string Id { get; set; }

        [Display(Name = "Site")]
        public int SiteId { get; set; }

        [StringLength(256)]
        public string Supplier { get; set; }

        [Display(Name = "Yesr First Used As New")]
        public DateTime YearFirstUsedAsNew { get; set; }

        [Display(Name = "Initial Hours Run")]
        public int HoursRun { get; set; }

        [Display(Name = "Fuel Capacity")]
        public int FuelCapacity { get; set; }

        [Display(Name = "Status")]
        public byte GeneratorStatusId { get; set; }

        [Display(Name = "Model")]
        public byte ModelId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpGenerator> GeneratorFuelUps { get; set; }

        public virtual GeneratorModel GeneratorModel { get; set; }

        public virtual GeneratorStatus GeneratorStatus { get; set; }

        public virtual Site Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GeneratorDeploymentHistory> GeneratorDeploymentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GeneratorServiceHistory> GeneratorServiceHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenFuelLevelCheck> GenFuelLevelChecks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenHoursRunCheck> GenHoursRunChecks { get; set; }
    }
}
