namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectricityTokenIssue")]
    public partial class ElectricityTokenIssue
    {
        [Display(Name = "Request Number")]
        public string Id { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Electricity Purchase")]
        public string ElectricityPurchaseId { get; set; }

        [Display(Name = "Date Issued")]
        public DateTime DateIssued { get; set; }

        public virtual ElectricityPurchases ElectricityPurchases { get; set; }

        public virtual ElectricityRecharge ElectricityRecharge { get; set; }

        public virtual ElectricityRequest ElectricityRequest { get; set; }
    }
}
