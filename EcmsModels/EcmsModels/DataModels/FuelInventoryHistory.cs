namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FuelInventoryHistory")]
    public partial class FuelInventoryHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Fuel Store")]
        public short FuelSourceId { get; set; }
        
        [Key]
        [Column(Order = 1)]
        [Display(Name = "Fuel Type")]
        public byte FuelTypeId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Year { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Period { get; set; }

        [Display(Name = "Closing Date")]
        public DateTime ClosingDate { get; set; }

        [Display(Name = "Closing Quantity")]
        public double ClosingQuantity { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelSource FuelSource { get; set; }

        public virtual FuelType FuelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelInventoryAdjustment> Adjustments { get; set; }

    }
}
