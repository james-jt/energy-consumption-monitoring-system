namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MaintenanceCentre")]
    public partial class MaintenanceCentre
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MaintenanceCentre()
        {
            MaintenanceCentreOverseerAssignmentHistory = new HashSet<MaintenanceCentreOverseerAssignmentHistory>();
            Sites = new HashSet<Site>();
            MaintenanceCentreSubZoneAssignmentHistory = new HashSet<MaintenanceCentreSubZoneAssignmentHistory>();
            SiteDepartmentAssignmentHistory = new HashSet<SiteDepartmentAssignmentHistory>();
        }

        [Display(Name = "Maintenance Centre Number")]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        [Index("IX_MaintenanceCentreNameAndSubZoneId", 1, IsUnique = true)]
        [Display(Name = "Maintenance Centre")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Overseer")]
        public string OverseerUserId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [Required]
        [Index("IX_MaintenanceCentreNameAndSubZoneId", 2, IsUnique = true)]
        public int SubZoneId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MaintenanceCentreOverseerAssignmentHistory> MaintenanceCentreOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Site> Sites { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MaintenanceCentreSubZoneAssignmentHistory> MaintenanceCentreSubZoneAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SiteDepartmentAssignmentHistory> SiteDepartmentAssignmentHistory { get; set; }

        public virtual SubZone SubZone { get; set; }
    }
}
