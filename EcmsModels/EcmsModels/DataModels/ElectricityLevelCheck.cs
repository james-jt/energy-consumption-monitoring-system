namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectricityLevelCheck")]
    public partial class ElectricityLevelCheck
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Meter Number")]
        public string MeterId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Checked")]
        public DateTime DateChecked { get; set; }

        [Required]
        [Range(0.0, 1.0)]
        public double Units { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual ElectrictyMeter ElectrictyMeter { get; set; }
    }
}
