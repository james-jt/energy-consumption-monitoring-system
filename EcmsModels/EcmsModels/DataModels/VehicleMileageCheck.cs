namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleMileageCheck")]
    public partial class VehicleMileageCheck
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Vehicle")]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Checked")]
        public DateTime DateChecked { get; set; }

        [Required]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Mileage. Please enter a value greater than 0.")]
        public double Mileage { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
