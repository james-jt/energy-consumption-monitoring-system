namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ElectricityRecharge")]
    public partial class ElectricityRecharge
    {
        public string Id { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Meter Number")]
        public string MeterId { get; set; }

        public DateTime Date { get; set; }

        [Display(Name = "Units Before")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Invalid value for Units Before. Please enter a positive integral value.")]
        public int UnitsBefore { get; set; }

        [Display(Name = "Units After")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Invalid value for Units After. Please enter a positive integral value.")]
        public int UnitsAfter { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual ElectricityTokenIssue ElectricityTokenIssue { get; set; }

        public virtual ElectrictyMeter ElectrictyMeter { get; set; }
    }
}
