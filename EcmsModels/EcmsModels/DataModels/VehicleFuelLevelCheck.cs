namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleFuelLevelCheck")]
    public partial class VehicleFuelLevelCheck
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Vehicle")]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Checked")]
        public DateTime DateChecked { get; set; }

        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Quantity. Please enter a value greater than 0.")]
        public int? Quantity { get; set; }

        [Required]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Level. Please enter a value between 0 and 1.")]
        public double Level { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
