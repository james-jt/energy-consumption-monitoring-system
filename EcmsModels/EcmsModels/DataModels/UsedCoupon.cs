namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UsedCoupon")]
    public partial class UsedCoupon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Coupon Number")]
        public string Id { get; set; }

        [Display(Name = "Batch Number")]
        public string BatchId { get; set; }

        [StringLength(128)]
        [Display(Name = "Request")]
        public string RequestId { get; set; }

        [Display(Name = "Date Redeemed")]
        public DateTime DateRedeemed { get; set; }

        [StringLength(256)]
        [Display(Name = "Redemption Point")]
        public string RedemptionPoint { get; set; }

        [StringLength(128)]
        [Display(Name = "Redeemed By")]
        public string RedeemedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual CouponsBatch CouponsBatch { get; set; }
    }
}
