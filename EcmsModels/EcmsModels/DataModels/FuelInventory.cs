namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;

    [Table("FuelInventory")]
    public partial class FuelInventory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Fuel Store")]
        public short FuelSourceId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Fuel Type")]
        public byte FuelTypeId { get; set; }

        [Required]
        public double Capacity { get; set; }

        [Display(Name = "Balance")]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Balance. Please enter a value greater than 0.")]
        public double CurrentQuantity { get; set; }

        [Display(Name = "Reorder Level")]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Reorder Level. Please enter a value greater than 0.")]
        public double ReorderLevel { get; set; }

        [Display(Name = "Re-Order Alert Triggered")]
        public bool ReorderTriggered { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual FuelSource FuelSource { get; set; }

        public virtual FuelType FuelType { get; set; }
         
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelInventoryAdjustment> Adjustments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Drawdown> Drawdowns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelDelivery> StockReceipts { get; set; }
    }
}
