namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GeneratorServiceHistory")]
    public partial class GeneratorServiceHistory
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Generator")]
        public string GeneratorId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Serviced")]
        public DateTime DateServiced { get; set; }

        [Required]
        [Display(Name = "Hours Run")]
        [Range(1, Double.MaxValue, ErrorMessage = "Invalid Hours Run. Please enter a value greater than 0.")]
        public double HoursRun { get; set; }

        [Required]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Level. Please enter a value between 0 and 1.")]
        [Display(Name = "Fuel Level")]
        public double FuelLevel { get; set; }
         
        [StringLength(128)]
        [Display(Name = "Job Card Number")]
        public string JobCardNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Serviced By")]
        public string ServicedBy { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Generator Generator { get; set; }
    }
}
