namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Landlord")]
    public partial class Landlord
    { 
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Landlord()
        { 
            Sites = new HashSet<Site>();
        } 

        [Display(Name = "Province Number")]
        public int Id { get; set; }

        [StringLength(256)]
        [Index(IsUnique = true)]
        [Display(Name = "Landlord")]
        [Required]
        public string Name { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        [EmailAddress]
        public string Email { get; set; }
         
        [Phone]
        public string Phone { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Site> Sites { get; set; }
    }
} 
