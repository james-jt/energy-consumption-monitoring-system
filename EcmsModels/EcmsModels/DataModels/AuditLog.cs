namespace EcmsModels.DataModels
{
    using EcmsModels.ViewModels.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AuditLog")]
    public partial class AuditLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Actioner")]
        public string ActionerId { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "Entity Type")]
        public string EntityType { get; set; }

        [StringLength(128)]
        [Display(Name = "Entity Id")]
        public string EntityId { get; set; }

        [Display(Name = "Action Type")]
        public string ActionType { get; set; }

        [Display(Name = "Date Actioned")]
        public DateTime DateActioned { get; set; }
    }
}
