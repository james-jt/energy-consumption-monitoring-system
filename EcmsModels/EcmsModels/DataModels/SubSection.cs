namespace EcmsModels.DataModels
{
    using EcmsModels.WebModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SubSection")]
    public partial class SubSection
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubSection()
        {
            Users = new HashSet<ApplicationUser>();
            CouponsBatches = new HashSet<CouponsBatch>();
            SectionSubSectionAssignmentHistory = new HashSet<SectionSubSectionAssignmentHistory>();
            SubSectionOverseerAssignmentHistory = new HashSet<SubSectionOverseerAssignmentHistory>();
            SubSectionRegionAssignmentHistory = new HashSet<SubSectionRegionAssignmentHistory>();
            Vehicles = new HashSet<Vehicle>();
            SubZones = new HashSet<SubZone>();
            SubZoneSubSectionAssignmentHistory = new HashSet<SubZoneSubSectionAssignmentHistory>();
            UserSubSectionAssignmentHistory = new HashSet<UserSubSectionAssignmentHistory>();
            VehicleSubSectionDeploymentHistory = new HashSet<VehicleSubSectionDeploymentHistory>();
        }

        [Display(Name = "Sub-Section Number")]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        [Index("IX_SubSectionNameAndSectionId", 1, IsUnique = true)]
        [Display(Name = "Sub Section")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Overseer")]
        public string OverseerUserId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [Required]
        [Index("IX_SubSectionNameAndSectionId", 2, IsUnique = true)]
        public int SectionId { get; set; }

        [Required]
        [Display(Name = "Region")]
        public int RegionId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationUser> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CouponsBatch> CouponsBatches { get; set; }

        public virtual Region Region { get; set; }

        public virtual Section Section { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SectionSubSectionAssignmentHistory> SectionSubSectionAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubSectionOverseerAssignmentHistory> SubSectionOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubSectionRegionAssignmentHistory> SubSectionRegionAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubZone> SubZones { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubZoneSubSectionAssignmentHistory> SubZoneSubSectionAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserSubSectionAssignmentHistory> UserSubSectionAssignmentHistory { get; set; }
         
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleSubSectionDeploymentHistory> VehicleSubSectionDeploymentHistory { get; set; }
    }
}
