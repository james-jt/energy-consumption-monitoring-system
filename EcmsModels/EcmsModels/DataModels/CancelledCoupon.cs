namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CancelledCoupon")]
    public partial class CancelledCoupon
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Coupon Number")]
        public string Id { get; set; }

        [Display(Name = "Batch Number")]
        public string BatchId { get; set; }

        [Display(Name = "Date Cancelled")]
        public DateTime DateCancelled { get; set; }

        [StringLength(512)]
        [Display(Name = "Reason Cancelled")]
        public string Reason { get; set; }

        [StringLength(128)]
        [Display(Name = "Cancelled By")]
        public string CancelledBy { get; set; }

        [StringLength(512)]
        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public virtual CouponsBatch CouponsBatch { get; set; }
    }
}
