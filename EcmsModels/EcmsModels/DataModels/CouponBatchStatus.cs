namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CouponBatchStatus
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CouponBatchStatus()
        {
            CouponsBatches = new HashSet<CouponsBatch>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Batch Status")]
        public byte Id { get; set; }

        [StringLength(32)]
        [Display(Name = "Batch Status")]
        public string Description { get; set; }

        public bool Disabled { get; set; }

        [StringLength(32)]
        public string Notes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CouponsBatch> CouponsBatches { get; set; }
    }
}
