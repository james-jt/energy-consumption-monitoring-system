namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Section")]
    public partial class Section
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Section()
        {
            DepartmentSubSectionAssignmentHistory = new HashSet<DepartmentSubSectionAssignmentHistory>();
            SectionOverseerAssignmentHistory = new HashSet<SectionOverseerAssignmentHistory>();
            SubSections = new HashSet<SubSection>();
            SectionSubSectionAssignmentHistory = new HashSet<SectionSubSectionAssignmentHistory>();
        }

        [Display(Name = "Section Number")]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        [Index("IX_SectionNameAndDepartmentId", 1, IsUnique = true)]
        [Display(Name = "Section")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Overseer")]
        public string OverseerUserId { get; set; }

        public bool Disabled { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        [Required]
        [Index("IX_SectionNameAndDepartmentId", 2, IsUnique = true)]
        [Display(Name = "Department")]
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DepartmentSubSectionAssignmentHistory> DepartmentSubSectionAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SectionOverseerAssignmentHistory> SectionOverseerAssignmentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubSection> SubSections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SectionSubSectionAssignmentHistory> SectionSubSectionAssignmentHistory { get; set; }
    }
}
