namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GenHoursRunCheck")]
    public partial class GenHoursRunCheck
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Generator")]
        public string GeneratorId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Checked")]
        public DateTime DateChecked { get; set; }

        [Required]
        [Display(Name = "Hours Run")]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Quantity. Please enter a value greater than 0.")]
        public double HoursRun { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged By")]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Generator Generator { get; set; }
    }
}
