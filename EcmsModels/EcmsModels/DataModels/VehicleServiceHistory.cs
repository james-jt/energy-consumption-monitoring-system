namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleServiceHistory")]
    public partial class VehicleServiceHistory
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Vehicle")]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [Display(Name = "Date Serviced")]
        public DateTime DateServiced { get; set; }

        [Required]
        [Range(1, Int32.MaxValue, ErrorMessage = "Invalid Mileage. Please enter a value greater than 0.")]
        public Double Mileage { get; set; }


        [Required]
        [Range(0, Double.MaxValue, ErrorMessage = "Invalid Level. Please enter a value between 0 and 1.")]
        [Display(Name = "Fuel Level")]
        public double FuelLevel { get; set; }

        [StringLength(128)]
        [Display(Name = "Job Card Number")]
        public string JobCardNumber { get; set; }

        [StringLength(128)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "Serviced By")]
        public string ServicedBy { get; set; }

        [StringLength(128)]
        [Display(Name = "Logged by")]
        public string LoggedBy { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
