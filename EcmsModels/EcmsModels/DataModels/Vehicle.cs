namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vehicle")]
    public partial class Vehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicle()
        {
            VehicleFuelUps = new HashSet<FuelUpVehicle>();
            VehicleDepartmentDeploymentHistory = new HashSet<VehicleSubSectionDeploymentHistory>();
            VehicleFuelLevelChecks = new HashSet<VehicleFuelLevelCheck>();
            VehicleMileageChecks = new HashSet<VehicleMileageCheck>();
            VehicleServiceHistory = new HashSet<VehicleServiceHistory>();
            VehicleTrips = new HashSet<VehicleTrip>();
        }

        [Display(Name = "Vehicle Reg Number")]
        public string Id { get; set; }

        [Display(Name = "SubSection")]
        public int SubSectionId { get; set; }

        [NotMapped]
        [Display(Name = "Make")]
        public short MakeId { get; set; }

        [Display(Name = "Model")]
        public short ModelId { get; set; }

        [Display(Name = "Body Type")]
        public byte BodyTypeId { get; set; }

        [Display(Name = "Fuel")]
        public byte FuelTypeId { get; set; }

        [Display(Name = "Status")]
        public byte StatusId { get; set; }

        [Display(Name = "Year First Used As New")]
        public DateTime YearFirstUsedAsNew { get; set; }

        [Display(Name = "Engine Capacity")]
        public double EngineCapacity { get; set; }

        [Display(Name = "Fuel Tank Capacity")]
        public short FuelTankCapacity { get; set; }

        [Display(Name = "Initial Mileage")]
        public int StartMileage { get; set; }

        public virtual FuelType FuelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FuelUpVehicle> VehicleFuelUps { get; set; }

        public virtual SubSection SubSection { get; set; }

        public virtual VehicleBodyType VehicleBodyType { get; set; }

        public virtual VehicleModel VehicleModel { get; set; }

        public virtual VehicleStatus VehicleStatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleSubSectionDeploymentHistory> VehicleDepartmentDeploymentHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleFuelLevelCheck> VehicleFuelLevelChecks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleMileageCheck> VehicleMileageChecks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleServiceHistory> VehicleServiceHistory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VehicleTrip> VehicleTrips { get; set; }
    }
}
