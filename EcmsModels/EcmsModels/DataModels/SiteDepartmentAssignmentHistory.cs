namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SiteDepartmentAssignmentHistory")]
    public partial class SiteDepartmentAssignmentHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SiteId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MaintenanceCentreId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime DateAssigned { get; set; }

        [StringLength(128)]
        public string AssignedBy { get; set; }

        public DateTime DateRecalled { get; set; }

        [StringLength(128)]
        public string RecalledBy { get; set; }

        public virtual MaintenanceCentre MaintenanceCentre { get; set; }

        public virtual Site Site { get; set; }
    }
}
