namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VehicleSubSectionDeploymentHistory")]
    public partial class VehicleSubSectionDeploymentHistory
    {
        [Key]
        [Column(Order = 0)]
        [Display(Name = "Vehicle Registration Number")]
        public string VehicleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Sub Section")]
        public int SubSectionId { get; set; }

        [Key]
        [Column(Order = 2)]
        [Display(Name = "Date Deployed")]
        public DateTime DateDeployed { get; set; }

        [StringLength(128)]
        [Display(Name = "Deployed By")]
        public string DeployedBy { get; set; }

        [Display(Name = "Date Recalled")]
        public DateTime DateRecalled { get; set; }

        [StringLength(128)]
        [Display(Name = "Recalled By")]
        public string RecalledBy { get; set; }

        public virtual SubSection SubSection { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
