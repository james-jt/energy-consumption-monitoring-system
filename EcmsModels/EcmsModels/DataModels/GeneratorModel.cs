namespace EcmsModels.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GeneratorModel")]
    public partial class GeneratorModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GeneratorModel()
        {
            Generators = new HashSet<Generator>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [StringLength(128)]
        [Index(IsUnique = true)]
        [Display(Name = "Model")]
        public string Name { get; set; }

        [StringLength(128)]
        [Display(Name = "Engine Make")]
        public string EngineMake { get; set; }

        [StringLength(128)]
        [Display(Name = "Engine Model")]
        public string EngineModel { get; set; }

        [StringLength(128)]
        [Display(Name = "Generator Make")]
        public string GeneratorMake { get; set; }

        [Display(Name = "Service Interval")]
        public int ServiceInterval { get; set; }

        [Display(Name = "Size")]
        public double GeneratorSize { get; set; }

        [Display(Name = "Normal Lifespan")]
        public int? NormalLifeSpan { get; set; }

        [Display(Name = "Normal Consumption Rate")]
        public double? NormalConsuptionRate { get; set; }

        public bool Disabled { get; set; }

        [Display(Name = "Fuel Type")]
        public byte FuelTypeId { get; set; }

        public virtual FuelType FuelType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Generator> Generators { get; set; }
    }
}
