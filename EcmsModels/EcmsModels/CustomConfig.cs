﻿using EcmsModels.DataModels;
using EcmsModels.DataModels.Extensions;
using EcmsModels.ViewModels;
using EcmsModels.WebModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.DataModels
{ 
    public partial class CustomConfig
    {
        public static void SetupDbEnvironment()
        {
            Setup();
        }

        private static void Setup()
        {
            var context = new EcmsDbContext();
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var defaultRegion = Region.GetDefault();
            if (defaultRegion == null)
            {
                defaultRegion = Region.CreateDefault();
                context.Region.Add(defaultRegion);
                context.SaveChanges();
            }

            var defaultDepartment = Department.GetDefault();
            if (defaultDepartment == null)
            {
                defaultDepartment = Department.CreateDefault();
                defaultDepartment.Disabled = false;
                context.Department.Add(defaultDepartment);
                context.SaveChanges();
            }

            var defaultSection = Section.GetDefault(defaultDepartment);
            if (defaultSection == null)
            {
                defaultSection = Section.CreateDefault(defaultDepartment);
                defaultSection.Disabled = false;
                context.Section.Add(defaultSection);
                context.SaveChanges();
            }

            var defaultSubSection = SubSection.GetDefault(defaultSection);
            if (defaultSubSection == null)
            {
                defaultSubSection = SubSection.CreateDefault(defaultSection);
                defaultSubSection.Disabled = false;
                defaultSubSection.Region = defaultRegion;
                defaultSubSection.RegionId = new Region().GetByName(DefaultValues.DefaultDbEntityName).Id;
                context.SubSection.Add(defaultSubSection);
                context.SaveChanges();
            }

            var defaultAdmin = ApplicationUser.CreateDefault(UserManager);

            CreateRoles(RoleManager);
            AddToRoles(UserManager, defaultAdmin.Email);
            AddClaims(UserManager, defaultAdmin);

            defaultRegion.OverseerUserId = defaultDepartment.OverseerUserId = defaultSection.OverseerUserId = defaultSection.OverseerUserId = defaultSubSection.OverseerUserId = ApplicationUser.GetDefault().Id;

            var defaultSubZone = SubZone.GetDefault(defaultSubSection);
            if (defaultSubZone == null)
            {
                defaultSubZone = SubZone.CreateDefault(defaultSubSection);
                defaultSubZone.Disabled = false;
                context.SubZone.Add(defaultSubZone);
                context.SaveChanges();
            }

            var defaultMaintenanceCentre = MaintenanceCentre.GetDefault(defaultSubZone);
            if (defaultMaintenanceCentre == null)
            {
                defaultMaintenanceCentre = MaintenanceCentre.CreateDefault(defaultSubZone);
                defaultMaintenanceCentre.Disabled = false;
                context.MaintenanceCentre.Add(defaultMaintenanceCentre);
                context.SaveChanges();
            }

            //var defaultSite = Site.GetDefault(defaultMaintenanceCentre);
            //if (defaultSite == null)
            //{
            //    defaultSite = Site.CreateDefault(defaultMaintenanceCentre);
            //    context.Site.Add(defaultSite);
            //    context.SaveChanges();
            //}

            DbSeed(context);
        }

        private static void CreateRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExists(DefaultValues.DefaultAdministratorRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultAdministratorRole;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(DefaultValues.DefaultDepartmentOverseerRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultDepartmentOverseerRole;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(DefaultValues.DefaultSectionOverseerRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultSectionOverseerRole;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(DefaultValues.DefaultSubSectionOverseerRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultSubSectionOverseerRole;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(DefaultValues.DefaultSubZoneOverseerRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultSubZoneOverseerRole;
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists(DefaultValues.DefaultMaintenanceCentreOverseerRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultMaintenanceCentreOverseerRole;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(DefaultValues.DefaultUserRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultUserRole;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(DefaultValues.DefaultCouponStoresControllerRole))
            {
                var role = new IdentityRole();
                role.Name = DefaultValues.DefaultCouponStoresControllerRole;
                roleManager.Create(role);
            }

        }

        private static void AddToRoles(UserManager<ApplicationUser> UserManager, string defaultAdminEmail)
        {
            var user = UserManager.FindByEmail(defaultAdminEmail);
            if (user != null)
            {
                if (!UserManager.IsInRole(user.Id, DefaultValues.DefaultAdministratorRole))
                {
                    UserManager.AddToRole(user.Id.ToString(), DefaultValues.DefaultAdministratorRole);
                }

                if (!UserManager.IsInRole(user.Id, DefaultValues.DefaultUserRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultUserRole);
                }
                if (!UserManager.IsInRole(user.Id.ToString(), DefaultValues.DefaultMaintenanceCentreOverseerRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultMaintenanceCentreOverseerRole);
                }
                if (!UserManager.IsInRole(user.Id.ToString(), DefaultValues.DefaultSubZoneOverseerRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultSubZoneOverseerRole);
                }
                if (!UserManager.IsInRole(user.Id.ToString(), DefaultValues.DefaultSubSectionOverseerRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultSubSectionOverseerRole);
                }
                if (!UserManager.IsInRole(user.Id.ToString(), DefaultValues.DefaultSectionOverseerRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultSectionOverseerRole);
                }
                if (!UserManager.IsInRole(user.Id.ToString(), DefaultValues.DefaultDepartmentOverseerRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultDepartmentOverseerRole);
                }
                if (!UserManager.IsInRole(user.Id.ToString(), DefaultValues.DefaultCouponStoresControllerRole))
                {
                    UserManager.AddToRole<ApplicationUser, string>(user.Id.ToString(), DefaultValues.DefaultCouponStoresControllerRole);
                }

            }
        }

        private static void AddClaims(UserManager<ApplicationUser> UserManager, ApplicationUser user)
        {
            if (UserManager.GetClaims(user.Id).FirstOrDefault(e => e.Type == "FullName") == null)
                UserManager.AddClaim(user.Id, new Claim("FullName", user.FullName));
            if (UserManager.GetClaims(user.Id).FirstOrDefault(e => e.Type == "Department") == null)
                UserManager.AddClaim(user.Id, new Claim("Department", user.SubSectionId.ToString()));
            if (UserManager.GetClaims(user.Id).FirstOrDefault(e => e.Type == "Section") == null)
                UserManager.AddClaim(user.Id, new Claim("Section", user.SubSectionId.ToString()));
            if (UserManager.GetClaims(user.Id).FirstOrDefault(e => e.Type == "SubSection") == null)
                UserManager.AddClaim(user.Id, new Claim("SubSection", user.SubSectionId.ToString()));
        }

        private static void DbSeed(EcmsModels.DataModels.EcmsDbContext context)
        {
            var BulawayoProvince = new Province() { Name = "Bulawayo" };
            var HarareProvince = new Province() { Name = "Harare" };
            var MasvingoProvince = new Province() { Name = "Masvingo" };
            var MidlandsProvince = new Province() { Name = "Midlands" };
            var ManicalandProvince = new Province() { Name = "Manicaland" };
            var MatNorthProvince = new Province() { Name = "Matabeleland North" };
            var MatSouthProvince = new Province() { Name = "Matabeleland South" };
            var MashEastProvince = new Province() { Name = "Mashonaland East" };
            var MashWestProvince = new Province() { Name = "Mashonaland West" };
            var MashCentralProvince = new Province() { Name = "Mashonaland Central" };
            context.Province.AddRange(
                new List<Province>()
                {
                    BulawayoProvince,
                    HarareProvince,
                    MasvingoProvince,
                    MidlandsProvince,
                    ManicalandProvince,
                    MatNorthProvince,
                    MatSouthProvince,
                    MashEastProvince,
                    MashWestProvince,
                    MashCentralProvince,
                }
            );
            context.SaveChanges();

            var PlatinumSegment = new SiteSegment() { Name = "Platinum" };
            var DiamondSegment = new SiteSegment() { Name = "Diamond" };
            var GoldSegment = new SiteSegment() { Name = "Gold" };
            var SilverSegment = new SiteSegment() { Name = "Silver" };
            var BronzeSegment = new SiteSegment() { Name = "Bronze" };
            context.Segment.AddRange(
                new List<SiteSegment>()
                {
                    PlatinumSegment,
                    DiamondSegment,
                    GoldSegment,
                    SilverSegment,
                    BronzeSegment
                });
            context.SaveChanges();

            var DefaultLandlord = new Landlord() { Name = "MyCompany" };
            var NUSTLandlord = new Landlord() { Name = "NUST" };
            var GirlsHighLandlord = new Landlord() { Name = "Girls' High School" };
            context.Landlord.Add(DefaultLandlord);
            context.Landlord.Add(NUSTLandlord);
            context.Landlord.Add(GirlsHighLandlord);
            context.SaveChanges();

            var petrol = new FuelType() { Name = "Petrol" };
            var diesel = new FuelType() { Name = "Diesel" };
            context.FuelType.AddOrUpdate(petrol, diesel);

            var warehouse = new FuelSourceType() { Name = DefaultValues.WarehouseFuelSourceTypeName };
            var tpGarage = new FuelSourceType() { Name = DefaultValues.TpGarageFuelSourceTypeName };
            var coupons = new FuelSourceType() { Name = DefaultValues.CouponsFuelSourceTypeName };
            context.FuelSourceType.AddOrUpdate(warehouse, tpGarage, coupons);

            var singleCabTruck = new VehicleBodyType() { Name = "Single-cab Truck" };
            var clubCabTruck = new VehicleBodyType() { Name = "Club-cab Truck" };
            var doubleCabTruck = new VehicleBodyType() { Name = "Double-cab Truck" };
            var hatchback = new VehicleBodyType() { Name = "Hatchback" };
            var sedan = new VehicleBodyType() { Name = "Sedan" };
            var van = new VehicleBodyType() { Name = "Van" };
            var minibus = new VehicleBodyType() { Name = "Mini Bus" };
            var bus = new VehicleBodyType() { Name = "Bus" };
            var suv = new VehicleBodyType() { Name = "SUV" };
            var coupe = new VehicleBodyType() { Name = "Coupe" };
            context.VehicleBodyType.AddOrUpdate(singleCabTruck, clubCabTruck, doubleCabTruck, hatchback, sedan, van, minibus, bus, suv, coupe);

            var mazda = new VehicleMake() { Name = "Mazda" };
            var nissan = new VehicleMake() { Name = "Nissan" };
            var toyota = new VehicleMake() { Name = "Toyota" };
            var mahindra = new VehicleMake() { Name = "Mahindra" };
            var foton = new VehicleMake() { Name = "Foton" };
            var isuzu = new VehicleMake() { Name = "Isuzu" };
            var benz = new VehicleMake() { Name = "Mercedes Benz" };
            var volvo = new VehicleMake() { Name = "Volvo" };
            var audi = new VehicleMake() { Name = "Audi" };
            var datsun = new VehicleMake() { Name = "Datsun" };
            var suzuki = new VehicleMake() { Name = "Suzuki" };
            var vw = new VehicleMake() { Name = "VW" };
            var hyundai = new VehicleMake() { Name = "Hyundai" };
            var honda = new VehicleMake() { Name = "Honda" };
            context.VehicleMake.AddOrUpdate(
                mazda,
                nissan,
                toyota,
                mahindra,
                foton,
                isuzu,
                benz,
                volvo,
                audi,
                datsun,
                suzuki,
                vw,
                hyundai,
                honda
                );


            var commercialPowerStatus = new CommercialPowerStatus() { Description = "No Connection" };
            var commercialPowerStatus1 = new CommercialPowerStatus() { Description = "Connected" };
            var commercialPowerStatus2 = new CommercialPowerStatus() { Description = "Faulty" };
            context.CommercialPowerStatus.AddOrUpdate(commercialPowerStatus, commercialPowerStatus1, commercialPowerStatus2);

            var vehicleStatus = new VehicleStatus() { Description = "Out of Order" };
            var vehicleStatus1 = new VehicleStatus() { Description = "Decommissioned" };
            var vehicleStatus2 = new VehicleStatus() { Description = "Intact" };
            context.VehicleStatus.AddOrUpdate(vehicleStatus, vehicleStatus1, vehicleStatus2);

            var generatorStatus = new GeneratorStatus() { Description = "Out of Order" };
            var generatorStatus1 = new GeneratorStatus() { Description = "Decommissioned" };
            var generatorStatus2 = new GeneratorStatus() { Description = "Intact" };
            context.GeneratorStatus.AddOrUpdate(generatorStatus, generatorStatus1, generatorStatus2);

            var meterStatus = new ElectricityMeterStatus() { Description = "Out of Order" };
            var meterStatus1 = new ElectricityMeterStatus() { Description = "Decommissioned" };
            var meterStatus2 = new ElectricityMeterStatus() { Description = "Intact" };
            context.ElectricityMeterStatus.AddOrUpdate(meterStatus, meterStatus1, meterStatus2);

            var fuelSourceStatus = new FuelSourceStatus() { Description = "Out of Order" };
            var fuelSourceStatus1 = new FuelSourceStatus() { Description = "Decommissioned" };
            var fuelSourceStatus2 = new FuelSourceStatus() { Description = "Intact" };
            context.FuelSourceStatus.AddOrUpdate(fuelSourceStatus, fuelSourceStatus1, fuelSourceStatus2);

            context.SaveChanges();

            var cleveland = new FuelSource() { Name = "Main Depot" };
            var byoDepot = new FuelSource() { Name = "Bulawayo Depot" };
            var clevelandStatus = context.FuelSourceStatus.Where(s => s.Description == "Intact").FirstOrDefault();
            var clevelandSourceType = context.FuelSourceType.FirstOrDefault(s => s.Name == DefaultValues.WarehouseFuelSourceTypeName);
            byoDepot.FuelSourceStatus = cleveland.FuelSourceStatus = clevelandStatus;
            byoDepot.StatusId = cleveland.StatusId = clevelandStatus.Id;
            byoDepot.FuelSourceType = cleveland.FuelSourceType = clevelandSourceType;
            byoDepot.FuelSourceTypeId = cleveland.FuelSourceTypeId = clevelandSourceType.Id;
            if (clevelandStatus.FuelSources.Where(f => f.Name == cleveland.Name).Count() == 0)
                clevelandStatus.FuelSources.Add(cleveland);
            if (clevelandStatus.FuelSources.Where(f => f.Name == byoDepot.Name).Count() == 0)
                clevelandStatus.FuelSources.Add(byoDepot);

            if (clevelandSourceType.FuelSources.Where(f => f.Name == cleveland.Name).Count() == 0)
                clevelandSourceType.FuelSources.Add(cleveland);
            if (clevelandSourceType.FuelSources.Where(f => f.Name == byoDepot.Name).Count() == 0)
                clevelandSourceType.FuelSources.Add(byoDepot);


            var puma = new FuelSource() { Name = "Puma" };
            var redan = new FuelSource() { Name = "Redan" };
            var pumaStatus = context.FuelSourceStatus.Where(s => s.Description == "Intact").FirstOrDefault();
            var pumaSourceType = context.FuelSourceType.FirstOrDefault(s => s.Name == DefaultValues.TpGarageFuelSourceTypeName);
            redan.FuelSourceStatus = puma.FuelSourceStatus = pumaStatus;
            redan.StatusId = puma.StatusId = pumaStatus.Id;
            redan.FuelSourceType = puma.FuelSourceType = pumaSourceType;
            redan.FuelSourceTypeId = puma.FuelSourceTypeId = pumaSourceType.Id;

            if (pumaStatus.FuelSources.Where(f => f.Name == puma.Name).Count() == 0)
                pumaStatus.FuelSources.Add(puma);
            if (pumaStatus.FuelSources.Where(f => f.Name == redan.Name).Count() == 0)
                pumaStatus.FuelSources.Add(redan);

            if (pumaSourceType.FuelSources.Where(f => f.Name == puma.Name).Count() == 0)
                pumaSourceType.FuelSources.Add(puma);
            if (pumaSourceType.FuelSources.Where(f => f.Name == redan.Name).Count() == 0)
                pumaSourceType.FuelSources.Add(redan);

            var coupns = new FuelSource() { Name = "Coupons" };
            var coupnsStatus = context.FuelSourceStatus.Where(s => s.Description == "Intact").FirstOrDefault();
            var coupnsSourceType = context.FuelSourceType.FirstOrDefault(s => s.Name == DefaultValues.CouponsFuelSourceTypeName);
            coupns.FuelSourceStatus = coupnsStatus;
            coupns.StatusId = coupnsStatus.Id;
            coupns.FuelSourceType = coupnsSourceType;
            coupns.FuelSourceTypeId = coupnsSourceType.Id;

            if (coupnsStatus.FuelSources.Where(f => f.Name == coupns.Name).Count() == 0)
                coupnsStatus.FuelSources.Add(coupns);

            if (coupnsSourceType.FuelSources.Where(f => f.Name == coupns.Name).Count() == 0)
                coupnsSourceType.FuelSources.Add(coupns);

            context.FuelSource.AddOrUpdate(cleveland, byoDepot, puma, redan, coupns);
            context.SaveChanges();

            var NP200 = new VehicleModel() { Name = "NP200" };
            var makeNissan = context.VehicleMake.Where(m => m.Name == "Nissan").FirstOrDefault();
            NP200.VehicleMake = makeNissan;
            NP200.VehicleMakeId = makeNissan.Id;
            NP200.YearOfManufacture = DateTime.Now;
            if (makeNissan.VehicleModels.Where(m => m.Name == NP200.Name).Count() == 0)
                makeNissan.VehicleModels.Add(NP200);

            var hilux = new VehicleModel() { Name = "Hilux" };
            var makeToyota = context.VehicleMake.Where(m => m.Name == "Toyota").FirstOrDefault();
            hilux.VehicleMake = makeToyota;
            hilux.VehicleMakeId = makeToyota.Id;
            hilux.YearOfManufacture = DateTime.Now;
            if (makeToyota.VehicleModels.Where(m => m.Name == hilux.Name).Count() == 0)
                makeToyota.VehicleModels.Add(hilux);

            context.VehicleModel.AddOrUpdate(NP200, hilux);
            context.SaveChanges();

            var department = Department.GetDefault();
            var section = Section.GetDefault(department);
            var subSection = SubSection.GetDefault(section);
            var subZone = SubZone.GetDefault(subSection);
            var maintenanceCentre = MaintenanceCentre.GetDefault(subZone);

            var statusNissan = context.VehicleStatus.Where(s => s.Description == "Intact").FirstOrDefault();

            var vehicleNissan = new Vehicle() { Id = "ABP 0220" };
            var modelNP200 = context.VehicleModel.Where(m => m.Name == "NP200").FirstOrDefault();
            var bodyNissan = context.VehicleBodyType.Where(b => b.Name == "Single-cab Truck").FirstOrDefault();
            var fuelNissan = context.FuelType.Where(f => f.Name == "Petrol").FirstOrDefault();
            vehicleNissan.BodyTypeId = bodyNissan.Id;
            vehicleNissan.ModelId = modelNP200.Id;
            vehicleNissan.FuelTypeId = fuelNissan.Id;
            vehicleNissan.SubSectionId = subSection.Id;
            vehicleNissan.EngineCapacity = 1400;
            vehicleNissan.FuelTankCapacity = 50;
            vehicleNissan.StartMileage = 200;
            vehicleNissan.StatusId = statusNissan.Id;
            vehicleNissan.YearFirstUsedAsNew = DateTime.Now;


            var vehicleToyota = new Vehicle() { Id = "ADR 3055" };
            var modelHilux = context.VehicleModel.Where(m => m.Name == "Hilux").FirstOrDefault();
            var bodyToyota = context.VehicleBodyType.Where(b => b.Name == "Double-cab Truck").FirstOrDefault();
            var fuelToyota = context.FuelType.Where(f => f.Name == "Diesel").FirstOrDefault();
            vehicleToyota.BodyTypeId = bodyToyota.Id;
            vehicleToyota.ModelId = hilux.Id;
            vehicleToyota.FuelTypeId = fuelToyota.Id;
            vehicleToyota.SubSectionId = subSection.Id;
            vehicleToyota.EngineCapacity = 2500;
            vehicleToyota.FuelTankCapacity = 80;
            vehicleToyota.StartMileage = 1357;
            vehicleToyota.StatusId = statusNissan.Id;
            vehicleToyota.YearFirstUsedAsNew = DateTime.Now;

            context.Vehicle.AddOrUpdate(vehicleNissan, vehicleToyota);
            context.SaveChanges();

            var vehicleToyotaDeploymentHistory = new VehicleSubSectionDeploymentHistory();
            var changedBy = ApplicationUser.GetDefault().Id;
            var dateChanged = DateTime.Today.GetCondensedDateTime();
            vehicleToyotaDeploymentHistory.SubSectionId = vehicleToyota.SubSectionId;
            vehicleToyotaDeploymentHistory.VehicleId = vehicleToyota.Id;
            vehicleToyotaDeploymentHistory.DateDeployed = dateChanged;
            vehicleToyotaDeploymentHistory.DeployedBy = changedBy;
            vehicleToyotaDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
            context.VehicleSubSectionDeploymentHistory.Add(vehicleToyotaDeploymentHistory);

            var vehicleDeploymentHistory = new VehicleSubSectionDeploymentHistory();
            vehicleDeploymentHistory.SubSectionId = vehicleNissan.SubSectionId;
            vehicleDeploymentHistory.VehicleId = vehicleNissan.Id;
            vehicleDeploymentHistory.DateDeployed = dateChanged;
            vehicleDeploymentHistory.DeployedBy = changedBy;
            vehicleDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
            context.VehicleSubSectionDeploymentHistory.Add(vehicleDeploymentHistory);

            context.SaveChanges();

            var powerStatus = context.CommercialPowerStatus.Where(s => s.Description == "Connected").FirstOrDefault();
            var electricityMeterStatus = context.ElectricityMeterStatus.Where(s => s.Description == "Intact").FirstOrDefault();

            var girlsHighSite = new Site() { Name = "Girl's High Harare" };
            girlsHighSite.CommercialPowerStatusId = powerStatus.Id;
            girlsHighSite.CommercialPowerAvailable = true;
            girlsHighSite.MaintenanceCentreId = maintenanceCentre.Id;
            girlsHighSite.Latitude = "102.2568";
            girlsHighSite.Longitude = "47.5698";
            girlsHighSite.Location = "Harare Girl's High School";
            girlsHighSite.SegmentId = PlatinumSegment.Id;
            girlsHighSite.ProvinceId = HarareProvince.Id;
            girlsHighSite.LandlordId = GirlsHighLandlord.Id;

            var nustSite = new Site() { Name = "NUST Bulawayo" };
            nustSite.CommercialPowerStatusId = powerStatus.Id;
            nustSite.CommercialPowerAvailable = true;
            nustSite.MaintenanceCentreId = maintenanceCentre.Id;
            nustSite.Latitude = "142.4245";
            nustSite.Longitude = "76.2097";
            nustSite.Location = "NUST Grounds, Behind Ceremonial Hall";
            nustSite.SegmentId = GoldSegment.Id;
            nustSite.ProvinceId = BulawayoProvince.Id;
            nustSite.LandlordId = NUSTLandlord.Id;

            context.Site.AddOrUpdate(girlsHighSite, nustSite);
            context.SaveChanges();

            var girlsHighMeter = new ElectrictyMeter() { Id = "12345678901" };
            girlsHighMeter.AccountNumber = "012345678901";
            girlsHighMeter.DateDeployed = DateTime.Now;
            girlsHighMeter.DeployedBy = department.OverseerUserId;
            girlsHighMeter.SiteId = girlsHighSite.Id;
            girlsHighMeter.MeterStatusId = electricityMeterStatus.Id;
            girlsHighMeter.Type = ViewModels.Enums.MeterTypes.Prepaid;

            var nustMeter = new ElectrictyMeter() { Id = "10987654321" };
            nustMeter.AccountNumber = "010987654321";
            nustMeter.DateDeployed = DateTime.Now;
            nustMeter.DeployedBy = department.OverseerUserId;
            nustMeter.SiteId = nustSite.Id;
            nustMeter.MeterStatusId = electricityMeterStatus.Id;
            nustMeter.Type = ViewModels.Enums.MeterTypes.Prepaid;

            context.ElectrictyMeter.AddOrUpdate(girlsHighMeter, nustMeter);
            context.SaveChanges();

            var nustSiteElectricityMeterAssignmentHistory = new SiteElectricityMeterAssignmentHistory();
            nustSiteElectricityMeterAssignmentHistory.MeterId = nustMeter.Id;
            nustSiteElectricityMeterAssignmentHistory.SiteId = nustMeter.SiteId;
            nustSiteElectricityMeterAssignmentHistory.AssignedBy = ApplicationUser.GetDefault().Id;
            nustSiteElectricityMeterAssignmentHistory.DateAssigned = DateTime.Now.GetCondensedDateTime();
            nustSiteElectricityMeterAssignmentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
            context.SiteElectricityMeterAssignmentHistory.Add(nustSiteElectricityMeterAssignmentHistory);

            var siteElectricityMeterAssignmentHistory = new SiteElectricityMeterAssignmentHistory();
            siteElectricityMeterAssignmentHistory.MeterId = girlsHighMeter.Id;
            siteElectricityMeterAssignmentHistory.SiteId = girlsHighMeter.SiteId;
            siteElectricityMeterAssignmentHistory.AssignedBy = ApplicationUser.GetDefault().Id;
            siteElectricityMeterAssignmentHistory.DateAssigned = DateTime.Now.GetCondensedDateTime();
            siteElectricityMeterAssignmentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
            context.SiteElectricityMeterAssignmentHistory.Add(siteElectricityMeterAssignmentHistory);

            context.SaveChanges();

            var genStatus = context.GeneratorStatus.Where(s => s.Description == "Intact").FirstOrDefault();

            var perkinsModel = new GeneratorModel() { Name = "Huawei-Perkins 20" };
            perkinsModel.EngineMake = "Perkins";
            perkinsModel.EngineModel = "KD4567TY";
            perkinsModel.FuelTypeId = diesel.Id;
            perkinsModel.NormalConsuptionRate = 23;
            perkinsModel.NormalLifeSpan = 10;
            perkinsModel.ServiceInterval = 5000;

            var cumminsModel = new GeneratorModel() { Name = "Cummins-Cummins 17.5" };
            cumminsModel.EngineMake = "Cummins";
            cumminsModel.EngineModel = "XZP1278";
            cumminsModel.FuelTypeId = diesel.Id;
            cumminsModel.NormalConsuptionRate = 15;
            cumminsModel.NormalLifeSpan = 15;
            cumminsModel.ServiceInterval = 10000;

            context.GeneratorModel.AddOrUpdate(perkinsModel, cumminsModel);
            context.SaveChanges();

            var girlsHighGenerator = new Generator() { Id = "GN71134R01060A" };
            var girlsHighGeneratorModel = context.GeneratorModel.Where(m => m.Name == perkinsModel.Name).FirstOrDefault();
            girlsHighGenerator.FuelCapacity = 1000;
            girlsHighGenerator.ModelId = girlsHighGeneratorModel.Id;
            girlsHighGenerator.GeneratorStatusId = genStatus.Id;
            girlsHighGenerator.SiteId = girlsHighSite.Id;
            girlsHighGenerator.Supplier = "Local Dealer";
            girlsHighGenerator.YearFirstUsedAsNew = DateTime.Now;

            var girlsHighGeneratorDeploymentHistory = new GeneratorDeploymentHistory();
            girlsHighGeneratorDeploymentHistory.SiteId = girlsHighGenerator.SiteId;
            girlsHighGeneratorDeploymentHistory.GeneratorId = girlsHighGenerator.Id;
            girlsHighGeneratorDeploymentHistory.DateDeployed = dateChanged;
            girlsHighGeneratorDeploymentHistory.DeployedBy = changedBy;
            girlsHighGeneratorDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
            context.GeneratorDeploymentHistory.Add(girlsHighGeneratorDeploymentHistory);

            var nustGenerator = new Generator() { Id = "C22D5TL1411200052" };
            var nustGeneratorModel = context.GeneratorModel.Where(m => m.Name == cumminsModel.Name).FirstOrDefault();
            nustGenerator.FuelCapacity = 500;
            nustGenerator.ModelId = nustGeneratorModel.Id;
            nustGenerator.GeneratorStatusId = genStatus.Id;
            nustGenerator.SiteId = nustSite.Id;
            nustGenerator.Supplier = "Foreign Dealer";
            nustGenerator.YearFirstUsedAsNew = DateTime.Now;

            var nustGeneratorDeploymentHistory = new GeneratorDeploymentHistory();
            nustGeneratorDeploymentHistory.SiteId = nustGenerator.SiteId;
            nustGeneratorDeploymentHistory.GeneratorId = nustGenerator.Id;
            nustGeneratorDeploymentHistory.DateDeployed = dateChanged;
            nustGeneratorDeploymentHistory.DeployedBy = changedBy;
            nustGeneratorDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
            context.GeneratorDeploymentHistory.Add(nustGeneratorDeploymentHistory);

            context.Generator.AddOrUpdate(girlsHighGenerator, nustGenerator);
            context.SaveChanges();

            var RedanCouponIssuer = new CouponIssuer() { Name = "Redan" };
            var SakundaCouponIssuer = new CouponIssuer() { Name = "Sakunda" };
            context.CouponIssuer.AddRange(
                new List<CouponIssuer>()
                {
                    RedanCouponIssuer,
                    SakundaCouponIssuer
                });
            context.SaveChanges();

            var couponsStatus = new CouponBatchStatus() { Description = "Normal" };
            var couponsStatus1 = new CouponBatchStatus() { Description = "Unbudgeted" };
            context.CouponBatchStatus.AddOrUpdate(couponsStatus, couponsStatus1);

            context.SaveChanges();

        }
    }
}
