﻿using EcmsModels.WebModels;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcmsModels.DataModels;

namespace EcmsModels.DataModels.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Replaces any date before 01.01.1753 with a Nullable of 
        /// DateTime with a value of null.
        /// </summary>
        /// <param name="date">Date to check</param>
        /// <returns>Input date if valid in the DB, or Null if date is 
        /// too early to be DB compatible.</returns>
        public static DateTime ConvertToDbMinDate(this DateTime date)
        {
            return (date >= (DateTime)SqlDateTime.MinValue) ? date : (DateTime)SqlDateTime.MinValue;
        }

        public static DateTime GetCondensedDateTime(this DateTime VerboseDateTime)
        {
            var condensedDateTime = VerboseDateTime.Date;
            condensedDateTime = condensedDateTime.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
            return condensedDateTime;
        }

    }
}
