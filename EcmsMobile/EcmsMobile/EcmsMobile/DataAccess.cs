﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using EcmsModels.ViewModels;
using System.IO;

namespace EcmsMobile
{
    class DataAccess
    {
        //private const string baseUrl = @"https://ecmswebservice.azurewebsites.net/";
        private const string baseUrl = @"http://197.221.249.19/EcmsWebService/";
        //private const string baseUrl = @"http://192.168.43.86/EcmsWebService/";
        //private const string baseUrl = @"http://127.0.0.1/EcmsWebService/";
        //private const string baseUrl = @"http://192.168.1.3/EcmsWebService/";
         
        public static async Task<List<string>> GetVehicleRegistrationNumbers(string subSectionId)
        {
            return await GetData<List<string>>($"api/Vehicle/RegistrationNumbers/{subSectionId}");
        }

        public static async Task<Dictionary<string, string>> GetDepartments()
        {
            return await GetData<Dictionary<string, string>>($"api/Department/Names");
        }

        public static async Task<Dictionary<string, string>> GetSections(string departmentId)
        {
            return await GetData<Dictionary<string, string>>($"api/Section/Names/{departmentId}");
        }
         
        public static async Task<Dictionary<string, string>> GetSubSections(string sectionId)
        {
            return await GetData<Dictionary<string, string>>($"api/SubSection/Names/{sectionId}");
        }

        public static async Task<Dictionary<string, string>> GetZones(string sectionId)
        {
            return await GetData<Dictionary<string, string>>($"api/SubSection/Zones/{sectionId}");
        }

        public static async Task<Dictionary<string, string>> GetSubZones(string zoneId)
        {
            return await GetData<Dictionary<string, string>>($"api/SubZone/Names/{zoneId}");
        }

        public static async Task<Dictionary<string, string>> GetMaintenanceCentres(string subZoneId)
        {
            return await GetData<Dictionary<string, string>>($"api/MaintenanceCentre/Names/{subZoneId}");
        }

        public static async Task<List<SiteViewModel>> GetSites(string maintenanceCentreId)
        {
            return await GetData<List<SiteViewModel>>($"api/Site/Names/{maintenanceCentreId}");
        }

        public static async Task<Dictionary<string, string>> GetWarehouseNames()
        {
            return await GetData<Dictionary<string, string>>($"api/FuelSource/Warehouses");
        }

        public static async Task<Dictionary<string, string>> GetTpGarageNames()
        {
            return await GetData<Dictionary<string, string>>($"api/FuelSource/TpGarages");
        }

        public static async Task<Dictionary<string, string>> GetFuelTypes()
        {
            return await GetData<Dictionary<string, string>>($"api/Request/FuelTypes");
        }

        public static async Task<List<DrawdownViewModel>> GetAuthorisedFuelRequestsForUserWithDrawdownCapacity(string username)
        { 
            string url = $"api/Vehicle/FuelUp/{username}";
            return await GetData<List<DrawdownViewModel>>(url);
        }

        public static async Task<List<DrawdownViewModel>> GetDrawdownsForUserWithDrawdownCapacity(string username)
        { 
            string url = $"api/Site/FuelUp/{username}";
            return await GetData<List<DrawdownViewModel>>(url);
        }

        public static async Task<List<DrawdownViewModel>> GetAuthorisedFuelRequestsForUserPendingDrawdown(string username)
        {
            string url = $"api/Request/FuelDrawdown/{username}";
            return await GetData<List<DrawdownViewModel>>(url);
        }

        public static async Task<List<RechargeMeterViewModel>> GetAuthorisedElectricityRequestsForUserPendingRecharge(string username)
        {
            string url = $"api/Request/ElectricityRecharge/{username}";
            return await GetData< List<RechargeMeterViewModel>>(url);
        }

        public static async Task<List<RequestViewModel>> GetFuelRequestsForAuthorisationByUser(string username)
        {
            string url = $"api/Request/FuelAuthorisation/{username}";
            return await GetData<List<RequestViewModel>>(url);
        }

        public static async Task<List<RequestViewModel>> GetElectricityRequestsForAuthorisationByUser(string username)
        {  
            string url = $"api/Request/ElectricityAuthorisation/{username}";
            return await GetData<List<RequestViewModel>>(url);
        }

        public static async Task<List<string>> GetNextRequestNumber(bool isFuelRequest)
        {
            return await GetData<List<string>>($"api/Request/NextRequestNumber/{isFuelRequest}");
        }

        public static async Task<string> GetPasswordHash(string username)
        {
            var result = await GetData<List<string>>($"api/Account/PasswordHash/{username}");
            return result.Count > 0 ? result[result.Count - 1] : string.Empty;
        }

        public static async Task<string> GetUserSubSection(string username)
        {
            var result = await GetData<List<string>>($"api/SubSection/User/{username}");
            return result.Count > 0 ? result[result.Count - 1] : string.Empty;
        }

        #region POSTs

        public static async Task<bool> RecordTripSubmission(RecordTripViewModel viewModel)
        {
            string url = "api/Vehicle/RecordTrip";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> FuelUpVehicleSubmission(FuelUpVehicleViewModel viewModel)
        {
            string url = "api/Vehicle/FuelUp";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> FuelUpGeneratorSubmission(FuelUpGeneratorViewModel viewModel)
        {
            string url = "api/Site/FuelUp";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> RechargeMeterSubmission(RechargeMeterViewModel viewModel)
        {
            string url = "api/Site/RechargeMeter";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> RequestRechargeTokenSubmission(RequestTokenSubmissionViewModel viewModel)
        {
            string url = "api/Request/ElectricityRequest";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> RequestDrawdownSubmission(RequestDrawdownSubmissionViewModel viewModel)
        {
            string url = "api/Request/FuelRequest";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> AuthoriseTR1Submission(AuthoriseViewModel viewModel)
        {
            string url = "api/Request/Authorize";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> AuthoriseTokenPurchaseSubmission(AuthoriseViewModel viewModel)
        {
            string url = "api/Request/Authorize";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        } 

        public static async Task<bool> GeneratorReadingsSubmission(GeneratorReadingsViewModel viewModel)
        {
            string url = "api/Site/GeneratorReadings";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> VehicleReadingsSubmission(VehicleReadingsViewModel viewModel)
        {
            string url = "api/Vehicle/Readings";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> MeterReadingSubmission(ElectricityMeterReadingViewModel viewModel)
        {
            string url = "api/Site/MeterReading";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        public static async Task<bool> DrawdownSubmission(DrawdownViewModel viewModel)
        {
            string url = "api/FuelSource/Drawdown";
            using (var client = CreateClient("application/json"))
            {
                var jsonInString = JsonConvert.SerializeObject(viewModel);
                return await PostData(url, jsonInString, client);
            }
        }

        #endregion

        public static async Task<bool> PostData(string url, string jsonInString, HttpClient httpClient)
        {
            try
            {
                HttpResponseMessage message = await httpClient.PostAsync(url, new StringContent(jsonInString, Encoding.UTF8, "application/json"));
                if (message.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                BusinessLogic.ShowMessageBox(new Xamarin.Forms.Page(), e.Message, "Error");
                return false;
            }
        }
        
        public static async Task<T> GetData<T>(string url) where T: class , new() 
        {
            using (var client = CreateClient("application/json"))
            {
                try
                {
                    //var settings = new DataContractJsonSerializerSettings() { UseSimpleDictionaryFormat = false };
                    //var serializer = new DataContractJsonSerializer(typeof(T), settings);
                    var streamTask = client.GetStreamAsync(url);
                    var stream = await streamTask;
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        string value = reader.ReadToEnd();
                        var data = JsonConvert.DeserializeObject<T>(value);
                        return data;
                    }
                    //var data = serializer.ReadObject(stream) as T;
                    //return data;
                }
                catch (Exception e)
                {
                    BusinessLogic.ShowMessageBox(new Xamarin.Forms.Page(), e.Message, "Error");
                    return new T();
                }

            }
        }

        private static HttpClient CreateClient(string mediaType)
        {
            String baseUri = baseUrl;// + port;
            HttpClient apiClient = new HttpClient();
            apiClient.BaseAddress = new Uri(baseUri);
            apiClient.DefaultRequestHeaders.Accept.Clear();
            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
            return apiClient;
        }
    }
}
