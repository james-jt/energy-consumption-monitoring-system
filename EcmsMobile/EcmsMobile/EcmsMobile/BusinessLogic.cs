﻿using Acr.UserDialogs;
using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EcmsMobile
{
    internal class BusinessLogic
    {
        private const int SaltByteSize = 16;
        private const int HashByteSize = 32;
        private const int HasingIterationsCount = 1000;

        #region Model Hashes 

        public static string RecordTripModelHash { get; set; }
        public static string FuelUpVehicleModelHash { get; set; }
        public static string FuelUpGeneratorModelHash { get; set; }
        public static string GeneratorReadingsModelHash { get; set; }
        public static string VehicleReadingsModelHash { get; set; }
        public static string DrawdownModelHash { get; set; }
        public static string RequestRechargeTokenModelHash { get; set; }
        public static string RechargeMeterModelHash { get; set; }
        public static string RequestFuelDrawdownHash { get; set; }
        public static string MeterReadingsModelHash { get; set; }

        #endregion

        #region Sign In

        public async Task<bool> AuthenticateUser(User user)
        {
            using (UserDialogs.Instance.Loading("Signing in...", null, null, true, MaskType.Gradient))
            {
                string hashedPassword = string.Empty;
                try
                {
                    hashedPassword = await DataAccess.GetPasswordHash(user.Username);
                    if (hashedPassword == string.Empty)
                        return false;
                }
                catch(Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                }
                return VerifyHashedPassword(hashedPassword, user.Password);
            }
        }

        public static bool VerifyHashedPassword(string hashedPassword, string password)
        {
            byte[] _passwordHashBytes;

            int _arrayLen = (SaltByteSize + HashByteSize) + 1;

            if (hashedPassword == null)
            {
                return false;
            }

            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            byte[] src = Convert.FromBase64String(hashedPassword);

            if ((src.Length != _arrayLen) || (src[0] != 0))
            {
                return false;
            }

            byte[] _currentSaltBytes = new byte[SaltByteSize];
            Buffer.BlockCopy(src, 1, _currentSaltBytes, 0, SaltByteSize);

            byte[] _currentHashBytes = new byte[HashByteSize];
            Buffer.BlockCopy(src, SaltByteSize + 1, _currentHashBytes, 0, HashByteSize);

            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, _currentSaltBytes, HasingIterationsCount))
            {
                _passwordHashBytes = bytes.GetBytes(SaltByteSize);
            }

            return AreHashesEqual(_currentHashBytes, _passwordHashBytes);

        }

        private static bool AreHashesEqual(byte[] firstHash, byte[] secondHash)
        {
            int _minHashLength = firstHash.Length <= secondHash.Length ? firstHash.Length : secondHash.Length;
            var xor = 0; 
            for (int i = 0; i < _minHashLength; i++) {
                var tempXor = firstHash[i] ^ secondHash[i];
                xor = xor | tempXor;
            }
            return 0 == xor;
        }

        public static async Task<string> GetUserSubSection(string username)
        {
            using (UserDialogs.Instance.Loading("Loading user info...", null, null, true, MaskType.Gradient))
            {
                try
                {
                    return await DataAccess.GetUserSubSection(username);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return string.Empty;
                }
            }
        }

        #endregion

        #region GETs

        public static async void PopulatePicker(CustomPicker picker, string filter = "All")
        {
            using (UserDialogs.Instance.Loading($"Loading {picker.AutomationId}...", null, null, true, MaskType.Gradient))
            {
                var data = new List<string>();
                try
                {
                    switch (picker.AutomationId)
                    {
                        case "Vehicle Registration Numbers":
                            var regNumbers = await DataAccess.GetVehicleRegistrationNumbers(filter);
                            data = regNumbers;
                            picker.ViewModel = regNumbers;
                            break;
                        case "Departments":
                            var departments = await DataAccess.GetDepartments();
                            data = new List<string>(departments.Keys); 
                            picker.ViewModel = departments;
                            break;
                        case "Sections":
                            var sections = await DataAccess.GetSections(filter);
                            data = new List<string>(sections.Keys);
                            picker.ViewModel = sections;
                            break;
                        case "Sub Sections":
                            var subSections = await DataAccess.GetSubSections(filter);
                            data = new List<string>(subSections.Keys);
                            picker.ViewModel = subSections;
                            break;
                        case "Zones":
                            var zones = await DataAccess.GetZones(filter);
                            data = new List<string>(zones.Keys);
                            picker.ViewModel = zones;
                            break;
                        case "Sub Zones":
                            var subZones = await DataAccess.GetSubZones(filter);
                            data = new List<string>(subZones.Keys);
                            picker.ViewModel = subZones;
                            break;
                        case "Maintenance Centres":
                            var maintenanceCentres = await DataAccess.GetMaintenanceCentres(filter);
                            data = new List<string>(maintenanceCentres.Keys);
                            picker.ViewModel = maintenanceCentres;
                            break;
                        case "Sites":
                            var sites = await DataAccess.GetSites(filter);
                            sites.ForEach(e => data.Add(e.Name));
                            picker.ViewModel = sites;
                            break;
                        case "Warehouses":
                            var warehouses = await DataAccess.GetWarehouseNames();
                            data = new List<string>(warehouses.Keys);
                            picker.ViewModel = warehouses;
                            break;

                        case "Third Party Garages":
                            var tpGarages = await DataAccess.GetTpGarageNames();
                            data = new List<string>(tpGarages.Keys);
                            picker.ViewModel = tpGarages;
                            break;
                        case "Generator Fuel Requests": 
                            var generatorFuelRequests = await GetAuthorisedFuelRequestsForUser(filter, FuelUpTargets.Generator);
                            picker.ViewModel = generatorFuelRequests;
                            generatorFuelRequests.ForEach(o => data.Add(o.RequestNumber));
                            break;
                        case "Vehicle Fuel Requests":
                            var vehicleFuelRequests = await GetAuthorisedFuelRequestsForUser(filter, FuelUpTargets.Vehicle);
                            picker.ViewModel = vehicleFuelRequests;
                            vehicleFuelRequests.ForEach(o => data.Add(o.RequestNumber));
                            break;
                        case "Drawdown Fuel Requests": 
                            var drawdownFuelRequests = await GetAuthorisedFuelRequestsForUser(filter, FuelUpTargets.Drawdown);
                            picker.ViewModel = drawdownFuelRequests;
                            drawdownFuelRequests.ForEach(o => data.Add(o.RequestNumber));
                            break;
                        case "Electricity Requests":
                            var electricityRecharges = await DataAccess.GetAuthorisedElectricityRequestsForUserPendingRecharge(filter);
                            picker.ViewModel = electricityRecharges;
                            electricityRecharges.ForEach(o => data.Add(o.RequestNumber));
                            break;
                        case "Fuel Requests For Authorisation": 
                            var fuelRequests = await DataAccess.GetFuelRequestsForAuthorisationByUser(filter);
                            picker.ViewModel = fuelRequests;
                            fuelRequests.ForEach(o => data.Add(o.RequestNumber));
                            break;
                        case "Electricity Requests For Authorisation": 
                            var electricityRequests = await DataAccess.GetElectricityRequestsForAuthorisationByUser(filter);
                            picker.ViewModel = electricityRequests;
                            electricityRequests.ForEach(o => data.Add(o.RequestNumber));
                            break;
                        case "Drawdown Sources":
                            data = GetDrawdownSource();
                            break;
                        case "Fuel Types":
                            var fuelTypes = await DataAccess.GetFuelTypes();
                            data = new List<string>(fuelTypes.Keys);
                            picker.ViewModel = fuelTypes;
                            break;
                        default:
                            data = new List<string>() { "Populate Me!" };
                            break;
                    }
                    picker.ItemsSource = data;
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                }
            }
        }

        public static async Task<List<DrawdownViewModel>> GetAuthorisedFuelRequestsForUser(string username, FuelUpTargets fuelUpTarget)
        {
            switch (fuelUpTarget)
            {
                case FuelUpTargets.Generator:
                    return await DataAccess.GetDrawdownsForUserWithDrawdownCapacity(username);
                case FuelUpTargets.Drawdown:
                    return await DataAccess.GetAuthorisedFuelRequestsForUserPendingDrawdown(username);
                case FuelUpTargets.Vehicle:
                    return await DataAccess.GetAuthorisedFuelRequestsForUserWithDrawdownCapacity(username);
                default:
                    return new List<DrawdownViewModel>();
            }
        }

        public static void ShowMessageBox(Page page, string message, string title = "Alert")
        {
            page.DisplayAlert(title, message, "OK");
        }

        public static string SanitizeCouponNumber(string value)
        {
            var legalCharacters = new string(value.ToUpper().Where(c => ((int)c >= 48 && (int)c <= 57) || ((int)c >= 65 && (int)c <= 90) || (int)c == 32 || (int)c == 44 || (int)c == 45).ToArray());
            return legalCharacters;
        }

        public static bool IsCouponsInputValid(string input, ref string errorMsg)
        { 
            var numerics = new string(input.SkipWhile(c => char.IsLetter(c)).ToArray());
            if (Regex.Matches(numerics, @"[a-zA-Z]").Count > 0)
                return false;
            var couponNumbersString = RemoveIllegalCharactersFromCouponNumber(input);
            var regexPattern = "^(?!([ \\d]*-){2})\\d+(?: *[-,] *\\d+)*$";
            var validator = new Regex(regexPattern);
            return validator.IsMatch(couponNumbersString.Trim());
        }

        public static string RemoveIllegalCharactersFromCouponNumber(string value)
        {
            var allowedChars = "0123456789,- ";
            var legalCharacters = new string(value.Where(c => allowedChars.Contains(c)).ToArray());
            return legalCharacters;
        }

        public static List<string> GetDrawdownSource()
        {
            return new List<string>()
            {
                DefaultValues.WarehouseFuelSourceTypeName,
                DefaultValues.TpGarageFuelSourceTypeName,
                DefaultValues.CouponsFuelSourceTypeName
            };
        }

        public static async void GetNextRequestNumber(Label label, bool isFuelRequest)
        {
            using (UserDialogs.Instance.Loading("Generating Request  Number...", null, null, true, MaskType.Gradient))
            {
                try
                {
                    var nextRequestNumber = await DataAccess.GetNextRequestNumber(isFuelRequest);
                    if (nextRequestNumber == null)
                        nextRequestNumber = new List<string>() { string.Empty };
                    label.Text = nextRequestNumber[0];
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    label.Text = string.Empty; ;
                }
            }

        }

        #endregion

        #region Camera

        public static async Task<bool> IsCameraReady()
        {
            try
            { 
            var initialize = await CrossMedia.Current.Initialize();
            var isCameraAvailable = CrossMedia.Current.IsCameraAvailable;
            var isTakePhotoSupported  = CrossMedia.Current.IsTakePhotoSupported;
            var isPickPhotoSupported  = CrossMedia.Current.IsPickPhotoSupported;
            var isTakeVideoSupported  = CrossMedia.Current.IsTakeVideoSupported;
            var isPickVideoSupported = CrossMedia.Current.IsPickVideoSupported;
            return
                initialize &&
                isCameraAvailable &&
                isTakePhotoSupported &&
                isPickPhotoSupported &&
                isTakeVideoSupported &&
                isPickVideoSupported;
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message);
                return false;
            }
        }

        public static async Task<MediaFile> TakePhoto()
        {
            try
            {
                using (UserDialogs.Instance.Loading($"Loading...", null, null, true, MaskType.Gradient))
                {
                    var pic = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        CompressionQuality = 92,
                        SaveToAlbum = true,
                        SaveMetaData = true,
                        Directory = "Ecms Mobile",
                        Name = "Reading.jpg"
                    });
                    return pic;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message);
                return null;
            }
        }

        public static async Task<MediaFile> PickPhoto()
        {
            try
            {
                using (UserDialogs.Instance.Loading($"Loading image...", null, null, true, MaskType.Gradient))
                {
                    return await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        CompressionQuality = 92,
                        SaveMetaData = true,
                    });
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message);
                return null;
            }
        }

        public static async void ShowImage(Page sender, MediaFile _image)
        {
            try
            {
                using (UserDialogs.Instance.Loading($"Opening image...", null, null, true, MaskType.Gradient))
                {
                    Image image = new Image();
                    image.Source = ImageSource.FromStream(() =>
                    {
                        var stream = _image.GetStream();
                        return stream;
                    });
                    await sender.Navigation.PushAsync(new ImagePage(image));
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message);
            }
        }

        #endregion

        #region POSTs

        public static async Task<bool> RecordTripSubmission(RecordTripViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.RecordTripSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> FuelUpVehicleSubmission(FuelUpVehicleViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.FuelUpVehicleSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> FuelUpGeneratorSubmission(FuelUpGeneratorViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.FuelUpGeneratorSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> RechargeMeterSubmission(RechargeMeterViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                return await DataAccess.RechargeMeterSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> RequestRechargeTokenSubmission(RequestTokenSubmissionViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.RequestRechargeTokenSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> RequestFuelDrawdownSubmission(RequestDrawdownSubmissionViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.RequestDrawdownSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> AuthoriseTR1Submission(AuthoriseViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.AuthoriseTR1Submission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> AuthoriseTokenPurchaseSubmission(AuthoriseViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.AuthoriseTokenPurchaseSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> GeneratorReadingsSubmission(GeneratorReadingsViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                {
                    return await DataAccess.GeneratorReadingsSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> VehicleReadingsSubmission(VehicleReadingsViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                {
                    return await DataAccess.VehicleReadingsSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> MeterReadingSubmission(ElectricityMeterReadingViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                { 
                    return await DataAccess.MeterReadingSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        public static async Task<bool> DrawdownSubmission(DrawdownViewModel viewModel)
        {
            using (UserDialogs.Instance.Loading("Sending...", null, null, true, MaskType.Gradient))
            {
                try
                {
                    return await DataAccess.DrawdownSubmission(viewModel);
                }
                catch (Exception e)
                {
                    UserDialogs.Instance.Toast(e.Message);
                    return false;
                }
            }
        }

        #endregion
    }
}
