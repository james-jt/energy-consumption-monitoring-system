﻿using Acr.UserDialogs;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EcmsMobile
{
    public static class Extensions
    {
        public static async Task<string> ConvertToBase64(this MediaFile image)
        {
            if (image != null)
            {
                using (var stream = image.GetStream())
                {
                    var bytes = new byte[stream.Length];
                    await stream.ReadAsync(bytes, 0, (int)stream.Length);
                    string base64 = System.Convert.ToBase64String(bytes);
                    return base64;
                }
            }
            return string.Empty;
        }
    }
}
