﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcmsMobile
{
    public class MasterPageGroup : List<MasterPageItem>
    { 
        public string LongName { get; set; }
        public string ShortName { get; set; }
    }
}
