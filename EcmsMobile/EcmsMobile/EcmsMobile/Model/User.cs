﻿namespace EcmsMobile
{
	public class User
	{
		public string Username { get; set; }

		public string Password { get; set; }

		public string Email { get; set; }

        public string MobileNumber { get; set; }

        public bool IsSignedIn { get; set; }

        public string SubSection { get; set; }

        public string SubSectionId { get; set; }
    }
}
 