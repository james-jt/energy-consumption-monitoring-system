﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcmsMobile
{
    public enum ReturnType
    {
        Go,
        Next,
        Done,
        Send,
        Search
    }
}
