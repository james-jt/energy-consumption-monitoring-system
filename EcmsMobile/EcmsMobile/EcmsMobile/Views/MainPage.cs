﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    public class MainPage : MasterDetailPage
    {
        MasterPage masterPage;

        public MainPage()
        {
            BackgroundImage = "background.png";
            Title = "ECMS";
            //BackgroundColor = Color.Black;
            masterPage = new MasterPage();
            Master = masterPage;
            masterPage.ListView.ItemSelected += OnItemSelected;
            Detail = new NavigationPage(new RecordTrip()) { BackgroundImage = "background.png", BarBackgroundColor = Color.DarkOrange };
            IsPresented = true;
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType)) { BackgroundImage = "background.png", BarBackgroundColor = Color.DarkOrange };
                masterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
        }
    }
}
