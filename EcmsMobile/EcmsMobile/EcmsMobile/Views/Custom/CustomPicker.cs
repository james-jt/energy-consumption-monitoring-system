﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    public class CustomPicker : Picker
    {
        public CustomPicker()
            : base()
        {
            HeightRequest = 40;
            Image = "ic_arrow_drop_down";
            TextColor = Color.White;
        }

        public object ViewModel { get; set; }

        public static readonly BindableProperty ImageProperty =
            BindableProperty.Create(nameof(Image), typeof(string), typeof(CustomPicker), string.Empty);

        public string Image
        {
            get { return (string)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }
    }
}
