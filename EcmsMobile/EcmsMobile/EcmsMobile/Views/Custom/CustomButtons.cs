﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    public class CustomButton : Button
    {
        public CustomButton()
            : base()
        { 
            BackgroundColor = Color.FromHex("#424242");
            BorderRadius = 100;
            Opacity = 50;
        }
    }

    public class TakePhotoButton : CustomButton
    {
        public TakePhotoButton()
            :base()
        {
            Image = "camera.png";
        }
    }

    public class PickPhotoButton : CustomButton
    {
        public PickPhotoButton()
            : base()
        {
            Image = "image.png";
        }
    }

    public class ViewPhotoButton : CustomButton
    {
        public ViewPhotoButton()
            :base()
        {
            Image = "view.png";
        }
    }

    public class ActionButton : Button
    {
        //public ActionButton()
        //    : base()
        //{
        //    BackgroundColor = Color.FromHex("#424242");
        //    TextColor = Color.White;
        //}
    }
}
