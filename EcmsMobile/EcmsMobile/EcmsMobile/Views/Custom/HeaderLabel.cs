﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    public class HeaderLabel : Label
    {
        public HeaderLabel()
            :base()
        {
            FontAttributes = FontAttributes.Bold;
            HorizontalOptions = LayoutOptions.Start;
            TextColor = Color.White;
        }
    }
}
