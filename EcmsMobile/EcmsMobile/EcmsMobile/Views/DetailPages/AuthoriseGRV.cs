﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class AuthoriseGRV : ContentPage
    {
        Label lblRequestDetail;
        Entry entComment;

        public AuthoriseGRV()
        {
            Title = "Authorise GRV";
            BackgroundColor = Color.Orange;
            entComment = new Entry();
            lblRequestDetail = new Label();

            var outputText = new Label();

            var requestPicker = new Picker { Title = "Tap to Select" };
            BusinessLogic.PopulatePicker(requestPicker);

            requestPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    outputText.Text = requestPicker.Items[selectedIndex];
                }
            };

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new Label { Text = "Request Number:", FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Start },
                    requestPicker,
                    new Label { Text = "Request Detail:", FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Start },
                    lblRequestDetail,
                    new Label { Text = "Comment:", FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Start },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            new Button { Text = "Cancel", BackgroundColor = Color.DarkOrange },
                            new Button { Text = "Submit", BackgroundColor = Color.DarkOrange },
                        }
                    }
                }
            };
            ScrollView scrollView = new ScrollView();
            scrollView.Content = content;
            Content = scrollView;
        }

    }
}
