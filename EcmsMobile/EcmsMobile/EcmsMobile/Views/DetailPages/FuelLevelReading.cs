﻿using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class FuelLevelReading : ContentPage
    {
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        Label lblSiteLocation;
        CustomEntry entGenFuelLevel;
        Button entGenFuelLevelTakePhoto;
        Button entGenFuelLevelPickPhoto;
        Button entGenFuelLevelViewPhoto;
        MediaFile entGenFuelLevelPhoto;
        FuelLevelReadingViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testGenFuelLevelBefore;

        public FuelLevelReading()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            entGenFuelLevel = new CustomEntry() { Keyboard = Keyboard.Numeric };
            entGenFuelLevel.TextChanged += OnNumericEntryTextChanged;
            entGenFuelLevel.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            entGenFuelLevelTakePhoto = new TakePhotoButton();
            entGenFuelLevelPickPhoto = new PickPhotoButton();
            entGenFuelLevelViewPhoto = new ViewPhotoButton();
            entGenFuelLevelTakePhoto.AutomationId = entGenFuelLevelPickPhoto.AutomationId = entGenFuelLevelViewPhoto.AutomationId = "Current Level";
            entGenFuelLevelTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entGenFuelLevelPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entGenFuelLevelViewPhoto.Clicked += OnViewPhotoButtonClicked;

            lblSiteLocation = new Label();

            maintenanceCentrePicker = new CustomPicker ();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            BusinessLogic.PopulatePicker(maintenanceCentrePicker, App.currentUser.Department);

            sitePicker = new CustomPicker ();
            sitePicker.AutomationId = "Sites";
            BusinessLogic.PopulatePicker(sitePicker, App.currentUser.Username);

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentrePicker.Items[selectedIndex]);
                    lblSiteLocation.Text = string.Empty;
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    lblSiteLocation.Text = sitePicker.Items[selectedIndex];
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var genFuelLevelGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.Children.Add(entGenFuelLevel);
            genFuelLevelGrid.Children.Add(entGenFuelLevelViewPhoto, 1, 0);
            genFuelLevelGrid.Children.Add(entGenFuelLevelPickPhoto, 2, 0);
            genFuelLevelGrid.Children.Add(entGenFuelLevelTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel { Text = "Fuel Level:" },
                    genFuelLevelGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Current Level":
                    entGenFuelLevelPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Current Level":
                    entGenFuelLevelPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Current Level":
                    if (entGenFuelLevelPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Generator Fuel Level field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entGenFuelLevelPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (entGenFuelLevelPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Generator Fuel Level?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            Image genFuelLevelImage = new Image();
            genFuelLevelImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entGenFuelLevelPhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.FuelLevelReadingSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            entGenFuelLevel.Text = string.Empty;
            entGenFuelLevelPhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public void BindModel()
        {
            viewModel = new FuelLevelReadingViewModel();
            viewModel.MaintenanceCentre = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.GenFuelLevel = testGenFuelLevelBefore;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            testGenFuelLevelBefore = double.TryParse(entGenFuelLevel.Text, out testGenFuelLevelBefore) ? testGenFuelLevelBefore : double.NaN;
            if (testGenFuelLevelBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Generator Fuel Level field.", "Error");
                entGenFuelLevel.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.FuelLevelReadingModelHash;
            return modelVerified;
        } 

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.GenFuelLevel);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.FuelLevelReadingModelHash = HashModel();
        }

        #endregion

    }
}
