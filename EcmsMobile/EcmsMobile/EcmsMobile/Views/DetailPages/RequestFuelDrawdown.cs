﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using EcmsModels.ViewModels;

namespace EcmsMobile
{ 
    class RequestFuelDrawdown : ContentPage
    {
        //CustomPicker departmentPicker;
        //CustomPicker sitePicker;
        //Label lblSiteLocation;
        //Label lblRequestType; 
        CustomPicker fuelTypePicker;
        HeaderLabel lblRequestNumber;
        CustomEntry entComment;
        RequestDrawdownSubmissionViewModel viewModel;
        Button btnClear;
        Button btnSubmit;
        CustomEntry entQuantity;
        int requestQuantity;

        public RequestFuelDrawdown()
        {
            BackgroundImage = "background.png";

            Title = "Send Fuel Request";
            ScrollView scrollView = new ScrollView();
            //sitePicker = new CustomPicker ();
            entComment = new CustomEntry();
            //lblSiteLocation = new Label();
            //lblRequestType = new Label();
            lblRequestNumber = new HeaderLabel();
            BusinessLogic.GetNextRequestNumber(lblRequestNumber, true);
            entQuantity = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entQuantity.Completed += (sender, e) => entComment.Focus();
            fuelTypePicker = new CustomPicker();
            fuelTypePicker.AutomationId = "Fuel Types";
            BusinessLogic.PopulatePicker(fuelTypePicker);

            //departmentPicker = new CustomPicker ();
            // departmentPicker.AutomationId = "Departments";
            //BusinessLogic.PopulatePicker(departmentPicker);

            //departmentPicker.SelectedIndexChanged += (sender, e) =>
            //{
            //    int selectedIndex = departmentPicker.SelectedIndex;
            //    if (selectedIndex != -1)
            //    {
            //        BusinessLogic.PopulatePicker(sitePicker, departmentPicker.Items[selectedIndex]);
            //        lblSiteLocation.Text = string.Empty;
            //    }
            //};

            //sitePicker.SelectedIndexChanged += (sender, e) =>
            //{
            //    int selectedIndex = sitePicker.SelectedIndex;
            //    if (selectedIndex != -1)
            //    {
            //        lblSiteLocation.Text = sitePicker.Items[selectedIndex];
            //    }
            //};

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    //new HeaderLabel { Text = "Department:" },
                    //departmentPicker,
                    //new HeaderLabel { Text = "Site Name:" },
                    //sitePicker,
                    //new HeaderLabel { Text = "Location:" },
                    //lblSiteLocation,
                    //new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Fuel Type:" },
                    fuelTypePicker,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel {Text = "Request Quantity" },
                    entQuantity,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Comment:" },
                    entComment,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Number:" },
                    lblRequestNumber,
                    new HeaderLabel { Text = "" },
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (string.IsNullOrEmpty(entComment.Text))
            {
                var response = await this.DisplayActionSheet("Submit without comment?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.RequestFuelDrawdownSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                var requestNumber = lblRequestNumber.Text;
                var response = await this.DisplayActionSheet($"Do you wish to make another request?", null, null, "Yes", "No");
                if (response == "Yes")
                {
                    OnClearButtonClicked(null, null);
                    BusinessLogic.GetNextRequestNumber(lblRequestNumber, true);
                }
                else
                    lblRequestNumber.Text = string.Empty;
                BusinessLogic.ShowMessageBox(this, $"Request successfully submitted. Your request number is {requestNumber}.", "Info");
                requestNumber = string.Empty;
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            //departmentPicker.SelectedIndex = -1;
            //sitePicker.SelectedIndex = -1;
            fuelTypePicker.SelectedIndex = -1;
            entQuantity.Text = string.Empty;
            entComment.Text = string.Empty;
            viewModel = null;
        }

        #endregion

        #region Model Binding

        public void BindModel()
        {
            viewModel = new RequestDrawdownSubmissionViewModel();
            viewModel.RequestDate = DateTime.Now;
            viewModel.Requestor = App.currentUser.Username;
            viewModel.SubSection = App.currentUser.SubSection; //(departmentPicker.SelectedItem as string).Trim();
            viewModel.RequestQuantity = requestQuantity.ToString();
            viewModel.FuelType = (fuelTypePicker.SelectedItem as string).Trim();
            viewModel.RequestNumber = lblRequestNumber.Text;
            viewModel.Comment = entComment.Text;
        }

        public bool ValidateModel()
        {
            
            if(!int.TryParse(entQuantity.Text, out requestQuantity) || requestQuantity <= 0)
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Quantity field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(lblRequestNumber.Text))
            {
                BusinessLogic.GetNextRequestNumber(lblRequestNumber, true);
                BusinessLogic.ShowMessageBox(this, "Request number generation failed, please try again.", "Error");
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.RequestFuelDrawdownHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.Requestor);
            hash.Append(viewModel.SubSection);
            hash.Append(viewModel.RequestQuantity);
            hash.Append(viewModel.FuelType);
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.Comment);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.RequestFuelDrawdownHash = hash;
        }

        #endregion


    }
}
