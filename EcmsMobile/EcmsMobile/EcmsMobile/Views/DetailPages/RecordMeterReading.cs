﻿using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class RecordMeterReading : ContentPage
    {
        CustomPicker zonePicker;
        CustomPicker subZonePicker;
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        CustomEntry entMeterReading;
        HeaderLabel lblSiteLocation;
        HeaderLabel lblMeterType;
        Button entMeterReadingTakePhoto;
        Button entMeterReadingPickPhoto;
        Button entMeterReadingViewPhoto;
        MediaFile entMeterReadingPhoto;
        ElectricityMeterReadingViewModel viewModel;
        Button btnClear;
        Button btnSubmit;
        double testMeterReading;
        bool submitWithoutMeterReadingImage = false;

        public RecordMeterReading()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();
            Title = "Log Meter Reading";
            entMeterReading = new CustomEntry() { Keyboard = Keyboard.Numeric };
            entMeterReading.TextChanged += OnNumericEntryTextChanged;
            entMeterReading.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            entMeterReadingTakePhoto = new TakePhotoButton();
            entMeterReadingPickPhoto = new PickPhotoButton();
            entMeterReadingViewPhoto = new ViewPhotoButton();
            entMeterReadingTakePhoto.AutomationId = entMeterReadingPickPhoto.AutomationId = entMeterReadingViewPhoto.AutomationId = "Meter Reading";
            entMeterReadingTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entMeterReadingPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entMeterReadingViewPhoto.Clicked += OnViewPhotoButtonClicked;

            lblSiteLocation = new HeaderLabel() ;
            lblMeterType = new HeaderLabel() ;

            zonePicker = new CustomPicker();
            zonePicker.AutomationId = "Zones";
            BusinessLogic.PopulatePicker(zonePicker, App.currentUser.SubSectionId);

            subZonePicker = new CustomPicker();
            subZonePicker.AutomationId = "Sub Zones";
            //BusinessLogic.PopulatePicker(subZonePicker);

            maintenanceCentrePicker = new CustomPicker();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            //BusinessLogic.PopulatePicker(maintenanceCentrePicker);
            //maintenanceCentrePicker.SelectedIndex = maintenanceCentrePicker.Items.IndexOf(App.currentUser.SubSection);

            sitePicker = new CustomPicker();
            sitePicker.AutomationId = "Sites";
            //BusinessLogic.PopulatePicker(sitePicker, siteViewObjects, App.currentUser.SubSection);

            zonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = zonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = zonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subZonePicker, zones[zonePicker.Items[selectedIndex]]);
                }
            };

            subZonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subZonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = subZonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(maintenanceCentrePicker, zones[subZonePicker.Items[selectedIndex]]);
                }
            };

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var maintenanceCentres = maintenanceCentrePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentres[maintenanceCentrePicker.Items[selectedIndex]]);
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sites = sitePicker.ViewModel as List<SiteViewModel>;
                    var siteName = sitePicker.Items[selectedIndex];
                    var site = sites.Find(s => s.Name == siteName);
                    lblSiteLocation.Text = site.Location;
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var meterReadingGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            meterReadingGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            meterReadingGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            meterReadingGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            meterReadingGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            meterReadingGrid.Children.Add(entMeterReading);
            meterReadingGrid.Children.Add(entMeterReadingViewPhoto, 1, 0);
            meterReadingGrid.Children.Add(entMeterReadingPickPhoto, 2, 0);
            meterReadingGrid.Children.Add(entMeterReadingTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Zone:" },
                    zonePicker,
                    new HeaderLabel { Text = "Sub Zone:" },
                    subZonePicker,
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel { Text = "" },
                    //new HeaderLabel { Text = "Meter Type:" },
                    //lblMeterType,
                    //new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Meter Reading:" },
                    meterReadingGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Meter Reading":
                    entMeterReadingPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Meter Reading":
                    entMeterReadingPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Meter Reading":
                    if (entMeterReadingPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Meter Reading field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entMeterReadingPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            await BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (entMeterReadingPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Meter Reading?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutMeterReadingImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutMeterReadingImage = true;
            }
            Image genFuelLevelImage = new Image();
            genFuelLevelImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entMeterReadingPhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.MeterReadingSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            zonePicker.SelectedIndex = -1;
            subZonePicker.SelectedIndex = -1;
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            entMeterReading.Text = string.Empty;
            entMeterReadingPhoto = null;
            lblSiteLocation.Text = string.Empty;
            lblMeterType.Text = string.Empty;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public async Task BindModel()
        {
            viewModel = new ElectricityMeterReadingViewModel();
            viewModel.MaintenanceCentre = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.MeterReading = testMeterReading;
            viewModel.MeterReadingImage = await entMeterReadingPhoto.ConvertToBase64();
            viewModel.LoggedBy = App.currentUser.Username;
            while ((viewModel.MeterReadingImage == null && !submitWithoutMeterReadingImage)) ;

        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(zonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subZonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            testMeterReading = double.TryParse(entMeterReading.Text, out testMeterReading) ? testMeterReading : double.NaN;
            if (testMeterReading.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Meter Reading field.", "Error");
                entMeterReading.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.MeterReadingsModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.MeterReading);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.MeterReadingsModelHash = hash;
        }

        #endregion

    }
}
