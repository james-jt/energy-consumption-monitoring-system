﻿using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class RechargeMeter : ContentPage
    {
        //CustomPicker maintenanceCentrePicker;
        //CustomPicker sitePicker;
        CustomPicker requestNumberPicker;
        CustomEntry entUnitsBefore;
        CustomEntry entUnitsAfter;
        HeaderLabel lblUnits;
        HeaderLabel lblSiteLocation;
        HeaderLabel lblMaintenanceCentre;
        HeaderLabel lblSite;
        HeaderLabel lblMeterNumber;

        Button entUnitsBeforeTakePhoto;
        Button entUnitsBeforePickPhoto;
        Button entUnitsBeforeViewPhoto;
        Button entUnitsAfterTakePhoto;
        Button entUnitsAfterPickPhoto;
        Button entUnitsAfterViewPhoto;
        MediaFile unitsBeforePhoto;
        MediaFile unitsAfterPhoto;
        RechargeMeterViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testUnitsBefore;
        double testUnitsAfter;

        public RechargeMeter()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Recharge Meter";

            entUnitsBefore = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entUnitsBefore.TextChanged += OnNumericEntryTextChanged;
            entUnitsBefore.Completed += (sender, e) => 
            {
                entUnitsAfter.Focus();
            };
            entUnitsBeforeTakePhoto = new TakePhotoButton();
            entUnitsBeforePickPhoto = new PickPhotoButton();
            entUnitsBeforeViewPhoto = new ViewPhotoButton();
            entUnitsBeforeTakePhoto.AutomationId = entUnitsBeforePickPhoto.AutomationId = entUnitsBeforeViewPhoto.AutomationId = "Units Before";
            entUnitsBeforeTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entUnitsBeforePickPhoto.Clicked += OnPickPhotoButtonClicked;
            entUnitsBeforeViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entUnitsAfter = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Done };
            entUnitsAfter.TextChanged += OnNumericEntryTextChanged;
            entUnitsAfterTakePhoto = new TakePhotoButton();
            entUnitsAfterPickPhoto = new PickPhotoButton();
            entUnitsAfterViewPhoto = new ViewPhotoButton();
            entUnitsAfterTakePhoto.AutomationId = entUnitsAfterPickPhoto.AutomationId = entUnitsAfterViewPhoto.AutomationId = "Units After";
            entUnitsAfterTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entUnitsAfterPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entUnitsAfterViewPhoto.Clicked += OnViewPhotoButtonClicked;

            lblUnits = new HeaderLabel();
            lblSiteLocation = new HeaderLabel();
            lblMaintenanceCentre = new HeaderLabel();
            lblSite = new HeaderLabel();
            lblMeterNumber = new HeaderLabel();
            //maintenanceCentrePicker = new CustomPicker ();
            //maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            //BusinessLogic.PopulatePicker(maintenanceCentrePicker);

            //sitePicker = new CustomPicker ();
            //sitePicker.AutomationId = "Sites";
            //BusinessLogic.PopulatePicker(sitePicker, App.currentUser.Username);

            requestNumberPicker = new CustomPicker ();
            requestNumberPicker.AutomationId = "Electricity Requests";
            BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);
            requestNumberPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestNumberPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var requestId = requestNumberPicker.Items[selectedIndex];
                    var requests = requestNumberPicker.ViewModel as List<RechargeMeterViewModel>;
                    var request = requests.Find(r => r.RequestNumber == requestId);
                    lblMaintenanceCentre.Text = request.MaintenanceCentre;
                    lblSite.Text = request.Site;
                    lblSiteLocation.Text = request.Location;
                    lblUnits.Text = request.Units;
                    lblMeterNumber.Text = request.MeterNumber;
                }
            };

            //maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            //{
            //    int selectedIndex = maintenanceCentrePicker.SelectedIndex;
            //    if (selectedIndex != -1)
            //    {
            //        BusinessLogic.PopulatePicker(sitePicker, maintenanceCentrePicker.Items[selectedIndex]);
            //        lblSiteLocation.Text = string.Empty;
            //    }
            //};

            //sitePicker.SelectedIndexChanged += (sender, e) =>
            //{
            //    int selectedIndex = sitePicker.SelectedIndex;
            //    if (selectedIndex != -1)
            //    {
            //        if (sitePicker.SelectedIndex.Equals(0))
            //        {
            //            lblSiteLocation.Text = "Warren Park";
            //        }
            //        if (sitePicker.SelectedIndex.Equals(1))
            //        {
            //            lblSiteLocation.Text = "Belvedere";
            //        }
            //        if (sitePicker.SelectedIndex.Equals(2))
            //        {
            //            lblSiteLocation.Text = "Belvedere";
            //        }
            //        if (sitePicker.SelectedIndex.Equals(3))
            //        {
            //            lblSiteLocation.Text = "Bradfield";
            //        }
            //        if (sitePicker.SelectedIndex.Equals(4))
            //        {
            //            lblSiteLocation.Text = "Tobbacco Sales Floor";
            //        }
            //        if (sitePicker.SelectedIndex.Equals(5))
            //        {
            //            lblSiteLocation.Text = "Warren Park";
            //        }
            //    }
            //};

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var unitsBeforeGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            unitsBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            unitsBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            unitsBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            unitsBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            unitsBeforeGrid.Children.Add(entUnitsBefore);
            unitsBeforeGrid.Children.Add(entUnitsBeforeViewPhoto, 1, 0);
            unitsBeforeGrid.Children.Add(entUnitsBeforePickPhoto, 2, 0);
            unitsBeforeGrid.Children.Add(entUnitsBeforeTakePhoto, 3, 0);

            var unitsAfterGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            unitsAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            unitsAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            unitsAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            unitsAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            unitsAfterGrid.Children.Add(entUnitsAfter);
            unitsAfterGrid.Children.Add(entUnitsAfterViewPhoto, 1, 0);
            unitsAfterGrid.Children.Add(entUnitsAfterPickPhoto, 2, 0);
            unitsAfterGrid.Children.Add(entUnitsAfterTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Request Number:" },
                    requestNumberPicker,
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    lblMaintenanceCentre,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Site Name:" },
                    lblSite,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Units:" },
                    lblUnits,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Meter Number:" },
                    lblMeterNumber,
                    new HeaderLabel(){ Text = "" },
                    new HeaderLabel { Text = "Units Before:" },
                    unitsBeforeGrid,
                    new HeaderLabel { Text = "Units After:" },
                    unitsAfterGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Units Before":
                    unitsBeforePhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Units After":
                    unitsAfterPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Units Before":
                    unitsBeforePhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Units After":
                    unitsAfterPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Units Before":
                    if (unitsBeforePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Units Before field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, unitsBeforePhoto);
                    break;
                case "Units After":
                    if (unitsAfterPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Units After field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, unitsAfterPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (unitsBeforePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Units Before?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            if (unitsAfterPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Units After?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            Image levelBeforeImage = new Image();
            levelBeforeImage.Source = ImageSource.FromStream(() =>
            {
                var stream = unitsBeforePhoto.GetStream();
                return stream;
            });
            Image levelAfterImage = new Image();
            levelAfterImage.Source = ImageSource.FromStream(() =>
            {
                var stream = unitsAfterPhoto.GetStream();
                return stream;
            });
            Image hoursRunImage = new Image();
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.RechargeMeterSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
                BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            //maintenanceCentrePicker.SelectedIndex = -1;
            //sitePicker.SelectedIndex = -1;
            requestNumberPicker.SelectedIndex = -1;
            entUnitsBefore.Text = string.Empty;
            entUnitsAfter.Text = string.Empty;
            lblUnits.Text = string.Empty;
            lblSiteLocation.Text = string.Empty;
            lblMaintenanceCentre.Text = string.Empty;
            lblSite.Text = string.Empty;
            lblMeterNumber.Text = string.Empty;
            unitsBeforePhoto = null;
            unitsAfterPhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public async Task BindModel()
        {
            viewModel = new RechargeMeterViewModel();
            viewModel.MaintenanceCentre = lblMaintenanceCentre.Text;
            viewModel.Site = lblSite.Text;
            viewModel.RequestNumber = (requestNumberPicker.SelectedItem as string).Trim();
            viewModel.UnitsAfter = testUnitsAfter;
            viewModel.UnitsBefore = testUnitsBefore;
            viewModel.UnitsBeforeImage = await unitsBeforePhoto.ConvertToBase64();
            viewModel.UnitsAfterImage = await unitsAfterPhoto.ConvertToBase64();
        }

        public bool ValidateModel()
        {
            /*if (string.IsNullOrEmpty(lblMaintenanceCentre.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(lblSite.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(lblMeterNumber.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(lblSiteLocation.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }*/
            if (string.IsNullOrEmpty(requestNumberPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            testUnitsBefore = double.TryParse(entUnitsBefore.Text, out testUnitsBefore) ? testUnitsBefore : double.NaN;
            if (testUnitsBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Units Before field.", "Error");
                entUnitsBefore.Focus();
                return false;
            }
            testUnitsAfter = double.TryParse(entUnitsAfter.Text, out testUnitsAfter) ? testUnitsAfter : double.NaN;
            if (testUnitsAfter.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Units After field.", "Error");
                entUnitsAfter.Focus();
                return false;
            }

            if (testUnitsAfter <= testUnitsBefore)
            {
                BusinessLogic.ShowMessageBox(this, "Units After cannot be less than or equal to Units Before.", "Error");
                entUnitsAfter.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.RechargeMeterModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.UnitsBefore);
            hash.Append(viewModel.UnitsAfter);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.RechargeMeterModelHash = hash;
        }

        #endregion


    }
}
