﻿using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class RecordGeneratorReadings : ContentPage
    {
        CustomPicker zonePicker;
        CustomPicker subZonePicker;
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        HeaderLabel lblSiteLocation;
        CustomEntry entGenHoursRun;
        CustomEntry entGenFuelLevel;
        Button entGenHoursRunTakePhoto;
        Button entGenHoursRunPickPhoto;
        Button entGenHoursRunViewPhoto;
        MediaFile entGenHoursRunPhoto;
        Button entGenFuelLevelTakePhoto;
        Button entGenFuelLevelPickPhoto;
        Button entGenFuelLevelViewPhoto;
        MediaFile entGenFuelLevelPhoto;

        GeneratorReadingsViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testGenHoursRunBefore;
        double testGenFuelLevelBefore;
        bool submitWithoutFuelLevelImage = false;
        bool submitWithoutHoursRunImage = false;

        public RecordGeneratorReadings()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Log Generator Readings";
            entGenHoursRun = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entGenHoursRun.TextChanged += OnNumericEntryTextChanged;
            entGenHoursRun.Completed += (sender, e) => entGenFuelLevel.Focus();

            entGenHoursRunTakePhoto = new TakePhotoButton();
            entGenHoursRunPickPhoto = new PickPhotoButton();
            entGenHoursRunViewPhoto = new ViewPhotoButton();
            entGenHoursRunTakePhoto.AutomationId = entGenHoursRunPickPhoto.AutomationId = entGenHoursRunViewPhoto.AutomationId = "Hours Run";
            entGenHoursRunTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entGenHoursRunPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entGenHoursRunViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entGenFuelLevel = new CustomEntry() { Keyboard = Keyboard.Numeric };
            entGenFuelLevel.TextChanged += OnNumericEntryTextChanged;
            entGenFuelLevel.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);

            entGenFuelLevelTakePhoto = new TakePhotoButton();
            entGenFuelLevelPickPhoto = new PickPhotoButton();
            entGenFuelLevelViewPhoto = new ViewPhotoButton();
            entGenFuelLevelTakePhoto.AutomationId = entGenFuelLevelPickPhoto.AutomationId = entGenFuelLevelViewPhoto.AutomationId = "Current Level";
            entGenFuelLevelTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entGenFuelLevelPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entGenFuelLevelViewPhoto.Clicked += OnViewPhotoButtonClicked;

            lblSiteLocation = new HeaderLabel();

            zonePicker = new CustomPicker();
            zonePicker.AutomationId = "Zones";
            BusinessLogic.PopulatePicker(zonePicker, App.currentUser.SubSectionId);

            subZonePicker = new CustomPicker();
            subZonePicker.AutomationId = "Sub Zones";
            //BusinessLogic.PopulatePicker(subZonePicker);

            maintenanceCentrePicker = new CustomPicker();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            //BusinessLogic.PopulatePicker(maintenanceCentrePicker);
            //maintenanceCentrePicker.SelectedIndex = maintenanceCentrePicker.Items.IndexOf(App.currentUser.SubSection);

            sitePicker = new CustomPicker ();
            sitePicker.AutomationId = "Sites";
            //BusinessLogic.PopulatePicker(sitePicker, siteViewObjects, App.currentUser.SubSection);

            zonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = zonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = zonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subZonePicker, zones[zonePicker.Items[selectedIndex]]);
                }
            };

            subZonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subZonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = subZonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(maintenanceCentrePicker, zones[subZonePicker.Items[selectedIndex]]);
                }
            };

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var maintenanceCentres = maintenanceCentrePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentres[maintenanceCentrePicker.Items[selectedIndex]]);
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sites = sitePicker.ViewModel as List<SiteViewModel>;
                    var siteName = sitePicker.Items[selectedIndex];
                    var site = sites.Find(s => s.Name == siteName);
                    lblSiteLocation.Text = site.Location;
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var genGenHoursRunGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.Children.Add(entGenHoursRun);
            genGenHoursRunGrid.Children.Add(entGenHoursRunViewPhoto, 1, 0);
            genGenHoursRunGrid.Children.Add(entGenHoursRunPickPhoto, 2, 0);
            genGenHoursRunGrid.Children.Add(entGenHoursRunTakePhoto, 3, 0);

            var genFuelLevelGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.Children.Add(entGenFuelLevel);
            genFuelLevelGrid.Children.Add(entGenFuelLevelViewPhoto, 1, 0);
            genFuelLevelGrid.Children.Add(entGenFuelLevelPickPhoto, 2, 0);
            genFuelLevelGrid.Children.Add(entGenFuelLevelTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Zone:" },
                    zonePicker,
                    new HeaderLabel { Text = "Sub Zone:" },
                    subZonePicker,
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel { Text = "Hours Run:" },
                    genGenHoursRunGrid,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel { Text = "Fuel Level:" },
                    genFuelLevelGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }


        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Hours Run":
                    entGenHoursRunPhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Current Level":
                    entGenFuelLevelPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Hours Run":
                    entGenHoursRunPhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Current Level":
                    entGenFuelLevelPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Hours Run":
                    if (entGenHoursRunPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Hours Run field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entGenHoursRunPhoto);
                    break;
                case "Current Level":
                    if (entGenFuelLevelPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Fuel Level field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entGenFuelLevelPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            await BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (entGenHoursRunPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Hours Run?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutHoursRunImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutHoursRunImage = true;
            }
            if (entGenFuelLevelPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Fuel Level?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutFuelLevelImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutFuelLevelImage = true;
            }
            Image genHoursRunImage = new Image();
            genHoursRunImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entGenHoursRunPhoto.GetStream();
                return stream;
            });
            Image genFuelLevelImage = new Image();
            genFuelLevelImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entGenFuelLevelPhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.GeneratorReadingsSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            zonePicker.SelectedIndex = -1;
            subZonePicker.SelectedIndex = -1;
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            entGenHoursRun.Text = string.Empty;
            entGenHoursRunPhoto = null;
            entGenFuelLevel.Text = string.Empty;
            entGenFuelLevelPhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public async Task BindModel()
        {
            viewModel = new GeneratorReadingsViewModel();
            viewModel.MaintenanceCentre = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.GenHoursRun = testGenHoursRunBefore;
            viewModel.GenFuelLevel = testGenFuelLevelBefore;
            viewModel.LoggedBy = App.currentUser.Username;
            viewModel.GenHoursRunImage = await entGenHoursRunPhoto.ConvertToBase64();
            viewModel.GenFuelLevelImage = await entGenFuelLevelPhoto.ConvertToBase64();
            while ((viewModel.GenFuelLevelImage == null && !submitWithoutFuelLevelImage) || (viewModel.GenHoursRunImage == null && !submitWithoutHoursRunImage)) ;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(zonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subZonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            testGenHoursRunBefore = double.TryParse(entGenHoursRun.Text, out testGenHoursRunBefore) ? testGenHoursRunBefore : double.NaN;
            if (testGenHoursRunBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Hours Run field.", "Error");
                entGenHoursRun.Focus();
                return false;
            }
            testGenFuelLevelBefore = double.TryParse(entGenFuelLevel.Text, out testGenFuelLevelBefore) ? testGenFuelLevelBefore : double.NaN;
            if (testGenFuelLevelBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Fuel Level field.", "Error");
                entGenFuelLevel.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.GeneratorReadingsModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.GenHoursRun);
            hash.Append(viewModel.GenFuelLevel);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.GeneratorReadingsModelHash = hash;
        }

        #endregion

    }
}
