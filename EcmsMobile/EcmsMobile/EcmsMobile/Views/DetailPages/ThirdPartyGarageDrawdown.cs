﻿using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class ThirdPartyGarageDrawdown : ContentPage
    {
        CustomPicker maintenanceCentrePicker;
        CustomPicker requestNumberPicker;
        CustomPicker thirdPartyGaragePicker;
        CustomEntry entComment;
        HeaderLabel lblAuthorisedBy;
        HeaderLabel lblDateAuthorised;
        HeaderLabel lblQuantity;
        HeaderLabel lblFuelType;

        Button btnClear;
        Button btnSubmit;

        List<DrawdownViewModel> drawdownViewObjects;

        ThirdPartyWarehouseDrawdownViewModel viewModel;

        public ThirdPartyGarageDrawdown()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "ThirdParty Warehouse Drawdown";
            drawdownViewObjects = new List<DrawdownViewModel>();
            lblAuthorisedBy = new HeaderLabel();
            lblDateAuthorised = new HeaderLabel();
            lblQuantity = new HeaderLabel();
            lblFuelType = new HeaderLabel();

            requestNumberPicker = new CustomPicker ();
            requestNumberPicker.AutomationId = "Fuel Requests";
            BusinessLogic.PopulatePicker(requestNumberPicker, drawdownViewObjects, FuelUpTargets.Drawdown, App.currentUser.Username);
            requestNumberPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestNumberPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var picker = sender as Picker;
                    var requestId = picker.Items[selectedIndex];
                    var request = drawdownViewObjects.Find(r => r.RequestNumber == requestId);
                    lblQuantity.Text = request.Quantity.ToString();
                    lblFuelType.Text = request.FuelType;
                    lblAuthorisedBy.Text = request.AuthorisedBy;
                    lblDateAuthorised.Text = request.DateAuthorised;
                }
            };

            entComment = new CustomEntry();
            entComment.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            maintenanceCentrePicker = new CustomPicker ();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            BusinessLogic.PopulatePicker(maintenanceCentrePicker);

            thirdPartyGaragePicker = new CustomPicker ();
            thirdPartyGaragePicker.AutomationId = "Third Party Garage";
            BusinessLogic.PopulatePicker(thirdPartyGaragePicker, App.currentUser.Department);

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    BusinessLogic.PopulatePicker(thirdPartyGaragePicker, maintenanceCentrePicker.Items[selectedIndex]);
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Request Number:" },
                    requestNumberPicker,
                    new HeaderLabel() {Text = "" },
                    new HeaderLabel() {Text = "Fuel Type:" },
                    lblFuelType,
                    new HeaderLabel() {Text = "" },
                    new HeaderLabel() {Text = "Quantity:" },
                    lblQuantity,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Authorised By:" },
                    lblAuthorisedBy,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Date Authorised:" },
                    lblDateAuthorised,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Third-Party Garage:" },
                    thirdPartyGaragePicker,
                    new HeaderLabel() { Text = "" },
                    new HeaderLabel() { Text = "Comment" },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.ThirdPartyGarageDrawdownSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            maintenanceCentrePicker.SelectedIndex = -1;
            requestNumberPicker.SelectedIndex = -1;
            thirdPartyGaragePicker.SelectedIndex = -1;
            lblAuthorisedBy.Text = string.Empty;
            lblDateAuthorised.Text = string.Empty;
            lblFuelType.Text = string.Empty;
            lblQuantity.Text = string.Empty;
            drawdownViewObjects.Clear();
            viewModel = null;
        }

        #region Model Binding

        public void BindModel()
        {
            viewModel = new ThirdPartyWarehouseDrawdownViewModel();
            viewModel.MaintenanceCentre = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.RequestNumber = (requestNumberPicker.SelectedItem as string).Trim();
            viewModel.ThirdPartyGarage = (thirdPartyGaragePicker.SelectedItem as string).Trim();
            viewModel.Comment = entComment.Text;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(requestNumberPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(thirdPartyGaragePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Third Party Garage field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(entComment.Text))
            {
                BusinessLogic.ShowMessageBox(this, "The comment field cannot be empty.", "Input required");
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.ThirdPartyGarageDrawdownModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.ThirdPartyGarage);
            hash.Append(viewModel.Comment);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.ThirdPartyGarageDrawdownModelHash = hash;
        }

        #endregion

    }
}
