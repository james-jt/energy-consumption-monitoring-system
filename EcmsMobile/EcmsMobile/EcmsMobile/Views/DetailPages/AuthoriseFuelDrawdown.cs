﻿using EcmsModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class AuthoriseFuelDrawdown : ContentPage
    {
        CustomPicker requestPicker;
        HeaderLabel lblRequestor;
        HeaderLabel lblRequestDate;
        HeaderLabel lblDepartment;
        HeaderLabel lblFuelType;
        HeaderLabel lblQuantity;
        HeaderLabel lblSite;
        //Label lblRequestType;
        HeaderLabel lblRequestDetail;
        CustomEntry entComment;
        AuthoriseViewModel viewModel;
        Button btnReject;
        Button btnApprove;

        public AuthoriseFuelDrawdown()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Approve Fuel Request";
            entComment = new CustomEntry();
            lblRequestor = new HeaderLabel();
            lblRequestDate = new HeaderLabel();
            lblDepartment = new HeaderLabel();
            lblFuelType = new HeaderLabel();
            lblSite = new HeaderLabel();
            lblQuantity = new HeaderLabel();
            //lblRequestType = new Label();
            lblRequestDetail = new HeaderLabel();
            requestPicker = new CustomPicker ();
            requestPicker.AutomationId = "Fuel Requests For Authorisation";
            BusinessLogic.PopulatePicker(requestPicker, App.currentUser.Username);            
            requestPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var requestId = requestPicker.Items[selectedIndex];
                    var requests = requestPicker.ViewModel as List<RequestViewModel>;
                    var request = requests.Find(r => r.RequestNumber == requestId);
                    lblRequestor.Text = request.RequestedBy;
                    lblRequestDate.Text = request.DateRequested.ToString("dd/MM/yyyy HH:mm:ss");
                    lblDepartment.Text = request.Department;
                    lblSite.Text = request.Site;
                    lblQuantity.Text = request.Quantity.ToString();
                    lblFuelType.Text = request.FuelType;
                    lblRequestDetail.Text = request.RequestorComment;
                }
            };

            btnReject = new ActionButton() { Text = "Reject" };
            btnReject.Clicked += OnRejectButtonClicked;
            btnApprove = new ActionButton() { Text = "Approve" };
            btnApprove.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Request Number:" },
                    requestPicker,
                    new HeaderLabel { Text = "Requestor:" },
                    lblRequestor,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Date:" },
                    lblRequestDate,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Department:" },
                    lblDepartment,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Quantity Rquested:" },
                    lblQuantity,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Fuel Type:" },
                    lblFuelType,
                    new HeaderLabel { Text = "" },
                    //new HeaderLabel { Text = "Site:" },
                    //lblSite,
                    //new HeaderLabel { Text = "Request Type:" },
                    //lblRequestType,
                    new HeaderLabel { Text = "Additional Info:" },
                    lblRequestDetail,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Comment:" },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnReject,
                            btnApprove,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        public void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            Submit(true);
        }

        public void OnRejectButtonClicked(object sender, EventArgs e)
        {
            Submit(false);
        }

        public async void Submit(bool authorised)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (string.IsNullOrEmpty(viewModel.ActionerComment))
            {
                var response = await this.DisplayActionSheet("Submit without comment?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            string decision = authorised ? "approve" : "reject";
            var confirmDecision = await this.DisplayActionSheet($"Are you sure you want to {decision} this request?", null, null, "Yes", "No");
            if (confirmDecision == "No")
            {
                BusinessLogic.ShowMessageBox(this, "Action aborted.");
                return;
            }
            viewModel.Authorised = authorised;
            if (!(await BusinessLogic.AuthoriseTR1Submission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error encountered.", "Error");
                return;
            }
            else
            {
                string message = authorised ? "Request approved." : "Request rejected.";
                BusinessLogic.ShowMessageBox(this, message, "Info");
                BusinessLogic.PopulatePicker(requestPicker, App.currentUser.Username);
            }

        }

        #region Model Binding

        public void BindModel()
        {
            viewModel = new AuthoriseViewModel();
            viewModel.RequestNumber = (requestPicker.SelectedItem as string).Trim();
            viewModel.Authorised = false;
            viewModel.ActionedBy = App.currentUser.Username;
            viewModel.ActionerComment = entComment.Text;
            viewModel.DateActioned = DateTime.Now;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(requestPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            return true;
        }

        #endregion
    }
}
