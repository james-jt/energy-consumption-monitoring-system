﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using EcmsModels.ViewModels;

namespace EcmsMobile
{ 
    class RequestVehicleFuelDrawdown : ContentPage
    {
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        Label lblSiteLocation;
        Label lblRequestType;
        Label lblRequestNumber;
        CustomEntry entComment;
        RequestTokenSubmissionViewModel viewModel;
        Button btnClear;
        Button btnSubmit;
         
        public RequestVehicleFuelDrawdown()
        {
            BackgroundImage = "background.png";

            Title = "Request Fuel Drawdown";
            ScrollView scrollView = new ScrollView();
            sitePicker = new CustomPicker ();
            entComment = new CustomEntry();
            lblSiteLocation = new Label();
            lblRequestType = new Label();
            lblRequestNumber = new Label();

            maintenanceCentrePicker = new CustomPicker ();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            BusinessLogic.PopulatePicker(maintenanceCentrePicker);

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentrePicker.Items[selectedIndex]);
                    lblSiteLocation.Text = string.Empty;
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    lblSiteLocation.Text = sitePicker.Items[selectedIndex];
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Type:" },
                    lblRequestType,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Number:" },
                    lblRequestNumber,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Comment:" },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (string.IsNullOrEmpty(entComment.Text))
            {
                var response = await this.DisplayActionSheet("Submit without comment?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.RequestRechargeTokenSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            entComment.Text = string.Empty;
            viewModel = null;
        }

        #endregion

        #region Model Binding

        public void BindModel()
        {
            viewModel = new RequestTokenSubmissionViewModel();
            viewModel.Department = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.RequestNumber = lblRequestNumber.Text;
            viewModel.RequestType = lblRequestType.Text;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.RequestRechargeTokenModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.Department);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.RequestType);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.RequestRechargeTokenModelHash = hash;
        }

        #endregion


    }
}
