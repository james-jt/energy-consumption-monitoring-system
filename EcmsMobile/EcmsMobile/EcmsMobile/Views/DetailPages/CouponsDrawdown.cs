﻿using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class CouponsDrawdown : ContentPage
    {
        CustomPicker requestNumberPicker;
        CustomEntry entCouponNumber;
        CustomEntry entComment;

        HeaderLabel lblAuthorisedBy;
        HeaderLabel lblDateAuthorised;
        HeaderLabel lblQuantity;
        HeaderLabel lblFuelType;

        Button btnClear;
        Button btnSubmit;

        List<DrawdownViewModel> drawdownViewObjects;

        CouponsDrawdownViewModel viewModel;

        string testEntCouponNumber;

        public CouponsDrawdown()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Coupons Drawdown";
            entCouponNumber = new CustomEntry() { ReturnType = ReturnType.Next };
            entCouponNumber.Completed += (sender, e) => entComment.Focus();
            entComment = new CustomEntry() { ReturnType = ReturnType.Done };
            entComment.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            drawdownViewObjects = new List<DrawdownViewModel>();
            lblAuthorisedBy = new HeaderLabel();
            lblDateAuthorised = new HeaderLabel();
            lblQuantity = new HeaderLabel();
            lblFuelType = new HeaderLabel();

            //quantity = new CustomEntry();
            //quantity.IsEnabled = false;
            //quantity.Text = "25" + " litres";

            requestNumberPicker = new CustomPicker ();
            requestNumberPicker.AutomationId = "Fuel Requests";
            BusinessLogic.PopulatePicker(requestNumberPicker, drawdownViewObjects, FuelUpTargets.Drawdown, App.currentUser.Username);
            requestNumberPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestNumberPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var picker = sender as Picker;
                    var requestId = picker.Items[selectedIndex];
                    var request = drawdownViewObjects.Find(r => r.RequestNumber == requestId);
                    lblQuantity.Text = request.Quantity.ToString();
                    lblFuelType.Text = request.FuelType;
                    lblAuthorisedBy.Text = request.AuthorisedBy;
                    lblDateAuthorised.Text = request.DateAuthorised;
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Request Number:" },
                    requestNumberPicker,
                    new HeaderLabel() {Text = "" },
                    new HeaderLabel() {Text = "Fuel Type:" },
                    lblFuelType,
                    new HeaderLabel() {Text = "" },
                    new HeaderLabel() {Text = "Quantity:" },
                    lblQuantity,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Authorised By:" },
                    lblAuthorisedBy,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Date Authorised:" },
                    lblDateAuthorised,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel { Text = "Coupon Number:" },
                    entCouponNumber,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel() { Text = "Comment:" },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.CouponsDrawdownSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            requestNumberPicker.SelectedIndex = -1;
            entCouponNumber.Text = string.Empty;
            lblAuthorisedBy.Text = string.Empty;
            lblDateAuthorised.Text = string.Empty;
            lblFuelType.Text = string.Empty;
            lblQuantity.Text = string.Empty;
            drawdownViewObjects.Clear();
            viewModel = null;
        }

        #region Model Binding

        public void BindModel()
        {
            viewModel = new CouponsDrawdownViewModel();
            viewModel.RequestNumber = (requestNumberPicker.SelectedItem as string).Trim();
            viewModel.Coupons = testEntCouponNumber;
            viewModel.Comment = entComment.Text;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(requestNumberPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(entCouponNumber.Text) || !BusinessLogic.IsCouponsInputValid(entCouponNumber.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Coupons field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(entComment.Text))
            {
                BusinessLogic.ShowMessageBox(this, "The comment field cannot be empty.", "Input required");
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.CouponsDrawdownModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.Coupons);
            hash.Append(viewModel.Comment);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.CouponsDrawdownModelHash = hash;
        }

        #endregion

    }
}
