﻿using EcmsModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    class AuthoriseRechargeTokenPurchase : ContentPage
    {
        CustomPicker requestPicker;
        HeaderLabel lblRequestor;
        HeaderLabel lblRequestDate;
        HeaderLabel lblMaintenanceCentre;
        HeaderLabel lblUnits;
        HeaderLabel lblSite;
        HeaderLabel lblRequestType;
        HeaderLabel lblRequestDetail;
        CustomEntry entComment;
        AuthoriseViewModel viewModel;
        Button btnReject;
        Button btnApprove;

        public AuthoriseRechargeTokenPurchase()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Approve Token Request";
            entComment = new CustomEntry();
            lblRequestor = new HeaderLabel();
            lblRequestDate = new HeaderLabel();
            lblMaintenanceCentre = new HeaderLabel();
            lblUnits = new HeaderLabel();
            lblSite = new HeaderLabel();
            lblRequestType = new HeaderLabel();
            lblRequestDetail = new HeaderLabel();
            requestPicker = new CustomPicker ();
            requestPicker.AutomationId = "Electricity Requests For Authorisation";
            BusinessLogic.PopulatePicker(requestPicker, App.currentUser.Username);
            requestPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var picker = sender as Picker;
                    var requestId = picker.Items[selectedIndex];
                    var requests = requestPicker.ViewModel as List<RequestViewModel>;
                    var request = requests.Find(r => r.RequestNumber == requestId);
                    lblRequestor.Text = request.RequestedBy;
                    lblRequestDate.Text = request.DateRequested.ToString("dd/MM/yyyy HH:mm:ss");
                    lblMaintenanceCentre.Text = request.Department;
                    lblSite.Text = request.Site;
                    lblUnits.Text = request.Quantity.ToString();
                    lblRequestDetail.Text = request.RequestorComment;
                }
            };

            btnReject = new ActionButton() { Text = "Reject" };
            btnReject.Clicked += OnRejectButtonClicked;
            btnApprove = new ActionButton() { Text = "Approve" };
            btnApprove.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Request Number:" },
                    requestPicker,
                    new HeaderLabel { Text = "Requestor:" },
                    lblRequestor,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Date:" },
                    lblRequestDate,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    lblMaintenanceCentre,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Site:" },
                    lblSite,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Units Requested:" },
                    lblUnits,
                    new HeaderLabel { Text = "" },
                    //new HeaderLabel { Text = "Request Type:" },
                    //lblRequestType,
                    new HeaderLabel { Text = "Additional Info:" },
                    lblRequestDetail,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Comment:" },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnReject,
                            btnApprove,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        public void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            Submit(true);
        }

        public void OnRejectButtonClicked(object sender, EventArgs e)
        {
            Submit(false);
        }

        public async void Submit(bool authorised)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (string.IsNullOrEmpty(viewModel.ActionerComment))
            {
                var response = await this.DisplayActionSheet("Submit without comment?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            string decision = authorised ? "approve" : "reject";
            var confirmDecision = await this.DisplayActionSheet($"Are you sure you want to {decision} this request?", null, null, "Yes", "No");
            if (confirmDecision == "No")
            {
                BusinessLogic.ShowMessageBox(this, "Action aborted.");
                return;
            }
            viewModel.Authorised = authorised;
            if (!(await BusinessLogic.AuthoriseTokenPurchaseSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error encountered.", "Error");
                return;
            }
            else
            {
                string message = authorised ? "Request approved." : "Request rejected.";
                BusinessLogic.ShowMessageBox(this, message, "Info");
                BusinessLogic.PopulatePicker(requestPicker, App.currentUser.Username);
            }

        }

        #region Model Binding

        public void BindModel()
        {
            viewModel = new AuthoriseViewModel();
            viewModel.RequestNumber = (requestPicker.SelectedItem as string).Trim();
            viewModel.Authorised = false;
            viewModel.ActionedBy = App.currentUser.Username;
            viewModel.ActionerComment = entComment.Text;
            viewModel.DateActioned = DateTime.Now;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(requestPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            return true;
        }

        #endregion
    }
}
