﻿using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EcmsMobile
{  
    class RecordVehicleReadings : ContentPage
    {
        CustomPicker departmentPicker;
        CustomPicker sectionPicker;
        CustomPicker subSectionPicker;
        CustomPicker vehiclePicker; 
        CustomEntry entMileage;
        CustomEntry entFuelLevel;
        Button entMileageTakePhoto;
        Button entMileagePickPhoto;
        Button entMileageViewPhoto;
        MediaFile entMileagePhoto;
        Button entFuelLevelTakePhoto;
        Button entFuelLevelPickPhoto;
        Button entFuelLevelViewPhoto;
        MediaFile entFuelLevelPhoto;

        VehicleReadingsViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testMileageBefore;
        double testFuelLevelBefore;
        bool submitWithoutFuelLevelImage = false;
        bool submitWithoutMileageImage = false;

        public RecordVehicleReadings()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Log Vehicle Readings";
            entMileage = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entMileage.TextChanged += OnNumericEntryTextChanged;
            entMileage.Completed += (sender, e) => entFuelLevel.Focus();

            entMileageTakePhoto = new TakePhotoButton();
            entMileagePickPhoto = new PickPhotoButton();
            entMileageViewPhoto = new ViewPhotoButton();
            entMileageTakePhoto.AutomationId = entMileagePickPhoto.AutomationId = entMileageViewPhoto.AutomationId = "Mileage";
            entMileageTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entMileagePickPhoto.Clicked += OnPickPhotoButtonClicked;
            entMileageViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entFuelLevel = new CustomEntry() { Keyboard = Keyboard.Numeric };
            entFuelLevel.TextChanged += OnNumericEntryTextChanged;
            entFuelLevel.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);

            entFuelLevelTakePhoto = new TakePhotoButton();
            entFuelLevelPickPhoto = new PickPhotoButton();
            entFuelLevelViewPhoto = new ViewPhotoButton();
            entFuelLevelTakePhoto.AutomationId = entFuelLevelPickPhoto.AutomationId = entFuelLevelViewPhoto.AutomationId = "Current Level";
            entFuelLevelTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entFuelLevelPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entFuelLevelViewPhoto.Clicked += OnViewPhotoButtonClicked;

            departmentPicker = new CustomPicker ();
            departmentPicker.AutomationId = "Departments";
            BusinessLogic.PopulatePicker(departmentPicker);

            sectionPicker = new CustomPicker();
            sectionPicker.AutomationId = "Sections";
            //BusinessLogic.PopulatePicker(sectionPicker);

            subSectionPicker = new CustomPicker();
            subSectionPicker.AutomationId = "Sub Sections";
            //BusinessLogic.PopulatePicker(subSectionPicker);

            vehiclePicker = new CustomPicker { HeightRequest = 40, Image = "ic_arrow_drop_down" };
            vehiclePicker.AutomationId = "Vehicle Registration Numbers";
            //BusinessLogic.PopulatePicker(vehiclePicker, App.currentUser.SubSection);

            departmentPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = departmentPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var departments = departmentPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sectionPicker, departments[departmentPicker.Items[selectedIndex]]);
                }
            };

            sectionPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sectionPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sections = sectionPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subSectionPicker, sections[sectionPicker.Items[selectedIndex]]);
                }
            };

            subSectionPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subSectionPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sections = subSectionPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(vehiclePicker, sections[subSectionPicker.Items[selectedIndex]]);
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var genGenHoursRunGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.Children.Add(entMileage);
            genGenHoursRunGrid.Children.Add(entMileageViewPhoto, 1, 0);
            genGenHoursRunGrid.Children.Add(entMileagePickPhoto, 2, 0);
            genGenHoursRunGrid.Children.Add(entMileageTakePhoto, 3, 0);

            var genFuelLevelGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genFuelLevelGrid.Children.Add(entFuelLevel);
            genFuelLevelGrid.Children.Add(entFuelLevelViewPhoto, 1, 0);
            genFuelLevelGrid.Children.Add(entFuelLevelPickPhoto, 2, 0);
            genFuelLevelGrid.Children.Add(entFuelLevelTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Department:" },
                    departmentPicker,
                    new HeaderLabel { Text = "Section:" },
                    sectionPicker,
                    new HeaderLabel { Text = "Sub Section:" },
                    subSectionPicker,
                    new HeaderLabel { Text = "Vehicle Registration Number:" },
                    vehiclePicker,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel { Text = "Mileage:" },
                    genGenHoursRunGrid,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel { Text = "Fuel Level:" },
                    genFuelLevelGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }


        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Mileage":
                    entMileagePhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Current Level":
                    entFuelLevelPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Mileage":
                    entMileagePhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Current Level":
                    entFuelLevelPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Mileage":
                    if (entMileagePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Mileage field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entMileagePhoto);
                    break;
                case "Current Level":
                    if (entFuelLevelPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Fuel Level field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entFuelLevelPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            await BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (entMileagePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Mileage?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutMileageImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutMileageImage = true;
            }
            if (entFuelLevelPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Fuel Level?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutFuelLevelImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutFuelLevelImage = true;
            }
            Image genHoursRunImage = new Image();
            genHoursRunImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entMileagePhoto.GetStream();
                return stream;
            });
            Image genFuelLevelImage = new Image();
            genFuelLevelImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entFuelLevelPhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.VehicleReadingsSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            departmentPicker.SelectedIndex = -1;
            sectionPicker.SelectedIndex = -1;
            subSectionPicker.SelectedIndex = -1;
            vehiclePicker.SelectedIndex = -1;
            entMileage.Text = string.Empty;
            entMileagePhoto = null;
            entFuelLevel.Text = string.Empty;
            entFuelLevelPhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public async Task BindModel()
        {
            viewModel = new VehicleReadingsViewModel();
            viewModel.VehicleRegistrationNumber = (vehiclePicker.SelectedItem as string).Trim();
            viewModel.Mileage = testMileageBefore;
            viewModel.FuelLevel = testFuelLevelBefore;
            viewModel.LoggedBy = App.currentUser.Username;
            viewModel.MileageImage = await entMileagePhoto.ConvertToBase64();
            viewModel.FuelLevelImage = await entFuelLevelPhoto.ConvertToBase64();
            while ((viewModel.FuelLevelImage == null && !submitWithoutFuelLevelImage) || (viewModel.MileageImage == null && !submitWithoutMileageImage)) ;

        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(departmentPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Department field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sectionPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Section field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subSectionPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Section field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(vehiclePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Vehicle Registration Number field.", "Error");
                return false;
            }
            testMileageBefore = double.TryParse(entMileage.Text, out testMileageBefore) ? testMileageBefore : double.NaN;
            if (testMileageBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Mileage field.", "Error");
                entMileage.Focus();
                return false;
            }
            testFuelLevelBefore = double.TryParse(entFuelLevel.Text, out testFuelLevelBefore) ? testFuelLevelBefore : double.NaN;
            if (testFuelLevelBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Fuel Level field.", "Error");
                entFuelLevel.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.VehicleReadingsModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.VehicleRegistrationNumber);
            hash.Append(viewModel.Mileage);
            hash.Append(viewModel.FuelLevel);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.VehicleReadingsModelHash = hash;
        }

        #endregion

    }
}
