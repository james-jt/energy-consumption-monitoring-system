﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;

namespace EcmsMobile
{ 
    class Drawdown : ContentPage
    {
        CustomPicker requestNumberPicker;
        CustomPicker sourceTypePicker;
        CustomPicker thirdPartyGaragePicker;
        CustomPicker warehousePicker;

        CustomEntry entCouponNumber;
        CustomEntry entComment;
        HeaderLabel sourceLabel;

        HeaderLabel lblAuthorisedBy;
        HeaderLabel lblDateAuthorised;
        HeaderLabel lblQuantity;
        HeaderLabel lblFuelType;

        Button btnClear;
        Button btnSubmit;
        DrawdownViewModel viewModel;
         
        public Drawdown()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Fuel Drawdown";
            lblAuthorisedBy = new HeaderLabel();
            lblDateAuthorised = new HeaderLabel();
            lblQuantity = new HeaderLabel();
            lblFuelType = new HeaderLabel();

            requestNumberPicker = new CustomPicker ();
            requestNumberPicker.AutomationId = "Drawdown Fuel Requests";
            BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);
            requestNumberPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestNumberPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var requestId = requestNumberPicker.Items[selectedIndex];
                    var requests = requestNumberPicker.ViewModel as List<DrawdownViewModel>;
                    var request = requests.Find(r => r.RequestNumber == requestId);
                    lblQuantity.Text = request.Quantity.ToString();
                    lblFuelType.Text = request.FuelType;
                    lblAuthorisedBy.Text = request.AuthorisedBy;
                    lblDateAuthorised.Text = request.DateAuthorised;
                }                                                  
            };

            sourceTypePicker = new CustomPicker();
            sourceTypePicker.AutomationId = "Drawdown Sources";
            BusinessLogic.PopulatePicker(sourceTypePicker);
            sourceTypePicker.SelectedIndexChanged += (sender, e) =>
            {
                var source = sourceTypePicker.SelectedIndex;
                if (source.Equals(0))
                {
                    sourceLabel.Text = "Warehouse:";
                    warehousePicker.IsVisible = true;
                    thirdPartyGaragePicker.IsVisible = false;
                    entCouponNumber.IsVisible = false;
                }
                if (source.Equals(1))
                {
                    sourceLabel.Text = "Garage:";
                    warehousePicker.IsVisible = false;
                    thirdPartyGaragePicker.IsVisible = true;
                    entCouponNumber.IsVisible = false;
                }
                if (source.Equals(2))
                {
                    sourceLabel.Text = "Coupons:";
                    warehousePicker.IsVisible = false;
                    thirdPartyGaragePicker.IsVisible = false;
                    entCouponNumber.IsVisible = true;
                }
            };

            thirdPartyGaragePicker = new CustomPicker();
            thirdPartyGaragePicker.AutomationId = "Third Party Garages";
            BusinessLogic.PopulatePicker(thirdPartyGaragePicker, App.currentUser.SubSection);
            thirdPartyGaragePicker.IsVisible = false;

            warehousePicker = new CustomPicker ();
            warehousePicker.AutomationId = "Warehouses";
            BusinessLogic.PopulatePicker(warehousePicker);
            warehousePicker.IsVisible = true;

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            entCouponNumber = new CustomEntry() { Keyboard = Keyboard.Text, ReturnType = ReturnType.Next }; ;
            entCouponNumber.IsVisible = false;
            entCouponNumber.Completed += (sender, e) => entComment.Focus();

            entComment = new CustomEntry();
            entComment.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);

            sourceLabel = new HeaderLabel() { Text = "Warehouse" };

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Request Number:" },
                    requestNumberPicker,
                    new HeaderLabel() {Text = "" },
                    new HeaderLabel() {Text = "Fuel Type:" },
                    lblFuelType,
                    new HeaderLabel() {Text = "" },
                    new HeaderLabel() {Text = "Quantity:" },
                    lblQuantity,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Authorised By:" },
                    lblAuthorisedBy,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Date Authorised:" },
                    lblDateAuthorised,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel { Text = "Source:" },
                    sourceTypePicker,
                    sourceLabel,
                    thirdPartyGaragePicker,
                    warehousePicker,
                    entCouponNumber,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() { Text = "Comment:" },
                    entComment,

                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "The view model was not populated.", "Error");
                return;
            }
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.DrawdownSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
                BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            requestNumberPicker.SelectedIndex = -1;
            warehousePicker.SelectedIndex = -1;
            lblAuthorisedBy.Text = string.Empty;
            lblDateAuthorised.Text = string.Empty;
            lblFuelType.Text = string.Empty;
            lblQuantity.Text = string.Empty;
            sourceTypePicker.SelectedIndex = -1;
            entCouponNumber.Text = string.Empty;
            thirdPartyGaragePicker.SelectedIndex = -1;
            entCouponNumber.Text = null;
            viewModel = null;
        }

        #region Model Binding

        public void BindModel()
        {
            viewModel = new DrawdownViewModel();
            viewModel.RequestNumber = (requestNumberPicker.SelectedItem as string).Trim();
            viewModel.SourceType = (requestNumberPicker.SelectedItem as string).Trim();
            viewModel.Source = GetSource();
            viewModel.Comment = entComment.Text;
        }

        private string GetSource()
        {
            var sourceType = sourceTypePicker.SelectedIndex;
            string source = string.Empty;
            if (sourceType.Equals(0))
            {
                source = warehousePicker.SelectedItem as string;
            }
            if (sourceType.Equals(1))
            {
                source = thirdPartyGaragePicker.SelectedItem as string;
            }
            if (sourceType.Equals(2))
            {
                source = entCouponNumber.Text.Trim();
            }
            return source;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(requestNumberPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sourceTypePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Source field.", "Error");
                return false;
            }
            if (sourceTypePicker.SelectedIndex.Equals(0))
            {
                if (string.IsNullOrEmpty(warehousePicker.SelectedItem as string))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value for the Warehouse field.", "Error");
                    return false;
                }
            }
            if (sourceTypePicker.SelectedIndex.Equals(1))
            {
                if (string.IsNullOrEmpty(thirdPartyGaragePicker.SelectedItem as string))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value for the Garage field.", "Error");
                    return false;
                }
            }
            if (sourceTypePicker.SelectedIndex.Equals(2))
            {
                entCouponNumber.Text = BusinessLogic.SanitizeCouponNumber(entCouponNumber.Text);
                string errorMsg = string.Empty;
                if (!BusinessLogic.IsCouponsInputValid(entCouponNumber.Text, ref errorMsg))
                {
                    BusinessLogic.ShowMessageBox(this, errorMsg, "Error");
                    return false;
                }
            }
            if (string.IsNullOrEmpty(entComment.Text))
            {
                BusinessLogic.ShowMessageBox(this, "The comment field cannot be empty.", "Input required");
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.DrawdownModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.Source);
            hash.Append(viewModel.Comment);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.DrawdownModelHash = hash;
        }

        #endregion

    }
}
