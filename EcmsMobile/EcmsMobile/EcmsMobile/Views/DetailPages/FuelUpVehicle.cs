﻿using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using System.Threading.Tasks;

namespace EcmsMobile
{ 
    class FuelUpVehicle : ContentPage
    {
        CustomPicker departmentPicker;
        CustomPicker sectionPicker;
        CustomPicker subSectionPicker;
        CustomPicker vehiclePicker;
        CustomPicker requestNumberPicker;
        CustomPicker sourceTypePicker;
        CustomPicker thirdPartyGaragePicker;
        CustomPicker warehousePicker;

        HeaderLabel sourceLabel;
        HeaderLabel lblAuthorisedBy;
        HeaderLabel lblDateAuthorised;
        HeaderLabel lblFuelType;
        HeaderLabel lblAvailableQuantity;
        CustomEntry entLevelBefore;
        CustomEntry entLevelAfter;
        CustomEntry entMileage;
        CustomEntry entCouponNumber;
        Button entLevelBeforeTakePhoto;
        Button entLevelBeforePickPhoto;
        Button entLevelBeforeViewPhoto;
        Button entLevelAfterTakePhoto;
        Button entLevelAfterPickPhoto;
        Button entLevelAfterViewPhoto;
        Button entMileageTakePhoto;
        Button entMileagePickPhoto;
        Button entMileageViewPhoto;
        MediaFile levelBeforePhoto;
        MediaFile levelAfterPhoto;
        MediaFile mileagePhoto;
        FuelUpVehicleViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testLevelBefore;
        double testLevelAfter;
        double testMileage;
        bool submitWithoutLevelBeforeImage = false;
        bool submitWithoutLevelAfterImage = false;
        bool submitWithoutMileageImage = false;

        public FuelUpVehicle()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            lblAvailableQuantity = new HeaderLabel();
            lblAuthorisedBy = new HeaderLabel();
            lblDateAuthorised = new HeaderLabel();
            lblFuelType = new HeaderLabel();

            Title = "Fuel Up Vehicle";
            entLevelBefore = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entLevelBefore.TextChanged += OnNumericEntryTextChanged;
            entLevelBefore.Completed += (sender, e) => entLevelAfter.Focus();
            entLevelBeforeTakePhoto = new TakePhotoButton();
            entLevelBeforePickPhoto = new PickPhotoButton();
            entLevelBeforeViewPhoto = new ViewPhotoButton();
            entLevelBeforeTakePhoto.AutomationId = entLevelBeforePickPhoto.AutomationId = entLevelBeforeViewPhoto.AutomationId = "Level Before";
            entLevelBeforeTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entLevelBeforePickPhoto.Clicked += OnPickPhotoButtonClicked;
            entLevelBeforeViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entLevelAfter = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entLevelAfter.TextChanged += OnNumericEntryTextChanged;
            entLevelAfter.Completed += (sender, e) => entMileage.Focus();
            entLevelAfterTakePhoto = new TakePhotoButton();
            entLevelAfterPickPhoto = new PickPhotoButton();
            entLevelAfterViewPhoto = new ViewPhotoButton();
            entLevelAfterTakePhoto.AutomationId = entLevelAfterPickPhoto.AutomationId = entLevelAfterViewPhoto.AutomationId = "Level After";
            entLevelAfterTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entLevelAfterPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entLevelAfterViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entMileage = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Done };
            entMileage.TextChanged += OnNumericEntryTextChanged;
            entMileage.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            entMileageTakePhoto = new TakePhotoButton();
            entMileagePickPhoto = new PickPhotoButton();
            entMileageViewPhoto = new ViewPhotoButton();
            entMileageTakePhoto.AutomationId = entMileagePickPhoto.AutomationId = entMileageViewPhoto.AutomationId = "Mileage";
            entMileageTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entMileagePickPhoto.Clicked += OnPickPhotoButtonClicked;
            entMileageViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entCouponNumber = new CustomEntry() { Keyboard = Keyboard.Text, ReturnType = ReturnType.Next }; ;
            entCouponNumber.IsVisible = false;
            entCouponNumber.Completed += (sender, e) => entLevelBefore.Focus();

            departmentPicker = new CustomPicker ();
            departmentPicker.AutomationId = "Departments";
            BusinessLogic.PopulatePicker(departmentPicker);

            sectionPicker = new CustomPicker();
            sectionPicker.AutomationId = "Sections";
            //BusinessLogic.PopulatePicker(sectionPicker);

            subSectionPicker = new CustomPicker();
            subSectionPicker.AutomationId = "Sub Sections";
            //BusinessLogic.PopulatePicker(subSectionPicker);

            vehiclePicker = new CustomPicker { HeightRequest = 40, Image = "ic_arrow_drop_down" };
            vehiclePicker.AutomationId = "Vehicle Registration Numbers";
            //BusinessLogic.PopulatePicker(vehiclePicker, App.currentUser.Username);

            departmentPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = departmentPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var departments = departmentPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sectionPicker, departments[departmentPicker.Items[selectedIndex]]);
                }
            };

            sectionPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sectionPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sections = sectionPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subSectionPicker, sections[sectionPicker.Items[selectedIndex]]);
                }
            };

            subSectionPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subSectionPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sections = subSectionPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(vehiclePicker, sections[subSectionPicker.Items[selectedIndex]]);
                }
            };

            requestNumberPicker = new CustomPicker ();
            requestNumberPicker.AutomationId = "Vehicle Fuel Requests";
            //BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);
            BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);

            requestNumberPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestNumberPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var picker = sender as Picker;
                    var requestId = picker.Items[selectedIndex];
                    var requests = requestNumberPicker.ViewModel as List<DrawdownViewModel>;
                    var request = requests.Find(r => r.RequestNumber == requestId);
                    lblAvailableQuantity.Text = request.Quantity.ToString();
                    lblFuelType.Text = request.FuelType;
                    lblAuthorisedBy.Text = request.AuthorisedBy;
                    lblDateAuthorised.Text = request.DateAuthorised;
                }
            };

            sourceTypePicker = new CustomPicker ();
            sourceTypePicker.AutomationId = "Drawdown Sources";
            BusinessLogic.PopulatePicker(sourceTypePicker);
            sourceTypePicker.SelectedIndexChanged += (sender, e) =>
            {
                var source = sourceTypePicker.SelectedIndex;
                if (source.Equals(0))
                {
                    sourceLabel.Text = "Warehouse:";
                    warehousePicker.IsVisible = true;
                    thirdPartyGaragePicker.IsVisible = false;
                    entCouponNumber.IsVisible = false;
                }
                if (source.Equals(1))
                {
                    sourceLabel.Text = "Garage:";
                    warehousePicker.IsVisible = false;
                    thirdPartyGaragePicker.IsVisible = true;
                    entCouponNumber.IsVisible = false;
                }
                if (source.Equals(2))
                {
                    sourceLabel.Text = "Coupons:";
                    warehousePicker.IsVisible = false;
                    thirdPartyGaragePicker.IsVisible = false;
                    entCouponNumber.IsVisible = true;
                }
            };

            thirdPartyGaragePicker = new CustomPicker();
            thirdPartyGaragePicker.AutomationId = "Third Party Garages";
            BusinessLogic.PopulatePicker(thirdPartyGaragePicker, App.currentUser.SubSection);
            thirdPartyGaragePicker.IsVisible = false;

            warehousePicker = new CustomPicker();
            warehousePicker.AutomationId = "Warehouses";
            BusinessLogic.PopulatePicker(warehousePicker);
            warehousePicker.IsVisible = true;

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var levelBeforeGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelBeforeGrid.Children.Add(entLevelBefore);
            levelBeforeGrid.Children.Add(entLevelBeforeViewPhoto, 1, 0);
            levelBeforeGrid.Children.Add(entLevelBeforePickPhoto, 2, 0);
            levelBeforeGrid.Children.Add(entLevelBeforeTakePhoto, 3, 0);

            var levelAfterGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelAfterGrid.Children.Add(entLevelAfter);
            levelAfterGrid.Children.Add(entLevelAfterViewPhoto, 1, 0);
            levelAfterGrid.Children.Add(entLevelAfterPickPhoto, 2, 0);
            levelAfterGrid.Children.Add(entLevelAfterTakePhoto, 3, 0);

            var mileageGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            mileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            mileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            mileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            mileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            mileageGrid.Children.Add(entMileage);
            mileageGrid.Children.Add(entMileageViewPhoto, 1, 0);
            mileageGrid.Children.Add(entMileagePickPhoto, 2, 0);
            mileageGrid.Children.Add(entMileageTakePhoto, 3, 0);

            sourceLabel = new HeaderLabel() { Text = "Warehouse" };

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Department:" },
                    departmentPicker,
                    new HeaderLabel { Text = "Section:" },
                    sectionPicker,
                    new HeaderLabel { Text = "Sub Section:" },
                    subSectionPicker,
                    new HeaderLabel { Text = "Vehicle Registration Number:" },
                    vehiclePicker,
                    new HeaderLabel { Text = "Request Number:" },
                    requestNumberPicker,
                    new HeaderLabel { Text = "Fuel Type:" },
                    lblFuelType,
                    new HeaderLabel{ Text = "" },
                    new HeaderLabel { Text = "Available Quantity:" },
                    lblAvailableQuantity,
                    new HeaderLabel{ Text = "" },
                    new HeaderLabel() {Text = "Authorised By:" },
                    lblAuthorisedBy,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel() {Text = "Date Authorised:" },
                    lblDateAuthorised,
                    new HeaderLabel() {Text = ""},
                    new HeaderLabel { Text = "Source:" },
                    sourceTypePicker,
                    sourceLabel,
                    thirdPartyGaragePicker,
                    warehousePicker,
                    entCouponNumber,
                    new HeaderLabel { Text = "Level Before:" },
                    levelBeforeGrid,
                    new HeaderLabel { Text = "Level After:" },
                    levelAfterGrid,
                    new HeaderLabel { Text = "Mileage:" },
                    mileageGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Level Before":
                    levelBeforePhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Level After":
                    levelAfterPhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Mileage":
                    mileagePhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Level Before":
                    levelBeforePhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Level After":
                    levelAfterPhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Mileage":
                    mileagePhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Level Before":
                    if (levelBeforePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Level Before field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, levelBeforePhoto);
                    break;
                case "Level After":
                    if (levelAfterPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Level After field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, levelAfterPhoto);
                    break;
                case "Mileage":
                    if (mileagePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Mileage field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, mileagePhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            await BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "The view model was not populated.", "Error");
                return;
            }
            if (levelBeforePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Level Before?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutLevelBeforeImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutLevelBeforeImage = true;
            }
            if (levelAfterPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Level After?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutLevelAfterImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutLevelAfterImage = true;
            }
            if (mileagePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Mileage?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutMileageImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutMileageImage = true;
            }
            Image levelBeforeImage = new Image();
            levelBeforeImage.Source = ImageSource.FromStream(() =>
            {
                var stream = levelBeforePhoto.GetStream();
                return stream;
            });
            Image levelAfterImage = new Image();
            levelAfterImage.Source = ImageSource.FromStream(() =>
            {
                var stream = levelAfterPhoto.GetStream();
                return stream;
            });
            Image mileageImage = new Image();
            mileageImage.Source = ImageSource.FromStream(() =>
            {
                var stream = mileagePhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.FuelUpVehicleSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            departmentPicker.SelectedIndex = -1;
            sectionPicker.SelectedIndex = -1;
            subSectionPicker.SelectedIndex = -1;
            vehiclePicker.SelectedIndex = -1;
            requestNumberPicker.SelectedIndex = -1;
            sourceTypePicker.SelectedIndex = -1;
            entLevelBefore.Text = string.Empty;
            entLevelAfter.Text = string.Empty;
            entMileage.Text = string.Empty;
            entCouponNumber.Text = string.Empty;
            levelBeforePhoto = null;
            levelAfterPhoto = null;
            mileagePhoto = null;
            thirdPartyGaragePicker.SelectedIndex = -1;
            warehousePicker.SelectedIndex = -1;
            entCouponNumber.Text = null;
            lblAuthorisedBy.Text = string.Empty;
            lblAvailableQuantity.Text = string.Empty;
            lblDateAuthorised.Text = string.Empty;
            lblFuelType.Text = string.Empty;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public async Task BindModel()
        {
            viewModel = new FuelUpVehicleViewModel();
            viewModel.Department = (departmentPicker.SelectedItem as string).Trim();
            viewModel.VehicleRegistrationNumber = (vehiclePicker.SelectedItem as string).Trim();
            viewModel.RequestNumber = (requestNumberPicker.SelectedItem as string).Trim();
            viewModel.SourceType = (sourceTypePicker.SelectedItem as string).Trim();
            viewModel.Source = GetSource(); 
            viewModel.LevelBefore = testLevelBefore;
            viewModel.LevelAfter = testLevelAfter;
            viewModel.Mileage = testMileage;
            viewModel.MileageImage = await mileagePhoto.ConvertToBase64();
            viewModel.LevelAfterImage = await levelAfterPhoto.ConvertToBase64();
            viewModel.LevelBeforeImage = await levelBeforePhoto.ConvertToBase64();
            while ((viewModel.LevelBeforeImage == null && !submitWithoutLevelBeforeImage) || (viewModel.LevelAfterImage == null && !submitWithoutLevelAfterImage) || (viewModel.MileageImage == null && !submitWithoutMileageImage)) ;
        }

        private string GetSource()
        {
            var sourceType = sourceTypePicker.SelectedIndex;
            string source = string.Empty;
            if (sourceType.Equals(0))
            {
                source = warehousePicker.SelectedItem as string;
            }
            if (sourceType.Equals(1))
            {
                source = thirdPartyGaragePicker.SelectedItem as string;
            }
            if (sourceType.Equals(2))
            {
                source = entCouponNumber.Text.Trim();
            }
            return source;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(departmentPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Department field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sectionPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Section field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subSectionPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Section field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(vehiclePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Vehicle Registration Number field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(requestNumberPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sourceTypePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Source field.", "Error");
                return false;
            }
            if (sourceTypePicker.SelectedIndex.Equals(0))
            {
                if (string.IsNullOrEmpty(warehousePicker.SelectedItem as string))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value for the Warehouse field.", "Error");
                    return false;
                }
            }
            if (sourceTypePicker.SelectedIndex.Equals(1))
            {
                if (string.IsNullOrEmpty(thirdPartyGaragePicker.SelectedItem as string))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value for the Garage field.", "Error");
                    return false;
                }
            }
            if (sourceTypePicker.SelectedIndex.Equals(2))
            {
                entCouponNumber.Text = BusinessLogic.SanitizeCouponNumber(entCouponNumber.Text);
                string errorMsg = string.Empty;
                if (!BusinessLogic.IsCouponsInputValid(entCouponNumber.Text, ref errorMsg))
                {
                    BusinessLogic.ShowMessageBox(this, errorMsg, "Error");
                    return false;
                }
            }

            testLevelBefore = double.TryParse(entLevelBefore.Text, out testLevelBefore) ? testLevelBefore : double.NaN;
            if (testLevelBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Level Before field.", "Error");
                entLevelBefore.Focus();
                return false;
            }
            testLevelAfter = double.TryParse(entLevelAfter.Text, out testLevelAfter) ? testLevelAfter : double.NaN;
            if (testLevelAfter.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Level After field.", "Error");
                entLevelAfter.Focus();
                return false;
            }
            testMileage = double.TryParse(entMileage.Text, out testMileage) ? testMileage : double.NaN;
            if (testMileage.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Mileage field.", "Error");
                entMileage.Focus();
                return false;
            }
            if (testLevelAfter <= testLevelBefore)
            {
                BusinessLogic.ShowMessageBox(this, "The Level After cannot be less than or equal to the Level Before.", "Error");
                entLevelAfter.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.FuelUpVehicleModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.Department);
            hash.Append(viewModel.VehicleRegistrationNumber);
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.SourceType);
            hash.Append(viewModel.Source);
            hash.Append(viewModel.LevelBefore);
            hash.Append(viewModel.LevelAfter);
            hash.Append(viewModel.Mileage);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.FuelUpVehicleModelHash = hash;
        }

        #endregion
    }
}
