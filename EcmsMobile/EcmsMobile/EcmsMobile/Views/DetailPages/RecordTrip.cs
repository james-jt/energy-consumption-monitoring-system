﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System.Threading.Tasks;

namespace EcmsMobile
{
    class RecordTrip : ContentPage
    {
        CustomPicker departmentPicker;
        CustomPicker sectionPicker;
        CustomPicker subSectionPicker; 
        CustomPicker vehiclePicker;
        CustomEntry entOpeningMileage;
        CustomEntry entClosingMileage;
        CustomEntry entOrigin;
        CustomEntry entDestination;
        CustomEntry entPurpose;
        Button btnClear;
        Button btnSubmit;
        Button openingMileageTakePhoto;
        Button openingMileagePickPhoto;
        Button openingMileageViewPhoto;
        Button closingMileageTakePhoto;
        Button closingMileagePickPhoto;
        Button closingMileageViewPhoto;
        MediaFile openingMileagePhoto;
        MediaFile closingMileagePhoto; 
        RecordTripViewModel viewModel;

        double testOpeningMileage;
        double testClosingMileage;
        bool submitWithoutOpeningMileageImage = false;
        bool submitWithoutClosingMileageImage = false;
         
        public RecordTrip()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            entOpeningMileage = new CustomEntry() {Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entOpeningMileage.TextChanged += OnNumericEntryTextChanged;
            entOpeningMileage.Completed += (object sender, EventArgs e) => entClosingMileage.Focus();
            openingMileageTakePhoto = new TakePhotoButton();
            openingMileagePickPhoto = new PickPhotoButton();
            openingMileageViewPhoto = new ViewPhotoButton();
            openingMileageTakePhoto.AutomationId = openingMileagePickPhoto.AutomationId = openingMileageViewPhoto.AutomationId = "Opening Mileage";
            openingMileageTakePhoto.Clicked += OnTakePhotoButtonClicked;
            openingMileagePickPhoto.Clicked += OnPickPhotoButtonClicked;
            openingMileageViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entClosingMileage = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entClosingMileage.TextChanged += OnNumericEntryTextChanged;
            entClosingMileage.Completed += (object sender, EventArgs e) => entOrigin.Focus();
            closingMileageTakePhoto = new TakePhotoButton();
            closingMileagePickPhoto = new PickPhotoButton();
            closingMileageViewPhoto = new ViewPhotoButton();
            closingMileageTakePhoto.AutomationId = closingMileagePickPhoto.AutomationId = closingMileageViewPhoto.AutomationId = "Closing Mileage";
            closingMileageTakePhoto.Clicked += OnTakePhotoButtonClicked;
            closingMileagePickPhoto.Clicked += OnPickPhotoButtonClicked;
            closingMileageViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entOrigin = new CustomEntry() { Keyboard = Keyboard.Text, ReturnType = ReturnType.Next };
            entOrigin.Completed += (object sender, EventArgs e) => entDestination.Focus();
            entDestination = new CustomEntry() { Keyboard = Keyboard.Text, ReturnType = ReturnType.Next };
            entDestination.Completed += (object sender, EventArgs e) => entPurpose.Focus();
            entPurpose = new CustomEntry() { Keyboard = Keyboard.Text };
            entPurpose.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);

            Title = "Log Trip";
            var outputText = new Label();

            departmentPicker = new CustomPicker ();
            departmentPicker.AutomationId = "Departments";
            BusinessLogic.PopulatePicker(departmentPicker);

            sectionPicker = new CustomPicker();
            sectionPicker.AutomationId = "Sections";
            //BusinessLogic.PopulatePicker(sectionPicker);

            subSectionPicker = new CustomPicker();
            subSectionPicker.AutomationId = "Sub Sections";
            //BusinessLogic.PopulatePicker(subSectionPicker);

            vehiclePicker = new CustomPicker ();
            vehiclePicker.AutomationId = "Vehicle Registration Numbers";
            //BusinessLogic.PopulatePicker(vehiclePicker, App.currentUser.Username);  

            departmentPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = departmentPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var departments = departmentPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sectionPicker, departments[departmentPicker.Items[selectedIndex]]);
                }
            };

            sectionPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sectionPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sections = sectionPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subSectionPicker, sections[sectionPicker.Items[selectedIndex]]);
                }
            };

            subSectionPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subSectionPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sections = subSectionPicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(vehiclePicker, sections[subSectionPicker.Items[selectedIndex]]);
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var openingMileageGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            openingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            openingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            openingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            openingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            openingMileageGrid.Children.Add(entOpeningMileage);
            openingMileageGrid.Children.Add(openingMileageViewPhoto, 1, 0);
            openingMileageGrid.Children.Add(openingMileagePickPhoto, 2, 0);
            openingMileageGrid.Children.Add(openingMileageTakePhoto, 3, 0);

            var closingMileageGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            closingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            closingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            closingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            closingMileageGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            closingMileageGrid.Children.Add(entClosingMileage);
            closingMileageGrid.Children.Add(closingMileageViewPhoto, 1, 0);
            closingMileageGrid.Children.Add(closingMileagePickPhoto, 2, 0);
            closingMileageGrid.Children.Add(closingMileageTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Department:" },
                    departmentPicker,
                    new HeaderLabel { Text = "Section:" },
                    sectionPicker,
                    new HeaderLabel { Text = "Sub Section:" },
                    subSectionPicker,
                    new HeaderLabel { Text = "Vehicle Registration Number:" },
                    vehiclePicker,
                    new HeaderLabel { Text = "Opening Mileage:" },
                    openingMileageGrid, 
                    new HeaderLabel { Text = "Closing Mileage:" },
                    closingMileageGrid,
                    new HeaderLabel { Text = "Origin:" },
                    entOrigin,
                    new HeaderLabel { Text = "Destination:" },
                    entDestination,
                    new HeaderLabel { Text = "Purpose:" },
                    entPurpose,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }

                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnTakePhotoButtonClicked (object sender, EventArgs e)
        {
            Button source = sender as Button;
            if(! await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Opening Mileage":            
                    openingMileagePhoto = await BusinessLogic.TakePhoto()?? openingMileagePhoto;
                    break;
                case "Closing Mileage":
                    closingMileagePhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked (object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Opening Mileage":
                    openingMileagePhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Closing Mileage":
                    closingMileagePhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Opening Mileage":
                    if (openingMileagePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Opening Mileage field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, openingMileagePhoto);
                    break;
                case "Closing Mileage":
                    if (closingMileagePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Closing Mileage field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, closingMileagePhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked (object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            await BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (openingMileagePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Opening Mileage?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutOpeningMileageImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutOpeningMileageImage = true;
            }
            if (closingMileagePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Closing Mileage?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutClosingMileageImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                } 
                submitWithoutClosingMileageImage = true;
            }
            Image openingMileageImage = new Image();
            openingMileageImage.Source = ImageSource.FromStream(() =>
            {
                var stream = openingMileagePhoto.GetStream();
                return stream;
            });
            Image closingMileageImage = new Image();
            closingMileageImage.Source = ImageSource.FromStream(() =>
            { 
                var stream = closingMileagePhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.RecordTripSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked (object sender, EventArgs e)
        {
            departmentPicker.SelectedIndex = -1;
            vehiclePicker.SelectedIndex = -1;
            sectionPicker.SelectedIndex = -1;
            subSectionPicker.SelectedIndex = -1;
            entOpeningMileage.Text = string.Empty;
            entClosingMileage.Text = string.Empty;
            entOrigin.Text = string.Empty;
            entDestination.Text = string.Empty;
            entPurpose.Text = string.Empty;
            openingMileagePhoto = null;
            closingMileagePhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if(!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        public async Task BindModel()
        {
            viewModel = new RecordTripViewModel();
            viewModel.Department = (departmentPicker.SelectedItem as string).Trim();
            viewModel.VehicleRegistrationNumber = (vehiclePicker.SelectedItem as string).Trim();
            viewModel.OpeningMileage = testOpeningMileage;
            viewModel.ClosingMileage = testClosingMileage;
            viewModel.Origin = entOrigin.Text.Trim();
            viewModel.Destination = entDestination.Text.Trim();
            viewModel.Purpose = entPurpose.Text.Trim();
            viewModel.LoggedBy = App.currentUser.Username;
            viewModel.OpeningMileageImage = await openingMileagePhoto.ConvertToBase64();
            viewModel.ClosingMileageImage = await closingMileagePhoto.ConvertToBase64();
            while ((viewModel.OpeningMileageImage == null && !submitWithoutOpeningMileageImage) || (viewModel.ClosingMileageImage == null && !submitWithoutClosingMileageImage)) ;

        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(departmentPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Department field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sectionPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Section field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subSectionPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Section field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(vehiclePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Vehicle Registration Number field.", "Error");
                return false;
            }
            testOpeningMileage = double.TryParse(entOpeningMileage.Text, out testOpeningMileage) ? testOpeningMileage : double.NaN;
            if (testOpeningMileage.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Opening Mileage field.", "Error");
                entOpeningMileage.Focus();
                return false;
            }
            testClosingMileage = double.TryParse(entClosingMileage.Text, out testClosingMileage) ? testClosingMileage : double.NaN;
            if (testClosingMileage.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Closing Mileage field.", "Error");
                entClosingMileage.Focus();
                return false;
            }
            if (testClosingMileage < testOpeningMileage)
            {
                BusinessLogic.ShowMessageBox(this, "The Closing Mileage cannot be less than the Opening Mileage.", "Error");
                entClosingMileage.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(entOrigin.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Please state the origin of your trip.");
                entOrigin.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(entDestination.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Please state the destination of your trip.");
                entDestination.Focus();
                return false;
            }
            if (entOrigin.Text.Equals(entDestination.Text))
            {
                BusinessLogic.ShowMessageBox(this, "The trip origin and destination cannot be the same.");
                entDestination.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(entPurpose.Text))
            {
                BusinessLogic.ShowMessageBox(this, "Please state the purpose of your trip.");
                entPurpose.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.RecordTripModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.Department);
            hash.Append(viewModel.VehicleRegistrationNumber);
            hash.Append(viewModel.OpeningMileage);
            hash.Append(viewModel.ClosingMileage);
            hash.Append(viewModel.Origin);
            hash.Append(viewModel.Destination);
            hash.Append(viewModel.Purpose);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.RecordTripModelHash = hash;
        }
    }
}
