﻿using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class FuelUpGen : ContentPage
    {
        CustomPicker zonePicker;
        CustomPicker subZonePicker;
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        CustomPicker requestNumberPicker;
        CustomPicker sourceTypePicker;
        CustomPicker thirdPartyGaragePicker;
        CustomPicker warehousePicker;

        List<DrawdownViewModel> drawdownViewObjects;

        HeaderLabel sourceLabel;
        HeaderLabel lblLocation;
        HeaderLabel lblAvailableQuantity;
        HeaderLabel lblFuelType;

        CustomEntry entCouponNumber;
        CustomEntry entLevelBefore;
        CustomEntry entLevelAfter;
        CustomEntry entHoursRun;
        CustomEntry entQuantity;

        Button entLevelBeforeTakePhoto;
        Button entLevelBeforePickPhoto;
        Button entLevelBeforeViewPhoto;
        Button entLevelAfterTakePhoto;
        Button entLevelAfterPickPhoto;
        Button entLevelAfterViewPhoto;
        Button entHoursRunTakePhoto;
        Button entHoursRunPickPhoto;
        Button entHoursRunViewPhoto;
        MediaFile levelBeforePhoto;
        MediaFile levelAfterPhoto;
        MediaFile hoursRunPhoto;
        FuelUpGeneratorViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testLevelBefore;
        double testLevelAfter;
        double testHoursRun;
        double testQuantity;
        bool submitWithoutLevelBeforeImage = false;
        bool submitWithoutLevelAfterImage = false;
        bool submitWithoutHoursRunImage = false;
         
        public FuelUpGen()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            drawdownViewObjects = new List<DrawdownViewModel>();

            lblLocation = new HeaderLabel();
            lblAvailableQuantity = new HeaderLabel();
            lblFuelType = new HeaderLabel();

            Title = "Fuel Up Generator";
            entCouponNumber = new CustomEntry() { Keyboard = Keyboard.Text, ReturnType = ReturnType.Next }; ;
            entCouponNumber.IsVisible = false;
            entCouponNumber.Completed += (sender, e) => entQuantity.Focus();

            entQuantity = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entQuantity.TextChanged += OnNumericEntryTextChanged;
            entQuantity.Completed += (sender, e) => entLevelBefore.Focus();

            entLevelBefore = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entLevelBefore.TextChanged += OnNumericEntryTextChanged;
            entLevelBefore.Completed += (sender, e) => entLevelAfter.Focus();
            entLevelBeforeTakePhoto = new TakePhotoButton();
            entLevelBeforePickPhoto = new PickPhotoButton();
            entLevelBeforeViewPhoto = new ViewPhotoButton();
            entLevelBeforeTakePhoto.AutomationId = entLevelBeforePickPhoto.AutomationId = entLevelBeforeViewPhoto.AutomationId = "Level Before";
            entLevelBeforeTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entLevelBeforePickPhoto.Clicked += OnPickPhotoButtonClicked;
            entLevelBeforeViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entLevelAfter = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Next };
            entLevelAfter.TextChanged += OnNumericEntryTextChanged;
            entLevelAfter.Completed += (sender, e) => entHoursRun.Focus();
            entLevelAfterTakePhoto = new TakePhotoButton();
            entLevelAfterPickPhoto = new PickPhotoButton();
            entLevelAfterViewPhoto = new ViewPhotoButton();
            entLevelAfterTakePhoto.AutomationId = entLevelAfterPickPhoto.AutomationId = entLevelAfterViewPhoto.AutomationId = "Level After";
            entLevelAfterTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entLevelAfterPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entLevelAfterViewPhoto.Clicked += OnViewPhotoButtonClicked;

            entHoursRun = new CustomEntry() { Keyboard = Keyboard.Numeric, ReturnType = ReturnType.Done };
            entHoursRun.TextChanged += OnNumericEntryTextChanged;
            entHoursRun.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            entHoursRunTakePhoto = new TakePhotoButton();
            entHoursRunPickPhoto = new PickPhotoButton();
            entHoursRunViewPhoto = new ViewPhotoButton();
            entHoursRunTakePhoto.AutomationId = entHoursRunPickPhoto.AutomationId = entHoursRunViewPhoto.AutomationId = "Hours Run";
            entHoursRunTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entHoursRunPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entHoursRunViewPhoto.Clicked += OnViewPhotoButtonClicked;

            zonePicker = new CustomPicker();
            zonePicker.AutomationId = "Zones";
            BusinessLogic.PopulatePicker(zonePicker, App.currentUser.SubSectionId);

            subZonePicker = new CustomPicker();
            subZonePicker.AutomationId = "Sub Zones";
            //BusinessLogic.PopulatePicker(subZonePicker);

            maintenanceCentrePicker = new CustomPicker ();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            //BusinessLogic.PopulatePicker(maintenanceCentrePicker);
            //maintenanceCentrePicker.SelectedIndex = maintenanceCentrePicker.Items.IndexOf(App.currentUser.SubSection);

            sitePicker = new CustomPicker ();
            sitePicker.AutomationId = "Sites";
            //BusinessLogic.PopulatePicker(sitePicker, siteViewObjects, App.currentUser.SubSection);

            zonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = zonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = zonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subZonePicker, zones[zonePicker.Items[selectedIndex]]);
                }
            };

            subZonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subZonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = subZonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(maintenanceCentrePicker, zones[subZonePicker.Items[selectedIndex]]);
                }
            };

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var maintenanceCentres = maintenanceCentrePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentres[maintenanceCentrePicker.Items[selectedIndex]]);
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sites = sitePicker.ViewModel as List<SiteViewModel>;
                    var siteName = sitePicker.Items[selectedIndex];
                    var site = sites.Find(s => s.Name == siteName);
                    lblLocation.Text = site.Location;
                }
            };

            requestNumberPicker = new CustomPicker ();
            requestNumberPicker.AutomationId = "Generator Fuel Requests";
            BusinessLogic.PopulatePicker(requestNumberPicker, App.currentUser.Username);
            requestNumberPicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = requestNumberPicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var picker = sender as Picker;
                    var requestId = picker.Items[selectedIndex];
                    var requests = requestNumberPicker.ViewModel as List<DrawdownViewModel>;
                    var request = requests.Find(r => r.RequestNumber == requestId);
                    lblAvailableQuantity.Text = request.Quantity.ToString();
                    lblFuelType.Text = request.FuelType;
                }
            };

            sourceTypePicker = new CustomPicker();
            sourceTypePicker.AutomationId = "Drawdown Sources";
            BusinessLogic.PopulatePicker(sourceTypePicker);
            sourceTypePicker.SelectedIndexChanged += (sender, e) =>
            {
                var source = sourceTypePicker.SelectedIndex;
                if (source.Equals(0))
                {
                    sourceLabel.Text = "Warehouse:";
                    warehousePicker.IsVisible = true;
                    thirdPartyGaragePicker.IsVisible = false;
                    entCouponNumber.IsVisible = false;
                }
                if (source.Equals(1))
                {
                    sourceLabel.Text = "Garage:";
                    warehousePicker.IsVisible = false;
                    thirdPartyGaragePicker.IsVisible = true;
                    entCouponNumber.IsVisible = false;
                }
                if (source.Equals(2))
                {
                    sourceLabel.Text = "Coupons:";
                    warehousePicker.IsVisible = false;
                    thirdPartyGaragePicker.IsVisible = false;
                    entCouponNumber.IsVisible = true;
                }
            };

            thirdPartyGaragePicker = new CustomPicker();
            thirdPartyGaragePicker.AutomationId = "Third Party Garages";
            BusinessLogic.PopulatePicker(thirdPartyGaragePicker, App.currentUser.SubSection);
            thirdPartyGaragePicker.IsVisible = false;

            warehousePicker = new CustomPicker();
            warehousePicker.AutomationId = "Warehouses";
            BusinessLogic.PopulatePicker(warehousePicker);
            warehousePicker.IsVisible = true;

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var levelBeforeGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelBeforeGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelBeforeGrid.Children.Add(entLevelBefore);
            levelBeforeGrid.Children.Add(entLevelBeforeViewPhoto, 1, 0);
            levelBeforeGrid.Children.Add(entLevelBeforePickPhoto, 2, 0);
            levelBeforeGrid.Children.Add(entLevelBeforeTakePhoto, 3, 0);

            var levelAfterGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelAfterGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            levelAfterGrid.Children.Add(entLevelAfter);
            levelAfterGrid.Children.Add(entLevelAfterViewPhoto, 1, 0);
            levelAfterGrid.Children.Add(entLevelAfterPickPhoto, 2, 0);
            levelAfterGrid.Children.Add(entLevelAfterTakePhoto, 3, 0);

            var hoursRunGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            hoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            hoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            hoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            hoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            hoursRunGrid.Children.Add(entHoursRun);
            hoursRunGrid.Children.Add(entHoursRunViewPhoto, 1, 0);
            hoursRunGrid.Children.Add(entHoursRunPickPhoto, 2, 0);
            hoursRunGrid.Children.Add(entHoursRunTakePhoto, 3, 0);

            sourceLabel = new HeaderLabel() { Text = "Warehouse" };

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Zone:" },
                    zonePicker,
                    new HeaderLabel { Text = "Sub Zone:" },
                    subZonePicker,
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblLocation,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Number:" },
                    requestNumberPicker,
                    new HeaderLabel { Text = "Fuel Type:" },
                    lblFuelType,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Available Quantity:" },
                    lblAvailableQuantity,
                    new HeaderLabel { Text = "" },
                    //new HeaderLabel { Text = "Source:" },
                    //sourceTypePicker,
                    //sourceLabel,
                    //thirdPartyGaragePicker,
                    //warehousePicker,
                    //entCouponNumber,
                    new HeaderLabel { Text = "Quantity:" },
                    entQuantity,
                    new HeaderLabel { Text = "Level Before:" },
                    levelBeforeGrid,
                    new HeaderLabel { Text = "Level After:" },
                    levelAfterGrid,
                    new HeaderLabel { Text = "Hours Run:" },
                    hoursRunGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Level Before":
                    levelBeforePhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Level After":
                    levelAfterPhoto = await BusinessLogic.TakePhoto();
                    break;
                case "Hours Run":
                    hoursRunPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Level Before":
                    levelBeforePhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Level After":
                    levelAfterPhoto = await BusinessLogic.PickPhoto();
                    break;
                case "Hours Run":
                    hoursRunPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Level Before":
                    if (levelBeforePhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Level Before field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, levelBeforePhoto);
                    break;
                case "Level After":
                    if (levelAfterPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Level After field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, levelAfterPhoto);
                    break;
                case "Hours Run":
                    if (hoursRunPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Hours Run field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, hoursRunPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            await BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (levelBeforePhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Level Before?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutLevelBeforeImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutLevelBeforeImage = true;
            }
            if (levelAfterPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Level After?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutLevelAfterImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutLevelAfterImage = true;
            }
            if (hoursRunPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Hours Run?", null, null, "Yes", "No");
                if (response == "No")
                {
                    submitWithoutHoursRunImage = false;
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
                submitWithoutHoursRunImage = true;
            }
            Image levelBeforeImage = new Image();
            levelBeforeImage.Source = ImageSource.FromStream(() =>
            {
                var stream = levelBeforePhoto.GetStream();
                return stream;
            });
            Image levelAfterImage = new Image();
            levelAfterImage.Source = ImageSource.FromStream(() =>
            {
                var stream = levelAfterPhoto.GetStream();
                return stream;
            });
            Image hoursRunImage = new Image();
            hoursRunImage.Source = ImageSource.FromStream(() =>
            {
                var stream = hoursRunPhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.FuelUpGeneratorSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            zonePicker.SelectedIndex = -1;
            subZonePicker.SelectedIndex = -1;
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            requestNumberPicker.SelectedIndex = -1;
            lblAvailableQuantity.Text = string.Empty;
            lblFuelType.Text = string.Empty;
            entLevelBefore.Text = string.Empty;
            entLevelAfter.Text = string.Empty;
            entHoursRun.Text = string.Empty;
            entQuantity.Text = string.Empty;
            levelBeforePhoto = null;
            levelAfterPhoto = null;
            hoursRunPhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public async Task BindModel()
        {
            viewModel = new FuelUpGeneratorViewModel();
            viewModel.MaintenanceCentre = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.RequestNumber = (requestNumberPicker.SelectedItem as string).Trim();
            //viewModel.SourceType = (sourceTypePicker.SelectedItem as string).Trim();
            //viewModel.Source = GetSource();
            viewModel.Quantity = testQuantity;
            viewModel.LevelBefore = testLevelBefore;
            viewModel.LevelAfter = testLevelAfter;
            viewModel.HoursRun = testHoursRun;
            viewModel.HoursRunImage = await hoursRunPhoto.ConvertToBase64();
            viewModel.LevelAfterImage = await levelAfterPhoto.ConvertToBase64();
            viewModel.LevelBeforeImage = await levelBeforePhoto.ConvertToBase64();
            while ((viewModel.LevelBeforeImage == null && !submitWithoutLevelBeforeImage) || (viewModel.LevelAfterImage == null && !submitWithoutLevelAfterImage) || (viewModel.HoursRunImage == null && !submitWithoutHoursRunImage)) ;
        }

        private string GetSource()
        {
            var sourceType = sourceTypePicker.SelectedIndex;
            string source = string.Empty;
            if (sourceType.Equals(0))
            {
                source = warehousePicker.SelectedItem as string;
            }
            if (sourceType.Equals(1))
            {
                source = thirdPartyGaragePicker.SelectedItem as string;
            }
            if (sourceType.Equals(2))
            {
                source = entCouponNumber.Text.Trim();
            }
            return source;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(zonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subZonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(requestNumberPicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Request Number field.", "Error");
                return false;
            }

            //if (string.IsNullOrEmpty(sourceTypePicker.SelectedItem as string))
            //{
            //    BusinessLogic.ShowMessageBox(this, "Invalid value for the Source field.", "Error");
            //    return false;
            //}
            //if (sourceTypePicker.SelectedIndex.Equals(0))
            //{
            //    if (string.IsNullOrEmpty(warehousePicker.SelectedItem as string))
            //    {
            //        BusinessLogic.ShowMessageBox(this, "Invalid value for the Warehouse field.", "Error");
            //        return false;
            //    }
            //}
            //if (sourceTypePicker.SelectedIndex.Equals(1))
            //{
            //    if (string.IsNullOrEmpty(thirdPartyGaragePicker.SelectedItem as string))
            //    {
            //        BusinessLogic.ShowMessageBox(this, "Invalid value for the Garage field.", "Error");
            //        return false;
            //    }
            //}
            //if (sourceTypePicker.SelectedIndex.Equals(2))
            //{
            //    if (!BusinessLogic.IsCouponsInputValid(entCouponNumber.Text))
            //    {
            //        BusinessLogic.ShowMessageBox(this, "Invalid input for the Coupons field.", "Error");
            //        return false;
            //    }
            //}
            testQuantity = double.TryParse(entQuantity.Text, out testQuantity) ? testQuantity : double.NaN;
            if (testQuantity.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Quantity field.", "Error");
                entQuantity.Focus();
                return false;
            }
            testLevelBefore = double.TryParse(entLevelBefore.Text, out testLevelBefore) ? testLevelBefore : double.NaN;
            if (testLevelBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Level Before field.", "Error");
                entLevelBefore.Focus();
                return false;
            }
            testLevelAfter = double.TryParse(entLevelAfter.Text, out testLevelAfter) ? testLevelAfter : double.NaN;
            if (testLevelAfter.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Level After field.", "Error");
                entLevelAfter.Focus();
                return false;
            }
            testHoursRun = double.TryParse(entHoursRun.Text, out testHoursRun) ? testHoursRun : double.NaN;
            if (testHoursRun.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Hours Run field.", "Error");
                entHoursRun.Focus();
                return false;
            }
            if (testLevelAfter <= testLevelBefore)
            {
                BusinessLogic.ShowMessageBox(this, "The Level After cannot be less than or equal to the Level Before.", "Error");
                entLevelAfter.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.FuelUpGeneratorModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.RequestNumber);
            //hash.Append(viewModel.SourceType);
            //hash.Append(viewModel.Source);
            hash.Append(viewModel.LevelBefore);
            hash.Append(viewModel.LevelAfter);
            hash.Append(viewModel.HoursRun);
            hash.Append(viewModel.Quantity);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.FuelUpGeneratorModelHash = hash;
        }

        #endregion

    }
}
