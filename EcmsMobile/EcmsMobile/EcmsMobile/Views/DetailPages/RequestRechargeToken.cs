﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using EcmsModels.ViewModels;

namespace EcmsMobile
{ 
    class RequestRechargeToken : ContentPage
    {
        CustomPicker zonePicker;
        CustomPicker subZonePicker;
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        HeaderLabel lblSiteLocation;
        HeaderLabel lblRequestType;
        HeaderLabel lblRequestNumber;
        CustomEntry entComment;
        RequestTokenSubmissionViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        public RequestRechargeToken()
        {
            BackgroundImage = "background.png";

            Title = "Request Recharge Token";
            ScrollView scrollView = new ScrollView();

            entComment = new CustomEntry();
            lblSiteLocation = new HeaderLabel();
            lblRequestType = new HeaderLabel() { Text = "On-Cycle" };
            lblRequestNumber = new HeaderLabel();
            BusinessLogic.GetNextRequestNumber(lblRequestNumber, false);

            zonePicker = new CustomPicker();
            zonePicker.AutomationId = "Zones";
            BusinessLogic.PopulatePicker(zonePicker, App.currentUser.SubSectionId);

            subZonePicker = new CustomPicker();
            subZonePicker.AutomationId = "Sub Zones";
            //BusinessLogic.PopulatePicker(subZonePicker);

            maintenanceCentrePicker = new CustomPicker();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            //BusinessLogic.PopulatePicker(maintenanceCentrePicker);
            //maintenanceCentrePicker.SelectedIndex = maintenanceCentrePicker.Items.IndexOf(App.currentUser.SubSection);

            sitePicker = new CustomPicker();
            sitePicker.AutomationId = "Sites";
            //BusinessLogic.PopulatePicker(sitePicker, siteViewObjects, App.currentUser.SubSection);

            zonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = zonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = zonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(subZonePicker, zones[zonePicker.Items[selectedIndex]]);
                }
            };

            subZonePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = subZonePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var zones = subZonePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(maintenanceCentrePicker, zones[subZonePicker.Items[selectedIndex]]);
                }
            };

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var maintenanceCentres = maintenanceCentrePicker.ViewModel as Dictionary<string, string>;
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentres[maintenanceCentrePicker.Items[selectedIndex]]);
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    var sites = sitePicker.ViewModel as List<SiteViewModel>;
                    var siteName = sitePicker.Items[selectedIndex];
                    var site = sites.Find(s => s.Name == siteName);
                    lblSiteLocation.Text = site.Location;
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Zone:" },
                    zonePicker,
                    new HeaderLabel { Text = "Sub Zone:" },
                    subZonePicker,
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel { Text = "" },
                    //new HeaderLabel { Text = "Request Type:" },
                    //lblRequestType,
                    //new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Request Number:" },
                    lblRequestNumber,
                    new HeaderLabel { Text = "" },
                    new HeaderLabel { Text = "Comment:" },
                    entComment,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }

        #region Event Handlers

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (string.IsNullOrEmpty(entComment.Text))
            {
                var response = await this.DisplayActionSheet("Submit without comment?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.RequestRechargeTokenSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                var requestNumber = lblRequestNumber.Text;
                var response = await this.DisplayActionSheet("Do you wish to make another request?", null, null, "Yes", "No");
                if (response == "Yes")
                {
                    OnClearButtonClicked(null, null);
                    BusinessLogic.GetNextRequestNumber(lblRequestNumber, false);
                }
                else
                    lblRequestNumber.Text = string.Empty;
                BusinessLogic.ShowMessageBox(this, $"Request successfully submitted. Your request number is {requestNumber}.", "Info");
                requestNumber = string.Empty;
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            entComment.Text = string.Empty;
            lblSiteLocation.Text = string.Empty;
            lblRequestType.Text = string.Empty;
            viewModel = null;
        }

        #endregion

        #region Model Binding

        public void BindModel()
        {
            viewModel = new RequestTokenSubmissionViewModel();
            viewModel.Requestor = App.currentUser.Username;
            viewModel.RequestDate = DateTime.Now;
            viewModel.Department = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.RequestNumber = lblRequestNumber.Text;
            viewModel.RequestType = lblRequestType.Text;
            viewModel.Comment = entComment.Text.Trim();
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(zonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(subZonePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Sub Zone field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(lblRequestNumber.Text))
            {
                BusinessLogic.GetNextRequestNumber(lblRequestNumber, false);
                BusinessLogic.ShowMessageBox(this, "Request number generation failed, please try again.", "Error");
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.RequestRechargeTokenModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.Requestor);
            hash.Append(viewModel.RequestDate);
            hash.Append(viewModel.Department);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.RequestNumber);
            hash.Append(viewModel.RequestType);
            hash.Append(viewModel.Comment);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.RequestRechargeTokenModelHash = hash;
        }

        #endregion


    }
}
