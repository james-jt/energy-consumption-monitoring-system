﻿using EcmsModels.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{ 
    class HoursRunReading : ContentPage
    {
        CustomPicker maintenanceCentrePicker;
        CustomPicker sitePicker;
        Label lblSiteLocation;
        CustomEntry entGenHoursRun;
        Button entGenHoursRunTakePhoto;
        Button entGenHoursRunPickPhoto;
        Button entGenHoursRunViewPhoto;
        MediaFile entGenHoursRunPhoto;
        HoursRunViewModel viewModel;
        Button btnClear;
        Button btnSubmit;

        double testGenHoursRunBefore;

        public HoursRunReading()
        {
            BackgroundImage = "background.png";

            ScrollView scrollView = new ScrollView();

            Title = "Hours Run Reading";
            entGenHoursRun = new CustomEntry() { Keyboard = Keyboard.Numeric };
            entGenHoursRun.TextChanged += OnNumericEntryTextChanged;
            entGenHoursRun.Completed += (sender, e) => scrollView.ScrollToAsync(btnSubmit, ScrollToPosition.MakeVisible, true);
            entGenHoursRunTakePhoto = new TakePhotoButton();
            entGenHoursRunPickPhoto = new PickPhotoButton();
            entGenHoursRunViewPhoto = new ViewPhotoButton();
            entGenHoursRunTakePhoto.AutomationId = entGenHoursRunPickPhoto.AutomationId = entGenHoursRunViewPhoto.AutomationId = "Hours Run";
            entGenHoursRunTakePhoto.Clicked += OnTakePhotoButtonClicked;
            entGenHoursRunPickPhoto.Clicked += OnPickPhotoButtonClicked;
            entGenHoursRunViewPhoto.Clicked += OnViewPhotoButtonClicked;

            lblSiteLocation = new Label();

            maintenanceCentrePicker = new CustomPicker ();
            maintenanceCentrePicker.AutomationId = "Maintenance Centres";
            BusinessLogic.PopulatePicker(maintenanceCentrePicker);

            sitePicker = new CustomPicker ();
            sitePicker.AutomationId = "Sites";
            BusinessLogic.PopulatePicker(sitePicker, App.currentUser.Username);

            maintenanceCentrePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = maintenanceCentrePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    BusinessLogic.PopulatePicker(sitePicker, maintenanceCentrePicker.Items[selectedIndex]);
                    lblSiteLocation.Text = string.Empty;
                }
            };

            sitePicker.SelectedIndexChanged += (sender, e) =>
            {
                int selectedIndex = sitePicker.SelectedIndex;
                if (selectedIndex != -1)
                {
                    lblSiteLocation.Text = sitePicker.Items[selectedIndex];
                }
            };

            btnClear = new ActionButton() { Text = "Clear" };
            btnClear.Clicked += OnClearButtonClicked;
            btnSubmit = new ActionButton() { Text = "Submit" };
            btnSubmit.Clicked += OnSubmitButtonClicked;

            var genGenHoursRunGrid = new Grid { Padding = new Thickness(1, 5), HeightRequest = 40 };
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(40) });
            genGenHoursRunGrid.Children.Add(entGenHoursRun);
            genGenHoursRunGrid.Children.Add(entGenHoursRunViewPhoto, 1, 0);
            genGenHoursRunGrid.Children.Add(entGenHoursRunPickPhoto, 2, 0);
            genGenHoursRunGrid.Children.Add(entGenHoursRunTakePhoto, 3, 0);

            var content = new StackLayout
            {
                Margin = new Thickness(20),
                Children = {
                    new HeaderLabel { Text = "Maintenance Centre:" },
                    maintenanceCentrePicker,
                    new HeaderLabel { Text = "Site Name:" },
                    sitePicker,
                    new HeaderLabel { Text = "Location:" },
                    lblSiteLocation,
                    new HeaderLabel {Text = "" },
                    new HeaderLabel { Text = "Hours Run:" },
                    genGenHoursRunGrid,
                    new StackLayout
                    {
                        Orientation = StackOrientation.Horizontal,
                        HorizontalOptions = LayoutOptions.Center,
                        Children = {
                            btnClear,
                            btnSubmit,
                        }
                    }
                }
            };
            scrollView.Content = content;
            Content = scrollView;
        }


        #region Event Handlers

        public async void OnTakePhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            if (!await BusinessLogic.IsCameraReady())
            {
                BusinessLogic.ShowMessageBox(this, "Camera not available.");
                return;
            }
            switch (source.AutomationId)
            {
                case "Hours Run":
                    entGenHoursRunPhoto = await BusinessLogic.TakePhoto();
                    break;
            }
        }

        public async void OnPickPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Hours Run":
                    entGenHoursRunPhoto = await BusinessLogic.PickPhoto();
                    break;
            }
        }

        public void OnViewPhotoButtonClicked(object sender, EventArgs e)
        {
            Button source = sender as Button;
            switch (source.AutomationId)
            {
                case "Hours Run":
                    if (entGenHoursRunPhoto is null)
                    {
                        BusinessLogic.ShowMessageBox(this, "No image exists for Generator Hours Run field.");
                        return;
                    }
                    BusinessLogic.ShowImage(this, entGenHoursRunPhoto);
                    break;
            }
        }

        public async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            if (!ValidateModel())
                return;
            BindModel();
            if (viewModel is null)
            {
                BusinessLogic.ShowMessageBox(this, "Error in processing form.", "Error");
                return;
            }
            if (entGenHoursRunPhoto is null)
            {
                var response = await this.DisplayActionSheet("Submit without image for Generator Hours Run?", null, null, "Yes", "No");
                if (response == "No")
                {
                    BusinessLogic.ShowMessageBox(this, "Submission aborted.");
                    return;
                }
            }
            Image genHoursRunImage = new Image();
            genHoursRunImage.Source = ImageSource.FromStream(() =>
            {
                var stream = entGenHoursRunPhoto.GetStream();
                return stream;
            });
            if (!IsModelVerified())
            {
                BusinessLogic.ShowMessageBox(this, "You have already submitted this form.", "Info");
                return;
            }
            if (!(await BusinessLogic.HoursRunReadingSubmission(viewModel)))
            {
                BusinessLogic.ShowMessageBox(this, "Error in submitting form.", "Error");
                return;
            }
            else
            {
                RegisterModelHash(HashModel());
                BusinessLogic.ShowMessageBox(this, "Data successfully submitted.", "Info");
            }
        }

        public void OnClearButtonClicked(object sender, EventArgs e)
        {
            maintenanceCentrePicker.SelectedIndex = -1;
            sitePicker.SelectedIndex = -1;
            entGenHoursRun.Text = string.Empty;
            entGenHoursRunPhoto = null;
            viewModel = null;
        }

        public void OnNumericEntryTextChanged(object sender, EventArgs e)
        {
            Entry entry = sender as Entry;
            double test;
            if (!double.TryParse(entry.Text, out test))
            {
                if (!string.IsNullOrEmpty(entry.Text))
                {
                    BusinessLogic.ShowMessageBox(this, "Invalid value entered.");
                    entry.Text = string.Empty;
                }
                entry.Focus();
            }
        }

        #endregion

        #region Model Binding

        public void BindModel()
        {
            viewModel = new HoursRunViewModel();
            viewModel.MaintenanceCentre = (maintenanceCentrePicker.SelectedItem as string).Trim();
            viewModel.Site = (sitePicker.SelectedItem as string).Trim();
            viewModel.GenHoursRun = testGenHoursRunBefore;
        }

        public bool ValidateModel()
        {
            if (string.IsNullOrEmpty(maintenanceCentrePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Maintenance Centre field.", "Error");
                return false;
            }
            if (string.IsNullOrEmpty(sitePicker.SelectedItem as string))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Site field.", "Error");
                return false;
            }
            testGenHoursRunBefore = double.TryParse(entGenHoursRun.Text, out testGenHoursRunBefore) ? testGenHoursRunBefore : double.NaN;
            if (testGenHoursRunBefore.Equals(double.NaN))
            {
                BusinessLogic.ShowMessageBox(this, "Invalid value for the Generator Hours Run field.", "Error");
                entGenHoursRun.Focus();
                return false;
            }
            return true;
        }

        public bool IsModelVerified()
        {
            var hashResult = HashModel();
            var modelVerified = hashResult != BusinessLogic.HoursRunReadingModelHash;
            return modelVerified;
        }

        public string HashModel()
        {
            StringBuilder hash = new StringBuilder();
            hash.Append(viewModel.MaintenanceCentre);
            hash.Append(viewModel.Site);
            hash.Append(viewModel.GenHoursRun);
            return hash.ToString();
        }

        public void RegisterModelHash(string hash)
        {
            BusinessLogic.HoursRunReadingModelHash = HashModel();
        }

        #endregion

    }
}
