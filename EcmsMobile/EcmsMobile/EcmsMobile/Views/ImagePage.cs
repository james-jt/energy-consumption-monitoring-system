﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    public class ImagePage : ContentPage
    {
        public ImagePage(Image image)
            :base()
        {

            Title = "View Image";
            BackgroundColor = Color.Black;
            var content = new StackLayout
            {
                Children =
                {
                    image
                }
            };
            ScrollView scrollView = new ScrollView();
            scrollView.Content = content;
            Content = scrollView;
        }
    }
}
