﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    public partial class LoginPage : ContentPage
    {

        public void InitializeComponents()
        {
            BackgroundImage = "background.png";
            indicator = new ActivityIndicator();
            var toolbarItem = new ToolbarItem
            {
                Icon = "add_user.png",
            };
            Title = "ECMS";
            //BackgroundColor = Color.DarkSlateGray;
            toolbarItem.Clicked += OnSignUpButtonClicked;
            ToolbarItems.Add(toolbarItem);

            logo = new Image { Source = "icon.png" };
            usernameEntry = new CustomEntry
            {
                Placeholder = "Username",
                Keyboard = Keyboard.Email,
                HeightRequest = 40,
                ReturnType = ReturnType.Next
            };
            usernameEntry.TextChanged += OnTextChanged;
            usernameEntry.Completed += (object sender, EventArgs e) => passwordEntry.Focus();

            passwordEntry = new CustomEntry
            {
                IsPassword = true,
                Placeholder = "Password",
                HeightRequest = 40,
                ReturnType = ReturnType.Go
            };
            passwordEntry.TextChanged += OnTextChanged;
            passwordEntry.Completed += OnLoginButtonClicked;
            var loginButton = new ActionButton
            {
                Text = "Sign in",
            };
            loginButton.Clicked += OnLoginButtonClicked;

            //messageLabel = new Label { HorizontalTextAlignment = TextAlignment.Center };

            indicator.HorizontalOptions = LayoutOptions.CenterAndExpand;
            indicator.SetBinding(ActivityIndicator.IsVisibleProperty, "IsBusy", BindingMode.OneWay);
            indicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsBusy", BindingMode.OneWay);
            indicator.Color = Color.White;

            var content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Children = {
                    logo,
                    usernameEntry,
                    passwordEntry,
                    loginButton,
                    //indicator,
                    //messageLabel,
                    
                }
            };

            Grid grid = new Grid();
            ScrollView scrollView = new ScrollView();
            scrollView.Content = content;
            Content = scrollView;
            Padding = new Thickness(20, 20);
        }
    }
}
