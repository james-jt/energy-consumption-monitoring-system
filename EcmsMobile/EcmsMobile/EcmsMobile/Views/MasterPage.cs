﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile
{
    class MasterPage : ContentPage
    {
        public ListView ListView { get { return listView; } }
          
        ListView listView;
        MasterPageGroup dataLoggingMasterPageItems;
        MasterPageGroup vehicleMasterPageItems;
        MasterPageGroup generatorMasterPageItems;
        MasterPageGroup fuelMasterPageItems;
        MasterPageGroup electricityMeterMasterPageItems;
        MasterPageGroup approveActionMasterPageItems;

        public MasterPage()
        {
            dataLoggingMasterPageItems = new MasterPageGroup { LongName = "Readings", ShortName = "R" };
            vehicleMasterPageItems = new MasterPageGroup { LongName = "Vehicles", ShortName = "V" };
            generatorMasterPageItems = new MasterPageGroup { LongName = "Generators", ShortName = "G" };
            fuelMasterPageItems = new MasterPageGroup { LongName = "Fuel", ShortName = "D" };
            electricityMeterMasterPageItems = new MasterPageGroup { LongName = "Electricity", ShortName = "E" };
            approveActionMasterPageItems = new MasterPageGroup { LongName = "Approval", ShortName = "A" };
            Title = "Required Title";
            BackgroundColor = Color.DarkOrange;
            Padding = new Thickness(5, 5);
            var itemTemplate = new DataTemplate(() =>
            {
                var grid = new Grid { Padding = new Thickness(5, 10) };
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(30) });
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });

                var image = new Image();
                image.SetBinding(Image.SourceProperty, "IconSource");
                var label = new Label { VerticalOptions = LayoutOptions.FillAndExpand, TextColor = Color.White };
                label.SetBinding(Label.TextProperty, "Title");

                grid.Children.Add(image);
                grid.Children.Add(label, 1, 0);

                return new ViewCell { View = grid };
            });
            var groupTemplate = new DataTemplate(() =>
            {
                var grid = new Grid { Padding = new Thickness(5, 10) };

                var label = new Label { VerticalOptions = LayoutOptions.Center, TextColor = Color.Black, FontAttributes = FontAttributes.Bold };
                label.SetBinding(Label.TextProperty, "LongName");

                grid.Children.Add(label, 1, 0);

                return new ViewCell { View = label };
            });

            List<MasterPageGroup> menuList = new List<MasterPageGroup>();

            #region Logs Items

            dataLoggingMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Log Trip",
                IconSource = "record_trip.png",
                TargetType = typeof(RecordTrip)
            });

            dataLoggingMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem

            {
                Title = "Log Vehicle Readings",
                IconSource = "gen_readings.png",
                TargetType = typeof(RecordVehicleReadings)
            });

            dataLoggingMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Log Generator Readings",
                IconSource = "gen_readings.png",
                TargetType = typeof(RecordGeneratorReadings)
            });

            dataLoggingMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Log Meter Reading",
                IconSource = "meter_reading.png",
                TargetType = typeof(RecordMeterReading)
            });

            #endregion

            #region Vehicle Items

            //vehicleMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem

            //{
            //    Title = "Log Trip",
            //    IconSource = "record_trip.png",
            //    TargetType = typeof(RecordTrip)
            //});

            //vehicleMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem

            //{
            //    Title = "Fuel Up Vehicle",
            //    IconSource = "fuel_up_vehicle.png",
            //    TargetType = typeof(FuelUpVehicle)
            //});

            //vehicleMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem

            //{
            //    Title = "Log Vehicle Readings",
            //    IconSource = "gen_readings.png",
            //    TargetType = typeof(RecordVehicleReadings)
            //});

            //vehiclesListView = new ListView
            //{
            //    ItemsSource = vehicleMasterPageItems,
            //    ItemTemplate = itemTemplate,
            //    SeparatorVisibility = SeparatorVisibility.Default
            //};
            #endregion

            #region Generator Items

            //generatorMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Fuel Up Generator",
            //    IconSource = "fuel_up_gen.png",
            //    TargetType = typeof(FuelUpGen)
            //});

            //generatorMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Log Generator Readings",
            //    IconSource = "gen_readings.png",
            //    TargetType = typeof(RecordGeneratorReadings)
            //});

            //generatorMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Generator Fuel Level Reading",
            //    IconSource = "contacts.png",
            //    TargetType = typeof(FuelLevelReading)
            //});

            //generatorMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Hours Run Reading",
            //    IconSource = "reminders.png",
            //    TargetType = typeof(HoursRunReading)
            //});

            //generatorsListView = new ListView
            //{
            //    ItemsSource = generatorMasterPageItems,
            //    ItemTemplate = itemTemplate,
            //    SeparatorVisibility = SeparatorVisibility.Default
            //};
            #endregion

            #region Fuel Drawdown Items

            fuelMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Send Fuel Request",
                IconSource = "request.png",
                TargetType = typeof(RequestFuelDrawdown)
            });

            fuelMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem

            {
                Title = "Fuel Up Vehicle",
                IconSource = "fuel_up_vehicle.png",
                TargetType = typeof(FuelUpVehicle)
            });

            fuelMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Drawdown Fuel",
                IconSource = "warehouse.png",
                TargetType = typeof(Drawdown)
            });

            fuelMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Fuel Up Generator",
                IconSource = "fuel_up_gen.png",
                TargetType = typeof(FuelUpGen)
            });

            //fuelDrawdownMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "From Third Party Garage",
            //    IconSource = "garage.png",
            //    TargetType = typeof(ThirdPartyGarageDrawdown)
            //});

            //fuelDrawdownMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Using Coupons",
            //    IconSource = "coupons.png",
            //    TargetType = typeof(CouponsDrawdown)
            //});

            //fuelDrawdownListView = new ListView
            //{
            //    ItemsSource = fuelDrawdownMasterPageItems,
            //    ItemTemplate = itemTemplate,
            //    SeparatorVisibility = SeparatorVisibility.Default
            //};
            #endregion

            #region Electricity Meter Items

            //electricityMeterMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Log Meter Reading",
            //    IconSource = "meter_reading.png",
            //    TargetType = typeof(RecordMeterReading)
            //});

            electricityMeterMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Send Recharge Token Request",
                IconSource = "request_token.png",
                TargetType = typeof(RequestRechargeToken)
            });

            electricityMeterMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Recharge Meter",
                IconSource = "recharge_meter.png",
                TargetType = typeof(RechargeMeter)
            });

            //electricityMeterListView = new ListView
            //{
            //    ItemsSource = electricityMeterMasterPageItems,
            //    ItemTemplate = itemTemplate,
            //    SeparatorVisibility = SeparatorVisibility.Default
            //};
            #endregion

            #region Authorise Action Items

            //authoriseActionMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Authorise IPR",
            //    IconSource = "contacts.png",
            //    TargetType = typeof(AuthoriseIPR)
            //});

            //authoriseActionMasterPageItems.Add(new MasterPageItem
            ////menuList.Add(new MasterPageItem
            //{
            //    Title = "Authorise GRV",
            //    IconSource = "reminders.png",
            //    TargetType = typeof(AuthoriseGRV)
            //});

            approveActionMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Approve Fuel Request",
                IconSource = "authorise.png",
                TargetType = typeof(AuthoriseFuelDrawdown)
            });

            approveActionMasterPageItems.Add(new MasterPageItem
            //menuList.Add(new MasterPageItem
            {
                Title = "Approve Recharge Token Request",
                IconSource = "authorise.png",
                TargetType = typeof(AuthoriseRechargeTokenPurchase)
            });

            //authoriseActionListView = new ListView
            //{
            //    ItemsSource = authoriseActionMasterPageItems,
            //    ItemTemplate = itemTemplate,
            //    SeparatorVisibility = SeparatorVisibility.Default
            //};
            #endregion

            //menuList.Add(vehicleMasterPageItems);
            //menuList.Add(generatorMasterPageItems);
            menuList.Add(dataLoggingMasterPageItems);
            menuList.Add(fuelMasterPageItems);
            menuList.Add(electricityMeterMasterPageItems);
            menuList.Add(approveActionMasterPageItems);

            listView = new ListView
            {
                ItemsSource = menuList,
                ItemTemplate = itemTemplate,
                SeparatorVisibility = SeparatorVisibility.Default,
                SeparatorColor = Color.White,
                IsGroupingEnabled = true,
                GroupDisplayBinding = new Binding("LongName"),
                GroupShortNameBinding = new Binding("ShortName"),
                GroupHeaderTemplate = groupTemplate
            };

            Button signOut = new ActionButton
            {
                Text = "Sign out",
                BackgroundColor = Color.DarkOrange,
                TextColor = Color.Black,
            };
            signOut.Clicked += OnLogoutButtonClicked;

            Content = new StackLayout
            {
                Children = { listView, signOut }
            };
        }

        async void OnLogoutButtonClicked(object sender, EventArgs e)
        {
            App.currentUser.IsSignedIn = false;
            Navigation.InsertPageBefore(new LoginPage(), (Page)this.Parent);
            await Navigation.PopAsync();
        }
    }
}
