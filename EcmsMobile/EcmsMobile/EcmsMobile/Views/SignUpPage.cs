﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace EcmsMobile
{
	public class SignUpPage : ContentPage
	{

		public SignUpPage ()
		{
            BackgroundImage = "background.png";
            Title = "Sign up";
            string signupUrl = "http://dummy.ecms.co.zw/ecms";

            Content = new StackLayout { 
				VerticalOptions = LayoutOptions.Center,
				Children = {
					new HeaderLabel { Text = $"Please visit", FontAttributes = FontAttributes.None, HorizontalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = 24 },
                    new HeaderLabel { Text = $"{signupUrl}" , FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = 18 },
                    new HeaderLabel { Text = $"from a computer on the internet" , FontAttributes = FontAttributes.None, HorizontalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = 14 },
                    new HeaderLabel { Text = $"to sign up." , FontAttributes = FontAttributes.None, HorizontalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = 18 },
                    new HeaderLabel { Text = $"\nKindly note:", FontAttributes = FontAttributes.None, HorizontalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = 24 },
                    new HeaderLabel { Text = $"Be sure to use an accessible email address during the sign up process." , FontAttributes = FontAttributes.None, HorizontalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = 14 },

                }
            };
		}
    }
}
