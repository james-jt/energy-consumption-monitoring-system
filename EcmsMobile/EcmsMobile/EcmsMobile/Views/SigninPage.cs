﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Acr.UserDialogs;
using EcmsModels.ViewModels;

namespace EcmsMobile
{
    public partial class LoginPage : ContentPage
    {
        CustomEntry usernameEntry;
        CustomEntry passwordEntry;
        //Label messageLabel;
        Image logo;
        ActivityIndicator indicator;


        public LoginPage()
        {
            InitializeComponents();
        }

        #region Event Handlers
        
        async void OnSignUpButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SignUpPage());
        }

        void OnTextChanged(object sender, EventArgs e)
        {
            //if (messageLabel.Text == "Invalid username or password.")
            //    return;
            //messageLabel.Text = string.Empty;
        }

        async void OnLoginButtonClicked(object sender, EventArgs e)
        {
            IsBusy = true;
            indicator.IsVisible = true;
            indicator.IsRunning = true;
            indicator.IsEnabled = true;
            //#region Remove on deployment
            //App.currentUser.IsSignedIn = true;
            //App.currentUser.Username = "admin";
            //App.currentUser.Department = await BusinessLogic.GetUserDepartment("admin");
            //Navigation.InsertPageBefore(new MainPage(), this);
            //await Navigation.PopAsync();
            //IsBusy = false;
            //return;
            //#endregion
        
            IsBusy = true;
            if (string.IsNullOrEmpty(usernameEntry.Text) && string.IsNullOrEmpty(passwordEntry.Text))
            {
                //messageLabel.Text = "Please enter a username and password.";
                UserDialogs.Instance.Toast("Please enter a username and password.");
                IsBusy = false;
                return;
            }
            if (string.IsNullOrEmpty(usernameEntry.Text))
            {
                //messageLabel.Text = "Please enter a username.";
                UserDialogs.Instance.Toast("Please enter a username.");
                IsBusy = false;
                return;
            }

            #region Remove on deployment

            //if (usernameEntry.Text.Trim() == "test")
            //{
            //    App.currentUser.IsSignedIn = true;
            //    App.currentUser.Username = usernameEntry.Text.Trim();
            //    var subSection = await BusinessLogic.GetUserSubSection(DefaultValues.DefaultAdministratorEmail);
            //    var subSectionSplit = subSection.Split('|');
            //    App.currentUser.SubSection = subSectionSplit.Length > 0 ? subSectionSplit[0] : string.Empty;
            //    App.currentUser.SubSectionId = subSectionSplit.Length > 1 ? subSectionSplit[1] : string.Empty;
            //    Navigation.InsertPageBefore(new MainPage(), this);
            //    await Navigation.PopAsync();
            //    return;
            //}

            #endregion

            if (string.IsNullOrEmpty(passwordEntry.Text))
            {
                UserDialogs.Instance.Toast("Please enter a password.");
                IsBusy = false;
                return;
            }
            usernameEntry.Text = usernameEntry.Text.Trim();
            passwordEntry.Text = passwordEntry.Text.Trim();
            var user = new User();
            user.Username = usernameEntry.Text.ToLower().Contains("@") ? usernameEntry.Text.Substring(0, usernameEntry.Text.IndexOf('@')) : usernameEntry.Text;
            user.Password = passwordEntry.Text;

            var isUserAuthenticated = await new BusinessLogic().AuthenticateUser(user);
            if (isUserAuthenticated)
            {
                App.currentUser.IsSignedIn = true;
                App.currentUser.Username = user.Username;
                var subSection = await BusinessLogic.GetUserSubSection(user.Username);
                var subSectionSplit = subSection.Split('|');
                App.currentUser.SubSection = subSectionSplit.Length > 0 ? subSectionSplit[0] : string.Empty;
                App.currentUser.SubSectionId = subSectionSplit.Length > 1 ? subSectionSplit[1] : string.Empty;

                Navigation.InsertPageBefore(new MainPage(), this);
                await Navigation.PopAsync();
            }
            else
            {
                UserDialogs.Instance.Toast("Invalid username or password.");
                passwordEntry.Text = string.Empty;
            }
            IsBusy = false;
        }

        #endregion


    }
}
