﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace EcmsMobile 
{
    public class App : Application
    {
        public static bool IsUserSignedIn { get; set; }
        public static User currentUser;

        public App()
        {
            currentUser = new User();
            currentUser.IsSignedIn = false;

            if (!currentUser.IsSignedIn)
            {
                MainPage = new NavigationPage(new LoginPage()) { BackgroundImage = "background.png", BarBackgroundColor = Color.Black, BarTextColor = Color.White };
            }
            else
            {
                MainPage = new NavigationPage(new MainPage()) { BackgroundImage = "background.png", BarBackgroundColor = Color.Black, BarTextColor = Color.White };
            }
        }
        
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
