﻿using EcmsModels.DataModels;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EcmsWebApplication.Startup))]
namespace EcmsWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //CustomConfig.SetupDbEnvironment();
        }
    }
}
