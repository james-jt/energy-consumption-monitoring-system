﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using EcmsBusinessLogic;
using EcmsModels.DataModels;
using Microsoft.Owin.Security;

namespace EcmsWebApplication.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static string GetFullName(this ClaimsPrincipal principal)
        {
            var fullName = principal.Claims.FirstOrDefault(c => c.Type == "FullName");
            return fullName?.Value;
        }

        public static string GetDepartmentName(this ClaimsPrincipal principal)
        {
            var department = principal.Claims.FirstOrDefault(c => c.Type == "Department");
            var result = department?.Value;
            if (result is null)
                return string.Empty;
            var response = new Department().GetById(Convert.ToInt32(result)).Name;
            return response;
        }

        public static string GetSectionName(this ClaimsPrincipal principal)
        {
            var section = principal.Claims.FirstOrDefault(c => c.Type == "Section");
            var result = section?.Value;
            if (result is null)
                return string.Empty;
            var response = new Section().GetById(Convert.ToInt32(result)).Name;
            return response;
        }

        public static string GetSubSectionName(this ClaimsPrincipal principal)
        {
            var subSection = principal.Claims.FirstOrDefault(c => c.Type == "SubSection");
            var result = subSection?.Value;
            if (result is null)
                return string.Empty;
            var response = new SubSection().GetById(Convert.ToInt32(result)).Name;
            return response;
        }

        public static Department GetDepartment(this ClaimsPrincipal principal)
        {
            var department = principal.Claims.FirstOrDefault(c => c.Type == "Department");
            var result = department?.Value;
            if (result is null)
                return new Department();
            var response = new Department().GetById(Convert.ToInt32(result));
            return response;
        }

        public static Section GetSection(this ClaimsPrincipal principal)
        {
            var section = principal.Claims.FirstOrDefault(c => c.Type == "Section");
            var result = section?.Value;
            if (result is null)
                return new Section();
            var response = new Section().GetById(Convert.ToInt32(result));
            return response;
        }

        public static SubSection GetSubSection(this ClaimsPrincipal principal)
        {
            var subSection = principal.Claims.FirstOrDefault(c => c.Type == "SubSection");
            var result = subSection?.Value;
            if (result is null)
                return new SubSection();
            var response = new SubSection().GetById(Convert.ToInt32(result));
            return response;
        }

    }
}