﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcmsWebApplication.Models.Reports
{
    public class ReportRow<T> where T : class
    {
        public bool HasTotals { get; set; }
        public string Heading { get; set; }
        public bool IsTotal { get; set; }
         
    }
}