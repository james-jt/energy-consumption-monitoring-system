﻿using EcmsModels.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcmsWebApplication.Models.Reports
{
    public abstract class ReportsBase<T> where T : class
    {  
        public string Title { get; set; }
        public T Entity { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; } 
        public ReportFrequency Frequency { get; set; }
        public string SortColumn { get; set; }
        public string DefaultPrimarySortColumn { get; set; }
        public string DefaultSecondarySortColumn { get; set; }
        public string DefaultTertiarySortColumn { get; set; }
        public ReportDistributionGroup DistributionGroup { get; set; }
        public bool IsSummary { get; set; }
        public bool HasRowTotals { get; set; }
        public bool HasColumnTotals { get; set; }
        public string PrimaryKeyColumn { get; set; }
        public string DrillDownColumn { get; set; }
        public string DefaultPrimaryDrillDownColumn { get; set; }
        public string DefaultSecondaryDrillDownColumn { get; set; }
        public string DefaultTertiaryDrillDownColumn { get; set; }
         
        ICollection<ReportColumn<T>> Columns { get; set; }
        ICollection<ReportRow<T>> Rows { get; set; }
 
    }
} 