﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcmsWebApplication.Models.Reports
{
    public class ReportColumn<T> where T : class
    {
        public bool HasTotals { get; set; }
        public bool IsTotal { get; set; }
        public string Heading { get; set; }
        public string Units { get; set; }
        public string NillDisplayValue { get; set; }
    }
}