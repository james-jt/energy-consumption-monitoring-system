﻿using EcmsModels.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcmsWebApplication.Models
{
    [NotMapped]
    public abstract class IDynamicEntityViewModel<T> where T : class
     {
        public List<T> EntityList = new List<T>();
        public IEnumerable<SelectListItem> EntitySelectList
        { 
            get
            {
                return new SelectList(EntityList, "Id", "Name");
            }
        }
    }

    [NotMapped]
    public class SubSectionViewModel : IDynamicEntityViewModel<SubSection>
    {
        public Int32 SelectedSubSectionId { get; set; }
    }

    [NotMapped]
    public class SectionViewModel : IDynamicEntityViewModel<Section>
    {
        public Int32 SelectedSectionId { get; set; }
    }
     
    [NotMapped]
    public class DepartmentViewModel : IDynamicEntityViewModel<Department>
    {
        public Int32 SelectedDepartmentId { get; set; }
    }

    [NotMapped]
    public class SubZoneViewModel : IDynamicEntityViewModel<SubZone>
    { 
        public Int32 SelectedSubZoneId { get; set; }
    }

    [NotMapped]
    public class VehicleModelViewModel : IDynamicEntityViewModel<VehicleModel>
    { 
        public Int32 SelectedModelId { get; set; }
    }

    [NotMapped]
    public class MaintenanceCentreViewModel : IDynamicEntityViewModel<MaintenanceCentre>
    { 
        public Int32 SelectedMaintenanceCentreViewModelId { get; set; }
    }

}