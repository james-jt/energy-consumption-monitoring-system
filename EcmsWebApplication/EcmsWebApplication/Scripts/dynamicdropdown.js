﻿    $(document).ready(function () {
        var selectedDepartment = $("#Department").val();
        var selectedSection = $("#Section").val();
        var sectionsSelect = $('#Section');
        sectionsSelect.empty();
        if (selectedDepartment !== null && selectedDepartment !== '') {
            AddSections(selectedDepartment, sectionsSelect);
        };
        if (selectedSection !== null && selectedSection !== '') {
            sectionsSelect.val = selectedSection;
        };

        selectedSection = $("#Section").val();
        var selectedSubSection = $("#SubSection").val();
        var subSectionsSelect = $('#SubSection');
        subSectionsSelect.empty();
        if (selectedSubSection !== null && selectedSubSection !== '') {
            AddSubSections(selectedSection, subSectionsSelect);
        }
        if (selectedSubSection !== null && selectedSubSection !== '') {
            subSectionsSelect.val = selectedSubSection;
        };

        selectedSubSection = $("#SubSection").val();
        var selectedSubZone = $("#SubZone").val();
        var subZonesSelect = $('#SubZone');
        subZonesSelect.empty();
        if (selectedSubZone !== null && selectedSubZone !== '') {
            AddSubZones(selectedSubSection, subZonesSelect);
        }
        if (selectedSubZone !== null && selectedSubZone !== '') {
            subZonesSelect.val = selectedSubZone;
        };

        selectedSubZone = $("#SubZone").val();
        var selectedMaintenanceCentre = $("#MaintenanceCentre").val();
        var maintenanceCentresSelect = $('#MaintenanceCentre');
        maintenanceCentresSelect.empty();
        if (selectedMaintenanceCentre !== null && selectedMaintenanceCentre !== '') {
            AddMaintenanceCentres(selectedSubZone, maintenanceCentresSelect);
        }
        if (selectedMaintenanceCentre !== null && selectedMaintenanceCentre !== '') {
            maintenanceCentresSelect.val = selectedMaintenanceCentre;
        };

        var selectedMake = $("#Make").val();
        var selectedModel = $("#Model").val();
        var modelsSelect = $('#Model');
        modelsSelect.empty();
        if (selectedMake !== null && selectedMake !== '') {
            AddModels(selectedMake, modelsSelect);
        };
        if (selectedModel !== null && selectedModel !== '') {
            modelsSelect.val = selectedModel;
        };

});

        $("#Department").change(function () {
            var selectedDepartmentId = $("#Department").val();
            var sectionsSelect = $('#Section');
            sectionsSelect.empty();
            var subSectionsSelect = $('#SubSection');
            subSectionsSelect.empty();
            if (selectedDepartmentId !== null && selectedDepartmentId !== '') {
                AddSections(selectedDepartmentId, sectionsSelect);
            }
        });

        function AddSections(selectedDepartmentId, sectionsSelect) {
            $.getJSON('http://localhost:52838/Sections/SectionView/', { departmentId: selectedDepartmentId }, function (sections) {
                if (sections !== null && !jQuery.isEmptyObject(sections)) {
                    sectionsSelect.append($('<option/>', {
                        value: null,
                        text: ""
                    }));
                    $.each(sections, function (index, section) {
                        sectionsSelect.append($('<option/>', {
                            value: section.Value,
                            text: section.Text
                        }));
                    });
                };
            });
        }

        $("#Section").change(function () {
            var selectedSectionId = $("#Section").val();
            var subSectionsSelect = $('#SubSection');
            subSectionsSelect.empty();
            if (selectedSectionId !== null && selectedSectionId !== '') {
                AddSubSections(selectedSectionId, subSectionsSelect);
            }
        });

        function AddSubSections(selectedSectionId, subSectionsSelect) {
            $.getJSON('http://localhost:52838/SubSections/SubSectionView/', { sectionId: selectedSectionId }, function (subSections) {
                if (subSections !== null && !jQuery.isEmptyObject(subSections)) {
                    subSectionsSelect.append($('<option/>', {
                        value: null,
                        text: ""
                    }));
                    $.each(subSections, function (index, subSection) {
                        subSectionsSelect.append($('<option/>', {
                            value: subSection.Value,
                            text: subSection.Text
                        }));
                    });
                };
            });
        }

        $("#SubSection").change(function () {
            var selectedSubSectionId = $("#SubSection").val();
            var subZonesSelect = $('#SubZone');
            subZonesSelect.empty();
            if (selectedSubSectionId !== null && selectedSubSectionId !== '') {
                AddSubZones(selectedSubSectionId, subZonesSelect);
            }
        });

        function AddSubZones(selectedSubSectionId, subZonesSelect) {
            $.getJSON('http://localhost:52838/SubZones/SubZoneView/', { subSectionId: selectedSubSectionId }, function (subZones) {
                if (subZones !== null && !jQuery.isEmptyObject(subZones)) {
                    subZonesSelect.append($('<option/>', {
                        value: null,
                        text: ""
                    }));
                    $.each(subZones, function (index, subZone) {
                        subZonesSelect.append($('<option/>', {
                            value: subZone.Value,
                            text: subZone.Text
                        }));
                    });
                };
            });
        }

        $("#SubZone").change(function () {
            var selectedSubZoneId = $("#SubZone").val();
            var maintenanceCentresSelect = $('#MaintenanceCentre');
            maintenanceCentresSelect.empty();
            if (selectedSubZoneId !== null && selectedSubZoneId !== '') {
                AddMaintenanceCentres(selectedSubZoneId, maintenanceCentresSelect);
            }
        });

        function AddMaintenanceCentres(selectedSubZoneId, maintenanceCentresSelect) {
            $.getJSON('http://localhost:52838/MaintenanceCentres/MaintenanceCentreView/', { subZoneId: selectedSubZoneId }, function (maintenanceCentres) {
                if (maintenanceCentres !== null && !jQuery.isEmptyObject(maintenanceCentres)) {
                    maintenanceCentresSelect.append($('<option/>', {
                        value: null,
                        text: ""
                    }));
                    $.each(maintenanceCentres, function (index, maintenanceCentre) {
                        maintenanceCentresSelect.append($('<option/>', {
                            value: maintenanceCentre.Value,
                            text: maintenanceCentre.Text
                        }));
                    });
                };
            });
        }

        $("#Make").change(function () {
            var selectedMakeId = $("#Make").val();
            var modelsSelect = $('#Model');
            modelsSelect.empty();
            if (selectedMakeId !== null && selectedMakeId !== '') {
                AddModels(selectedMakeId, modelsSelect);
            }
        });

        function AddModels(selectedMakeId, modelsSelect) {
            $.getJSON('http://localhost:52838/VehicleModels/VehicleModelView/', { makeId: selectedMakeId }, function (models) {
                if (models !== null && !jQuery.isEmptyObject(models)) {
                    modelsSelect.append($('<option/>', {
                        value: null,
                        text: ""
                    }));
                    $.each(models, function (index, model) {
                        modelsSelect.append($('<option/>', {
                            value: model.Value,
                            text: model.Text
                        }));
                    });
                };
            });
        }

