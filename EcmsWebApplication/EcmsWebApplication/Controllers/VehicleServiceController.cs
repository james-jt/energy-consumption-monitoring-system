﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class VehicleServiceController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: VehicleService
        public async Task<ActionResult> Index()
        {
            var vehicleServiceHistory = db.VehicleServiceHistory.Include(v => v.Vehicle);
            return View(await vehicleServiceHistory.ToListAsync());
        }

        // GET: VehicleService/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleServiceHistory vehicleServiceHistory = await db.VehicleServiceHistory.FindAsync(id);
            if (vehicleServiceHistory == null)
            {
                return HttpNotFound();
            }
            return View(vehicleServiceHistory);
        }

        // GET: VehicleService/Create
        public ActionResult Create()
        {
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id");
            return View();
        }

        // POST: VehicleService/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "VehicleId,DateServiced,Mileage,JobCardNumber,DocumentNumber,ServicedBy,LoggedBy,Notes")] VehicleServiceHistory vehicleServiceHistory)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleServiceHistory.VehicleId);
                return View(vehicleServiceHistory);
            }
            var logger = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;
            if (logger == null)
            {
                ModelState.AddModelError("", "Currently logged on user could not be identified.");
                ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleServiceHistory.VehicleId);
                return View(vehicleServiceHistory);
            }
            vehicleServiceHistory.LoggedBy = logger;
            if (vehicleServiceHistory.DateServiced.ToString("HH:mm:ss") == "00:00:00")
                vehicleServiceHistory.DateServiced = vehicleServiceHistory.DateServiced.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
            db.VehicleServiceHistory.Add(vehicleServiceHistory);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: VehicleService/Edit/5
        public async Task<ActionResult> Edit(string vehicleId, string serviceDate)
        {
            if (vehicleId == null || serviceDate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dateServiced = Convert.ToDateTime(serviceDate);
            VehicleServiceHistory vehicleServiceHistory = await db.VehicleServiceHistory.FindAsync(vehicleId, dateServiced);
            if (vehicleServiceHistory == null)
            {
                return HttpNotFound();
            }
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleServiceHistory.VehicleId);
            return View(vehicleServiceHistory);
        }

        // POST: VehicleService/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "VehicleId,DateServiced,Mileage,JobCardNumber,DocumentNumber,ServicedBy,LoggedBy,Notes")] VehicleServiceHistory vehicleServiceHistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicleServiceHistory).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleServiceHistory.VehicleId);
            return View(vehicleServiceHistory);
        }

        //// GET: VehicleService/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VehicleServiceHistory vehicleServiceHistory = await db.VehicleServiceHistory.FindAsync(id);
        //    if (vehicleServiceHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicleServiceHistory);
        //}

        //// POST: VehicleService/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    VehicleServiceHistory vehicleServiceHistory = await db.VehicleServiceHistory.FindAsync(id);
        //    db.VehicleServiceHistory.Remove(vehicleServiceHistory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
