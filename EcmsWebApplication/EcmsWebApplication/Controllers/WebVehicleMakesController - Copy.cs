﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsWebApplication.Models;
using Models.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class WebVehicleMakesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: WebVehicleMakes
        public async Task<ActionResult> Index()
        {
            return View(await db.VehicleMakes.ToListAsync());
        }

        // GET: WebVehicleMakes/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebVehicleMake webVehicleMake = await db.VehicleMakes.FindAsync(id);
            if (webVehicleMake == null)
            {
                return HttpNotFound();
            }
            return View(webVehicleMake);
        }

        // GET: WebVehicleMakes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WebVehicleMakes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "VehicleMakeId,Description,Notes")] WebVehicleMake webVehicleMake)
        {
            if (ModelState.IsValid)
            {
                db.VehicleMakes.Add(webVehicleMake);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(webVehicleMake);
        }

        // GET: WebVehicleMakes/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebVehicleMake webVehicleMake = await db.VehicleMakes.FindAsync(id);
            if (webVehicleMake == null)
            {
                return HttpNotFound();
            }
            return View(webVehicleMake);
        }

        // POST: WebVehicleMakes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "VehicleMakeId,Description,Notes")] WebVehicleMake webVehicleMake)
        {
            if (ModelState.IsValid)
            {
                db.Entry(webVehicleMake).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(webVehicleMake);
        }

        // GET: WebVehicleMakes/Delete/5
        public async Task<ActionResult> Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebVehicleMake webVehicleMake = await db.VehicleMakes.FindAsync(id);
            if (webVehicleMake == null)
            {
                return HttpNotFound();
            }
            return View(webVehicleMake);
        }

        // POST: WebVehicleMakes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(short id)
        {
            WebVehicleMake webVehicleMake = await db.VehicleMakes.FindAsync(id);
            db.VehicleMakes.Remove(webVehicleMake);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
