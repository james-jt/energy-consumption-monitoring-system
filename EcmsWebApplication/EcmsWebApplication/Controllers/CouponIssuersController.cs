﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class CouponIssuersController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CouponIssuers
        public async Task<ActionResult> Index()
        {
            return View(await db.CouponIssuer.ToListAsync());
        }

        // GET: CouponIssuers/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponIssuer couponIssuer = await db.CouponIssuer.FindAsync(id);
            if (couponIssuer == null)
            {
                return HttpNotFound();
            }
            return View(couponIssuer);
        }

        // GET: CouponIssuers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CouponIssuers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Disabled,Notes")] CouponIssuer couponIssuer)
        {
            if (ModelState.IsValid)
            {
                db.CouponIssuer.Add(couponIssuer);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(couponIssuer);
        }

        // GET: CouponIssuers/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponIssuer couponIssuer = await db.CouponIssuer.FindAsync(id);
            if (couponIssuer == null)
            {
                return HttpNotFound();
            }
            return View(couponIssuer);
        }

        // POST: CouponIssuers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Disabled,Notes")] CouponIssuer couponIssuer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(couponIssuer).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(couponIssuer);
        }

        //// GET: CouponIssuers/Delete/5
        //public async Task<ActionResult> Delete(byte? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponIssuer couponIssuer = await db.CouponIssuer.FindAsync(id);
        //    if (couponIssuer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(couponIssuer);
        //}

        //// POST: CouponIssuers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(byte id)
        //{
        //    CouponIssuer couponIssuer = await db.CouponIssuer.FindAsync(id);
        //    db.CouponIssuer.Remove(couponIssuer);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
