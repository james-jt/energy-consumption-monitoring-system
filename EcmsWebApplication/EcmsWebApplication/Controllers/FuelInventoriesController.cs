﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class FuelInventoriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelInventories
        public async Task<ActionResult> Index()
        {
            var fuelInventory = db.FuelInventory.Include(f => f.FuelSource).Include(f => f.FuelType);
            return View(await fuelInventory.ToListAsync());
        }

        // GET: FuelInventories/Details/5
        public async Task<ActionResult> Details(int? sourceId, int? fuelTypeId)
        {
            if (sourceId == null || fuelTypeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventory fuelInventory = await db.FuelInventory.FindAsync(sourceId, fuelTypeId);
            if (fuelInventory == null)
            {
                return HttpNotFound();
            }
            return View(fuelInventory);
        }

        // GET: FuelInventories/Create
        public ActionResult Create()
        {
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name");
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");
            return View();
        }

        // POST: FuelInventories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FuelSourceId,FuelTypeId,Capacity,CurrentQuantity,ReorderLevel,ReorderTriggered,Disabled,Notes")] FuelInventory fuelInventory)
        {
            if (ModelState.IsValid)
            {
                var testFuelInventory = await db.FuelInventory.FindAsync(fuelInventory.FuelSourceId, fuelInventory.FuelTypeId);
                if (testFuelInventory == null)
                {
                    var testFuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(fuelInventory.FuelSourceId, fuelInventory.FuelTypeId, DateTime.Now.Year, DateTime.Now.Month);
                    if (testFuelInventoryHistory == null)
                    {
                        var history = new FuelInventoryHistory();
                        history.FuelSourceId = fuelInventory.FuelSourceId;
                        history.FuelTypeId = fuelInventory.FuelTypeId;
                        history.Year = DateTime.Now.Year;
                        history.Period = DateTime.Now.Month;
                        history.ClosingDate = DateTime.MinValue.ConvertToDbMinDate();
                        db.FuelInventoryHistory.Add(history);
                    }
                    db.FuelInventory.Add(fuelInventory);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "An inventory item for the selected Fuel Store and Fuel Type combination already exists.");
            }

            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventory.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventory.FuelTypeId);
            return View(fuelInventory);
        }

        // GET: FuelInventories/Edit/5
        public async Task<ActionResult> Edit(int? sourceId, int? fuelTypeId)
        {
            if (sourceId == null || fuelTypeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventory fuelInventory = await db.FuelInventory.FindAsync(sourceId, fuelTypeId);
            if (fuelInventory == null)
            {
                return HttpNotFound();
            }
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventory.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventory.FuelTypeId);
            return View(fuelInventory);
        }

        // POST: FuelInventories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FuelSourceId,FuelTypeId,Capacity,CurrentQuantity,ReorderLevel,ReorderTriggered,Disabled,Notes")] FuelInventory fuelInventory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelInventory).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventory.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventory.FuelTypeId);
            return View(fuelInventory);
        }

        //// GET: FuelInventories/Delete/5
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FuelInventory fuelInventory = await db.FuelInventory.FindAsync(id);
        //    if (fuelInventory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fuelInventory);
        //}
        //// POST: FuelInventories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(short id)
        //{
        //    FuelInventory fuelInventory = await db.FuelInventory.FindAsync(id);
        //    db.FuelInventory.Remove(fuelInventory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
