﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class LandlordsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: Landlords
        public async Task<ActionResult> Index()
        {
            return View(await db.Landlord.ToListAsync());
        }

        // GET: Landlords/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Landlord landlord = await db.Landlord.FindAsync(id);
            if (landlord == null)
            {
                return HttpNotFound();
            }
            return View(landlord);
        }

        // GET: Landlords/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Landlords/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Address,Email,Phone,Disabled,Notes")] Landlord landlord)
        {
            if (ModelState.IsValid)
            {
                var check = db.Landlord.FirstOrDefault(e => e.Name == landlord.Name);
                if (check == null)
                {
                    db.Landlord.Add(landlord);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A landlord with the same name already exists. The name must be unique.");
            }

            return View(landlord);
        }

        // GET: Landlords/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Landlord landlord = await db.Landlord.FindAsync(id);
            if (landlord == null)
            {
                return HttpNotFound();
            }
            return View(landlord);
        }

        // POST: Landlords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Address,Email,Phone,Disabled,Notes")] Landlord landlord)
        {
            if (ModelState.IsValid)
            {
                var check = db.Landlord.FirstOrDefault(e => e.Name == landlord.Name);
                if (check == null)
                {
                    db.Entry(landlord).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A landlord with the same name already exists. The name must be unique.");
            }
            return View(landlord);
        }

        //// GET: Landlords/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Landlord landlord = await db.Landlords.FindAsync(id);
        //    if (landlord == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(landlord);
        //}
        //// POST: Landlords/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Landlord landlord = await db.Landlords.FindAsync(id);
        //    db.Landlords.Remove(landlord);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
