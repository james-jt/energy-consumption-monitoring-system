﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EcmsBusinessLogic;
using EcmsModels.DataModels;
using EcmsWebApplication.Models;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class DepartmentController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();
        public static DepartmentViewModel departmentDynamicViewModel = new DepartmentViewModel();

        // GET: Department
        public ActionResult Index()
        {
            var departments = db.Department;
            return View(departments.ToList());
        }

        // GET: Department/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Department.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);

        }

        // GET: Department/Create
        public ActionResult Create()
        {
            ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: Department/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department department)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email");
                return View(department);
            }
            try
            {
                var result = db.Department.SingleOrDefault(d => d.Name == department.Name);
                if (result == null)
                {
                    db.Department.Add(department);
                    db.SaveChanges();
                    var defaultSection = Section.CreateDefault(department);
                    db.SaveChanges();
                    var defaultSubSection = SubSection.CreateDefault(defaultSection);
                    db.SubSection.Add(defaultSubSection);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A Department with the same name already exists.");
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", department.OverseerUserId);
                return View(department);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", department.OverseerUserId);
                return View();
            }
        }

        // GET: Department/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var department = db.Department.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            ViewBag.OverseerUserId = new SelectList( department.GetUsers(), "Id", "Email", department.OverseerUserId );
            return View(department);
        }

        // POST: Department/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Department department)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.OverseerUserId = new SelectList(department.GetUsers(), "Id", "Email", department.OverseerUserId);
                return View(department);
            }
            try
            {
                var oldDepartment = db.Department.Find(department.Id);
                if (oldDepartment != null)
                {
                    var result = db.Department.SingleOrDefault(d => d.Name == department.Name && d.Id != department.Id);
                    if (result == null)
                    {
                        db.Entry(oldDepartment).State = System.Data.Entity.EntityState.Detached;
                        db.Entry(department).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", "A department with the same name already exists.");
                    ViewBag.OverseerUserId = new SelectList(department.GetUsers(), "Id", "Email", department.OverseerUserId);
                    return View(department);
                }
                ModelState.AddModelError("", "The specified department no longer exists.");
                ViewBag.OverseerUserId = new SelectList(department.GetUsers(), "Id", "Email", department.OverseerUserId);
                return View(department);
            }
            catch(Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.OverseerUserId = new SelectList(department.GetUsers(), "Id", "Email", department.OverseerUserId);
                return View(department);
            }
        }

        public ActionResult DepartmentPartial()
        {
            departmentDynamicViewModel.EntityList.Clear();
            departmentDynamicViewModel.EntityList.AddRange(db.Department);
            return View(departmentDynamicViewModel);
        }

        //// GET: Department/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Department/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
