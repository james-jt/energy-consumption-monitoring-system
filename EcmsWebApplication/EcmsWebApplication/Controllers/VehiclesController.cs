﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class VehiclesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: Vehicles
        public async Task<ActionResult> Index()
        {
            var vehicles = db.Vehicle.Include(v => v.SubSection).Include(v => v.FuelType).Include(v => v.VehicleBodyType).Include(v => v.VehicleModel).Include(v => v.VehicleStatus);
            return View(await vehicles.ToListAsync());
        }

        // GET: Vehicle/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = await db.Vehicle.FindAsync(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // GET: Vehicles/Create
        public ActionResult Create()
        {
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name");
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");
            ViewBag.BodyTypeId = new SelectList(db.VehicleBodyType, "Id", "Name");
            ViewBag.MakeId = new SelectList(db.VehicleMake, "Id", "Name");
            ViewBag.ModelId = new SelectList(db.VehicleModel, "Id", "Name");
            ViewBag.StatusId = new SelectList(db.VehicleStatus, "Id", "Description");
            return View();
        }

        // POST: Vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,DepartmentId,ModelId,BodyTypeId,FuelTypeId,StatusId,YearFirstUsedAsNew,EngineCapacity,FuelTankCapacity,StartMileage")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                var check = db.Vehicle.FindAsync(vehicle.Id);
                if (check == null)
                {
                    vehicle.Id = vehicle.Id.Replace(" ", "").Trim().ToUpper();
                    db.Vehicle.Add(vehicle);
                    var subSection = await db.SubSection.FindAsync(vehicle.SubSectionId);
                    if (subSection == null)
                        subSection = SubSection.GetDefault(Section.GetDefault(Department.GetDefault()));
                    if (subSection != null)
                    {
                        var vehicleDeploymentHistory = new VehicleSubSectionDeploymentHistory();
                        var changedBy = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;
                        var dateChanged = DateTime.Today.GetCondensedDateTime();
                        vehicleDeploymentHistory.SubSectionId = vehicle.SubSectionId;
                        vehicleDeploymentHistory.VehicleId = vehicle.Id;
                        vehicleDeploymentHistory.DateDeployed = dateChanged;
                        vehicleDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                        vehicleDeploymentHistory.DeployedBy = changedBy;
                        db.VehicleSubSectionDeploymentHistory.Add(vehicleDeploymentHistory);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", "Could not assign vehicle to a sub-section.");
                }
                ModelState.AddModelError("", "A vehicle with the same registration number already exists.");
            }
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", vehicle.SubSectionId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", vehicle.FuelTypeId);
            ViewBag.BodyTypeId = new SelectList(db.VehicleBodyType, "Id", "Name", vehicle.BodyTypeId);
            ViewBag.MakeId = new SelectList(db.VehicleMake, "Id", "Name");
            ViewBag.ModelId = new SelectList(db.VehicleModel, "Id", "Name", vehicle.ModelId);
            ViewBag.StatusId = new SelectList(db.VehicleStatus, "Id", "Description", vehicle.StatusId);
            return View(vehicle);
        }

        // GET: Vehicle/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = await db.Vehicle.FindAsync(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", vehicle.SubSectionId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", vehicle.FuelTypeId);
            ViewBag.BodyTypeId = new SelectList(db.VehicleBodyType, "Id", "Name", vehicle.BodyTypeId);
            ViewBag.MakeId = new SelectList(db.VehicleMake, "Id", "Name");
            ViewBag.ModelId = new SelectList(db.VehicleModel, "Id", "Name", vehicle.ModelId);
            ViewBag.StatusId = new SelectList(db.VehicleStatus, "Id", "Description", vehicle.StatusId);
            return View(vehicle);
        }

        // POST: Vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DepartmentId,ModelId,BodyTypeId,FuelTypeId,StatusId,YearFirstUsedAsNew,EngineCapacity,FuelTankCapacity,StartMileage")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                var oldVehicle = await db.Vehicle.FindAsync(vehicle.Id);
                db.Entry(oldVehicle).State = EntityState.Detached;
                if (oldVehicle.SubSectionId != vehicle.SubSectionId)
                {
                    var changedBy = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;
                    var history = db.VehicleSubSectionDeploymentHistory.Where(h => h.VehicleId == oldVehicle.Id && h.SubSectionId == oldVehicle.SubSectionId && string.IsNullOrEmpty(h.RecalledBy)).OrderByDescending(o => o.DateDeployed).FirstOrDefault();
                    if (history != null)
                    {
                        history.RecalledBy = changedBy;
                        history.DateRecalled = DateTime.Today.GetCondensedDateTime();
                        db.Entry(history).State = EntityState.Modified;
                    }
                    var vehicleDeploymentHistory = new VehicleSubSectionDeploymentHistory();
                    vehicleDeploymentHistory.SubSectionId = vehicle.SubSectionId;
                    vehicleDeploymentHistory.VehicleId = vehicle.Id;
                    vehicleDeploymentHistory.DateDeployed = DateTime.Today.GetCondensedDateTime();
                    vehicleDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                    vehicleDeploymentHistory.DeployedBy = changedBy;
                    db.VehicleSubSectionDeploymentHistory.Add(vehicleDeploymentHistory);
                }
                db.Entry(vehicle).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", vehicle.SubSectionId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", vehicle.FuelTypeId);
            ViewBag.BodyTypeId = new SelectList(db.VehicleBodyType, "Id", "Name", vehicle.BodyTypeId);
            ViewBag.MakeId = new SelectList(db.VehicleMake, "Id", "Name");
            ViewBag.ModelId = new SelectList(db.VehicleModel, "Id", "Name", vehicle.ModelId);
            ViewBag.StatusId = new SelectList(db.VehicleStatus, "Id", "Description", vehicle.StatusId);
            return View(vehicle);
        }

        //// GET: Vehicle/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Vehicle vehicle = await db.Vehicle.FindAsync(id);
        //    if (vehicle == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicle);
        //}
        //// POST: Vehicle/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    Vehicle vehicle = await db.Vehicle.FindAsync(id);
        //    db.Vehicle.Remove(vehicle);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
