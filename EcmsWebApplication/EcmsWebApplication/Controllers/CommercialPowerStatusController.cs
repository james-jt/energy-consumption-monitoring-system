﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class CommercialPowerStatusController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CommercialPowerStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.CommercialPowerStatus.ToListAsync());
        }

        // GET: CommercialPowerStatus/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommercialPowerStatus commercialPowerStatus = await db.CommercialPowerStatus.FindAsync(id);
            if (commercialPowerStatus == null)
            {
                return HttpNotFound();
            }
            return View(commercialPowerStatus);
        }

        // GET: CommercialPowerStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CommercialPowerStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Notes")] CommercialPowerStatus commercialPowerStatus)
        {
            if (ModelState.IsValid)
            {
                var check = db.CommercialPowerStatus.FirstOrDefault(e => e.Description == commercialPowerStatus.Description);
                if(check == null)
                {
                    db.CommercialPowerStatus.Add(commercialPowerStatus);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A Power Status with the same description already exists.");
            }

            return View(commercialPowerStatus);
        }

        // GET: CommercialPowerStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommercialPowerStatus commercialPowerStatus = await db.CommercialPowerStatus.FindAsync(id);
            if (commercialPowerStatus == null)
            {
                return HttpNotFound();
            }
            return View(commercialPowerStatus);
        }

        // POST: CommercialPowerStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Notes")] CommercialPowerStatus commercialPowerStatus)
        {
            if (ModelState.IsValid)
            {
                var check = db.CommercialPowerStatus.FirstOrDefault(e => e.Description == commercialPowerStatus.Description && e.Id != commercialPowerStatus.Id);
                if (check == null)
                { 
                    db.Entry(commercialPowerStatus).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A Power Status with the same description already exists.");
            }
                return View(commercialPowerStatus);
        }

        // GET: CommercialPowerStatus/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommercialPowerStatus commercialPowerStatus = await db.CommercialPowerStatus.FindAsync(id);
            if (commercialPowerStatus == null)
            {
                return HttpNotFound();
            }
            return View(commercialPowerStatus);
        }

        // POST: CommercialPowerStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            CommercialPowerStatus commercialPowerStatus = await db.CommercialPowerStatus.FindAsync(id);
            db.CommercialPowerStatus.Remove(commercialPowerStatus);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
