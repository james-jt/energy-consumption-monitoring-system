﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class CouponBatchStatusController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CouponBatchStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.CouponBatchStatus.ToListAsync());
        }

        // GET: CouponBatchStatus/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponBatchStatus couponBatchStatus = await db.CouponBatchStatus.FindAsync(id);
            if (couponBatchStatus == null)
            {
                return HttpNotFound();
            }
            return View(couponBatchStatus);
        }

        // GET: CouponBatchStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CouponBatchStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Notes")] CouponBatchStatus couponBatchStatus)
        {
            if (ModelState.IsValid)
            {
                db.CouponBatchStatus.Add(couponBatchStatus);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(couponBatchStatus);
        }

        // GET: CouponBatchStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponBatchStatus couponBatchStatus = await db.CouponBatchStatus.FindAsync(id);
            if (couponBatchStatus == null)
            {
                return HttpNotFound();
            }
            return View(couponBatchStatus);
        }

        // POST: CouponBatchStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Notes")] CouponBatchStatus couponBatchStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(couponBatchStatus).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(couponBatchStatus);
        }

        // GET: CouponBatchStatus/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponBatchStatus couponBatchStatus = await db.CouponBatchStatus.FindAsync(id);
            if (couponBatchStatus == null)
            {
                return HttpNotFound();
            }
            return View(couponBatchStatus);
        }

        // POST: CouponBatchStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            CouponBatchStatus couponBatchStatus = await db.CouponBatchStatus.FindAsync(id);
            db.CouponBatchStatus.Remove(couponBatchStatus);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
