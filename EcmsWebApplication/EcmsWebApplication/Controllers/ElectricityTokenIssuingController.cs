﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.WebModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class ElectricityTokenIssuingController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectricityTokenIssuing
        public async Task<ActionResult> Index()
        {
            var electricityTokenIssues = db.ElectricityTokenIssue.Include(e => e.ElectricityPurchases).Include(e => e.ElectricityRecharge).Include(e => e.ElectricityRequest);
            return View(await electricityTokenIssues.ToListAsync());
        }

        // GET: ElectricityTokenIssuing/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityTokenIssue electricityTokenIssue = await db.ElectricityTokenIssue.FindAsync(id);
            if (electricityTokenIssue == null)
            {
                return HttpNotFound();
            }
            return View(electricityTokenIssue);
        }

        // GET: ElectricityTokenIssuing/Create
        public ActionResult Create()
        {
            ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchase.Where(p => p.IssuedBy == null && p.ReceiptNumber != null), "Id", "MeterNumber");
            //ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId");
            //ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy");
            return View();
        }

        // POST: ElectricityTokenIssuing/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ElectricityTokenIssueViewModel electricityTokenIssueViewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchase.Where(p => p.IssuedBy == null && p.ReceiptNumber != null), "Id", "MeterNumber");
                //ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId", electricityTokenIssue.Id);
                //ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy", electricityTokenIssue.Id);
                return View(electricityTokenIssueViewModel);
            }
            if (!electricityTokenIssueViewModel.TokenIssueConfirmed)
            {
                var purchase = await db.ElectricityPurchase.FindAsync(electricityTokenIssueViewModel.ElectricityPurchaseId);
                if(purchase == null)
                {
                    ModelState.AddModelError("", "The associated token purchase record no longer exists. Contact IT for assistance.");
                    ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchase.Where(p => p.IssuedBy == null && p.ReceiptNumber != null), "Id", "MeterNumber");
                    //ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId", electricityTokenIssue.Id);
                    //ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy", electricityTokenIssue.Id);
                    return View(electricityTokenIssueViewModel);
                }
                electricityTokenIssueViewModel.TokenIssueConfirmed = true;
                //bind rest of properties from the purchase object to the electricityTokenIssueViewModel object here. No time to do it for now :-)
                return View(electricityTokenIssueViewModel);
            }
            var electricityPurchase = await db.ElectricityPurchase.FindAsync(electricityTokenIssueViewModel.ElectricityPurchaseId);
            if(electricityPurchase == null)
            {
                ModelState.AddModelError("", "The associated token purchase record no longer exists. Contact IT for assistance.");
                ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchase.Where(p => p.IssuedBy == null && p.ReceiptNumber != null), "Id", "MeterNumber");
                //ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId", electricityTokenIssue.Id);
                //ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy", electricityTokenIssue.Id);
                return View(electricityTokenIssueViewModel);
            }
            var issuedBy = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;
            electricityPurchase.IssuedBy = issuedBy;
            ElectricityTokenIssue electricityTokenIssue = new ElectricityTokenIssue();
            electricityTokenIssue.DateIssued = DateTime.Today.GetCondensedDateTime();
            electricityTokenIssue.Id = electricityPurchase.Id;
            electricityTokenIssue.ElectricityPurchases = electricityPurchase;
            db.ElectricityTokenIssue.Add(electricityTokenIssue);
            var electricityRequest = await db.ElectricityRequest.FindAsync(electricityTokenIssue.Id);
            if(electricityRequest == null)
            {
                ModelState.AddModelError("", "The associated request no longer exists. Contact IT for assistance.");
                ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchase.Where(p => p.IssuedBy == null && p.ReceiptNumber != null), "Id", "MeterNumber");
                //ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId", electricityTokenIssue.Id);
                //ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy", electricityTokenIssue.Id);
                return View(electricityTokenIssueViewModel);
            }
            await db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        //// GET: ElectricityTokenIssuing/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectricityTokenIssue electricityTokenIssue = await db.ElectricityTokenIssues.FindAsync(id);
        //    if (electricityTokenIssue == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchases, "Id", "ReceiptNumber", electricityTokenIssue.ElectricityPurchaseId);
        //    ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId", electricityTokenIssue.Id);
        //    ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy", electricityTokenIssue.Id);
        //    return View(electricityTokenIssue);
        //}

        //// POST: ElectricityTokenIssuing/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,ElectricityPurchaseId,DateIssued")] ElectricityTokenIssue electricityTokenIssue)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(electricityTokenIssue).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ElectricityPurchaseId = new SelectList(db.ElectricityPurchases, "Id", "ReceiptNumber", electricityTokenIssue.ElectricityPurchaseId);
        //    ViewBag.Id = new SelectList(db.ElectricityRecharges, "Id", "MeterId", electricityTokenIssue.Id);
        //    ViewBag.Id = new SelectList(db.ElectricityRequests, "Id", "RequestedBy", electricityTokenIssue.Id);
        //    return View(electricityTokenIssue);
        //}

        //// GET: ElectricityTokenIssuing/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectricityTokenIssue electricityTokenIssue = await db.ElectricityTokenIssues.FindAsync(id);
        //    if (electricityTokenIssue == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(electricityTokenIssue);
        //}

        //// POST: ElectricityTokenIssuing/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    ElectricityTokenIssue electricityTokenIssue = await db.ElectricityTokenIssues.FindAsync(id);
        //    db.ElectricityTokenIssues.Remove(electricityTokenIssue);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
