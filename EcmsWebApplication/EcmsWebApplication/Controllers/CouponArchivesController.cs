﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class CouponArchivesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CouponArchives
        public async Task<ActionResult> Index()
        {
            return View(await db.CouponArchive.ToListAsync());
        }

        // GET: CouponArchives/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponArchive couponArchive = await db.CouponArchive.FindAsync(id);
            if (couponArchive == null)
            {
                return HttpNotFound();
            }
            return View(couponArchive);
        }

        //// GET: CouponArchives/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}
        //// POST: CouponArchives/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,DateActioned,BatchId,RequestId,RedemptionPoint,RedeemedBy,ReasonCancelled,CancelledBy,Notes,FuelTypeId,SubSectionId,CreatedBy,ExpiryDate,DateCreated,Denomination,CouponIssuerId")] CouponArchive couponArchive)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.CouponArchive.Add(couponArchive);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(couponArchive);
        //}
       //// GET: CouponArchives/Edit/5
        //public async Task<ActionResult> Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponArchive couponArchive = await db.CouponArchive.FindAsync(id);
        //    if (couponArchive == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(couponArchive);
        //}
        //// POST: CouponArchives/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,DateActioned,BatchId,RequestId,RedemptionPoint,RedeemedBy,ReasonCancelled,CancelledBy,Notes,FuelTypeId,SubSectionId,CreatedBy,ExpiryDate,DateCreated,Denomination,CouponIssuerId")] CouponArchive couponArchive)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(couponArchive).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(couponArchive);
        //}
        //// GET: CouponArchives/Delete/5
        //public async Task<ActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponArchive couponArchive = await db.CouponArchive.FindAsync(id);
        //    if (couponArchive == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(couponArchive);
        //}
        //// POST: CouponArchives/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(long id)
        //{
        //    CouponArchive couponArchive = await db.CouponArchive.FindAsync(id);
        //    db.CouponArchive.Remove(couponArchive);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
