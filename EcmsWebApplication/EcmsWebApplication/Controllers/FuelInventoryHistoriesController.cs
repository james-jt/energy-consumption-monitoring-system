﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class FuelInventoryHistoriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelInventoryHistories
        public async Task<ActionResult> Index()
        {
            var fuelInventoryHistory = db.FuelInventoryHistory.Include(f => f.FuelSource).Include(f => f.FuelType);
            return View(await fuelInventoryHistory.ToListAsync());
        }

        // GET: FuelInventoryHistories/Details/5
        public async Task<ActionResult> Details(int fuelSourceId, int fuelTypeId, int Year, int Period)
        {
            FuelInventoryHistory fuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(fuelSourceId, fuelTypeId, Year, Period);
            if (fuelInventoryHistory == null)
            {
                return HttpNotFound();
            }
            return View(fuelInventoryHistory);
        }

        //// GET: FuelInventoryHistories/Create
        //public ActionResult Create()
        //{
        //    ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name");
        //    ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");
        //    return View();
        //}
        //// POST: FuelInventoryHistories/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "FuelSourceId,FuelTypeId,Year,Period,OpeningDate,OpeningQuantity,Notes")] FuelInventoryHistory fuelInventoryHistory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.FuelInventoryHistory.Add(fuelInventoryHistory);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventoryHistory.FuelSourceId);
        //    ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventoryHistory.FuelTypeId);
        //    return View(fuelInventoryHistory);
        //}


        //// GET: FuelInventoryHistories/Edit/5
        //public async Task<ActionResult> Edit(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FuelInventoryHistory fuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(id);
        //    if (fuelInventoryHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventoryHistory.FuelSourceId);
        //    ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventoryHistory.FuelTypeId);
        //    return View(fuelInventoryHistory);
        //}

        //// POST: FuelInventoryHistories/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "FuelSourceId,FuelTypeId,Year,Period,OpeningDate,OpeningQuantity,Notes")] FuelInventoryHistory fuelInventoryHistory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(fuelInventoryHistory).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventoryHistory.FuelSourceId);
        //    ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventoryHistory.FuelTypeId);
        //    return View(fuelInventoryHistory);
        //}

        //// GET: FuelInventoryHistories/Delete/5
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FuelInventoryHistory fuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(id);
        //    if (fuelInventoryHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fuelInventoryHistory);
        //}
        //// POST: FuelInventoryHistories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(short id)
        //{
        //    FuelInventoryHistory fuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(id);
        //    db.FuelInventoryHistory.Remove(fuelInventoryHistory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
