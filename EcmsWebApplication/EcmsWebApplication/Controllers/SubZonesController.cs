﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsWebApplication.Models;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class SubZonesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();
        public static SubZoneViewModel subZoneDynamicViewModel = new SubZoneViewModel();

        // GET: SubZones
        public async Task<ActionResult> Index()
        {
            var subZone = db.SubZone.Include(s => s.SubSection);
            return View(await subZone.ToListAsync());
        }

        // GET: SubZones/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubZone subZone = await db.SubZone.FindAsync(id);
            if (subZone == null)
            {
                return HttpNotFound();
            }
            return View(subZone);
        }

        // GET: SubZones/Create
        public ActionResult Create()
        {
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name");
            ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: SubZones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,SubSectionId")] SubZone subZone)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subZone.OverseerUserId);
                return View(subZone);
            }
            try
            {
                var subSection = await db.SubSection.FindAsync(subZone.SubSectionId);
                if (subSection != null)
                {
                    var result = subSection.SubZones.SingleOrDefault(e => e.Name == subZone.Name);
                    if (result == null)
                    {
                        db.SubZone.Add(subZone);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Sub Zone with the same name already exists in the {subSection.Name} Sub Section.");
                    ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                    ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subZone.OverseerUserId);
                    return View(subZone);
                }
                ModelState.AddModelError("", "The Sub Section you specified no longer exists.");
                ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subZone.OverseerUserId);
                return View(subZone);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subZone.OverseerUserId);
                return View(subZone);
            }
        }

        // GET: SubZones/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubZone subZone = await db.SubZone.FindAsync(id);
            if (subZone == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
            ViewBag.OverseerUserId = new SelectList(db.SubSection.Find(subZone.SubSectionId).GetUsers(), "Id", "Email", subZone.OverseerUserId);
            return View(subZone);
        }

        // POST: SubZones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,SubSectionId")] SubZone subZone)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                ViewBag.OverseerUserId = new SelectList(db.SubSection.Find(subZone.SubSectionId).GetUsers(), "Id", "Email", subZone.OverseerUserId);
                return View(subZone);
            }
            try
            {
                var oldSubZone = await db.SubZone.FindAsync(subZone.Id);
                if (oldSubZone != null)
                {
                    var result = oldSubZone.SubSection.SubZones.SingleOrDefault(e => e.Name == subZone.Name && e.Id != subZone.Id);
                    if (result == null)
                    {
                        db.Entry(oldSubZone).State = EntityState.Detached;
                        db.Entry(subZone).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Sub Zone with the same name already exists in the {oldSubZone.Name} Sub Section.");
                    ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                    ViewBag.OverseerUserId = new SelectList(db.SubSection.Find(subZone.SubSectionId).GetUsers(), "Id", "Email", subZone.OverseerUserId);
                    return View(subZone);
                }
                ModelState.AddModelError("", "The Sub Zone you specified no longer exists.");
                ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                ViewBag.OverseerUserId = new SelectList(db.SubSection.Find(subZone.SubSectionId).GetUsers(), "Id", "Email", subZone.OverseerUserId);
                return View(subZone);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", subZone.SubSectionId);
                ViewBag.OverseerUserId = new SelectList(db.SubSection.Find(subZone.SubSectionId).GetUsers(), "Id", "Email", subZone.OverseerUserId);
                return View(subZone);
            }
        }

        public ActionResult SubZonePartial(int? subSectionId)
        {
            subZoneDynamicViewModel.EntityList.Clear();
            if (subSectionId != null)
            {
                var subSection = db.SubSection.Find(subSectionId);
                subZoneDynamicViewModel.EntityList.AddRange(subSection.SubZones);
            }
            return View(subZoneDynamicViewModel);
        }

        public ActionResult SubZoneView(int? subSectionId)
        {
            subZoneDynamicViewModel.EntityList.Clear();
            if (subSectionId != null)
            {
                var subsection = db.SubSection.Find(subSectionId);
                subZoneDynamicViewModel.EntityList.AddRange(subsection.SubZones);
            }
            return Json(subZoneDynamicViewModel.EntitySelectList, JsonRequestBehavior.AllowGet);
        }


        //// GET: SubZones/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SubZone subZone = await db.SubZone.FindAsync(id);
        //    if (subZone == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(subZone);
        //}

        //// POST: SubZones/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    SubZone subZone = await db.SubZone.FindAsync(id);
        //    db.SubZone.Remove(subZone);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
