﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class ElectricityRequestsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectricityRequests
        public async Task<ActionResult> Index()
        {
            var electricityRequests = db.ElectricityRequest.Include(e => e.ElectricityTokenIssue).Include(e => e.Requestor);
            return View(await electricityRequests.ToListAsync());
        }

        // GET: ElectricityRequests/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityRequest electricityRequest = await db.ElectricityRequest.FindAsync(id);
            if (electricityRequest == null)
            {
                return HttpNotFound();
            }
            return View(electricityRequest);
        }

        // GET: ElectricityRequests/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.ElectricityTokenIssue, "Id", "ElectricityPurchaseId");
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName");
            return View();
        }

        // POST: ElectricityRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,SiteId,DateRequested,RequestedBy,DepartmentId,SectionId,SubSectionId,RequestorComment,Units,DocumentNumber,DateActioned,ActionedBy,Authorised,ActionerComment,Notes")] ElectricityRequest electricityRequest)
        {
            if (ModelState.IsValid)
            {
                db.ElectricityRequest.Add(electricityRequest);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.ElectricityTokenIssue, "Id", "ElectricityPurchaseId", electricityRequest.Id);
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName", electricityRequest.RequestedBy);
            return View(electricityRequest);
        }

        // GET: ElectricityRequests/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityRequest electricityRequest = await db.ElectricityRequest.FindAsync(id);
            if (electricityRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.ElectricityTokenIssue, "Id", "ElectricityPurchaseId", electricityRequest.Id);
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName", electricityRequest.RequestedBy);
            return View(electricityRequest);
        }

        // POST: ElectricityRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,SiteId,DateRequested,RequestedBy,DepartmentId,SectionId,SubSectionId,RequestorComment,Units,DocumentNumber,DateActioned,ActionedBy,Authorised,ActionerComment,Notes")] ElectricityRequest electricityRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(electricityRequest).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.ElectricityTokenIssue, "Id", "ElectricityPurchaseId", electricityRequest.Id);
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName", electricityRequest.RequestedBy);
            return View(electricityRequest);
        }

        // GET: ElectricityRequests/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityRequest electricityRequest = await db.ElectricityRequest.FindAsync(id);
            if (electricityRequest == null)
            {
                return HttpNotFound();
            }
            return View(electricityRequest);
        }

        // POST: ElectricityRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ElectricityRequest electricityRequest = await db.ElectricityRequest.FindAsync(id);
            db.ElectricityRequest.Remove(electricityRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Approve()
        {
            var electricityRequest = db.ElectricityRequest.Include(f => f.ElectricityTokenIssue).Include(f => f.Requestor).FirstOrDefault();
            return View(electricityRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
