﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class ElectricityMeterStatusController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectricityMeterStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.ElectricityMeterStatus.ToListAsync());
        }

        // GET: ElectricityMeterStatus/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityMeterStatus electricityMeterStatus = await db.ElectricityMeterStatus.FindAsync(id);
            if (electricityMeterStatus == null)
            {
                return HttpNotFound();
            }
            return View(electricityMeterStatus);
        }

        // GET: ElectricityMeterStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ElectricityMeterStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Notes,Disabled")] ElectricityMeterStatus electricityMeterStatus)
        {
            if (ModelState.IsValid)
            {
                var check = db.ElectricityMeterStatus.FirstOrDefault(e => e.Description == electricityMeterStatus.Description);
                if (check == null)
                {
                    db.ElectricityMeterStatus.Add(electricityMeterStatus);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "An Electricity Meter Status with the same description already exists.");
            }

            return View(electricityMeterStatus);
        }

        // GET: ElectricityMeterStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityMeterStatus electricityMeterStatus = await db.ElectricityMeterStatus.FindAsync(id);
            if (electricityMeterStatus == null)
            {
                return HttpNotFound();
            }
            return View(electricityMeterStatus);
        }

        // POST: ElectricityMeterStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Notes,Disabled")] ElectricityMeterStatus electricityMeterStatus)
        {
            if (ModelState.IsValid)
            {
                var check = db.ElectricityMeterStatus.FirstOrDefault(e => e.Description == electricityMeterStatus.Description && e.Id != electricityMeterStatus.Id);
                if (check == null)
                {
                    db.Entry(electricityMeterStatus).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "An Electricity Meter Status with the same description already exists.");

            }
            return View(electricityMeterStatus);
        }

        // GET: ElectricityMeterStatus/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityMeterStatus electricityMeterStatus = await db.ElectricityMeterStatus.FindAsync(id);
            if (electricityMeterStatus == null)
            {
                return HttpNotFound();
            }
            return View(electricityMeterStatus);
        }

        // POST: ElectricityMeterStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            ElectricityMeterStatus electricityMeterStatus = await db.ElectricityMeterStatus.FindAsync(id);
            db.ElectricityMeterStatus.Remove(electricityMeterStatus);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
