﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class GeneratorStatusController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: GeneratorStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.GeneratorStatus.ToListAsync());
        }

        // GET: GeneratorStatus/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneratorStatus generatorStatus = await db.GeneratorStatus.FindAsync(id);
            if (generatorStatus == null)
            {
                return HttpNotFound();
            }
            return View(generatorStatus);
        }

        // GET: GeneratorStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GeneratorStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Notes,Disabled")] GeneratorStatus generatorStatus)
        {
            if (ModelState.IsValid)
            {
                db.GeneratorStatus.Add(generatorStatus);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(generatorStatus);
        }

        // GET: GeneratorStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneratorStatus generatorStatus = await db.GeneratorStatus.FindAsync(id);
            if (generatorStatus == null)
            {
                return HttpNotFound();
            }
            return View(generatorStatus);
        }

        // POST: GeneratorStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Notes,Disabled")] GeneratorStatus generatorStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(generatorStatus).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(generatorStatus);
        }

        // GET: GeneratorStatus/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneratorStatus generatorStatus = await db.GeneratorStatus.FindAsync(id);
            if (generatorStatus == null)
            {
                return HttpNotFound();
            }
            return View(generatorStatus);
        }

        // POST: GeneratorStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            GeneratorStatus generatorStatus = await db.GeneratorStatus.FindAsync(id);
            db.GeneratorStatus.Remove(generatorStatus);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
