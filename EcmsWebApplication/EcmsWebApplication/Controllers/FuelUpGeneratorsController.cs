﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class FuelUpGeneratorsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelUpGenerators
        public async Task<ActionResult> Index()
        {
            var fuelUpGenerator = db.FuelUpGenerator.Include(f => f.Drawdown).Include(f => f.Generator);
            return View(await fuelUpGenerator.ToListAsync());
        }

        // GET: FuelUpGenerators/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelUpGenerator fuelUpGenerator = await db.FuelUpGenerator.FindAsync(id);
            if (fuelUpGenerator == null)
            {
                return HttpNotFound();
            }
            return View(fuelUpGenerator);
        }

        // GET: FuelUpGenerators/Create
        public ActionResult Create()
        {
            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id");
            ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Supplier");
            return View();
        }

        // POST: FuelUpGenerators/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GeneratorId,DrawdownId,Date,Quantity,LevelBefore,LevelAfter,HoursRun,DocumentNumber,FuelledBy,Notes")] FuelUpGenerator fuelUpGenerator)
        {
            if (ModelState.IsValid)
            {
                db.FuelUpGenerator.Add(fuelUpGenerator);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id", fuelUpGenerator.DrawdownId);
            ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Supplier", fuelUpGenerator.GeneratorId);
            return View(fuelUpGenerator);
        }

        // GET: FuelUpGenerators/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelUpGenerator fuelUpGenerator = await db.FuelUpGenerator.FindAsync(id);
            if (fuelUpGenerator == null)
            {
                return HttpNotFound();
            }
            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id", fuelUpGenerator.DrawdownId);
            ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Supplier", fuelUpGenerator.GeneratorId);
            return View(fuelUpGenerator);
        }

        // POST: FuelUpGenerators/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GeneratorId,DrawdownId,Date,Quantity,LevelBefore,LevelAfter,HoursRun,DocumentNumber,FuelledBy,Notes")] FuelUpGenerator fuelUpGenerator)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelUpGenerator).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id", fuelUpGenerator.DrawdownId);
            ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Supplier", fuelUpGenerator.GeneratorId);
            return View(fuelUpGenerator);
        }

        // GET: FuelUpGenerators/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelUpGenerator fuelUpGenerator = await db.FuelUpGenerator.FindAsync(id);
            if (fuelUpGenerator == null)
            {
                return HttpNotFound();
            }
            return View(fuelUpGenerator);
        }

        // POST: FuelUpGenerators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            FuelUpGenerator fuelUpGenerator = await db.FuelUpGenerator.FindAsync(id);
            db.FuelUpGenerator.Remove(fuelUpGenerator);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
