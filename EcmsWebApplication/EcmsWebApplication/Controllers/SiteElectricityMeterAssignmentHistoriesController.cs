﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.WebModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    public class SiteElectricityMeterAssignmentHistoriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: SiteElectricityMeterAssignmentHistories
        public async Task<ActionResult> Index()
        {
            var siteElectricityMeterAssignmentHistory = db.SiteElectricityMeterAssignmentHistory.Include(s => s.ElectrictyMeter).Include(s => s.Site);
            return View(await siteElectricityMeterAssignmentHistory.ToListAsync());
        }

        // GET: SiteElectricityMeterAssignmentHistories/Details/5
        public async Task<ActionResult> Details(int? siteId , string meterId, string dateAssigned)
        {
            if (siteId == null || meterId == null || dateAssigned == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var assignmentDate = Convert.ToDateTime(dateAssigned);
            SiteElectricityMeterAssignmentHistory siteElectricityMeterAssignmentHistory = await db.SiteElectricityMeterAssignmentHistory.FindAsync(siteId, meterId, assignmentDate);
            if (siteElectricityMeterAssignmentHistory == null)
            {
                return HttpNotFound();
            }
            return View(siteElectricityMeterAssignmentHistory);
        }

        // GET: SiteElectricityMeterAssignmentHistories/Create
        public ActionResult Create()
        {
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "Id");
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name");
            return View();
        }

        // POST: SiteElectricityMeterAssignmentHistories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SiteId,MeterId,DateAssigned,AssignedBy,DateRecalled,RecalledBy")] SiteElectricityMeterAssignmentHistory siteElectricityMeterAssignmentHistory)
        {
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "Id", siteElectricityMeterAssignmentHistory.MeterId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", siteElectricityMeterAssignmentHistory.SiteId);
            if (ModelState.IsValid)
            {
                var assignedBy = new ApplicationUser().GetByUsername(User.Identity.Name);
                var meter = await db.ElectrictyMeter.FindAsync(siteElectricityMeterAssignmentHistory.MeterId);
                if(meter.SiteId == siteElectricityMeterAssignmentHistory.SiteId)
                {
                    ModelState.AddModelError("", $"Meter number{meter.Id} is already assigned to {meter.Site.Name} site. This assignment is redundant.");
                    return View(siteElectricityMeterAssignmentHistory);
                }

                //Recall meter from it's previous assignment
                var history = db.SiteElectricityMeterAssignmentHistory.Where(e => e.SiteId == meter.SiteId && e.MeterId == meter.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateAssigned).FirstOrDefault();
                if (history != null)
                {
                    history.RecalledBy = assignedBy.Id;
                    history.DateRecalled = DateTime.Now.GetCondensedDateTime();
                    db.Entry(history).State = EntityState.Modified;
                }

                //recall this site's previous meter
                var site = await db.Site.FindAsync(siteElectricityMeterAssignmentHistory.SiteId);
                if (site != null)
                {
                    var previousMeter = site.Generators.FirstOrDefault();
                    if (previousMeter != null)
                    {
                        var siteHistory = db.GeneratorDeploymentHistory.Where(e => e.SiteId == previousMeter.SiteId && e.GeneratorId == previousMeter.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateDeployed).FirstOrDefault();
                        if (siteHistory != null)
                        {
                            siteHistory.RecalledBy = assignedBy.Id;
                            siteHistory.DateRecalled = DateTime.Now.GetCondensedDateTime();
                            db.Entry(siteHistory).State = EntityState.Modified;
                        }
                        previousMeter.SiteId = 0;
                        db.Entry(previousMeter).State = EntityState.Modified;
                    }
                }

                //Asign meter
                meter.SiteId = siteElectricityMeterAssignmentHistory.SiteId;
                db.Entry(meter).State = EntityState.Modified;

                //Complete fields for the new entry in the history table
                siteElectricityMeterAssignmentHistory.AssignedBy = assignedBy.Id;
                siteElectricityMeterAssignmentHistory.DateAssigned = DateTime.Now.GetCondensedDateTime();
                siteElectricityMeterAssignmentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                db.SiteElectricityMeterAssignmentHistory.Add(siteElectricityMeterAssignmentHistory);

                //Save to database
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(siteElectricityMeterAssignmentHistory);
        }

        //// GET: SiteElectricityMeterAssignmentHistories/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SiteElectricityMeterAssignmentHistory siteElectricityMeterAssignmentHistory = await db.SiteElectricityMeterAssignmentHistory.FindAsync(id);
        //    if (siteElectricityMeterAssignmentHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "MeterId", "MeterId", siteElectricityMeterAssignmentHistory.MeterId);
        //    ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Name", siteElectricityMeterAssignmentHistory.SiteId);
        //    return View(siteElectricityMeterAssignmentHistory);
        //}
        //// POST: SiteElectricityMeterAssignmentHistories/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "SiteId,MeterId,DateAssigned,AssignedBy,DateRecalled,RecalledBy")] SiteElectricityMeterAssignmentHistory siteElectricityMeterAssignmentHistory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(siteElectricityMeterAssignmentHistory).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "MeterId", "MeterId", siteElectricityMeterAssignmentHistory.MeterId);
        //    ViewBag.SiteId = new SelectList(db.Site, "SiteId", "Name", siteElectricityMeterAssignmentHistory.SiteId);
        //    return View(siteElectricityMeterAssignmentHistory);
        //}
        //// GET: SiteElectricityMeterAssignmentHistories/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SiteElectricityMeterAssignmentHistory siteElectricityMeterAssignmentHistory = await db.SiteElectricityMeterAssignmentHistory.FindAsync(id);
        //    if (siteElectricityMeterAssignmentHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(siteElectricityMeterAssignmentHistory);
        //}
        //// POST: SiteElectricityMeterAssignmentHistories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    SiteElectricityMeterAssignmentHistory siteElectricityMeterAssignmentHistory = await db.SiteElectricityMeterAssignmentHistory.FindAsync(id);
        //    db.SiteElectricityMeterAssignmentHistory.Remove(siteElectricityMeterAssignmentHistory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
