﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class VehicleStatusController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: VehicleStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.VehicleStatus.ToListAsync());
        }

        // GET: VehicleStatus/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleStatus vehicleStatus = await db.VehicleStatus.FindAsync(id);
            if (vehicleStatus == null)
            {
                return HttpNotFound();
            }
            return View(vehicleStatus);
        }

        // GET: VehicleStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Notes,Disabled")] VehicleStatus vehicleStatus)
        {
            if (ModelState.IsValid)
            {
                db.VehicleStatus.Add(vehicleStatus);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(vehicleStatus);
        }

        // GET: VehicleStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleStatus vehicleStatus = await db.VehicleStatus.FindAsync(id);
            if (vehicleStatus == null)
            {
                return HttpNotFound();
            }
            return View(vehicleStatus);
        }

        // POST: VehicleStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Notes,Disabled")] VehicleStatus vehicleStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicleStatus).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(vehicleStatus);
        }

        //// GET: VehicleStatus/Delete/5
        //public async Task<ActionResult> Delete(byte? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VehicleStatus vehicleStatus = await db.VehicleStatus.FindAsync(id);
        //    if (vehicleStatus == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicleStatus);
        //}
        //// POST: VehicleStatus/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(byte id)
        //{
        //    VehicleStatus vehicleStatus = await db.VehicleStatus.FindAsync(id);
        //    db.VehicleStatus.Remove(vehicleStatus);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
