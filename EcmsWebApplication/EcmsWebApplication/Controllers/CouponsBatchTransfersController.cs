﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.WebModels;
using EcmsBusinessLogic;
using Microsoft.AspNet.Identity;
using EcmsModels.ViewModels;

namespace EcmsWebApplication.Controllers
{
    public class CouponsBatchTransfersController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CouponsBatchTransfers
        public async Task<ActionResult> Index()
        {
            var user = new ApplicationUser().GetByUsername(User.Identity.Name);
            if (User.IsInRole(DefaultValues.DefaultCouponStoresControllerRole))
            {
                var couponsBatchTransfer = db.CouponsBatchTransfer.Include(c => c.CouponsBatch);
                return View(await couponsBatchTransfer.ToListAsync());
            }
            else
            {
                var couponsBatchTransfer = db.CouponsBatchTransfer.Where(e => e.SenderSubSectionId == user.SubSectionId || e.ReceiverSubSectionId == user.SubSectionId).Include(c => c.CouponsBatch);
                return View(await couponsBatchTransfer.ToListAsync());
            }
        }

        // GET: CouponsBatchTransfers/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponsBatchTransfer couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(id);
            if (couponsBatchTransfer == null)
            {
                return HttpNotFound();
            }
            return View(couponsBatchTransfer);
        }

        // GET: CouponsBatchTransfers/Create
        public ActionResult Create()
        {
            var user = new ApplicationUser().GetByUsername(User.Identity.Name);
            ViewBag.BatchId = new SelectList(db.CouponsBatch.Where(e => e.SubSectionId == user.SubSectionId && !e.IsArchivable && !e.IsInTransit), "Id", "Id");
            ViewBag.ReceiverSubSectionId = new SelectList(db.SubSection, "Id", "Name");
            return View();
        }

        // POST: CouponsBatchTransfers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BatchId,BatchSize,CouponIssuerId,FuelTypeId,DateTransferred,SenderSubSectionId,ReceiverSubSectionId,DateReceived,TransferredBy,ReceivedBy,Notes")] CouponsBatchTransfer couponsBatchTransfer)
        {
            var sender = new ApplicationUser().GetByUsername(User.Identity.Name);
            ViewBag.BatchId = new SelectList(db.CouponsBatch.Where(e => e.SubSectionId == sender.SubSectionId && !e.IsArchivable && !e.IsInTransit), "Id", "Id");
            ViewBag.ReceiverSubSectionId = new SelectList(db.SubSection, "Id", "Name");
            if (ModelState.IsValid)
            {
                if (couponsBatchTransfer.ReceiverSubSectionId != sender.SubSectionId) 
                {
                    var couponsBatch = await db.CouponsBatch.FindAsync(couponsBatchTransfer.BatchId);
                    if (!couponsBatch.IsInTransit)
                    {
                        couponsBatchTransfer.DateTransferred = DateTime.Now.GetCondensedDateTime();
                        couponsBatchTransfer.DateReceived = DateTime.MinValue.ConvertToDbMinDate();
                        couponsBatchTransfer.SenderSubSectionId = sender.SubSectionId;
                        couponsBatchTransfer.TransferredBy = sender.Id;
                        couponsBatchTransfer.ReceivedBy = string.Empty;
                        db.CouponsBatchTransfer.Add(couponsBatchTransfer);
                        couponsBatch.IsInTransit = true;
                        db.Entry(couponsBatch).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        var receiver = couponsBatch.SubSection.GetOverseer();
                        if(receiver != null)
                        {
                            var message = new IdentityMessage();
                            message.Subject = $"Incoming Fuel Coupons Transfer: Batch Number {couponsBatch.Id}.";
                            message.Body = $"{sender.FullName} has sent for your receipt Batch Number {couponsBatch.Id} with {couponsBatch.BatchSize.ToString("N0")} {couponsBatch.FuelType.Name} coupons. You will need to receive these coupons before they can be used.";
                            if (receiver.Email != null)
                            {
                                message.Destination = receiver.Email;
                                await new EmailService().SendAsync(message);
                            }
                            if (receiver.PhoneNumber != null)
                            {
                                message.Destination = receiver.PhoneNumber;
                                await new SmsService().SendAsync(message);
                            }
                        }
                        var super = sender.SubSection.GetOverseer();
                        if (super != null)
                        {
                            var message = new IdentityMessage();
                            message.Subject = $"Outgoing Fuel Coupons Transfer: Batch Number {couponsBatch.Id}.";
                            message.Body = $"{sender.FullName} has sent Batch Number {couponsBatch.Id} with {couponsBatch.BatchSize.ToString("N0")} {couponsBatch.FuelType.Name} coupons to the {receiver.SubSection.Name} sub-sction of the {receiver.SubSection.Section.Name} section in the {receiver.SubSection.Section.Department.Name} department.";
                            if (super.Email != null)
                            {
                                message.Destination = super.Email;
                                await new EmailService().SendAsync(message);
                            }
                            if (super.PhoneNumber != null)
                            {
                                message.Destination = super.PhoneNumber;
                                await new SmsService().SendAsync(message);
                            }
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", "This batch is in transit.");
                    return View(couponsBatchTransfer);
                }
                ModelState.AddModelError("", "The destination is the same as the current location of the batch. This transfer is redundant.");
                return View(couponsBatchTransfer);
            }
            return View(couponsBatchTransfer);
        }

        //GET
        public async Task<ActionResult> Receive(string batchId, string transferDate)
        {
            if(string.IsNullOrEmpty(batchId) || string.IsNullOrEmpty(transferDate))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dateTransferred = Convert.ToDateTime(transferDate);
            var couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(batchId, dateTransferred);
            return View(couponsBatchTransfer);
        }

        // POST: CouponsBatchTransfers/Receive
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Receive([Bind(Include = "BatchId,DateTransferred,SenderSubSectionId,ReceiverSubSectionId,DateReceived,TransferredBy,ReceivedBy,Notes")] CouponsBatchTransfer couponsBatchTransfer)
        {
            var receiver = new ApplicationUser().GetByUsername(User.Identity.Name);
            ViewBag.BatchId = new SelectList(db.CouponsBatchTransfer.Where(e => e.ReceivedBy == string.Empty && e.ReceiverSubSectionId == receiver.SubSectionId), "Id", "Id");
            if (ModelState.IsValid)
            {
                couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(couponsBatchTransfer.BatchId, couponsBatchTransfer.DateTransferred);
                if (couponsBatchTransfer.ReceiverSubSectionId == receiver.SubSectionId)
                {
                    var couponsBatch = await db.CouponsBatch.FindAsync(couponsBatchTransfer.BatchId);
                    if (couponsBatch.IsInTransit)
                    {
                        //couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(couponsBatchTransfer.BatchId, couponsBatchTransfer.DateTransferred);
                        couponsBatchTransfer.DateReceived = DateTime.Now.GetCondensedDateTime();
                        couponsBatchTransfer.ReceivedBy = receiver.Id;
                        db.CouponsBatchTransfer.Add(couponsBatchTransfer);
                        couponsBatch.SubSectionId = couponsBatchTransfer.ReceiverSubSectionId;
                        couponsBatch.IsInTransit = false;
                        db.Entry(couponsBatch).State = EntityState.Modified;
                        db.Entry(couponsBatchTransfer).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        var sender = db.Users.Find(couponsBatchTransfer.TransferredBy);
                        var senderSuper = sender.SubSection.GetOverseer();
                        if (senderSuper != null)
                        {
                            var message = new IdentityMessage();
                            message.Subject = $"Outgoing Fuel Coupons Transfer CONFIRMED: Batch Number {couponsBatch.Id}.";
                            message.Body = $"{receiver.FullName} has transferred Batch Number {couponsBatch.Id} with {couponsBatch.BatchSize.ToString("N0")} {couponsBatch.FuelType.Name} coupons. You will need to receive these coupons before they can be used.";
                            if (senderSuper.Email != null)
                            {
                                message.Destination = senderSuper.Email;
                                await new EmailService().SendAsync(message);
                            }
                            if (senderSuper.PhoneNumber != null)
                            {
                                message.Destination = senderSuper.PhoneNumber;
                                await new SmsService().SendAsync(message);
                            }
                        }
                        var super = receiver.SubSection.GetOverseer();
                        if (super != null)
                        {
                            var message = new IdentityMessage();
                            message.Subject = $"Fuel Coupons RECEIVED: Batch Number {couponsBatch.Id}.";
                            message.Body = $"{receiver.FullName} has received Batch Number {couponsBatch.Id} with {couponsBatch.BatchSize.ToString("N0")} {couponsBatch.FuelType.Name} coupons sent by {sender.FullName}.";
                            if (super.Email != null)
                            {
                                message.Destination = super.Email;
                                await new EmailService().SendAsync(message);
                            }
                            if (super.PhoneNumber != null)
                            {
                                message.Destination = super.PhoneNumber;
                                await new SmsService().SendAsync(message);
                            }
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", "This batch is not in transit.");
                    return View(couponsBatchTransfer);
                }
                ModelState.AddModelError("", "This batch transfer is not destined for your unit.");
                return View(couponsBatchTransfer);
            }
            ModelState.AddModelError("", "An error occurred.");
            return View(couponsBatchTransfer);
        }


        //// GET: CouponsBatchTransfers/Edit/5
        //public async Task<ActionResult> Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponsBatchTransfer couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(id);
        //    if (couponsBatchTransfer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    //ViewBag.BatchId = new SelectList(db.CouponsBatch, "Id", "Id", couponsBatchTransfer.BatchId);
        //    //ViewBag.CouponIssuerId = new SelectList(db.CouponIssuer, "Id", "Name", couponsBatchTransfer.CouponIssuerId);
        //    //ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", couponsBatchTransfer.FuelTypeId);

        //    return View(couponsBatchTransfer);
        //}
        //// POST: CouponsBatchTransfers/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "BatchId,BatchSize,CouponIssuerId,FuelTypeId,DateTransferred,SenderSubSectionId,ReceiverSubSectionId,DateReceived,TransferredBy,ReceivedBy,Notes")] CouponsBatchTransfer couponsBatchTransfer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(couponsBatchTransfer).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    //ViewBag.BatchId = new SelectList(db.CouponsBatch, "Id", "Id", couponsBatchTransfer.BatchId);
        //    //ViewBag.CouponIssuerId = new SelectList(db.CouponIssuer, "Id", "Name", couponsBatchTransfer.CouponIssuerId);
        //    //ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", couponsBatchTransfer.FuelTypeId);
        //    return View(couponsBatchTransfer);
        //}

        //// GET: CouponsBatchTransfers/Delete/5
        //public async Task<ActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponsBatchTransfer couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(id);
        //    if (couponsBatchTransfer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(couponsBatchTransfer);
        //}

        //// POST: CouponsBatchTransfers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(long id)
        //{
        //    CouponsBatchTransfer couponsBatchTransfer = await db.CouponsBatchTransfer.FindAsync(id);
        //    db.CouponsBatchTransfer.Remove(couponsBatchTransfer);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
