﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class FuelSourceStatusController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelSourceStatus
        public async Task<ActionResult> Index()
        {
            return View(await db.FuelSourceStatus.ToListAsync());
        }

        // GET: FuelSourceStatus/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSourceStatus fuelSourceStatus = await db.FuelSourceStatus.FindAsync(id);
            if (fuelSourceStatus == null)
            {
                return HttpNotFound();
            }
            return View(fuelSourceStatus);
        }

        // GET: FuelSourceStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FuelSourceStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Description,Notes,Disabled")] FuelSourceStatus fuelSourceStatus)
        {
            if (ModelState.IsValid)
            {
                db.FuelSourceStatus.Add(fuelSourceStatus);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(fuelSourceStatus);
        }

        // GET: FuelSourceStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSourceStatus fuelSourceStatus = await db.FuelSourceStatus.FindAsync(id);
            if (fuelSourceStatus == null)
            {
                return HttpNotFound();
            }
            return View(fuelSourceStatus);
        }

        // POST: FuelSourceStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Description,Notes,Disabled")] FuelSourceStatus fuelSourceStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelSourceStatus).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(fuelSourceStatus);
        }

        // GET: FuelSourceStatus/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSourceStatus fuelSourceStatus = await db.FuelSourceStatus.FindAsync(id);
            if (fuelSourceStatus == null)
            {
                return HttpNotFound();
            }
            return View(fuelSourceStatus);
        }

        // POST: FuelSourceStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            FuelSourceStatus fuelSourceStatus = await db.FuelSourceStatus.FindAsync(id);
            db.FuelSourceStatus.Remove(fuelSourceStatus);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
