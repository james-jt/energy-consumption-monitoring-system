﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class FuelSourcesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelSources
        public async Task<ActionResult> Index()
        {
            var fuelSources = db.FuelSource.Include(f => f.FuelSourceStatus).Include(f => f.FuelSourceType);
            return View(await fuelSources.ToListAsync());
        }

        // GET: FuelSources/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSource fuelSource = await db.FuelSource.FindAsync(id);
            if (fuelSource == null)
            {
                return HttpNotFound();
            }
            return View(fuelSource);
        }

        // GET: FuelSources/Create
        public ActionResult Create()
        {
            ViewBag.StatusId = new SelectList(db.FuelSourceStatus, "Id", "Description");
            ViewBag.FuelSourceTypeId = new SelectList(db.FuelSourceType, "Id", "Name");
            return View();
        }

        // POST: FuelSources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,FuelSourceTypeId,ContactPerson,Email,CellNumber,Description,StatusId,Disabled,Notes")] FuelSource fuelSource)
        {
            if (ModelState.IsValid)
            {
                db.FuelSource.Add(fuelSource);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.StatusId = new SelectList(db.FuelSourceStatus, "Id", "Description", fuelSource.StatusId);
            ViewBag.FuelSourceTypeId = new SelectList(db.FuelSourceType, "Id", "Name", fuelSource.FuelSourceTypeId);
            return View(fuelSource);
        }

        // GET: FuelSources/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSource fuelSource = await db.FuelSource.FindAsync(id);
            if (fuelSource == null)
            {
                return HttpNotFound();
            }
            ViewBag.StatusId = new SelectList(db.FuelSourceStatus, "Id", "Description", fuelSource.StatusId);
            ViewBag.FuelSourceTypeId = new SelectList(db.FuelSourceType, "Id", "Name", fuelSource.FuelSourceTypeId);
            return View(fuelSource);
        }

        // POST: FuelSources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FuelSourceTypeId,ContactPerson,Email,CellNumber,Description,StatusId,Disabled,Notes")] FuelSource fuelSource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelSource).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.StatusId = new SelectList(db.FuelSourceStatus, "Id", "Description", fuelSource.StatusId);
            ViewBag.FuelSourceTypeId = new SelectList(db.FuelSourceType, "Id", "Name", fuelSource.FuelSourceTypeId);
            return View(fuelSource);
        }

        //// GET: FuelSources/Delete/5
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FuelSource fuelSource = await db.FuelSource.FindAsync(id);
        //    if (fuelSource == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fuelSource);
        //}

        //// POST: FuelSources/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(short id)
        //{
        //    FuelSource fuelSource = await db.FuelSource.FindAsync(id);
        //    db.FuelSource.Remove(fuelSource);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
