﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class FuelUpVehiclesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelUpVehicles
        public async Task<ActionResult> Index()
        {
            var fuelUpVehicle = db.FuelUpVehicle.Include(f => f.Drawdown).Include(f => f.Vehicle);
            return View(await fuelUpVehicle.ToListAsync());
        }

        // GET: FuelUpVehicles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelUpVehicle fuelUpVehicle = await db.FuelUpVehicle.FindAsync(id);
            if (fuelUpVehicle == null)
            {
                return HttpNotFound();
            }
            return View(fuelUpVehicle);
        }

        // GET: FuelUpVehicles/Create
        public ActionResult Create()
        {
            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id");
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id");
            return View();
        }

        // POST: FuelUpVehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "VehicleId,DrawdownId,Date,Quantity,LevelBefore,LevelAfter,Mileage,DocumentNumber,FuelledBy,Notes")] FuelUpVehicle fuelUpVehicle)
        {
            if (ModelState.IsValid)
            {
                db.FuelUpVehicle.Add(fuelUpVehicle);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id", fuelUpVehicle.DrawdownId);
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", fuelUpVehicle.VehicleId);
            return View(fuelUpVehicle);
        }

        // GET: FuelUpVehicles/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelUpVehicle fuelUpVehicle = await db.FuelUpVehicle.FindAsync(id);
            if (fuelUpVehicle == null)
            {
                return HttpNotFound();
            }
            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id", fuelUpVehicle.DrawdownId);
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", fuelUpVehicle.VehicleId);
            return View(fuelUpVehicle);
        }

        // POST: FuelUpVehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "VehicleId,DrawdownId,Date,Quantity,LevelBefore,LevelAfter,Mileage,DocumentNumber,FuelledBy,Notes")] FuelUpVehicle fuelUpVehicle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelUpVehicle).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DrawdownId = new SelectList(db.Drawdown, "Id", "Id", fuelUpVehicle.DrawdownId);
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", fuelUpVehicle.VehicleId);
            return View(fuelUpVehicle);
        }

        // GET: FuelUpVehicles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelUpVehicle fuelUpVehicle = await db.FuelUpVehicle.FindAsync(id);
            if (fuelUpVehicle == null)
            {
                return HttpNotFound();
            }
            return View(fuelUpVehicle);
        }

        // POST: FuelUpVehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            FuelUpVehicle fuelUpVehicle = await db.FuelUpVehicle.FindAsync(id);
            db.FuelUpVehicle.Remove(fuelUpVehicle);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
