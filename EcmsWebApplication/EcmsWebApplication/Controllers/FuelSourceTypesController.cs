﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class FuelSourceTypesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelSourceTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.FuelSourceType.ToListAsync());
        }

        // GET: FuelSourceTypes/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSourceType fuelSourceType = await db.FuelSourceType.FindAsync(id);
            if (fuelSourceType == null)
            {
                return HttpNotFound();
            }
            return View(fuelSourceType);
        }

        // GET: FuelSourceTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FuelSourceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Notes,Disabled")] FuelSourceType fuelSourceType)
        {
            if (ModelState.IsValid)
            {
                db.FuelSourceType.Add(fuelSourceType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(fuelSourceType);
        }

        // GET: FuelSourceTypes/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelSourceType fuelSourceType = await db.FuelSourceType.FindAsync(id);
            if (fuelSourceType == null)
            {
                return HttpNotFound();
            }
            return View(fuelSourceType);
        }

        // POST: FuelSourceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Notes,Disabled")] FuelSourceType fuelSourceType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelSourceType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(fuelSourceType);
        }

        //// GET: FuelSourceTypes/Delete/5
        //public async Task<ActionResult> Delete(byte? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FuelSourceType fuelSourceType = await db.FuelSourceType.FindAsync(id);
        //    if (fuelSourceType == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fuelSourceType);
        //}

        //// POST: FuelSourceTypes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(byte id)
        //{
        //    FuelSourceType fuelSourceType = await db.FuelSourceType.FindAsync(id);
        //    db.FuelSourceType.Remove(fuelSourceType);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
