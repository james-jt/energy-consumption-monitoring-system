﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class SiteSegmentsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: SiteSegments
        public async Task<ActionResult> Index()
        {
            return View(await db.Segment.ToListAsync());
        }

        // GET: SiteSegments/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiteSegment siteSegment = await db.Segment.FindAsync(id);
            if (siteSegment == null)
            {
                return HttpNotFound();
            }
            return View(siteSegment);
        }

        // GET: SiteSegments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SiteSegments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Disabled,Notes")] SiteSegment siteSegment)
        {
            if (ModelState.IsValid)
            {
                var check = db.Segment.FirstOrDefault(e => e.Name == siteSegment.Name);
                if(check == null)
                {
                    db.Segment.Add(siteSegment);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            ModelState.AddModelError("", "A Site Segment with the same name already exists.");
        }

            return View(siteSegment);
        }

        // GET: SiteSegments/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SiteSegment siteSegment = await db.Segment.FindAsync(id);
            if (siteSegment == null)
            {
                return HttpNotFound();
            }
            return View(siteSegment);
        }

        // POST: SiteSegments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Disabled,Notes")] SiteSegment siteSegment)
        {
            if (ModelState.IsValid)
            {
                var check = db.Segment.FirstOrDefault(e => e.Name == siteSegment.Name);
                if (check == null)
                {
                    db.Entry(siteSegment).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A Site Segment with the same name already exists.");
            }
            return View(siteSegment);
        }

        //// GET: SiteSegments/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SiteSegment siteSegment = await db.Segment.FindAsync(id);
        //    if (siteSegment == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(siteSegment);
        //}
        //// POST: SiteSegments/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    SiteSegment siteSegment = await db.Segment.FindAsync(id);
        //    db.Segment.Remove(siteSegment);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
