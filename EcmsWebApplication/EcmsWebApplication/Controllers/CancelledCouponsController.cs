﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class CancelledCouponsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CancelledCoupons
        public async Task<ActionResult> Index()
        {
            var cancelledCoupon = db.CancelledCoupon.Include(c => c.CouponsBatch);
            return View(await cancelledCoupon.ToListAsync());
        }

        // GET: CancelledCoupons/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CancelledCoupon cancelledCoupon = await db.CancelledCoupon.FindAsync(id);
            if (cancelledCoupon == null)
            {
                return HttpNotFound();
            }
            return View(cancelledCoupon);
        }

        // GET: CancelledCoupons/Create
        public ActionResult Create()
        {
            ViewBag.BatchId = new SelectList(db.CouponsBatch, "Id", "CreatedBy");
            return View();
        }

        // POST: CancelledCoupons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,BatchId,BatchSize,CouponIssuerId,FuelTypeId,DateCancelled,Reason,CancelledBy,Notes")] CancelledCoupon cancelledCoupon)
        {
            if (ModelState.IsValid)
            {
                db.CancelledCoupon.Add(cancelledCoupon);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.BatchId = new SelectList(db.CouponsBatch, "Id", "CreatedBy", cancelledCoupon.BatchId);
            return View(cancelledCoupon);
        }

        // GET: CancelledCoupons/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CancelledCoupon cancelledCoupon = await db.CancelledCoupon.FindAsync(id);
            if (cancelledCoupon == null)
            {
                return HttpNotFound();
            }
            ViewBag.BatchId = new SelectList(db.CouponsBatch, "Id", "CreatedBy", cancelledCoupon.BatchId);
            return View(cancelledCoupon);
        }

        // POST: CancelledCoupons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,BatchId,BatchSize,CouponIssuerId,FuelTypeId,DateCancelled,Reason,CancelledBy,Notes")] CancelledCoupon cancelledCoupon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cancelledCoupon).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.BatchId = new SelectList(db.CouponsBatch, "Id", "CreatedBy", cancelledCoupon.BatchId);
            return View(cancelledCoupon);
        }

        //// GET: CancelledCoupons/Delete/5
        //public async Task<ActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CancelledCoupon cancelledCoupon = await db.CancelledCoupon.FindAsync(id);
        //    if (cancelledCoupon == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(cancelledCoupon);
        //}
        //// POST: CancelledCoupons/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(long id)
        //{
        //    CancelledCoupon cancelledCoupon = await db.CancelledCoupon.FindAsync(id);
        //    db.CancelledCoupon.Remove(cancelledCoupon);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
