﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class ElectricityPurchasesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectricityPurchases
        public async Task<ActionResult> Index()
        {
            return View(await db.ElectricityPurchase.ToListAsync());
        }

        // GET: ElectricityPurchases/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityPurchases electricityPurchase = await db.ElectricityPurchase.FindAsync(id);
            if (electricityPurchase == null)
            {
                return HttpNotFound();
            }
            return View(electricityPurchase);
        }

        // GET: ElectricityPurchases/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ElectricityPurchases/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Date,ReceiptNumber,MeterNumber,CustomerName,Token,Tariff,EnergyBought,TenderAmount,EnergyCharge,DebtCollected,ReLevy,VatRate,VatAmount,TotalPaid,DebtBalBf,DebtBalCf,VendorNumberAndName,VendorInfo1,VendorInfo2,VendorInfo3,TariffIndex,SupplyGroupCode,KeyRevNumber,BoughtBy,AuthorisedBy,Notes")] ElectricityPurchases electricityPurchase)
        {
            if (ModelState.IsValid)
            {
                db.ElectricityPurchase.Add(electricityPurchase);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(electricityPurchase);
        }

        //// GET: ElectricityPurchases/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectricityPurchase electricityPurchase = await db.ElectricityPurchases.FindAsync(id);
        //    if (electricityPurchase == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(electricityPurchase);
        //}

        //// POST: ElectricityPurchases/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,Date,ReceiptNumber,MeterNumber,CustomerName,Token,Tariff,EnergyBought,TenderAmount,EnergyCharge,DebtCollected,ReLevy,VatRate,VatAmount,TotalPaid,DebtBalBf,DebtBalCf,VendorNumberAndName,VendorInfo1,VendorInfo2,VendorInfo3,TariffIndex,SupplyGroupCode,KeyRevNumber,BoughtBy,AuthorisedBy,Notes")] ElectricityPurchase electricityPurchase)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(electricityPurchase).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(electricityPurchase);
        //}

        //// GET: ElectricityPurchases/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectricityPurchase electricityPurchase = await db.ElectricityPurchases.FindAsync(id);
        //    if (electricityPurchase == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(electricityPurchase);
        //}

        //// POST: ElectricityPurchases/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    ElectricityPurchase electricityPurchase = await db.ElectricityPurchases.FindAsync(id);
        //    db.ElectricityPurchases.Remove(electricityPurchase);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
