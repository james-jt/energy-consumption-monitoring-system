﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.ViewModels.Enums;
using EcmsModels.WebModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class ElectrictyMetersController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectrictyMeters
        public async Task<ActionResult> Index()
        {
            var electrictyMeters = db.ElectrictyMeter.Include(e => e.ElectricityMeterStatus).Include(e => e.Site);
            return View(await electrictyMeters.ToListAsync());
        }

        // GET: ElectrictyMeters/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectrictyMeter electrictyMeter = await db.ElectrictyMeter.FindAsync(id);
            if (electrictyMeter == null)
            {
                return HttpNotFound();
            }
            return View(electrictyMeter);
        }

        // GET: ElectrictyMeters/Create
        public ActionResult Create()
        {
            ViewBag.MeterStatusId = new SelectList(db.ElectricityMeterStatus, "Id", "Description");
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name");
            ViewBag.DeployedBy = new SelectList(db.Users, "Id", "Email");
            Array meterTypes = Enum.GetValues(typeof(MeterTypes));
            var meterTypesItems = new List<SelectListItem>();
            foreach (var i in meterTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(MeterTypes), i);
                item.Value = ((int)i).ToString();
                meterTypesItems.Add(item);
            }
            ViewBag.Type = new SelectList(meterTypesItems, "Value", "Text");
            return View();
        }

        // POST: ElectrictyMeters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,SiteId,AccountNumber,DateDeployed,DeployedBy,Type,Disabled,MeterStatusId,Notes")] ElectrictyMeter electrictyMeter)
        {
            ViewBag.MeterStatusId = new SelectList(db.ElectricityMeterStatus, "Id", "Description", electrictyMeter.MeterStatusId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", electrictyMeter.SiteId);
            ViewBag.DeployedBy = new SelectList(db.Users, "Id", "Email", electrictyMeter.DeployedBy);
            Array meterTypes = Enum.GetValues(typeof(MeterTypes));
            var meterTypesItems = new List<SelectListItem>();
            foreach (var i in meterTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(MeterTypes), i);
                item.Value = ((int)i).ToString();
                meterTypesItems.Add(item);
            }
            ViewBag.Type = new SelectList(meterTypesItems, "Value", "Text", electrictyMeter.Type);

            if (ModelState.IsValid)
            {
                electrictyMeter.Id = electrictyMeter.Id.Replace(" ", "").Trim().ToUpper();
                var checkMeter = await db.ElectrictyMeter.FindAsync(electrictyMeter.Id);
                if (checkMeter == null)
                {
                    db.ElectrictyMeter.Add(electrictyMeter);
                    var site = await db.Site.FindAsync(electrictyMeter.SiteId);
                    if(site != null)
                    {
                        var assignedBy = new ApplicationUser().GetByUsername(User.Identity.Name);
                        var siteElectricityMeterAssignmentHistory = new SiteElectricityMeterAssignmentHistory();
                        siteElectricityMeterAssignmentHistory.MeterId = electrictyMeter.Id;
                        siteElectricityMeterAssignmentHistory.SiteId = electrictyMeter.SiteId;
                        siteElectricityMeterAssignmentHistory.AssignedBy = assignedBy.Id;
                        siteElectricityMeterAssignmentHistory.DateAssigned = DateTime.Now.GetCondensedDateTime();
                        siteElectricityMeterAssignmentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                        db.SiteElectricityMeterAssignmentHistory.Add(siteElectricityMeterAssignmentHistory);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", "Specified site not found, meter not added.");
                }
                ModelState.AddModelError("", "A meter with the same Meter Number already exists.");
            }

            return View(electrictyMeter);
        }

        // GET: ElectrictyMeters/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectrictyMeter electrictyMeter = await db.ElectrictyMeter.FindAsync(id);
            if (electrictyMeter == null)
            {
                return HttpNotFound();
            }
            ViewBag.MeterStatusId = new SelectList(db.ElectricityMeterStatus, "Id", "Description", electrictyMeter.MeterStatusId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", electrictyMeter.SiteId);
            ViewBag.DeployedBy = new SelectList(db.Users, "Id", "Email", electrictyMeter.DeployedBy);
            Array meterTypes = Enum.GetValues(typeof(MeterTypes));
            var meterTypesItems = new List<SelectListItem>();
            foreach (var i in meterTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(MeterTypes), i);
                item.Value = ((int)i).ToString();
                meterTypesItems.Add(item);
            }
            ViewBag.Type = new SelectList(meterTypesItems, "Value", "Text", electrictyMeter.Type);
            return View(electrictyMeter);
        }

        // POST: ElectrictyMeters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,SiteId,AccountNumber,DateDeployed,DeployedBy,Type,Disabled,MeterStatusId,Notes")] ElectrictyMeter electrictyMeter)
        {
            ViewBag.MeterStatusId = new SelectList(db.ElectricityMeterStatus, "Id", "Description", electrictyMeter.MeterStatusId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", electrictyMeter.SiteId);
            ViewBag.DeployedBy = new SelectList(db.Users, "Id", "Email", electrictyMeter.DeployedBy);
            Array meterTypes = Enum.GetValues(typeof(MeterTypes));
            var meterTypesItems = new List<SelectListItem>();
            foreach (var i in meterTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(MeterTypes), i);
                item.Value = ((int)i).ToString();
                meterTypesItems.Add(item);
            }
            ViewBag.Type = new SelectList(meterTypesItems, "Value", "Text", electrictyMeter.Type);
            if (ModelState.IsValid)
            {
                electrictyMeter.Id = electrictyMeter.Id.Replace(" ", "").Trim().ToUpper();
                var oldMeter = db.ElectrictyMeter.Find(electrictyMeter.Id);
                db.Entry(oldMeter).State = EntityState.Detached;
                if (electrictyMeter.SiteId != oldMeter.SiteId)
                {
                    var assignedBy = new ApplicationUser().GetByUsername(User.Identity.Name);

                    //Recall meter from its previous assignment
                    var history = db.SiteElectricityMeterAssignmentHistory.Where(e => e.SiteId == oldMeter.SiteId && e.MeterId == oldMeter.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateAssigned).FirstOrDefault();
                    if (history != null)
                    {
                        history.RecalledBy = assignedBy.Id;
                        history.DateRecalled = DateTime.Now.GetCondensedDateTime();
                        db.Entry(history).State = EntityState.Modified;
                    }

                    //recall this site's previous meter
                    var site = await db.Site.FindAsync(electrictyMeter.SiteId);
                    if (site != null)
                    {
                        var previousMeter = site.Generators.FirstOrDefault();
                        if (previousMeter != null)
                        {
                            var siteHistory = db.GeneratorDeploymentHistory.Where(e => e.SiteId == previousMeter.SiteId && e.GeneratorId == previousMeter.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateDeployed).FirstOrDefault();
                            if (siteHistory != null)
                            {
                                siteHistory.RecalledBy = assignedBy.Id;
                                siteHistory.DateRecalled = DateTime.Now.GetCondensedDateTime();
                                db.Entry(siteHistory).State = EntityState.Modified;
                            }
                            previousMeter.SiteId = 0;
                            db.Entry(previousMeter).State = EntityState.Modified;
                        }
                    }

                    //Create new assignment history entry
                    var siteElectricityMeterAssignmentHistory = new SiteElectricityMeterAssignmentHistory();
                    siteElectricityMeterAssignmentHistory.MeterId = electrictyMeter.Id;
                    siteElectricityMeterAssignmentHistory.SiteId = electrictyMeter.SiteId;
                    siteElectricityMeterAssignmentHistory.AssignedBy = assignedBy.Id;
                    siteElectricityMeterAssignmentHistory.DateAssigned = DateTime.Now.GetCondensedDateTime();
                    siteElectricityMeterAssignmentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                    db.SiteElectricityMeterAssignmentHistory.Add(siteElectricityMeterAssignmentHistory);
                }
                db.Entry(electrictyMeter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(electrictyMeter);
        }

        //// GET: ElectrictyMeters/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectrictyMeter electrictyMeter = await db.ElectrictyMeter.FindAsync(id);
        //    if (electrictyMeter == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(electrictyMeter);
        //}
        //// POST: ElectrictyMeters/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    ElectrictyMeter electrictyMeter = await db.ElectrictyMeter.FindAsync(id);
        //    db.ElectrictyMeter.Remove(electrictyMeter);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
