﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class GeneratorServiceHistoriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: GeneratorServiceHistories
        public async Task<ActionResult> Index()
        {
            var generatorServiceHistory = db.GeneratorServiceHistory.Include(g => g.Generator);
            return View(await generatorServiceHistory.ToListAsync());
        }

        // GET: GeneratorServiceHistories/Details/5
        public async Task<ActionResult> Details(string generatorId, string serviceDate)
        {
            if (generatorId == null || serviceDate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            GeneratorServiceHistory generatorServiceHistory = await db.GeneratorServiceHistory.FindAsync(generatorId, serviceDate);
            if (generatorServiceHistory == null)
            {
                return HttpNotFound();
            }
            return View(generatorServiceHistory);
        }

        // GET: GeneratorServiceHistories/Create
        public ActionResult Create()
        {
            //ViewBag.SiteId = new SelectList(db.Sites, "Id", "Name");
            ViewBag.GeneratorId = new SelectList(db.Generators, "Id", "Id");

            return View();
        }

        // POST: GeneratorServiceHistories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GeneratorId,DateServiced,HoursRun,JobCardNumber,DocumentNumber,ServicedBy,LoggedBy,Notes")] GeneratorServiceHistory generatorServiceHistory)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.GeneratorId = new SelectList(db.Generators, "Id", "Id", generatorServiceHistory.GeneratorId);
                return View(generatorServiceHistory);
            }
            var logger = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;
            if(logger == null)
            {
                ModelState.AddModelError("", "Currently logged on user could not be identified.");
                ViewBag.GeneratorId = new SelectList(db.Generators, "Id", "Id", generatorServiceHistory.GeneratorId);
                return View(generatorServiceHistory);
            }
            generatorServiceHistory.LoggedBy = logger;
            if (generatorServiceHistory.DateServiced.ToString("HH:mm:ss") == "00:00:00")
                generatorServiceHistory.DateServiced = generatorServiceHistory.DateServiced.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
            db.GeneratorServiceHistory.Add(generatorServiceHistory);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        // GET: GeneratorServiceHistories/Edit/5
        public async Task<ActionResult> Edit(string generatorId, string serviceDate)
        {
            if (generatorId == null || serviceDate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dateServiced = Convert.ToDateTime(serviceDate);

            GeneratorServiceHistory generatorServiceHistory = await db.GeneratorServiceHistory.FindAsync(generatorId, dateServiced);
            if (generatorServiceHistory == null)
            {
                return HttpNotFound();
            }
            ViewBag.GeneratorId = new SelectList(db.Generators, "Id", "Id", generatorServiceHistory.GeneratorId);
            return View(generatorServiceHistory);
        }

        // POST: GeneratorServiceHistories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GeneratorId,DateServiced,HoursRun,JobCardNumber,DocumentNumber,ServicedBy,LoggedBy,Notes")] GeneratorServiceHistory generatorServiceHistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(generatorServiceHistory).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.GeneratorId = new SelectList(db.Generators, "Id", "Supplier", generatorServiceHistory.GeneratorId);
            return View(generatorServiceHistory);
        }

        //// GET: GeneratorServiceHistories/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GeneratorServiceHistory generatorServiceHistory = await db.GeneratorServiceHistory.FindAsync(id);
        //    if (generatorServiceHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(generatorServiceHistory);
        //}

        //// POST: GeneratorServiceHistories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    GeneratorServiceHistory generatorServiceHistory = await db.GeneratorServiceHistory.FindAsync(id);
        //    db.GeneratorServiceHistory.Remove(generatorServiceHistory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
