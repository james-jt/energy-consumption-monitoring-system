﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class VehicleBodyTypesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: VehicleBodyTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.VehicleBodyType.ToListAsync());
        }

        // GET: VehicleBodyTypes/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleBodyType vehicleBodyType = await db.VehicleBodyType.FindAsync(id);
            if (vehicleBodyType == null)
            {
                return HttpNotFound();
            }
            return View(vehicleBodyType);
        }

        // GET: VehicleBodyTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleBodyTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Notes,Disabled")] VehicleBodyType vehicleBodyType)
        {
            if (ModelState.IsValid)
            {
                db.VehicleBodyType.Add(vehicleBodyType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(vehicleBodyType);
        }

        // GET: VehicleBodyTypes/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleBodyType vehicleBodyType = await db.VehicleBodyType.FindAsync(id);
            if (vehicleBodyType == null)
            {
                return HttpNotFound();
            }
            return View(vehicleBodyType);
        }

        // POST: VehicleBodyTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Notes,Disabled")] VehicleBodyType vehicleBodyType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicleBodyType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(vehicleBodyType);
        }

        //// GET: VehicleBodyTypes/Delete/5
        //public async Task<ActionResult> Delete(byte? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VehicleBodyType vehicleBodyType = await db.VehicleBodyType.FindAsync(id);
        //    if (vehicleBodyType == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicleBodyType);
        //}
        //// POST: VehicleBodyTypes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(byte id)
        //{
        //    VehicleBodyType vehicleBodyType = await db.VehicleBodyType.FindAsync(id);
        //    db.VehicleBodyType.Remove(vehicleBodyType);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
