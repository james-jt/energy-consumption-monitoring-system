﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class GeneratorModelsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: GeneratorModels
        public async Task<ActionResult> Index()
        {
            var generatorModels = db.GeneratorModel.Include(g => g.FuelType);
            return View(await generatorModels.ToListAsync());
        }

        // GET: GeneratorModels/Details/5
        public async Task<ActionResult> Details(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneratorModel generatorModel = await db.GeneratorModel.FindAsync(id);
            if (generatorModel == null)
            {
                return HttpNotFound();
            }
            return View(generatorModel);
        }

        // GET: GeneratorModels/Create
        public ActionResult Create()
        {
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");
            return View();
        }

        // POST: GeneratorModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,EngineMake,EngineModel,GeneratorMake,ServiceInterval,GeneratorSize,NormalLifeSpan,NormalConsuptionRate,Disabled,FuelTypeId")] GeneratorModel generatorModel)
        {
            if (ModelState.IsValid)
            {
                db.GeneratorModel.Add(generatorModel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", generatorModel.FuelTypeId);
            return View(generatorModel);
        }

        // GET: GeneratorModels/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneratorModel generatorModel = await db.GeneratorModel.FindAsync(id);
            if (generatorModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", generatorModel.FuelTypeId);
            return View(generatorModel);
        }

        // POST: GeneratorModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,EngineMake,EngineModel,GeneratorMake,ServiceInterval,GeneratorSize,NormalLifeSpan,NormalConsuptionRate,Disabled,FuelTypeId")] GeneratorModel generatorModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(generatorModel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", generatorModel.FuelTypeId);
            return View(generatorModel);
        }

        //// GET: GeneratorModels/Delete/5
        //public async Task<ActionResult> Delete(byte? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GeneratorModel generatorModel = await db.GeneratorModel.FindAsync(id);
        //    if (generatorModel == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(generatorModel);
        //}
        //// POST: GeneratorModels/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(byte id)
        //{
        //    GeneratorModel generatorModel = await db.GeneratorModel.FindAsync(id);
        //    db.GeneratorModel.Remove(generatorModel);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
