﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class FuelRequestsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelRequests
        public async Task<ActionResult> Index()
        {
            var fuelRequests = db.FuelRequest.Include(f => f.Drawdown).Include(f => f.Requestor);
            return View(await fuelRequests.ToListAsync());
        }

        // GET: FuelRequests/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelRequest fuelRequest = await db.FuelRequest.FindAsync(id);
            if (fuelRequest == null)
            {
                return HttpNotFound();
            }
            return View(fuelRequest);
        }

        // GET: FuelRequests/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.Drawdown, "Id", "Id");
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName");
            return View();
        }

        // POST: FuelRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,DateRequested,RequestedBy,DepartmentId,SectionId,SubSectionId,DateActioned,FuelType,Quantity,DocumentNumber,ActionedBy,Authorised,RequestorComment,ActionerComment,Notes")] FuelRequest fuelRequest)
        {
            if (ModelState.IsValid)
            {
                db.FuelRequest.Add(fuelRequest);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.Drawdown, "Id", "Id", fuelRequest.Id);
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName", fuelRequest.RequestedBy);
            return View(fuelRequest);
        }

        // GET: FuelRequests/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelRequest fuelRequest = await db.FuelRequest.FindAsync(id);
            if (fuelRequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Drawdown, "Id", "Id", fuelRequest.Id);
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName", fuelRequest.RequestedBy);
            return View(fuelRequest);
        }

        // POST: FuelRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DateRequested,RequestedBy,DepartmentId,SectionId,SubSectionId,DateActioned,FuelType,Quantity,DocumentNumber,ActionedBy,Authorised,RequestorComment,ActionerComment,Notes")] FuelRequest fuelRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fuelRequest).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.Drawdown, "Id", "Id", fuelRequest.Id);
            ViewBag.RequestedBy = new SelectList(db.Users, "Id", "FullName", fuelRequest.RequestedBy);
            return View(fuelRequest);
        }

        // GET: FuelRequests/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelRequest fuelRequest = await db.FuelRequest.FindAsync(id);
            if (fuelRequest == null)
            {
                return HttpNotFound();
            }
            return View(fuelRequest);
        }

        // POST: FuelRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            FuelRequest fuelRequest = await db.FuelRequest.FindAsync(id);
            db.FuelRequest.Remove(fuelRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
         
        public async Task<ActionResult> Approve()
        {
            var fuelRequest = db.FuelRequest.Include(f => f.Drawdown).Include(f => f.Requestor).FirstOrDefault();
            return View(fuelRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
