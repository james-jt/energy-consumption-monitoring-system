﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.WebModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    public class VehicleSubSectionDeploymentHistoriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: VehicleSubSectionDeploymentHistories
        public async Task<ActionResult> Index()
        {
            var vehicleDepartmentDeploymentHistory = db.VehicleSubSectionDeploymentHistory.Include(v => v.SubSection).Include(v => v.Vehicle);
            return View(await vehicleDepartmentDeploymentHistory.ToListAsync());
        }

        // GET: VehicleSubSectionDeploymentHistories/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleSubSectionDeploymentHistory vehicleSubSectionDeploymentHistory = await db.VehicleSubSectionDeploymentHistory.FindAsync(id);
            if (vehicleSubSectionDeploymentHistory == null)
            {
                return HttpNotFound();
            }
            return View(vehicleSubSectionDeploymentHistory);
        }

        // GET: VehicleSubSectionDeploymentHistories/Create
        public ActionResult Create()
        {
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name");
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id");
            return View();
        }

        // POST: VehicleSubSectionDeploymentHistories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "VehicleId,SubSectionId,DateDeployed,DeployedBy,DateRecalled,RecalledBy")] VehicleSubSectionDeploymentHistory vehicleSubSectionDeploymentHistory)
        {
            ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", vehicleSubSectionDeploymentHistory.SubSectionId);
            ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleSubSectionDeploymentHistory.VehicleId);
           if (ModelState.IsValid)
            {
                var assignedBy = new ApplicationUser().GetByUsername(User.Identity.Name);
                var vehicle = await db.Vehicle.FindAsync(vehicleSubSectionDeploymentHistory.VehicleId);
                if (vehicle.SubSectionId == vehicleSubSectionDeploymentHistory.SubSectionId)
                {
                    ModelState.AddModelError("", $"Vehicle with registration number {vehicle.Id} is already assigned to {vehicle.SubSection.Name} sub-section of the {vehicle.SubSection.Section.Name} section in the {vehicle.SubSection.Section.Department.Name} department. This assignment is redundant.");
                    return View(vehicleSubSectionDeploymentHistory);
                }

                //Recall vehicle from it's previous assignment
                var history = db.VehicleSubSectionDeploymentHistory.Where(e => e.SubSectionId == vehicle.SubSectionId && e.VehicleId == vehicle.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateDeployed).FirstOrDefault();
                if (history != null)
                {
                    history.RecalledBy = assignedBy.Id;
                    history.DateRecalled = DateTime.Now.GetCondensedDateTime();
                    db.Entry(history).State = EntityState.Modified;
                }

                //Assign vehicle
                vehicle.SubSectionId = vehicleSubSectionDeploymentHistory.SubSectionId;
                db.Entry(vehicle).State = EntityState.Modified;

                vehicleSubSectionDeploymentHistory.DeployedBy = assignedBy.Id;
                vehicleSubSectionDeploymentHistory.DateDeployed = DateTime.Now.GetCondensedDateTime();
                vehicleSubSectionDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                db.VehicleSubSectionDeploymentHistory.Add(vehicleSubSectionDeploymentHistory);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(vehicleSubSectionDeploymentHistory);
        }

        //// GET: VehicleSubSectionDeploymentHistories/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VehicleSubSectionDeploymentHistory vehicleSubSectionDeploymentHistory = await db.VehicleDepartmentDeploymentHistory.FindAsync(id);
        //    if (vehicleSubSectionDeploymentHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", vehicleSubSectionDeploymentHistory.SubSectionId);
        //    ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleSubSectionDeploymentHistory.VehicleId);
        //    return View(vehicleSubSectionDeploymentHistory);
        //}
        //// POST: VehicleSubSectionDeploymentHistories/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "VehicleId,SubSectionId,DateDeployed,DeployedBy,DateRecalled,RecalledBy")] VehicleSubSectionDeploymentHistory vehicleSubSectionDeploymentHistory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(vehicleSubSectionDeploymentHistory).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.SubSectionId = new SelectList(db.SubSection, "Id", "Name", vehicleSubSectionDeploymentHistory.SubSectionId);
        //    ViewBag.VehicleId = new SelectList(db.Vehicle, "Id", "Id", vehicleSubSectionDeploymentHistory.VehicleId);
        //    return View(vehicleSubSectionDeploymentHistory);
        //}
        //// GET: VehicleSubSectionDeploymentHistories/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VehicleSubSectionDeploymentHistory vehicleSubSectionDeploymentHistory = await db.VehicleDepartmentDeploymentHistory.FindAsync(id);
        //    if (vehicleSubSectionDeploymentHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicleSubSectionDeploymentHistory);
        //}
        //// POST: VehicleSubSectionDeploymentHistories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    VehicleSubSectionDeploymentHistory vehicleSubSectionDeploymentHistory = await db.VehicleDepartmentDeploymentHistory.FindAsync(id);
        //    db.VehicleDepartmentDeploymentHistory.Remove(vehicleSubSectionDeploymentHistory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
