﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsBusinessLogic;
using EcmsModels.ViewModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class FuelDeliveriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelDeliveries
        public async Task<ActionResult> Index()
        {
            var fuelDeliveries = db.FuelDelivery.Include(f => f.FuelSource).Include(f => f.FuelType).ToList();
            fuelDeliveries.ForEach(d =>
            {
                var receiver = db.Users.Find(d.ReceivedBy);
                var logger = db.Users.Find(d.LoggedBy);
                d.ReceivedBy = receiver.FullName + " <" + receiver.UserName + ">";
                d.LoggedBy = logger.FullName + " <" + logger.UserName + ">";
            });
            return View(fuelDeliveries);
        }

        // GET: FuelDeliveries/Details/5
        public async Task<ActionResult> Details(int? sourceId, int? fuelTypeId, string deliveryDate)
        {

            if (sourceId == null || fuelTypeId == null || deliveryDate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dateDelivered = Convert.ToDateTime(deliveryDate);
            FuelDelivery fuelDelivery = await db.FuelDelivery.FindAsync(sourceId, fuelTypeId, dateDelivered);
            if (fuelDelivery == null) // Why is fuelDelivery null? 
            {
                return HttpNotFound();
            }
            var receiver = db.Users.Find(fuelDelivery.ReceivedBy);
            var logger = db.Users.Find(fuelDelivery.LoggedBy);
            if(receiver == null || logger == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            fuelDelivery.ReceivedBy = receiver.FullName + " <" + receiver.UserName + ">";
            fuelDelivery.LoggedBy = logger.FullName + " <" + logger.UserName + ">";
            return View(fuelDelivery);
        }

        // GET: FuelDeliveries/Create
        public ActionResult Create()
        {
            var user = db.Users.FirstOrDefault(e => e.UserName == User.Identity.Name);
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name");
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");
            ViewBag.ReceivedBy = new SelectList(db.Users.Where(e => e.SubSectionId == user.SubSectionId), "Id", "Username", user.Id);
            return View();
        }

        // POST: FuelDeliveries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FuelSourceId,FuelTypeId,DateDelivered,Quantity,QuantityBefore,OrderNumber,ReceivedBy,LoggedBy,DocumentNumber,Notes")] FuelDelivery fuelDelivery)
        {
            var user = db.Users.FirstOrDefault(e => e.UserName == User.Identity.Name);
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
            ViewBag.ReceivedBy = new SelectList(db.Users.Where(e => e.SubSectionId == user.SubSectionId), "Id", "Username", fuelDelivery.ReceivedBy);
            if (!ModelState.IsValid)
            {
                return View(fuelDelivery);
            }
            var fuelSource = await db.FuelSource.FindAsync(fuelDelivery.FuelSourceId);
            if(fuelSource == null)
            {
                ModelState.AddModelError("", "No Fuel Store found that matches the Fuel Store you entered.");
                return View(fuelDelivery);
            }
            var fuelType = await db.FuelType.FindAsync(fuelDelivery.FuelTypeId);
            if (fuelType == null)
            {
                ModelState.AddModelError("", "No Fuel Type found that matches the Fuel Type you entered.");
                return View(fuelDelivery);
            }
            var receiver = db.Users.Find(fuelDelivery.ReceivedBy);
            if (receiver == null)
            {
                ModelState.AddModelError("", "No user found that matches the value for Received By.");
                return View(fuelDelivery);
            }

            var inventory = await db.FuelInventory.FindAsync(fuelDelivery.FuelSourceId, fuelDelivery.FuelTypeId);
            if(inventory == null)
            {
                var newInventory = new FuelInventory();
                newInventory.FuelSourceId = fuelSource.Id;
                newInventory.FuelSource = fuelSource;
                newInventory.FuelTypeId = fuelType.Id;
                newInventory.FuelType = fuelType;
                newInventory.Capacity = DefaultValues.FuelSourceCapacity;
                newInventory.ReorderLevel = Convert.ToInt32(newInventory.Capacity * DefaultValues.FuelSourceReorderLevelPercentage);
                newInventory.ReorderTriggered = false;
                db.FuelInventory.Add(newInventory);
                inventory = newInventory;
                var testFuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(newInventory.FuelSourceId, newInventory.FuelTypeId, DateTime.Now.Year, DateTime.Now.Month);
                if (testFuelInventoryHistory == null)
                {
                    var history = new FuelInventoryHistory();
                    history.FuelSourceId = newInventory.FuelSourceId;
                    history.FuelTypeId = newInventory.FuelTypeId;
                    history.Year = DateTime.Now.Year;
                    history.Period = DateTime.Now.Month;
                    history.ClosingDate = DateTime.MinValue.ConvertToDbMinDate();
                    db.FuelInventoryHistory.Add(history);
                }

            }
            var testTotalQuantity = inventory.CurrentQuantity + fuelDelivery.Quantity;
            if(testTotalQuantity > inventory.Capacity)
            {
                ModelState.AddModelError("", "The Fuel Source does not have sufficient capacity for the quantity you wish to add. Consider increasing this capacity.");
                return View(fuelDelivery);
            }
            inventory.CurrentQuantity += fuelDelivery.Quantity;
            BusinessLogic.ManageReorderTrigger(inventory);
            fuelDelivery.LoggedBy = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;
            if (fuelDelivery.DateDelivered.ToString("HH:mm:ss") == "00:00:00")
                fuelDelivery.DateDelivered = fuelDelivery.DateDelivered.GetCondensedDateTime();
            db.FuelDelivery.Add(fuelDelivery);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        // GET: FuelDeliveries/Edit/5
        public async Task<ActionResult> Edit(int? sourceId, int? fuelTypeId, string deliveryDate)
        {
            if (sourceId == null || fuelTypeId == null || deliveryDate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var dateDelivered = Convert.ToDateTime(deliveryDate);
            FuelDelivery fuelDelivery = await db.FuelDelivery.FindAsync(sourceId, fuelTypeId, dateDelivered);
            if (fuelDelivery == null)
            {
                return HttpNotFound();
            }
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
            ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);

            return View(fuelDelivery);
        }

        // POST: FuelDeliveries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FuelSourceId,FuelTypeId,DateDelivered,Quantity,QuantityBefore,OrderNumber,ReceivedBy,LoggedBy,DocumentNumber,Notes")] FuelDelivery fuelDelivery)
        {
            if (!ModelState.IsValid)
            {
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
            ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
            return View(fuelDelivery);
            }
            var fuelSource = await db.FuelSource.FindAsync(fuelDelivery.FuelSourceId);
            if (fuelSource == null)
            {
                ModelState.AddModelError("", "No Fuel Store found that matches the Fuel Store you entered.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            var fuelType = await db.FuelType.FindAsync(fuelDelivery.FuelTypeId);
            if (fuelType == null)
            {
                ModelState.AddModelError("", "No Fuel Type found that matches the Fuel Type you entered.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            var receiver = db.Users.Find(fuelDelivery.ReceivedBy);
            if (receiver == null)
            {
                ModelState.AddModelError("", "No user found that matches the value for Received By.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            var oldFuelDelivery = await db.FuelDelivery.FindAsync(fuelSource.Id, fuelType.Id, fuelDelivery.DateDelivered);
            if (oldFuelDelivery == null)
            {
                ModelState.AddModelError("", "The original record no longer exists. Cannot proceed with this edit.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            db.Entry(oldFuelDelivery).State = EntityState.Detached;
            var inventory = await db.FuelInventory.FindAsync(fuelDelivery.FuelSourceId, fuelDelivery.FuelTypeId);
            if (inventory == null)
            {
                ModelState.AddModelError("", "The inventory record for this delivery no longer exists. Cannot proceed with this edit.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            var testQuantity = inventory.CurrentQuantity - oldFuelDelivery.Quantity + fuelDelivery.Quantity;
            if (testQuantity < 0)
            {
                ModelState.AddModelError("", "This action will leave a negative value for the inventory quantity. Cannot proceed with this edit.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            if (testQuantity > inventory.Capacity)
            {
                ModelState.AddModelError("", "The Fuel Source does not have sufficient capacity for the quantity you wish to add. Cannot proceed with this edit.");
                ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelDelivery.FuelSourceId);
                ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelDelivery.FuelTypeId);
                ViewBag.ReceivedBy = new SelectList(db.Users, "Id", "Username", fuelDelivery.ReceivedBy);
                return View(fuelDelivery);
            }
            inventory.CurrentQuantity -= oldFuelDelivery.Quantity;
            inventory.CurrentQuantity += fuelDelivery.Quantity;
            BusinessLogic.ManageReorderTrigger(inventory);
            fuelDelivery.FuelSource = fuelSource;
            fuelDelivery.FuelType = fuelType;
            db.Entry(fuelDelivery).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        //// GET: FuelDeliveries/Delete/5
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    FuelDelivery fuelDelivery = await db.FuelDeliveries.FindAsync(id);
        //    if (fuelDelivery == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(fuelDelivery);
        //}
        //// POST: FuelDeliveries/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(short id)
        //{
        //    FuelDelivery fuelDelivery = await db.FuelDeliveries.FindAsync(id);
        //    db.FuelDeliveries.Remove(fuelDelivery);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
