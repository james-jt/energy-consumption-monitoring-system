﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class ProvincesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: Provinces
        public async Task<ActionResult> Index()
        {
            return View(await db.Province.ToListAsync());
        }

        // GET: Provinces/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Province province = await db.Province.FindAsync(id);
            if (province == null)
            {
                return HttpNotFound();
            }
            return View(province);
        }

        // GET: Provinces/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Provinces/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Disabled,Notes")] Province province)
        {
            if (ModelState.IsValid)
            {
                var check = db.Province.FirstOrDefault(e => e.Name == province.Name);
                if (check == null)
                {
                    db.Province.Add(province);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            ModelState.AddModelError("", "A province with the same name already exists.");
        }

            return View(province);
        }

        // GET: Provinces/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Province province = await db.Province.FindAsync(id);
            if (province == null)
            {
                return HttpNotFound();
            }
            return View(province);
        }

        // POST: Provinces/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Disabled,Notes")] Province province)
        {
            if (ModelState.IsValid)
            {
                var check = db.Province.FirstOrDefault(e => e.Name == province.Name && e.Name != province.Name);
                if (check == null)
                {
                    db.Entry(province).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A province with the same name already exists.");
            }
            return View(province);
        }

        //// GET: Provinces/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Province province = await db.Province.FindAsync(id);
        //    if (province == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(province);
        //}
        //// POST: Provinces/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Province province = await db.Province.FindAsync(id);
        //    db.Province.Remove(province);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
