﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EcmsWebApplication.Controllers
{
    public class VehicleReadingsController : Controller
    {
        // GET: VehicleReadings
        public ActionResult Index()
        {
            return View();
        }

        // GET: VehicleReadings/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: VehicleReadings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleReadings/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VehicleReadings/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VehicleReadings/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VehicleReadings/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VehicleReadings/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
