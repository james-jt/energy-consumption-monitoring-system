﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class DrawdownsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: Drawdowns
        public async Task<ActionResult> Index()
        {
            var drawdown = db.Drawdown.Include(d => d.FuelRequest);
            return View(await drawdown.ToListAsync());
        }

        // GET: Drawdowns/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Drawdown drawdown = await db.Drawdown.FindAsync(id);
            if (drawdown == null)
            {
                return HttpNotFound();
            }
            return View(drawdown);
        }

        // GET: Drawdowns/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.FuelRequest, "Id", "RequestedBy");
            return View();
        }

        // POST: Drawdowns/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,FuelSourceId,Date,AvailableQuantity,DocumentNumber,Notes")] Drawdown drawdown)
        {
            if (ModelState.IsValid)
            {
                db.Drawdown.Add(drawdown);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.FuelRequest, "Id", "RequestedBy", drawdown.Id);
            return View(drawdown);
        }

        // GET: Drawdowns/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Drawdown drawdown = await db.Drawdown.FindAsync(id);
            if (drawdown == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.FuelRequest, "Id", "RequestedBy", drawdown.Id);
            return View(drawdown);
        }

        // POST: Drawdowns/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FuelSourceId,Date,AvailableQuantity,DocumentNumber,Notes")] Drawdown drawdown)
        {
            if (ModelState.IsValid)
            {
                db.Entry(drawdown).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.FuelRequest, "Id", "RequestedBy", drawdown.Id);
            return View(drawdown);
        }

        //// GET: Drawdowns/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Drawdown drawdown = await db.Drawdown.FindAsync(id);
        //    if (drawdown == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(drawdown);
        //}
        //// POST: Drawdowns/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    Drawdown drawdown = await db.Drawdown.FindAsync(id);
        //    db.Drawdown.Remove(drawdown);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
