﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsBusinessLogic;
using EcmsModels.WebModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class CouponsBatchController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: CouponsBatch
        public async Task<ActionResult> Index()
        {
            var CouponsBatch = db.CouponsBatch.Include(c => c.CouponsBatchTransfers).Include(c => c.SubSection).Include(c => c.FuelType).Include(c => c.ParentBatch).Include(c => c.CouponBatchStatus);
            return View(await CouponsBatch.ToListAsync());
        }

        // GET: CouponsBatch
        public async Task<ActionResult> ChildIndex(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var CouponsBatch = db.CouponsBatch.Where(e => e.ParentBatchId == id).Include(c => c.CouponsBatchTransfers).Include(c => c.SubSection).Include(c => c.FuelType).Include(c => c.ParentBatch).Include(c => c.CouponBatchStatus);
            return View("Index", await CouponsBatch.ToListAsync());
        }

        // GET: CouponsBatch/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponsBatch couponsBatch = await db.CouponsBatch.FindAsync(id);
            if (couponsBatch == null)
            {
                return HttpNotFound();
            }
            return View(couponsBatch);
        }

        // GET: CouponsBatch/Create
        public ActionResult Create(int subSectionId)
        {
            var user = new ApplicationUser().GetByUsername(User.Identity.Name);
            ViewBag.ParentBatchId = new SelectList(db.CouponsBatch.Where(e => e.SubSectionId == user.SubSectionId && !e.IsArchivable && !e.IsInTransit), "Id", "Id");
            var couponBatch = new CouponsBatch();
            couponBatch.SubSectionId = user.SubSectionId;
            couponBatch.Id = "Temp Id";
            couponBatch.BookSize = 1;
            couponBatch.Denomination = 1;
            return View(couponBatch);
        }

        // GET: CouponsBatch/Create
        public ActionResult DirectCreate(int subSectionId, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.ParentBatchId = new SelectList(db.CouponsBatch.Where(e => e.SubSectionId == subSectionId && !e.IsArchivable && !e.IsInTransit), "Id", "Id", id);
            var couponBatch = new CouponsBatch();
            couponBatch.SubSectionId = subSectionId;
            couponBatch.Id = "Temp Id";
            couponBatch.BookSize = 1;
            couponBatch.Denomination = 1;
            return View("Create", couponBatch);
        }

        // POST: CouponsBatch/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,ParentBatchId,SubSectionId,BookSize,FuelTypeId,Denomination,BatchSize,DateCreated,ExpiryDate,Issuer,CreatedBy,Description,StatusId,Notes")] CouponsBatch couponsBatch)
        {
            ViewBag.ParentBatchId = new SelectList(db.CouponsBatch.Where(e => e.SubSectionId ==couponsBatch.SubSectionId), "Id", "Id");
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(couponsBatch.ParentBatchId))
                {
                    if (couponsBatch.BatchSize > 0)
                    {
                        var parentBatch = await db.CouponsBatch.FindAsync(couponsBatch.ParentBatchId);
                        if (parentBatch != null)
                        {
                            if (parentBatch.BatchSize > couponsBatch.BatchSize)
                            {
                                var creator = new ApplicationUser().GetByUsername(User.Identity.Name);
                                parentBatch.BatchSize -= couponsBatch.BatchSize;
                                var numericParentBatchId = BusinessLogic.GetNumericCouponNumber(parentBatch.Id);
                                var numericNewBatchId = numericParentBatchId + parentBatch.BatchSize;
                                couponsBatch.Id = BusinessLogic.GetStringCouponNumber(parentBatch.Id, numericNewBatchId);
                                couponsBatch.BookSize = parentBatch.BookSize;
                                couponsBatch.CouponIssuerId = parentBatch.CouponIssuerId;
                                couponsBatch.CreatedBy = creator.Id;
                                couponsBatch.DateCreated = DateTime.Now.GetCondensedDateTime();
                                couponsBatch.Denomination = parentBatch.Denomination;
                                couponsBatch.ExpiryDate = parentBatch.ExpiryDate;
                                couponsBatch.FuelTypeId = parentBatch.FuelTypeId;
                                couponsBatch.IsArchivable = false;
                                couponsBatch.StatusId = parentBatch.StatusId;
                                couponsBatch.SubSectionId = parentBatch.SubSectionId;
                                db.CouponsBatch.Add(couponsBatch);
                                await db.SaveChangesAsync();
                                return RedirectToAction("Index");
                            }
                            ModelState.AddModelError("", "The parent batch does not have sufficient coupons for this child batch to be created.");
                            return View(couponsBatch);
                        }
                        ModelState.AddModelError("", "The specified parent batch was not found.");
                        return View(couponsBatch);
                    }
                    ModelState.AddModelError("", "The parent batch was not specified.");
                    return View(couponsBatch);
                }
                ModelState.AddModelError("", "Invalid batch size.");
                return View(couponsBatch);
            }
            return View(couponsBatch);
        }

        // GET: CouponsBatch/StockIn
        public ActionResult StockIn()
        {
            //ViewBag.Id = new SelectList(db.CouponsBatchTransfer, "BatchId", "TransferredBy");
            //ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name");
            //ViewBag.ParentBatchId = new SelectList(db.CouponsBatch.Where(e => e.SubSectionId == subSectionId), "Id", "Issuer");
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");
            ViewBag.StatusId = new SelectList(db.CouponBatchStatus, "Id", "Description");
            ViewBag.CouponIssuerId = new SelectList(db.CouponIssuer, "Id", "Name");

            return View();
        }

        // POST: CouponsBatch/StockIn
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> StockIn([Bind(Include = "Id,ParentBatchId,DepartmentId,BookSize,FuelTypeId,Denomination,BatchSize,DateCreated,ExpiryDate,CouponIssuerId,CreatedBy,Description,StatusId,Notes")] CouponsBatch couponsBatch)
        {
            couponsBatch.Id = couponsBatch.Id.Replace(" ", "").Trim().ToUpper();
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", couponsBatch.FuelTypeId);
            ViewBag.StatusId = new SelectList(db.CouponBatchStatus, "Id", "Description", couponsBatch.StatusId);
            ViewBag.CouponIssuerId = new SelectList(db.CouponIssuer, "Id", "Name", couponsBatch.CouponIssuerId);
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(couponsBatch.Id))
                {
                    var check = await db.CouponsBatch.FindAsync(couponsBatch.Id);
                    if (check == null)
                    {
                        var creator = new ApplicationUser().GetByUsername(User.Identity.Name);
                        couponsBatch.DateCreated = DateTime.Now.GetCondensedDateTime();
                        couponsBatch.CreatedBy = creator.Id;
                        couponsBatch.ParentBatchId = couponsBatch.Id;
                        couponsBatch.IsArchivable = false;
                        couponsBatch.SubSectionId = creator.SubSectionId;

                        db.CouponsBatch.Add(couponsBatch);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A batch with the same Batch Number already exists.");
                    return View(couponsBatch);
                }
                ModelState.AddModelError("", $"Please specify a batch number.");
                return View(couponsBatch);
            }
            return View(couponsBatch);
        }

        //// GET: CouponsBatch/Edit/5
        //public async Task<ActionResult> Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponsBatch couponsBatch = await db.CouponsBatch.FindAsync(id);
        //    if (couponsBatch == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.Id = new SelectList(db.CouponsBatchTransfer, "BatchId", "TransferredBy", couponsBatch.Id);
        //    ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", couponsBatch.SubSectionId);
        //    ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", couponsBatch.FuelTypeId);
        //    ViewBag.ParentBatchId = new SelectList(db.CouponsBatch, "Id", "Issuer", couponsBatch.ParentBatchId);
        //    ViewBag.StatusId = new SelectList(db.CouponBatchStatus, "Id", "Description", couponsBatch.StatusId);
        //    ViewBag.CouponIssuerId = new SelectList(db.CouponIssuer, "Id", "Name", couponsBatch.CouponIssuerId);
        //    return View(couponsBatch);
        //}
        //// POST: CouponsBatch/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,ParentBatchId,DepartmentId,BookSize,FuelTypeId,Denomination,BatchSize,DateCreated,ExpiryDate,Issuer,CreatedBy,Description,StatusId,Notes")] CouponsBatch couponsBatch)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(couponsBatch).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.Id = new SelectList(db.CouponsBatchTransfer, "BatchId", "TransferredBy", couponsBatch.Id);
        //    ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", couponsBatch.SubSectionId);
        //    ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", couponsBatch.FuelTypeId);
        //    ViewBag.ParentBatchId = new SelectList(db.CouponsBatch, "Id", "Issuer", couponsBatch.ParentBatchId);
        //    ViewBag.StatusId = new SelectList(db.CouponBatchStatus, "Id", "Description", couponsBatch.StatusId);
        //    ViewBag.CouponIssuerId = new SelectList(db.CouponIssuer, "Id", "Name", couponsBatch.CouponIssuerId);
        //    return View(couponsBatch);
        //}

        //// GET: CouponsBatches/Delete/5
        //public async Task<ActionResult> Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    CouponsBatch couponsBatch = await db.CouponsBatch.FindAsync(id);
        //    if (couponsBatch == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(couponsBatch);
        //}
        //// POST: CouponsBatches/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(long id)
        //{
        //    CouponsBatch couponsBatch = await db.CouponsBatch.FindAsync(id);
        //    db.CouponsBatch.Remove(couponsBatch);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
