﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsWebApplication.Models;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class SubSectionsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();
        public static SubSectionViewModel subSectionDynamicViewModel = new SubSectionViewModel();
        
        // GET: SubSections
        public async Task<ActionResult> Index()
        {
            var subSection = db.SubSection.Include(s => s.Region).Include(s => s.Section);
            return View(await subSection.ToListAsync());
        }

        // GET: SubSections/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSection subSection = await db.SubSection.FindAsync(id);
            if (subSection == null)
            {
                return HttpNotFound();
            }
            return View(subSection);
        }

        // GET: SubSections/Create
        public ActionResult Create()
        {
            ViewBag.RegionId = new SelectList(db.Region, "Id", "Name");
            ViewBag.SectionId = new SelectList(db.Section, "Id", "Name");
            ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: SubSections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,SectionId,RegionId")] SubSection subSection)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subSection.OverseerUserId);
                return View(subSection);
            }
            try
            {
                var section = await db.Section.FindAsync(subSection.SectionId);
                if (section != null)
                {
                    var result = section.SubSections.SingleOrDefault(d => d.Name == subSection.Name);
                    if (result == null)
                    {
                        db.SubSection.Add(subSection);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Sub Section with the same name already exists in the {section.Name} section.");
                    ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                    ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                    ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subSection.OverseerUserId);
                    return View(subSection);
                }
                ModelState.AddModelError("", "The Section you specified no longer exists.");
                ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subSection.OverseerUserId);
                return View(subSection);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", subSection.OverseerUserId);
                return View(subSection);
            }
        }

        // GET: SubSections/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSection subSection = await db.SubSection.FindAsync(id);
            if (subSection == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
            ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
            ViewBag.OverseerUserId = new SelectList(subSection.GetUsers(), "Id", "Email", subSection.OverseerUserId);
            return View(subSection);
        }

        // POST: SubSections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,SectionId,RegionId")] SubSection subSection)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                ViewBag.OverseerUserId = new SelectList(subSection.GetUsers(), "Id", "Email", subSection.OverseerUserId);
                return View(subSection);
            }
            try
            {
                var oldSubSection = await db.SubSection.FindAsync(subSection.Id);
                if (oldSubSection != null)
                {
                    var result = oldSubSection.Section.SubSections.SingleOrDefault(e => e.Name == subSection.Name && e.Id != subSection.Id);
                    if (result == null)
                    {
                        db.Entry(oldSubSection).State = System.Data.Entity.EntityState.Detached;
                        db.Entry(subSection).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Sub Section with the same name already exists in the {oldSubSection.Section.Name} section.");
                    ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                    ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                    ViewBag.OverseerUserId = new SelectList(subSection.GetUsers(), "Id", "Email", subSection.OverseerUserId);
                    return View(subSection);
                }
                ModelState.AddModelError("", "The Sub Section specified no longer exists.");
                ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                ViewBag.OverseerUserId = new SelectList(subSection.GetUsers(), "Id", "Email", subSection.OverseerUserId);
                return View(subSection);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.RegionId = new SelectList(db.Region, "Id", "Name", subSection.RegionId);
                ViewBag.SectionId = new SelectList(db.Section, "Id", "Name", subSection.SectionId);
                ViewBag.OverseerUserId = new SelectList(subSection.GetUsers(), "Id", "Email", subSection.OverseerUserId);
                return View(subSection);
            }
        }

        public ActionResult SubSectionPartial(int? sectionId)
        {
            subSectionDynamicViewModel.EntityList.Clear();
            if (sectionId != null)
            {
                var section = db.Section.Find(sectionId);
                subSectionDynamicViewModel.EntityList.AddRange(section.SubSections);
            }
            return View(subSectionDynamicViewModel);
        }

        public ActionResult SubSectionView(int? sectionId)
        { 
            subSectionDynamicViewModel.EntityList.Clear();
            if (sectionId != null)
            {
                var section = db.Section.Find(sectionId);
                subSectionDynamicViewModel.EntityList.AddRange(section.SubSections);
            }
            return Json(subSectionDynamicViewModel.EntitySelectList, JsonRequestBehavior.AllowGet);
        }

        //// GET: SubSections/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SubSection subSection = await db.SubSection.FindAsync(id);
        //    if (subSection == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(subSection);
        //}

        //// POST: SubSections/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    SubSection subSection = await db.SubSection.FindAsync(id);
        //    db.SubSection.Remove(subSection);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
