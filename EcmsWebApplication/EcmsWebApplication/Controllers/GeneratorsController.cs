﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class GeneratorsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: Generators
        public async Task<ActionResult> Index()
        {
            var generators = db.Generator.Include(g => g.GeneratorModel).Include(g => g.GeneratorStatus).Include(g => g.Site);
            return View(await generators.ToListAsync());
        }

        // GET: Generators/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Generator generator = await db.Generator.FindAsync(id);
            if (generator == null)
            {
                return HttpNotFound();
            }
            return View(generator);
        }

        // GET: Generators/Create
        public ActionResult Create()
        {
            ViewBag.ModelId = new SelectList(db.GeneratorModel, "Id", "Name");
            ViewBag.GeneratorStatusId = new SelectList(db.GeneratorStatus, "Id", "Description");
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name");
            return View();
        }

        // POST: Generators/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,SiteId,Supplier,YearFirstUsedAsNew,HoursRun,FuelCapacity,GeneratorStatusId,ModelId")] Generator generator)
        {
            ViewBag.ModelId = new SelectList(db.GeneratorModel, "Id", "Name", generator.ModelId);
            ViewBag.GeneratorStatusId = new SelectList(db.GeneratorStatus, "Id", "Description", generator.GeneratorStatusId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", generator.SiteId);
            if (!ModelState.IsValid)
            {
                return View(generator);
            }
            generator.Id = generator.Id.Replace(" ", "").Trim().ToUpper();
            var check = db.Generator.FindAsync(generator.Id);
            if(check != null)
            {
                ModelState.AddModelError("", "A generator with the same Serial Number already exists.");
                return View(generator);
            }
            var site = await db.Site.FindAsync(generator.SiteId);
            if (site != null)
            {
                var generatorDeploymentHistory = new GeneratorDeploymentHistory();
                var changedBy = db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).Id;
                var dateChanged = DateTime.Today.GetCondensedDateTime();
                generatorDeploymentHistory.SiteId = generator.SiteId;
                generatorDeploymentHistory.GeneratorId = generator.Id;
                generatorDeploymentHistory.DeployedBy = changedBy;
                generatorDeploymentHistory.DateDeployed = dateChanged;
                generatorDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                db.GeneratorDeploymentHistory.Add(generatorDeploymentHistory);
                db.Generator.Add(generator);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Specified site not found. Generator not created.");
            return View(generator);
        }

        // GET: Generators/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Generator generator = await db.Generator.FindAsync(id);
            if (generator == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModelId = new SelectList(db.GeneratorModel, "Id", "Name", generator.ModelId);
            ViewBag.GeneratorStatusId = new SelectList(db.GeneratorStatus, "Id", "Description", generator.GeneratorStatusId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", generator.SiteId);
            return View(generator);
        }

        // POST: Generators/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,SiteId,Supplier,YearFirstUsedAsNew,HoursRun,FuelCapacity,GeneratorStatusId,ModelId")] Generator generator)
        {
            ViewBag.ModelId = new SelectList(db.GeneratorModel, "Id", "Name", generator.ModelId);
            ViewBag.GeneratorStatusId = new SelectList(db.GeneratorStatus, "Id", "Description", generator.GeneratorStatusId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", generator.SiteId);
           if (!ModelState.IsValid)
            {
                return View(generator);
            }
            var oldGenerator = await db.Generator.FindAsync(generator.Id);
            db.Entry(oldGenerator).State = EntityState.Detached;
            if (oldGenerator.SiteId != generator.SiteId)
            {
               var changedBy = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;

               //Recall generator from its previuos assignment
               var history = db.GeneratorDeploymentHistory.Where(h => h.GeneratorId == oldGenerator.Id && h.SiteId == oldGenerator.SiteId && string.IsNullOrEmpty(h.RecalledBy)).OrderByDescending(o => o.DateDeployed).FirstOrDefault();
                if (history != null)
                {
                    history.RecalledBy = changedBy;
                    history.DateRecalled = DateTime.Today.GetCondensedDateTime();
                    db.Entry(history).State = EntityState.Modified;
                }

                //recall this site's previous generator
                var site = await db.Site.FindAsync(generator.SiteId);
                if (site != null)
                {
                    var previousGenerator = site.Generators.FirstOrDefault();
                    if (previousGenerator != null)
                    {
                        var siteHistory = db.GeneratorDeploymentHistory.Where(e => e.SiteId == previousGenerator.SiteId && e.GeneratorId == previousGenerator.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateDeployed).FirstOrDefault();
                        if (siteHistory != null)
                        {
                            siteHistory.RecalledBy = changedBy;
                            siteHistory.DateRecalled = DateTime.Now.GetCondensedDateTime();
                            db.Entry(siteHistory).State = EntityState.Modified;
                        }
                        previousGenerator.SiteId = 0;
                        db.Entry(previousGenerator).State = EntityState.Modified;
                    }
                }

                //Create new histoy entry for the current deployment
                var generatorDeploymentHistory = new GeneratorDeploymentHistory();
                generatorDeploymentHistory.SiteId = generator.SiteId;
                generatorDeploymentHistory.GeneratorId = generator.Id;
                generatorDeploymentHistory.DeployedBy = changedBy;
                generatorDeploymentHistory.DateDeployed = DateTime.Today.GetCondensedDateTime();
                generatorDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                db.GeneratorDeploymentHistory.Add(generatorDeploymentHistory);
            }
            db.Entry(generator).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        //// GET: Generators/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Generator generator = await db.Generator.FindAsync(id);
        //    if (generator == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(generator);
        //}
        //// POST: Generators/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    Generator generator = await db.Generator.FindAsync(id);
        //    db.Generator.Remove(generator);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
