﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsWebApplication.Models;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class MaintenanceCentresController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();
        public static MaintenanceCentreViewModel maintenanceCentreViewModelDynamicViewModel = new MaintenanceCentreViewModel();


        // GET: MaintenanceCentres
        public async Task<ActionResult> Index()
        {
            var maintenanceCentre = db.MaintenanceCentre.Include(m => m.SubZone);
            return View(await maintenanceCentre.ToListAsync());
        }

        // GET: MaintenanceCentres/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaintenanceCentre maintenanceCentre = await db.MaintenanceCentre.FindAsync(id);
            if (maintenanceCentre == null)
            {
                return HttpNotFound();
            }
            return View(maintenanceCentre);
        }

        // GET: MaintenanceCentres/Create
        public ActionResult Create()
        {
            ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name");
            ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: MaintenanceCentres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,SubZoneId")] MaintenanceCentre maintenanceCentre)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", maintenanceCentre.OverseerUserId);
                return View(maintenanceCentre);
            }
            try
            {
                var subZone = await db.SubZone.FindAsync(maintenanceCentre.SubZoneId);
                if (subZone != null)
                {
                    var result = subZone.MaintenanceCentres.SingleOrDefault(e => e.Name == maintenanceCentre.Name && e.Id != maintenanceCentre.Id);
                    if (result == null)
                    {
                        db.MaintenanceCentre.Add(maintenanceCentre);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Maintenance Centre with the same name already exists in the {subZone.Name} sub-zone.");
                    ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                    ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", maintenanceCentre.OverseerUserId);
                    return View(maintenanceCentre);
                }
                ModelState.AddModelError("", $"The Sub Zone you selected does not exist.");
                ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", maintenanceCentre.OverseerUserId);
                return View(maintenanceCentre);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", maintenanceCentre.OverseerUserId);
                return View(maintenanceCentre);
            }
        }

        // GET: MaintenanceCentres/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaintenanceCentre maintenanceCentre = await db.MaintenanceCentre.FindAsync(id);
            if (maintenanceCentre == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
            ViewBag.OverseerUserId = new SelectList(maintenanceCentre.SubZone.SubSection.GetUsers(), "Id", "Email", maintenanceCentre.OverseerUserId);
            return View(maintenanceCentre);
        }

        // POST: MaintenanceCentres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,SubZoneId")] MaintenanceCentre maintenanceCentre)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                ViewBag.OverseerUserId = new SelectList(db.SubZone.Find(maintenanceCentre.SubZoneId).SubSection.GetUsers(), "Id", "Email", maintenanceCentre.OverseerUserId);
                return View(maintenanceCentre);
            }
            try
            {
                var oldMaintenanceCentre = await db.MaintenanceCentre.FindAsync(maintenanceCentre.Id);
                if (oldMaintenanceCentre != null)
                {
                    var result = oldMaintenanceCentre.SubZone.MaintenanceCentres.SingleOrDefault(e => e.Name == maintenanceCentre.Name && e.Id != maintenanceCentre.Id);
                    if (result == null)
                    {
                        db.Entry(oldMaintenanceCentre).State = EntityState.Detached;
                        db.Entry(maintenanceCentre).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Maintenance Centre with the same name already exists in the {oldMaintenanceCentre.Name} sub-zone.");
                    ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                    ViewBag.OverseerUserId = new SelectList(db.SubZone.Find(maintenanceCentre.SubZoneId).SubSection.GetUsers(), "Id", "Email", maintenanceCentre.OverseerUserId);
                    return View(maintenanceCentre);
                }
                ModelState.AddModelError("", "The Maintenance Centre you specified no longer exists.");
                ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                ViewBag.OverseerUserId = new SelectList(db.SubZone.Find(maintenanceCentre.SubZoneId).SubSection.GetUsers(), "Id", "Email", maintenanceCentre.OverseerUserId);
                return View(maintenanceCentre);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.SubZoneId = new SelectList(db.SubZone, "Id", "Name", maintenanceCentre.SubZoneId);
                ViewBag.OverseerUserId = new SelectList(db.SubZone.Find(maintenanceCentre.SubZoneId).SubSection.GetUsers(), "Id", "Email", maintenanceCentre.OverseerUserId);
                return View(maintenanceCentre);
            }
        }

        public ActionResult MaintenanceCentrePartial(int? subZoneId)
        {
            maintenanceCentreViewModelDynamicViewModel.EntityList.Clear();
            if (subZoneId != null)
            {
                var subZone = db.SubZone.Find(subZoneId);
                maintenanceCentreViewModelDynamicViewModel.EntityList.AddRange(subZone.MaintenanceCentres);
            }
            return View(maintenanceCentreViewModelDynamicViewModel);
        }
         
        public ActionResult MaintenanceCentreView(int? subZoneId)
        {
            maintenanceCentreViewModelDynamicViewModel.EntityList.Clear();
            if (subZoneId != null)
            { 
                var subZone = db.SubZone.Find(subZoneId);
                maintenanceCentreViewModelDynamicViewModel.EntityList.AddRange(subZone.MaintenanceCentres);
            }
            return Json(maintenanceCentreViewModelDynamicViewModel.EntitySelectList, JsonRequestBehavior.AllowGet);
        }

        //// GET: MaintenanceCentres/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    MaintenanceCentre maintenanceCentre = await db.MaintenanceCentre.FindAsync(id);
        //    if (maintenanceCentre == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(maintenanceCentre);
        //}

        //// POST: MaintenanceCentres/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    MaintenanceCentre maintenanceCentre = await db.MaintenanceCentre.FindAsync(id);
        //    db.MaintenanceCentre.Remove(maintenanceCentre);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
