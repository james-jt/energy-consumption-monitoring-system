﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class ElectricityRechargesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectricityRecharges
        public async Task<ActionResult> Index()
        {
            var electricityRecharges = db.ElectricityRecharge.Include(e => e.ElectricityTokenIssue).Include(e => e.ElectrictyMeter);
            return View(await electricityRecharges.ToListAsync());
        }

        // GET: ElectricityRecharges/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityRecharge electricityRecharge = await db.ElectricityRecharge.FindAsync(id);
            if (electricityRecharge == null)
            {
                return HttpNotFound();
            }
            return View(electricityRecharge);
        }

        // GET: ElectricityRecharges/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.ElectricityTokenIssue, "Id", "ElectricityPurchaseId");
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "AccountNumber");
            return View();
        }

        // POST: ElectricityRecharges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,MeterId,Date,UnitsBefore,UnitsAfter,DocumentNumber,Notes")] ElectricityRecharge electricityRecharge)
        {
            if (ModelState.IsValid)
            {
                db.ElectricityRecharge.Add(electricityRecharge);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.ElectricityTokenIssue, "Id", "ElectricityPurchaseId", electricityRecharge.Id);
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "AccountNumber", electricityRecharge.MeterId);
            return View(electricityRecharge);
        }

        //// GET: ElectricityRecharges/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectricityRecharge electricityRecharge = await db.ElectricityRecharges.FindAsync(id);
        //    if (electricityRecharge == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.Id = new SelectList(db.ElectricityTokenIssues, "Id", "ElectricityPurchaseId", electricityRecharge.Id);
        //    ViewBag.MeterId = new SelectList(db.ElectrictyMeters, "Id", "AccountNumber", electricityRecharge.MeterId);
        //    return View(electricityRecharge);
        //}

        //// POST: ElectricityRecharges/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,MeterId,Date,UnitsBefore,UnitsAfter,DocumentNumber,Notes")] ElectricityRecharge electricityRecharge)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(electricityRecharge).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.Id = new SelectList(db.ElectricityTokenIssues, "Id", "ElectricityPurchaseId", electricityRecharge.Id);
        //    ViewBag.MeterId = new SelectList(db.ElectrictyMeters, "Id", "AccountNumber", electricityRecharge.MeterId);
        //    return View(electricityRecharge);
        //}

        //// GET: ElectricityRecharges/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ElectricityRecharge electricityRecharge = await db.ElectricityRecharges.FindAsync(id);
        //    if (electricityRecharge == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(electricityRecharge);
        //}

        //// POST: ElectricityRecharges/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    ElectricityRecharge electricityRecharge = await db.ElectricityRecharges.FindAsync(id);
        //    db.ElectricityRecharges.Remove(electricityRecharge);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
