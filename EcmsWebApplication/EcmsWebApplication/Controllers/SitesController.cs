﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class SitesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: Sites
        public async Task<ActionResult> Index()
        {
            var sites = db.Site.Include(s => s.CommercialPowerStatus).Include(s => s.MaintenanceCentre);
            return View(await sites.ToListAsync());
        }

        // GET: Sites/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = await db.Site.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // GET: Sites/Create
        public ActionResult Create()
        {
            ViewBag.CommercialPowerStatusId = new SelectList(db.CommercialPowerStatus, "Id", "Description");
            ViewBag.MaintenanceCentreId = new SelectList(db.MaintenanceCentre, "Id", "Name");
            ViewBag.ProvinceId = new SelectList(db.Province, "Id", "Name");
            ViewBag.SegmentId = new SelectList(db.Segment, "Id", "Name");
            ViewBag.LandlordId = new SelectList(db.Landlord, "Id", "Name");
            return View();
        }

        // POST: Sites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,DepartmentId,ZoneNumber,Name,CommercialPowerAvailable,Latitude,Longitude,CommercialPowerStatusId,Notes")] Site site)
        {
            ViewBag.CommercialPowerStatusId = new SelectList(db.CommercialPowerStatus, "Id", "Description", site.CommercialPowerStatusId);
            ViewBag.MaintenanceCentreId = new SelectList(db.MaintenanceCentre, "Id", "Name", site.MaintenanceCentreId);
            ViewBag.ProvinceId = new SelectList(db.Province, "Id", "Name", site.ProvinceId);
            ViewBag.SegmentId = new SelectList(db.Segment, "Id", "Name", site.SegmentId);
            ViewBag.LandlordId = new SelectList(db.Landlord, "Id", "Name", site.LandlordId);
            if (ModelState.IsValid)
            {
                var checkSite = db.Site.FirstOrDefault(e => e.Name == site.Name);
                if (checkSite == null)
                {
                    db.Site.Add(site);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A site with the same name already exists.");
            }
            return View(site);
        }

        // GET: Sites/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Site site = await db.Site.FindAsync(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            ViewBag.CommercialPowerStatusId = new SelectList(db.CommercialPowerStatus, "Id", "Description", site.CommercialPowerStatusId);
            ViewBag.MaintenanceCentreId = new SelectList(db.MaintenanceCentre, "Id", "Name", site.MaintenanceCentreId);
            ViewBag.ProvinceId = new SelectList(db.Province, "Id", "Name", site.ProvinceId);
            ViewBag.SegmentId = new SelectList(db.Segment, "Id", "Name", site.SegmentId);
            ViewBag.LandlordId = new SelectList(db.Landlord, "Id", "Name", site.LandlordId);
            return View(site);
        }

        // POST: Sites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DepartmentId,ZoneNumber,Name,CommercialPowerAvailable,Latitude,Longitude,CommercialPowerStatusId,Notes")] Site site)
        {
            ViewBag.CommercialPowerStatusId = new SelectList(db.CommercialPowerStatus, "Id", "Description", site.CommercialPowerStatusId);
            ViewBag.MaintenanceCentreId = new SelectList(db.MaintenanceCentre, "Id", "Name", site.MaintenanceCentreId);
            ViewBag.ProvinceId = new SelectList(db.Province, "Id", "Name", site.ProvinceId);
            ViewBag.SegmentId = new SelectList(db.Segment, "Id", "Name", site.SegmentId);
            ViewBag.LandlordId = new SelectList(db.Landlord, "Id", "Name", site.LandlordId);
            if (ModelState.IsValid)
            {
                var checkSite = db.Site.FirstOrDefault(e => e.Name == site.Name && e.Id != site.Id);
                if (checkSite == null)
                {
                    db.Entry(site).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError("", "A site with the same name already exists.");
            }
            return View(site);
        }

        //// GET: Sites/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Site site = await db.Site.FindAsync(id);
        //    if (site == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(site);
        //}

        //// POST: Sites/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Site site = await db.Site.FindAsync(id);
        //    db.Site.Remove(site);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
