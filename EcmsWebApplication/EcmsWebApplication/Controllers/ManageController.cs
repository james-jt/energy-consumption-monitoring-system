﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using EcmsModels.WebModels;
using EcmsBusinessLogic;
using System.Collections.Generic;
using System.Security.Claims;
using EcmsWebApplication.Extensions;
using EcmsModels.DataModels;
using EcmsModels.ViewModels;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private EcmsDbContext db = new EcmsDbContext();

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        } 

        //
        // GET: /Manage/Index 
        //public async Task<ActionResult> Index(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage =
        //        message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
        //        : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
        //        : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
        //        : message == ManageMessageId.Error ? "An error has occurred."
        //        : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
        //        : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
        //        : message == ManageMessageId.ChangeEmailAddressSuccess ? "Your email address was added."
        //        : message == ManageMessageId.ChangeDepartmentSuccess ? "Your department was added."
        //        : "";

        //    var userId = User.Identity.GetUserId();
        //    var user = await UserManager.FindByIdAsync(userId);
        //    var model = new IndexViewModel
        //    {
        //        HasPassword = HasPassword(),
        //        PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
        //        Email = await UserManager.GetEmailAsync(userId),
        //        Department = UserManager.GetDepartment(user.UserName),
        //        FullName = user.FullName,
        //        Role = user.Role,
        //        Username = user.UserName,
        //        TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
        //        Logins = await UserManager.GetLoginsAsync(userId),
        //        BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
        //    };
        //    return View(model);
        //}

        //
        // GET: /Manage/UserInstance 
        public async Task<ActionResult> Index(string id)
        {
            if (string.IsNullOrEmpty(id))
                id = User.Identity.GetUserId();
            var user = await UserManager.FindByIdAsync(id);
            var model = new IndexViewModel
            {
                UserId = id,
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(id),
                Email = await UserManager.GetEmailAsync(id),
                Department = user.GetDepartment().Name,
                Section = user.GetSection().Name,
                SubSection = user.SubSection.Name,
                //SubZone = user.GetSubZone().Name,
                //MaintenanceCentre = user.GetMaintenanceCentre().Name,
                FullName = user.FullName,
                Role = (await UserManager.GetRolesAsync(user.Id)).FirstOrDefault(),
                Username = user.UserName,
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(id),
                Logins = await UserManager.GetLoginsAsync(id),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(id)
            };
            ViewBag.Title = user.FullName;
            return View("Index", model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var model = new AddPhoneNumberViewModel() { UserId = id };
            return View(model);
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(model.UserId, model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }



        //
        // GET: /Manage/Users
        [Authorize(Roles = DefaultValues.DefaultAdministratorRole)]
        public async Task<ActionResult> Users()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            var model = new List<ApplicationUser>() { user };
            if (User.IsInRole(DefaultValues.DefaultAdministratorRole))
            {
                model = UserManager.Users.ToList();
            }
            else if (User.IsInRole(DefaultValues.DefaultSectionOverseerRole))
            {
                model = UserManager.Users.Where(u => u.SubSectionId == user.SubSectionId).ToList();
            }
            
            return View(model);
        }

        //
        // GET: /Manage/ChangeEmail
        public ActionResult ChangeEmail(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var user = UserManager.FindById(id);
            var model = new ChangeEmailViewModel() { UserId = id, Email = user.Email };
            return View(model);
        }

        //
        // POST: /Manage/ChangeEmail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeEmail(ChangeEmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            string userId = model.UserId;
            await UserManager.SetEmailAsync(userId, model.Email);
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            if (UserManager.EmailService != null)
            {
                //Send an email with this link
                string code = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = userId, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(userId, "Confirm your email address", "Please confirm your email address by clicking <a href=\"" + callbackUrl + "\">here</a>");
            }
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // GET: /Manage/ChangeSubSection
        public ActionResult ChangeSubSection(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var user = db.Users.Find(id);
            if (user != null)
            {
                ViewBag.SubSectionId = new SelectList(db.SubSection.Find(user.SubSection.Id).Section.SubSections, "Id", "Name", user.SubSectionId);
                var model = new ChangeSubSectionViewModel() { UserId = id };
                return View(model);
            }
            return new HttpNotFoundResult();
        }

        //
        // POST: /Manage/ChangeSubSection
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeSubSection(ChangeSubSectionViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                return new HttpNotFoundResult();
            }
            ViewBag.SubSectionId = new SelectList(db.SubSection.Find(user.SubSection.Id).Section.SubSections, "Id", "Name", user.SubSectionId);
            if (!ModelState.IsValid)
            {
                   return View(model);
            }
            user.SubSectionId = Convert.ToInt32(model.SubSectionId);
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to update user's sub-section.");
                return View(model);
            }
            await AddUpdateClaimAsync(user, "SubSection", user.SubSectionId.ToString());
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // GET: /Manage/ChangeSection
        public ActionResult ChangeSection(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var user = db.Users.Find(id);
            if (user != null)
            {
                var section = db.SubSection.Find(user.SubSection.Id).Section;
                ViewBag.SectionId = new SelectList(section.Department.Sections, "Id", "Name", section.Id);
                ViewBag.SubSectionId = new SelectList(section.SubSections, "Id", "Name", user.SubSectionId);
                var model = new ChangeSectionViewModel() { UserId = id, DepartmentId = section.DepartmentId.ToString() };
                return View(model);
            }
            return new HttpNotFoundResult();
        }

        //
        // POST: /Manage/ChangeSubSection
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeSection(ChangeSectionViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                return new HttpNotFoundResult();
            }
            var section = db.Section.Find(Convert.ToInt32(model.SectionId));
            ViewBag.SectionId = new SelectList(section.Department.Sections, "Id", "Name", section.Id);
            ViewBag.SubSectionId = new SelectList(section.SubSections, "Id", "Name", user.SubSectionId);
            if (!ModelState.IsValid)
            {
                return View(model);
            } 
            user.SubSectionId = Convert.ToInt32(model.SubSectionId);
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to update user's sub-section.");
                return View(model);
            }
            await AddUpdateClaimAsync(user, "Section", model.SectionId.ToString());
            await AddUpdateClaimAsync(user, "SubSection", model.SubSectionId.ToString());
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // GET: /Manage/ChangeDepartment
        public ActionResult ChangeDepartment(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var user = db.Users.Find(id);
            if (user != null)
            {
                var department = user.GetDepartment();
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", department.Id);
                ViewBag.SectionId = new SelectList(department.Sections, "Id", "Name", user.SubSection.Section.Id);
                ViewBag.SubSectionId = new SelectList(user.SubSection.Section.SubSections, "Id", "Name", user.SubSectionId);
                var model = new ChangeDepartmentViewModel() { UserId = id };
                return View(model);
            }
            return new HttpNotFoundResult();
        }

        //
        // POST: /Manage/ChangeDepartment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeDepartment(ChangeDepartmentViewModel model)
        {
            var user = await UserManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                return new HttpNotFoundResult();
            }
            var department = user.GetDepartment();
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", department.Id);
            ViewBag.SectionId = new SelectList(department.Sections, "Id", "Name", user.SubSection.Section.Id);
            ViewBag.SubSectionId = new SelectList(user.SubSection.Section.SubSections, "Id", "Name", user.SubSectionId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            user.SubSectionId = Convert.ToInt32(model.SubSectionId);
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to update user's department.");
                return View(model);
            } 
            await AddUpdateClaimAsync(user, "Department", model.DepartmentId.ToString());
            await AddUpdateClaimAsync(user, "Section", model.SectionId.ToString());
            await AddUpdateClaimAsync(user, "SubSection", model.SubSectionId.ToString());
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // GET: /Manage/ChangeFullName
        public ActionResult ChangeFullName(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var user = UserManager.FindById(id);
            var model = new ChangeFullNameViewModel() { UserId = id, FullName = user.FullName };
            return View(model);
        }

        //
        // POST: /Manage/ChangeFullName
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeFullName(ChangeFullNameViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByIdAsync(model.UserId);
            user.FullName = model.FullName;
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to update user's full name.");
                return View(model);
            }
            await AddUpdateClaimAsync(user, "FullName", model.FullName);
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // GET: /Manage/ChangeUsername
        public ActionResult ChangeUsername(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var user = UserManager.FindById(id);
            var model = new ChangeUsernameViewModel() { UserId = id, Username = user.UserName };
            return View(model);
        }

        //
        // POST: /Manage/ChangeUsername
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeUsername(ChangeUsernameViewModel model)
        {
            ViewBag.DepartmentId = Department.GetSelectList();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (!model.Username.ToLower().EndsWith(DefaultValues.DefaultDomainName))
            {
                if (model.Username.ToLower().Contains("@"))
                {
                    model.Username = model.Username.Remove(model.Username.IndexOf('@'));
                }
                model.Username += DefaultValues.DefaultDomainName;
            }
            var user = await UserManager.FindByIdAsync(model.UserId);
            user.UserName = model.Username;
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to update user's username.");
                return View(model);
            }
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // GET: /Manage/ChangeRole
        public ActionResult ChangeRole(string id = "")
        {
            var roles = Roles.GetSelectList();
            if (!User.IsInRole(DefaultValues.DefaultAdministratorRole))
            {
                roles.Where
                    (i =>
                        i.Text == DefaultValues.DefaultAdministratorRole ||
                        i.Text == DefaultValues.DefaultDepartmentOverseerRole ||
                        i.Text == DefaultValues.DefaultSectionOverseerRole ||
                        i.Text == DefaultValues.DefaultSubSectionOverseerRole ||
                        i.Text == DefaultValues.DefaultSubZoneOverseerRole ||
                        i.Text == DefaultValues.DefaultMaintenanceCentreOverseerRole
                    ).ToList().ForEach(r => roles.ToList().Remove(r));
            }
            ViewBag.Roles = roles;
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var model = new ChangeRoleViewModel() { UserId = id };
            return View();
        }

        //
        // POST: /Manage/ChangeRole
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeRole(ChangeRoleViewModel model)
        {
            var roles = Roles.GetSelectList();
            if (!User.IsInRole("Admin"))
            {
                roles.Where
                    (i =>
                        i.Text == DefaultValues.DefaultAdministratorRole ||
                        i.Text == DefaultValues.DefaultDepartmentOverseerRole ||
                        i.Text == DefaultValues.DefaultSectionOverseerRole ||
                        i.Text == DefaultValues.DefaultSubSectionOverseerRole ||
                        i.Text == DefaultValues.DefaultSubZoneOverseerRole ||
                        i.Text == DefaultValues.DefaultMaintenanceCentreOverseerRole
                    ).ToList().ForEach(r => roles.ToList().Remove(r));
            }
            ViewBag.Roles = roles;

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByIdAsync(model.UserId);
            user.Roles.ToList().ForEach(r => UserManager.RemoveFromRoleAsync(user.Id, r.ToString()));
            var result =await UserManager.AddToRoleAsync(user.Id, model.Role);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Failed to update user's role.");
                return View(model);
            }
            return RedirectToAction("Index", "Manage", new { id = model.UserId });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var model = new ChangePasswordViewModel() { UserId = id };
            return View(model);
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(model.UserId, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(model.UserId);
                //if (user != null)
                //{
                //    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                //}
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword(string id = "")
        {
            id = string.IsNullOrEmpty(id) ? User.Identity.GetUserId() : id;
            var model = new SetPasswordViewModel() { UserId = id };
            return View(model);
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(model.UserId, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    //if (user != null)
                    //{
                    //    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    //}
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            ChangePhoneNumberSuccess,
            ChangeEmailAddressSuccess,
            ChangeDepartmentSuccess,
            Error
        }

        public async Task AddUpdateClaimAsync(ApplicationUser user, string key, string value)
        {
            var claim = UserManager.GetClaims(user.Id).Where(c => c.Type == key).FirstOrDefault();
            if (!(claim is null))
                await UserManager.RemoveClaimAsync(user.Id, claim);
            await UserManager.AddClaimAsync(user.Id, new Claim(key, value));
            if (user.Id.Equals(User.Identity.GetUserId()))
            {
                var originalIdentity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                var authenticationManager = HttpContext.GetOwinContext().Authentication;
                authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                authenticationManager.SignIn
                    (
                        new AuthenticationProperties()
                        {
                            IsPersistent = false
                        },
                        originalIdentity
                    );
            }
        }

        #endregion
    }
}