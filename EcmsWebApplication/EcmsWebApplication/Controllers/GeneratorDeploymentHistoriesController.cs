﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.WebModels;
using EcmsBusinessLogic;

namespace EcmsWebApplication.Controllers
{
    public class GeneratorDeploymentHistoriesController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: GeneratorDeploymentHistories
        public async Task<ActionResult> Index()
        {
            var generatorDeploymentHistory = db.GeneratorDeploymentHistory.Include(g => g.Generator).Include(g => g.Site);
            return View(await generatorDeploymentHistory.ToListAsync());
        }

        // GET: GeneratorDeploymentHistories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GeneratorDeploymentHistory generatorDeploymentHistory = await db.GeneratorDeploymentHistory.FindAsync(id);
            if (generatorDeploymentHistory == null)
            {
                return HttpNotFound();
            }
            return View(generatorDeploymentHistory);
        }

        // GET: GeneratorDeploymentHistories/Create
        public ActionResult Create()
        {
            ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Id");
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name");
            return View();
        }

        // POST: GeneratorDeploymentHistories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SiteId,GeneratorId,DateDeployed,DeployedBy,DateRecalled,RecalledBy")] GeneratorDeploymentHistory generatorDeploymentHistory)
        {
            ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Id", generatorDeploymentHistory.GeneratorId);
            ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", generatorDeploymentHistory.SiteId);
            if (ModelState.IsValid)
            {
                var assignedBy = new ApplicationUser().GetByUsername(User.Identity.Name);
                var generator = await db.Generator.FindAsync(generatorDeploymentHistory.GeneratorId);
                if (generator.SiteId == generatorDeploymentHistory.SiteId)
                {
                    ModelState.AddModelError("", $"Generator with serial number {generator.Id} is already assigned to {generator.Site.Name} site. This assignment is redundant.");
                    return View(generatorDeploymentHistory);
                }

                //recall this generator from it's previous site
                var history = db.GeneratorDeploymentHistory.Where(e => e.SiteId == generator.SiteId && e.GeneratorId == generator.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e =>e.DateDeployed).FirstOrDefault();
                if (history != null)
                {
                    history.RecalledBy = assignedBy.Id;
                    history.DateRecalled = DateTime.Now.GetCondensedDateTime(); 
                    db.Entry(history).State = EntityState.Modified;
                }

                //recall this site's previous generator
                var site = await db.Site.FindAsync(generatorDeploymentHistory.SiteId);
                if (site != null)
                {
                    var previousGenerator = site.Generators.FirstOrDefault();
                    if (previousGenerator != null)
                    {
                        var siteHistory = db.GeneratorDeploymentHistory.Where(e => e.SiteId == previousGenerator.SiteId && e.GeneratorId == previousGenerator.Id && string.IsNullOrEmpty(e.RecalledBy)).OrderByDescending(e => e.DateDeployed).FirstOrDefault();
                        if (siteHistory != null)
                        {
                            siteHistory.RecalledBy = assignedBy.Id;
                            siteHistory.DateRecalled = DateTime.Now.GetCondensedDateTime();
                            db.Entry(siteHistory).State = EntityState.Modified;
                        }
                        previousGenerator.SiteId = 0;
                        db.Entry(previousGenerator).State = EntityState.Modified;
                    }
                }

                //Assign generator
                generator.SiteId = generatorDeploymentHistory.SiteId;
                db.Entry(generator).State = EntityState.Modified;
                
                //Complete fields for the new entry in the history table
                generatorDeploymentHistory.DeployedBy = assignedBy.Id;
                generatorDeploymentHistory.DateDeployed = DateTime.Now.GetCondensedDateTime();
                generatorDeploymentHistory.DateRecalled = DateTime.MinValue.ConvertToDbMinDate();
                db.GeneratorDeploymentHistory.Add(generatorDeploymentHistory);

                //Save to database
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(generatorDeploymentHistory);
        }

        //// GET: GeneratorDeploymentHistories/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GeneratorDeploymentHistory generatorDeploymentHistory = await db.GeneratorDeploymentHistory.FindAsync(id);
        //    if (generatorDeploymentHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Supplier", generatorDeploymentHistory.GeneratorId);
        //    ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", generatorDeploymentHistory.SiteId);
        //    return View(generatorDeploymentHistory);
        //}
        //// POST: GeneratorDeploymentHistories/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "SiteId,GeneratorId,DateDeployed,DeployedBy,DateRecalled,RecalledBy")] GeneratorDeploymentHistory generatorDeploymentHistory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(generatorDeploymentHistory).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.GeneratorId = new SelectList(db.Generator, "Id", "Supplier", generatorDeploymentHistory.GeneratorId);
        //    ViewBag.SiteId = new SelectList(db.Site, "Id", "Name", generatorDeploymentHistory.SiteId);
        //    return View(generatorDeploymentHistory);
        //}
        // GET: GeneratorDeploymentHistories/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GeneratorDeploymentHistory generatorDeploymentHistory = await db.GeneratorDeploymentHistory.FindAsync(id);
        //    if (generatorDeploymentHistory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(generatorDeploymentHistory);
        //}
        //// POST: GeneratorDeploymentHistories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    GeneratorDeploymentHistory generatorDeploymentHistory = await db.GeneratorDeploymentHistory.FindAsync(id);
        //    db.GeneratorDeploymentHistory.Remove(generatorDeploymentHistory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
