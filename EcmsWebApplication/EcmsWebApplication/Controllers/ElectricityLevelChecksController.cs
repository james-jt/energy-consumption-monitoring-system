﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;

namespace EcmsWebApplication.Controllers
{
    public class ElectricityLevelChecksController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: ElectricityLevelChecks
        public async Task<ActionResult> Index()
        {
            var electricityLevelCheck = db.ElectricityLevelCheck.Include(e => e.ElectrictyMeter);
            return View(await electricityLevelCheck.ToListAsync());
        }

        // GET: ElectricityLevelChecks/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityLevelCheck electricityLevelCheck = await db.ElectricityLevelCheck.FindAsync(id);
            if (electricityLevelCheck == null)
            {
                return HttpNotFound();
            }
            return View(electricityLevelCheck);
        }

        // GET: ElectricityLevelChecks/Create
        public ActionResult Create()
        {
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "AccountNumber");
            return View();
        }

        // POST: ElectricityLevelChecks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MeterId,DateChecked,Units,DocumentNumber,LoggedBy,Notes")] ElectricityLevelCheck electricityLevelCheck)
        {
            if (ModelState.IsValid)
            {
                db.ElectricityLevelCheck.Add(electricityLevelCheck);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "AccountNumber", electricityLevelCheck.MeterId);
            return View(electricityLevelCheck);
        }

        // GET: ElectricityLevelChecks/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityLevelCheck electricityLevelCheck = await db.ElectricityLevelCheck.FindAsync(id);
            if (electricityLevelCheck == null)
            {
                return HttpNotFound();
            }
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "AccountNumber", electricityLevelCheck.MeterId);
            return View(electricityLevelCheck);
        }

        // POST: ElectricityLevelChecks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MeterId,DateChecked,Units,DocumentNumber,LoggedBy,Notes")] ElectricityLevelCheck electricityLevelCheck)
        {
            if (ModelState.IsValid)
            {
                db.Entry(electricityLevelCheck).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.MeterId = new SelectList(db.ElectrictyMeter, "Id", "AccountNumber", electricityLevelCheck.MeterId);
            return View(electricityLevelCheck);
        }

        // GET: ElectricityLevelChecks/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ElectricityLevelCheck electricityLevelCheck = await db.ElectricityLevelCheck.FindAsync(id);
            if (electricityLevelCheck == null)
            {
                return HttpNotFound();
            }
            return View(electricityLevelCheck);
        }

        // POST: ElectricityLevelChecks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ElectricityLevelCheck electricityLevelCheck = await db.ElectricityLevelCheck.FindAsync(id);
            db.ElectricityLevelCheck.Remove(electricityLevelCheck);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
