﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsWebApplication.Models.Reports;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using EcmsModels.WebModels;
using System.Data.Entity;

namespace EcmsWebApplication.Controllers
{

    public class ReportsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        //public ActionResult Index()
        //{
        //    return View();
        //}

        //GET
        public async Task<ActionResult> Index(string reportName)
        {
            var fuelInventory = db.FuelInventory.Include(e => e.FuelSource.FuelDeliveries).Include(e => e.FuelSource.FuelDrawdowns).FirstOrDefault();
            return View("InventoryStatement", fuelInventory);
        }


        //// POST: Reports
        //[HttpPost]
        //public ActionResult Index(string title)
        //{
        //    //Get data from the POST object
        //    var draw = Request.Form.GetValues("draw").FirstOrDefault();
        //    var start = Request.Form.GetValues("start").FirstOrDefault();
        //    var length = Request.Form.GetValues("length").FirstOrDefault();
        //    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
        //    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
        //    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

        //    //Paging Size (10,20,50,100)    
        //    int pageSize = length != null ? Convert.ToInt32(length) : 0;
        //    int skip = start != null ? Convert.ToInt32(start) : 0;
        //    int recordsTotal = 0;


        //    switch (title)
        //    {
        //        case "InventoryStatement":

        //            //var report = new InventoryStatement();
        //            var reportData = context.FuelInventory.ToList();
        //            // Getting all Customer data    
        //            //Sorting    
        //            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
        //            {
        //                reportData = reportData.OrderBy(sortColumn + " " + sortColumnDir).ToList();
        //            }
        //            //Search    
        //            if (!string.IsNullOrEmpty(searchValue))
        //            {
        //                reportData = reportData.Where(e => e.FuelSourceId.ToString() == searchValue).ToList();
        //            }
        //            //total number of rows count     
        //            recordsTotal = reportData.Count();
        //            //Paging     
        //            var data = reportData.Skip(skip).Take(pageSize).ToList();
        //            //Returning Json Data    
        //            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

        //        default:
        //            return Json(new {  });
        //    }

        //}
        //GET
        //public async Task<ActionResult> InventoryStatement()
        //{
            
        //}
    }
}