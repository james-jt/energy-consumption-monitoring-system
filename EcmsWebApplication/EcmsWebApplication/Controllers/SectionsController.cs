﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsWebApplication.Models;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class SectionsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();
        public static SectionViewModel sectionDynamicViewModel = new SectionViewModel();

        // GET: Sections
        public async Task<ActionResult> Index()
        {
            var section = db.Section.Include(s => s.Department);
            return View(await section.ToListAsync());
        }

        // GET: Sections/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = await db.Section.FindAsync(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);
        }

        // GET: Sections/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name");
            ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: Sections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,DepartmentId")] Section section)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", section.OverseerUserId);
                return View(section);
            }
            try
            {
                var department = await db.Department.FindAsync(section.DepartmentId);
                if(department != null)
                {
                    var result = department.Sections.SingleOrDefault(d => d.Name == section.Name);
                    if (result == null)
                    {
                        db.Section.Add(section);
                        await db.SaveChangesAsync();
                        var defaultSubSection = SubSection.CreateDefault(section);
                        db.SubSection.Add(defaultSubSection);
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Section with the same name already exists in the {department.Name} department.");
                    ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", section.OverseerUserId);
                    ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                    return View(section);
                }
                ModelState.AddModelError("", "The department you specified no longer exists.");
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", section.OverseerUserId);
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                return View(section);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.OverseerUserId = new SelectList(db.Users, "Id", "Email", section.OverseerUserId);
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                return View();
            }
        }

        // GET: Sections/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Section section = await db.Section.FindAsync(id);
            if (section == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
            ViewBag.OverseerUserId = new SelectList(section.GetUsers(), "Id", "Email", section.OverseerUserId);
            return View(section);
        }

        // POST: Sections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,OverseerUserId,Disabled,Notes,DepartmentId")] Section section)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                ViewBag.OverseerUserId = new SelectList(section.GetUsers(), "Id", "Email", section.OverseerUserId);
                return View(section);
            }
            try
            {
                var oldSection = await db.Section.FindAsync(section.Id);
                if (oldSection != null)
                {
                    var result = oldSection.Department.Sections.SingleOrDefault(d => d.Name == section.Name && d.Id != section.Id);
                    if (result == null)
                    {
                        db.Entry(oldSection).State = System.Data.Entity.EntityState.Detached;
                        db.Entry(section).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("", $"A Section already exists with the same name in the {oldSection.Department.Name}.");
                    ViewBag.OverseerUserId = new SelectList(section.GetUsers(), "Id", "Email", section.OverseerUserId);
                    ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                    return View(section);
                }
                ModelState.AddModelError("", "The specified Section no longer exists.");
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                ViewBag.OverseerUserId = new SelectList(section.GetUsers(), "Id", "Email", section.OverseerUserId);
                return View(section);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                ViewBag.DepartmentId = new SelectList(db.Department, "Id", "Name", section.DepartmentId);
                ViewBag.OverseerUserId = new SelectList(section.GetUsers(), "Id", "Email", section.OverseerUserId);
                return View(section);
            }




            db.Entry(section).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public ActionResult SectionPartial(int? departmentId)
        {
            sectionDynamicViewModel.EntityList.Clear();
            if (departmentId != null)
            {
                var department = db.Department.Find(departmentId);
                sectionDynamicViewModel.EntityList.AddRange(department.Sections);
            }
            return View(sectionDynamicViewModel);
        }

        public ActionResult SectionView(int? departmentId)
        {
            sectionDynamicViewModel.EntityList.Clear();
            if (departmentId != null)
            {
                var department = db.Department.Find(departmentId);
                sectionDynamicViewModel.EntityList.AddRange(department.Sections);
            }
            return Json(sectionDynamicViewModel.EntitySelectList, JsonRequestBehavior.AllowGet);
        }

        //// GET: Sections/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Section section = await db.Section.FindAsync(id);
        //    if (section == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(section);
        //}

        //// POST: Sections/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Section section = await db.Section.FindAsync(id);
        //    db.Section.Remove(section);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
