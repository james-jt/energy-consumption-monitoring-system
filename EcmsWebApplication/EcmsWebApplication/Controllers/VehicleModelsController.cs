﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsWebApplication.Models;

namespace EcmsWebApplication.Controllers
{
    [Authorize]
    public class VehicleModelsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();
        public static VehicleModelViewModel vehicleModelDynamicViewModel = new VehicleModelViewModel();

        // GET: VehicleModels
        public async Task<ActionResult> Index()
        {
            var vehicleModels = db.VehicleModel.Include(v => v.VehicleMake);
            return View(await vehicleModels.ToListAsync());
        }

        // GET: VehicleModels/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleModel vehicleModel = await db.VehicleModel.FindAsync(id);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }
            return View(vehicleModel);
        }

        // GET: VehicleModels/Create
        public ActionResult Create()
        {
            ViewBag.VehicleMakeId = new SelectList(db.VehicleMake, "Id", "Name");
            return View();
        }

        // POST: VehicleModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,VehicleMakeId,YearOfManufacture,Name,Notes,Disabled")] VehicleModel vehicleModel)
        {
            if (ModelState.IsValid)
            {
                db.VehicleModel.Add(vehicleModel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.VehicleMakeId = new SelectList(db.VehicleMake, "Id", "Name", vehicleModel.VehicleMakeId);
            return View(vehicleModel);
        }

        // GET: VehicleModels/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleModel vehicleModel = await db.VehicleModel.FindAsync(id);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.VehicleMakeId = new SelectList(db.VehicleMake, "Id", "Name", vehicleModel.VehicleMakeId);
            return View(vehicleModel);
        }

        // POST: VehicleModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,VehicleMakeId,YearOfManufacture,Name,Notes,Disabled")] VehicleModel vehicleModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicleModel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.VehicleMakeId = new SelectList(db.VehicleMake, "Id", "Name", vehicleModel.VehicleMakeId);
            return View(vehicleModel);
        }

        public ActionResult VehicleModelView(int? makeId)
        {
            vehicleModelDynamicViewModel.EntityList.Clear();
            if (makeId != null)
            {
                var make = db.VehicleMake.Find(makeId);
                vehicleModelDynamicViewModel.EntityList.AddRange(make.VehicleModels);
            }
            return Json(vehicleModelDynamicViewModel.EntitySelectList, JsonRequestBehavior.AllowGet);
        }

        //// GET: VehicleModels/Delete/5
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    VehicleModel vehicleModel = await db.VehicleModel.FindAsync(id);
        //    if (vehicleModel == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicleModel);
        //}

        //// POST: VehicleModels/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(short id)
        //{
        //    VehicleModel vehicleModel = await db.VehicleModel.FindAsync(id);
        //    db.VehicleModel.Remove(vehicleModel);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
