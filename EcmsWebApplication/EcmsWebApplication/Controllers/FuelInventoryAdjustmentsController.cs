﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EcmsModels.DataModels;
using EcmsModels.ViewModels.Enums;
using System.Web.UI.WebControls;
using EcmsBusinessLogic;
using Microsoft.AspNet.Identity;

namespace EcmsWebApplication.Controllers
{
    public class FuelInventoryAdjustmentsController : Controller
    {
        private EcmsDbContext db = new EcmsDbContext();

        // GET: FuelInventoryAdjustments
        public async Task<ActionResult> Index()
        {
            var inventoryAdjustment = db.InventoryAdjustment.Include(f => f.InventoryHistoryItem).Include(f => f.InventoryItem);
            return View(await inventoryAdjustment.ToListAsync());
        }

        // GET: FuelInventoryAdjustments/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if (fuelInventoryAdjustment == null)
            {
                return HttpNotFound();
            }
            return View(fuelInventoryAdjustment);
        }

        // GET: FuelInventoryAdjustments/Create
        public ActionResult Create()
        {
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name");
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name");

            Array adjustmentTypes = Enum.GetValues(typeof(InventoryAdjustmentTypes));
            var adjustmentTypesItems = new List<SelectListItem>();
            foreach (var i in adjustmentTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(InventoryAdjustmentTypes), i);
                item.Value = ((int)i).ToString();
                adjustmentTypesItems.Add(item);
            }
            ViewBag.AdjustmentType = new SelectList(adjustmentTypesItems, "Value", "Text");

            Array periods = Enum.GetValues(typeof(Periods));
            var periodItems = new List<SelectListItem>();
            foreach (var i in periods)
            {
                periodItems.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(Periods), i),
                    Value = ((int)i).ToString()
                });
            }
            ViewBag.Period = new SelectList(periodItems, "Value", "Text");

            var years = new List<SelectListItem>();
            var currentYear = new SelectListItem();
            currentYear.Text = "Current";
            currentYear.Value = "0";
            years.Add(currentYear);
            db.FuelInventoryHistory.ForEachAsync(e =>
            {
                var yr = new SelectListItem();
                yr.Text = e.Year.ToString();
                yr.Value = e.Year.ToString();
                years.Add(yr);
            });
            ViewBag.Year = new SelectList(years.Distinct(), "Value", "Text"); 

            return View();
        }

        // POST: FuelInventoryAdjustments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,FuelSourceId,FuelTypeId,Year,Period,AdjustmentType,AdjustmentQuantity,AdjustmentReason,InitiatedBy,DateInitiated,Actioned,ActionedBy,DateActioned,Notes")] FuelInventoryAdjustment fuelInventoryAdjustment)
        {
            
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventoryAdjustment.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventoryAdjustment.FuelTypeId);
            Array adjustmentTypes = Enum.GetValues(typeof(InventoryAdjustmentTypes));
            var adjustmentTypesItems = new List<SelectListItem>();
            foreach (var i in adjustmentTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(InventoryAdjustmentTypes), i);
                item.Value = ((int)i).ToString();
                adjustmentTypesItems.Add(item);
            }
            ViewBag.AdjustmentType = new SelectList(adjustmentTypesItems, "Value", "Text", fuelInventoryAdjustment.AdjustmentType); 

            Array periods = Enum.GetValues(typeof(Periods));
            var periodItems = new List<SelectListItem>();
            foreach (var i in periods)
            {
                periodItems.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(Periods), i),
                    Value = ((int)i).ToString()
                });
            }
            ViewBag.Period = new SelectList(periodItems, "Value", "Text", fuelInventoryAdjustment.Period);

            var years = new List<SelectListItem>();
            var currentYear = new SelectListItem();
            currentYear.Text = "Current";
            currentYear.Value = "0";
            years.Add(currentYear);
            await db.FuelInventoryHistory.ForEachAsync(e =>
            {
                var yr = new SelectListItem();
                yr.Text = e.Year.ToString();
                yr.Value = e.Year.ToString();
                years.Add(yr);
            });
            ViewBag.Year = new SelectList(years.Distinct(), "Value", "Text", fuelInventoryAdjustment.Year);

            if (ModelState.IsValid)
            {
                var initiator = new EcmsModels.WebModels.ApplicationUser().GetByUsername(User.Identity.Name);
                fuelInventoryAdjustment.InitiatedBy = initiator.Id;
                fuelInventoryAdjustment.DateInitiated = DateTime.Now.GetCondensedDateTime();
                fuelInventoryAdjustment.Actioned = false;
                fuelInventoryAdjustment.DateActioned = DateTime.MinValue.ConvertToDbMinDate();
                string fuelStore;
                string fuelType;
                if(fuelInventoryAdjustment.Year == 0)
                {
                    fuelInventoryAdjustment.Year = DateTime.Now.Year; 
                }
                if(fuelInventoryAdjustment.Period == 0)
                {
                    fuelInventoryAdjustment.Period = DateTime.Now.Month;

                    //var fuelInventory = await db.FuelInventory.FindAsync(fuelInventoryAdjustment.FuelSourceId, fuelInventoryAdjustment.FuelTypeId);
                    //if(fuelInventory == null)
                    //{
                    //    ModelState.AddModelError("", $"No inventory entry found for the specified fuel store and fuel type combination in the current period.");
                    //    return View(fuelInventoryAdjustment); 
                    //}
                }
                var fuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(fuelInventoryAdjustment.FuelSourceId, fuelInventoryAdjustment.FuelTypeId, fuelInventoryAdjustment.Year, fuelInventoryAdjustment.Period);
                if (fuelInventoryHistory == null)
                {
                    ModelState.AddModelError("", $"No inventory history entry found for the specified fuel store, fuel type, year and period combination.");
                    return View(fuelInventoryAdjustment);
                }
                fuelStore = fuelInventoryHistory.FuelSource.Name;
                fuelType = fuelInventoryHistory.FuelType.Name;
                db.InventoryAdjustment.Add(fuelInventoryAdjustment);
                await db.SaveChangesAsync();
                var super = initiator.SubSection.GetOverseer();
                if (super != null)
                {
                    var message = new IdentityMessage();
                    message.Subject = $"Fuel Stock Adjustment on {fuelStore} for {fuelType}.";
                    message.Body = $"{initiator.FullName} has made a fuel stock adjustment in period {fuelInventoryAdjustment.Period} of year {fuelInventoryAdjustment.Year} on {fuelStore} for {fuelType} of {fuelInventoryAdjustment.AdjustmentQuantity} litres that needs your approval.";
                    if (super.Email != null)
                    {
                        message.Destination = super.Email;
                        await new EmailService().SendAsync(message);
                    }
                    if (super.PhoneNumber != null)
                    {
                        message.Destination = super.PhoneNumber;
                        await new SmsService().SendAsync(message);
                    }
                }
                return RedirectToAction("Index");
            }
            return View(fuelInventoryAdjustment);
        }

        // GET: FuelInventoryAdjustments/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if (fuelInventoryAdjustment == null)
            {
                return HttpNotFound();
            }
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventoryAdjustment.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventoryAdjustment.FuelTypeId);
            Array adjustmentTypes = Enum.GetValues(typeof(InventoryAdjustmentTypes));
            var adjustmentTypesItems = new List<SelectListItem>();
            foreach (var i in adjustmentTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(InventoryAdjustmentTypes), i);
                item.Value = ((int)i).ToString();
                adjustmentTypesItems.Add(item);
            }
            ViewBag.AdjustmentType = new SelectList(adjustmentTypesItems, "Value", "Text", fuelInventoryAdjustment.AdjustmentType);

            Array periods = Enum.GetValues(typeof(Periods));
            var periodItems = new List<SelectListItem>();
            foreach (var i in periods)
            {
                periodItems.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(Periods), i),
                    Value = ((int)i).ToString()
                });
            }
            ViewBag.Period = new SelectList(periodItems, "Value", "Text", fuelInventoryAdjustment.Period);

            var years = new List<SelectListItem>();
            var currentYear = new SelectListItem();
            currentYear.Text = "Current";
            currentYear.Value = "0";
            years.Add(currentYear);
            await db.FuelInventoryHistory.ForEachAsync(e =>
            {
                var yr = new SelectListItem();
                yr.Text = e.Year.ToString();
                yr.Value = e.Year.ToString();
                years.Add(yr);
            });
            ViewBag.Year = new SelectList(years.Distinct(), "Value", "Text", fuelInventoryAdjustment.Year);
            return View(fuelInventoryAdjustment);
        }

        // POST: FuelInventoryAdjustments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FuelSourceId,FuelTypeId,Year,Period,AdjustmentType,AdjustmentQuantity,AdjustmentReason,InitiatedBy,DateInitiated,Actioned,ActionedBy,DateActioned,Notes")] FuelInventoryAdjustment fuelInventoryAdjustment)
        {
            ViewBag.FuelSourceId = new SelectList(db.FuelSource, "Id", "Name", fuelInventoryAdjustment.FuelSourceId);
            ViewBag.FuelTypeId = new SelectList(db.FuelType, "Id", "Name", fuelInventoryAdjustment.FuelTypeId);
            Array adjustmentTypes = Enum.GetValues(typeof(InventoryAdjustmentTypes));
            var adjustmentTypesItems = new List<SelectListItem>();
            foreach (var i in adjustmentTypes)
            {
                var item = new SelectListItem();
                item.Text = Enum.GetName(typeof(InventoryAdjustmentTypes), i);
                item.Value = ((int)i).ToString();
                adjustmentTypesItems.Add(item);
            }
            ViewBag.AdjustmentType = new SelectList(adjustmentTypesItems, "Value", "Text", fuelInventoryAdjustment.AdjustmentType);

            Array periods = Enum.GetValues(typeof(Periods));
            var periodItems = new List<SelectListItem>();
            foreach (var i in periods)
            {
                periodItems.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(Periods), i),
                    Value = ((int)i).ToString()
                });
            }
            ViewBag.Period = new SelectList(periodItems, "Value", "Text", fuelInventoryAdjustment.Period);

            var years = new List<SelectListItem>();
            var currentYear = new SelectListItem();
            currentYear.Text = "Current";
            currentYear.Value = "0";
            years.Add(currentYear);
            await db.FuelInventoryHistory.ForEachAsync(e =>
            {
                var yr = new SelectListItem();
                yr.Text = e.Year.ToString();
                yr.Value = e.Year.ToString();
                years.Add(yr);
            });
            ViewBag.Year = new SelectList(years.Distinct(), "Value", "Text", fuelInventoryAdjustment.Year);
            if (ModelState.IsValid)
            {
                string fuelStore;
                string fuelType;
                if (fuelInventoryAdjustment.Year == 0)
                {
                    fuelInventoryAdjustment.Year = DateTime.Now.Year;
                }
                if (fuelInventoryAdjustment.Period == 0)
                {
                    fuelInventoryAdjustment.Period = DateTime.Now.Month;

                    //var fuelInventory = await db.FuelInventory.FindAsync(fuelInventoryAdjustment.FuelSourceId, fuelInventoryAdjustment.FuelTypeId);
                    //if (fuelInventory == null)
                    //{
                    //    ModelState.AddModelError("", $"No inventory entry found for the specified fuel store and fuel type combination in the current period.");
                    //    return View(fuelInventoryAdjustment);
                    //}
                }
                var fuelInventoryHistory = await db.FuelInventoryHistory.FindAsync(fuelInventoryAdjustment.FuelSourceId, fuelInventoryAdjustment.FuelTypeId, fuelInventoryAdjustment.Year, fuelInventoryAdjustment.Period);
                if (fuelInventoryHistory == null)
                {
                    ModelState.AddModelError("", $"No inventory history entry found for the specified fuel store, fuel type, year and period combination.");
                    return View(fuelInventoryAdjustment);
                }
                fuelStore = fuelInventoryHistory.FuelSource.Name;
                fuelType = fuelInventoryHistory.FuelType.Name;
                db.Entry(fuelInventoryAdjustment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                var initiator = new EcmsModels.WebModels.ApplicationUser().GetByUsername(User.Identity.Name);
                var super = initiator.SubSection.GetOverseer();
                if (super != null)
                {
                    var message = new IdentityMessage();
                    message.Subject = $"AMMENDMENT to Fuel Stock Adjustment on {fuelStore} for {fuelType}.";
                    message.Body = $"{initiator.FullName} has edited a fuel stock adjustment in period {fuelInventoryAdjustment.Period} of year {fuelInventoryAdjustment.Year} on {fuelStore} for {fuelType} of {fuelInventoryAdjustment.AdjustmentQuantity} litres that needs your approval.";
                    if (super.Email != null)
                    {
                        message.Destination = super.Email;
                        await new EmailService().SendAsync(message);
                    }
                    if (super.PhoneNumber != null)
                    {
                        message.Destination = super.PhoneNumber;
                        await new SmsService().SendAsync(message);
                    }
                }
                return RedirectToAction("Index");
            }
            return View(fuelInventoryAdjustment);
       }

        // GET: FuelInventoryAdjustments/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if (fuelInventoryAdjustment == null)
            {
                return HttpNotFound();
            }
            return View(fuelInventoryAdjustment);
        }

        // POST: FuelInventoryAdjustments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            db.InventoryAdjustment.Remove(fuelInventoryAdjustment);
            await db.SaveChangesAsync();
            var initiator = new EcmsModels.WebModels.ApplicationUser().GetByUsername(User.Identity.Name);
            var super = initiator.SubSection.GetOverseer();
            if (super != null)
            {
                var message = new IdentityMessage();
                message.Subject = $"DELETION of Fuel Stock Adjustment.";
                message.Body = $"{initiator.FullName} has deleted a fuel stock adjustment in period {fuelInventoryAdjustment.Period} of year {fuelInventoryAdjustment.Year} for {fuelInventoryAdjustment.AdjustmentQuantity} litres. The adjustment was yet to be approved and effected.";
                if (super.Email != null)
                {
                    message.Destination = super.Email;
                    await new EmailService().SendAsync(message);
                }
                if (super.PhoneNumber != null)
                {
                    message.Destination = super.PhoneNumber;
                    await new SmsService().SendAsync(message);
                }
            }
            return RedirectToAction("Index");
        }

        // GET: FuelInventoryAdjustments/Approve/5
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if (fuelInventoryAdjustment == null)
            {
                return HttpNotFound();
            }
            return View(fuelInventoryAdjustment);
        }

        // POST: FuelInventoryAdjustments/Approve/5
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ApproveConfirmed(int id)
        {
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if (fuelInventoryAdjustment.Actioned == true)
            {
                ModelState.AddModelError("", "The adjustment has already been actioned.");
                return View(fuelInventoryAdjustment);
            }
            var actioner = new EcmsModels.WebModels.ApplicationUser().GetByUsername(User.Identity.Name);
            fuelInventoryAdjustment.ActionedBy = actioner.Id;
            fuelInventoryAdjustment.DateActioned = DateTime.Now.GetCondensedDateTime();
            fuelInventoryAdjustment.Actioned = true;
            fuelInventoryAdjustment.Approved = true;
            var inventoryHistory = await db.FuelInventoryHistory.FindAsync(fuelInventoryAdjustment.FuelSourceId, fuelInventoryAdjustment.FuelTypeId, fuelInventoryAdjustment.Year, fuelInventoryAdjustment.Period);
            if(inventoryHistory.Year == DateTime.Now.Year && inventoryHistory.Period == DateTime.Now.Month)
            {
                //Do nothing, the adjustment will be considered in calculating the daily running balance for the current period at day end run.
            }
            else
            {
                if(fuelInventoryAdjustment.AdjustmentType == InventoryAdjustmentTypes.Upward)
                {
                    inventoryHistory.ClosingQuantity += fuelInventoryAdjustment.AdjustmentQuantity;
                }
                else
                {
                    inventoryHistory.ClosingQuantity -= fuelInventoryAdjustment.AdjustmentQuantity;
                }
            }
            db.Entry(fuelInventoryAdjustment).State = EntityState.Modified;
            db.Entry(inventoryHistory).State = EntityState.Modified;
            await db.SaveChangesAsync();
            var initiator = db.Users.Find(fuelInventoryAdjustment.InitiatedBy);
            if (initiator != null)
            {
                var message = new IdentityMessage();
                message.Subject = $"Fuel Stock Adjustment Approved.";
                message.Body = $"{actioner.FullName} has APPROVED your fuel stock adjustment in period {fuelInventoryAdjustment.Period} of year {fuelInventoryAdjustment.Year} for {fuelInventoryAdjustment.AdjustmentQuantity} litres. The adjustment has been effected.";
                if (initiator.Email != null)
                {
                    message.Destination = initiator.Email;
                    await new EmailService().SendAsync(message);
                }
                if (initiator.PhoneNumber != null)
                {
                    message.Destination = initiator.PhoneNumber;
                    await new SmsService().SendAsync(message);
                }
            }
            return RedirectToAction("Index");
        }

        // GET: FuelInventoryAdjustments/Reject/5
        public async Task<ActionResult> Reject(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if (fuelInventoryAdjustment == null)
            {
                return HttpNotFound();
            }
            return View(fuelInventoryAdjustment);
        }

        // POST: FuelInventoryAdjustments/Reject/5
        [HttpPost, ActionName("Reject")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RejectConfirmed(int id)
        {
            FuelInventoryAdjustment fuelInventoryAdjustment = await db.InventoryAdjustment.FindAsync(id);
            if(fuelInventoryAdjustment.Actioned == true)
            {
                ModelState.AddModelError("", "The adjustment has already been actioned.");
                return View(fuelInventoryAdjustment);
            }
            var actioner = new EcmsModels.WebModels.ApplicationUser().GetByUsername(User.Identity.Name);
            fuelInventoryAdjustment.ActionedBy = actioner.Id;
            fuelInventoryAdjustment.DateActioned = DateTime.Now.GetCondensedDateTime();
            fuelInventoryAdjustment.Actioned = true;
            fuelInventoryAdjustment.Approved = false;
            db.Entry(fuelInventoryAdjustment).State = EntityState.Modified;
            await db.SaveChangesAsync();
            var initiator = db.Users.Find(fuelInventoryAdjustment.InitiatedBy);
            if (initiator != null)
            {
                var message = new IdentityMessage();
                message.Subject = $"Fuel Stock Adjustment Rejected.";
                message.Body = $"{actioner.FullName} has REJECTED your fuel stock adjustment in period {fuelInventoryAdjustment.Period} of year {fuelInventoryAdjustment.Year} for {fuelInventoryAdjustment.AdjustmentQuantity} litres.";
                if (initiator.Email != null)
                {
                    message.Destination = initiator.Email;
                    await new EmailService().SendAsync(message);
                }
                if (initiator.PhoneNumber != null)
                {
                    message.Destination = initiator.PhoneNumber;
                    await new SmsService().SendAsync(message);
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
