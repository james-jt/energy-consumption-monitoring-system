﻿using System.Web;
using System.Web.Optimization;

namespace EcmsWebApplication
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/bower_components/gentelella/vendors/jquery/dist/jquery.js"
                        //"~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        //"~/Scripts/jquery.validate.unobtrusive",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/globalize").Include(
                      "~/bower_components/cldrjs/dist/cldr.js",
                      "~/bower_components/globalize/dist/globalize.js",
                      "~/bower_components/globalize/dist/globalize.js",
                      "~/bower_components/globalize/dist/globalize/date.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval_globalized").Include(
                  "~/bower_components/jquery-validation-globalize/jquery.validate.globalize.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryunobtrusiveajax").Include(
            //"~/Scripts/jquery.validate.unobtrusive",jquery.validate.unobtrusive
            "~/Scripts/jquery.unobtrusive-ajax.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"
                      //"~/Content/datepicker.css"
                      ));


            //Custom bundles
            bundles.Add(new StyleBundle("~/Gentelella/css").Include(
                      "~/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.css",
                      "~/bower_components/gentelella/vendors/font-awesome/css/font-awesome.css",
                      "~/bower_components/gentelella/vendors/animate.css/animate.css",
                      "~/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css",
                      "~/bower_components/gentelella/vendors/nprogress/nprogress.css",
                      "~/bower_components/gentelella/vendors/iCheck/skins/flat.css",
                      "~/bower_components/gentelella/vendors/google-code-prettify/bin/prettify.css",
                      "~/bower_components/gentelella/vendors/select2/dist/css/select2.css",
                      "~/bower_components/gentelella/vendors/switchery/dist/switchery.css",
                      "~/bower_components/gentelella/vendors/starrr/dist/starrr.css"
                      ));

            bundles.Add(new StyleBundle("~/Custom/css").Include(
                      "~/bower_components/gentelella/build/css/custom.css"));

            bundles.Add(new StyleBundle("~/reportsmain/css").Include(
                      "~/Content/reportsmain.css"));


            bundles.Add(new ScriptBundle("~/bundles/gentelella").Include(
                "~/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.js",
                "~/bower_components/gentelella/vendors/fastclick/lib/fastclick.js",
                "~/bower_components/gentelella/vendors/nprogress/nprogress.js",
                "~/bower_components/gentelella/vendors/Chart.js/dist/Chart.js",
                "~/bower_components/gentelella/vendors/jquery-sparkline/dist/jquery.sparkline.js",
                "~/bower_components/gentelella/vendors/Flot/jquery.flot.js",
                "~/bower_components/gentelella/vendors/Flot/jquery.flot.pie.js",
                "~/bower_components/gentelella/vendors/Flot/jquery.flot.time.js",
                "~/bower_components/gentelella/vendors/Flot/jquery.flot.stack.js",
                "~/bower_components/gentelella/vendors/Flot/jquery.flot.resize.js",
                "~/bower_components/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                "~/bower_components/gentelella/vendors/flot-spline/js/jquery.flot.spline.js",
                "~/bower_components/gentelella/vendors/flot.curvedlines/curvedLines.js",
                "~/bower_components/gentelella/vendors/DateJS/build/date.js",
                "~/bower_components/gentelella/vendors/moment/min/moment.min.js",
                 "~/bower_components/gentelella/vendors/switchery/dist/switchery.js",
                 "~/bower_components/gentelella/vendors/select2/dist/js/select2.full.js",
                 "~/bower_components/gentelella/vendors/starrr/dist/starrr.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/gentelella_reports").Include(
                //"~/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.js"
                //"~/bower_components/gentelella/vendors/fastclick/lib/fastclick.js",
                //"~/bower_components/gentelella/vendors/nprogress/nprogress.js",
                //"~/bower_components/gentelella/vendors/Chart.js/dist/Chart.js",
                //"~/bower_components/gentelella/vendors/jquery-sparkline/dist/jquery.sparkline.js"
                //"~/bower_components/gentelella/vendors/Flot/jquery.flot.js",
                //"~/bower_components/gentelella/vendors/Flot/jquery.flot.pie.js",
                //"~/bower_components/gentelella/vendors/Flot/jquery.flot.time.js",
                //"~/bower_components/gentelella/vendors/Flot/jquery.flot.stack.js",
                //"~/bower_components/gentelella/vendors/Flot/jquery.flot.resize.js",
                //"~/bower_components/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                //"~/bower_components/gentelella/vendors/flot-spline/js/jquery.flot.spline.js",
                //"~/bower_components/gentelella/vendors/flot.curvedlines/curvedLines.js"
                //"~/bower_components/gentelella/vendors/DateJS/build/date.js",
                //"~/bower_components/gentelella/vendors/moment/min/moment.min.js",
                // "~/bower_components/gentelella/vendors/switchery/dist/switchery.js",
                // "~/bower_components/gentelella/vendors/select2/dist/js/select2.full.js",
                // "~/bower_components/gentelella/vendors/starrr/dist/starrr.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/Custom").Include(
                        "~/bower_components/gentelella/build/js/custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/customfileupload").Include(
                        "~/Scripts/customfileupload.js"));

            bundles.Add(new ScriptBundle("~/bundles/showmodalimage").Include(
                        "~/Scripts/showmodalimage.js"));

            //bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
            //            "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/dynamicdropdown").Include(
                        "~/Scripts/dynamicdropdown.js"));

            //bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
            //    "~/bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.js",
            //    "~/bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.js",
            //    "~/bower_components/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.js",
            //    "~/bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.js",
            //    "~/bower_components/gentelella/vendors/datatables.net-buttons/bs/js/buttons.bootstrap.js",
            //    "~/bower_components/gentelella/vendors/jszip/dist/jszip.js",
            //    "~/bower_components/gentelella/vendors/pdfmake/build/pdfmake.js",
            //    "~/bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js",
            //    "~/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.js",
            //     "~/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.js",
            //    "~/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.js"
            //   ));

            bundles.Add(new ScriptBundle("~/bundles/initializedatatables").Include(
            "~/Scripts/initializeDatatables.js"));

            bundles.Add(new StyleBundle("~/datatables/css").Include(
                "~/bower_components/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.css",
                "~/bower_components/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.css",
                "~/bower_components/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.css"
                ));

        }
    }
}
