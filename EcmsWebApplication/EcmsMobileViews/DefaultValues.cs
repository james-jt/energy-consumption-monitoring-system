﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public static partial class DefaultValues
    { 
        public const int FuelSourceCapacity = 10000;
        public const float FuelSourceReorderLevelPercentage = 0.2f;
        public const string DefaultDomainName = "@gmail.com";
        public const string DefaultDbEntityName = "Default";
        public const string DefaultAdministratorRole = "Admin";
        public const string DefaultAdministratorName = "Default Admin";
        public const string DefaultAdministratorEmail = "jamestaruvinga@gmail.com";
        public const string DefaultAdministratorPhoneNumber = "0640367654";
        public const string DefaultAdministratorPassword = "Admin@123";
        public const string DefaultDepartmentOverseerRole = "Department Overseer";
        public const string DefaultSectionOverseerRole = "Section Overseer";
        public const string DefaultSubSectionOverseerRole = "SubSection Overseer";
        public const string DefaultSubZoneOverseerRole = "SubZone Overseer";
        public const string DefaultMaintenanceCentreOverseerRole = "Maintenance Centre Overseer";
        public const string DefaultCouponStoresControllerRole = "Coupon Stores Controller";
        public const string DefaultUserRole = "User";
        public const string WarehouseFuelSourceTypeName = "Warehouse";
        public const string TpGarageFuelSourceTypeName = "Third Party Garage";
        public const string CouponsFuelSourceTypeName = "Coupons";
        public static string DefaultApplicationLogFilePath = $"{Directory.GetCurrentDirectory()}\\Application Log.txt";
    }
}
