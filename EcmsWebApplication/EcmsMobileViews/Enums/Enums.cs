﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels.Enums
{
    public enum FuelUpTargets
    {
        Vehicle,
        Generator,
        Drawdown
    }

    public enum AuditActionType
    {
        Deletion,
        Insertion,
        Modification,
        Creation,
        Access
    }

    public enum InventoryAdjustmentTypes
    { 
        Upward = 0, 
        Downward =1
    }

    public enum Periods
    {
        Current = 0,
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    public enum MeterTypes
    {  
        Prepaid = 0,
        Postpaid = 1
    }

    [Flags]
    public enum ReportFrequency
    {
        None        = 0,
        Annual      = 1 << 0,
        SemiAnnual  = 1 << 1,
        Quarterly   = 1 << 2,
        Monthly     = 1 << 3,
        Weekly      = 1 << 4,
        Daily       = 1 << 5,
        Hourly      = 1 << 6,
        OnDemand    = 1 << 7
    }
     

}
