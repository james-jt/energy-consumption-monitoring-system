﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class DrawdownViewModel
    {
        public string RequestNumber { get; set; }
        public string Comment { get; set; }
        public double Quantity { get; set; }
        public string FuelType { get; set; }
        public string AuthorisedBy { get; set; }
        public string DateAuthorised { get; set; }
        public string SourceType { get; set; }
        public string Source { get; set; }
        public string MaintenanceCentre { get; set; }
    }
}
