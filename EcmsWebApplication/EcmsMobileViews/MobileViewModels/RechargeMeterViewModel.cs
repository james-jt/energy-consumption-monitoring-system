﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class RechargeMeterViewModel
    {
        public string MaintenanceCentre { get; set; }
        public string Site { get; set; }
        public string Location { get; set; }
        public string RequestNumber { get; set; }
        public string Units { get; set; }
        public string MeterNumber { get; set; }
        public double UnitsBefore { get; set; }
        public double UnitsAfter { get; set; }
        public string UnitsBeforeImage { get; set; }
        public string UnitsAfterImage { get; set; }
    }
}
