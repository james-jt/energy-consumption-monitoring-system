﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class RequestViewModel
    {
        public string RequestNumber { get; set; }
        public DateTime DateRequested { get; set; }
        public string RequestedBy { get; set; }
        public double Quantity { get; set; }
        public string RequestorComment { get; set; }
        public string Department { get; set; }
        public string Site { get; set; }
        public string FuelType { get; set; }
    }
}
