﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class VehicleReadingsViewModel
    {
        public virtual string VehicleRegistrationNumber { get; set; }
        public string LoggedBy { get; set; }
        public double Mileage { get; set; }
        public double FuelLevel { get; set; }
        public string MileageImage { get; set; }
        public string FuelLevelImage { get; set; }
    }
} 
 