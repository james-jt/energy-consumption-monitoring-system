﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class ThirdPartyWarehouseDrawdownViewModel : DrawdownViewModel
    {
        public string MaintenanceCentre { get; set; }
        public string ThirdPartyGarage { get; set; }
    }
}
