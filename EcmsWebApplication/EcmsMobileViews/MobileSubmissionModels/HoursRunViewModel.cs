﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class HoursRunViewModel
    {
        public string MaintenanceCentre { get; set; }
        public string Site { get; set; } 
        public double GenHoursRun { get; set; }
    }
}
