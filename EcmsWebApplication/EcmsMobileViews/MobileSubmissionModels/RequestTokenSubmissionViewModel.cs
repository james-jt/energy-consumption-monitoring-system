﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class RequestTokenSubmissionViewModel
    {
        public string Requestor { get; set; }
        public DateTime RequestDate { get; set; }
        public string Department { get; set; }
        public string Site { get; set; }
        public string RequestType { get; set; }
        public string RequestNumber { get; set; }
        public string Comment { get; set; }
    }
}
