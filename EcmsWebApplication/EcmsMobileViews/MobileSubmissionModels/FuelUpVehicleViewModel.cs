﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EcmsModels.ViewModels
{
    public class FuelUpVehicleViewModel
    {  
        public string Department { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public string RequestNumber { get; set; }
        public string SourceType { get; set; }
        public string Source { get; set; }
        public double Quantity { get; set; }
        public double LevelBefore { get; set; }
        public double LevelAfter { get; set; }
        public double Mileage { get; set; }
        public string LevelBeforeImage { get; set; }
        public string LevelAfterImage { get; set; }
        public string MileageImage { get; set; }

    }
}
