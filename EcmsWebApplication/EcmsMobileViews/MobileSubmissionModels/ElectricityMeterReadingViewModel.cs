﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class ElectricityMeterReadingViewModel
    {
        public string LoggedBy { get; set; }
        public string MaintenanceCentre { get; set; }
        public string Site { get; set; }
        public string MeterReadingImage { get; set; }
        public double MeterReading { get; set; }
    } 
}
 