﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class FuelUpGeneratorViewModel
    {
        public string MaintenanceCentre { get; set; }
        public string Site { get; set; }
        public string RequestNumber { get; set; }
        //public string SourceType { get; set; }
        //public string Source { get; set; }
        public double LevelBefore { get; set; }
        public double LevelAfter { get; set; }
        public double HoursRun { get; set; } 
        public double Quantity { get; set; }
        public string LevelBeforeImage { get; set; }
        public string LevelAfterImage { get; set; }
        public string HoursRunImage { get; set; }
    }
} 
