﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class CouponsDrawdownViewModel : DrawdownViewModel
    {
        public string Coupons { get; set; }
    }
}
