﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class RequestDrawdownSubmissionViewModel
    { 
        public string Requestor { get; set; }
        public DateTime RequestDate { get; set; }
        public string SubSection { get; set; }
        public string RequestQuantity { get; set; }
        public string FuelType { get; set; }
        public string RequestType { get; set; }
        public string RequestNumber { get; set; }
        public string Comment { get; set; }
    }
}
