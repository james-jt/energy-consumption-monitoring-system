﻿using System;
using System.Collections.Generic;
using System.Text;
 
namespace EcmsModels.ViewModels
{
    public class RecordTripViewModel
    { 
        public string Department { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public double OpeningMileage { get; set; }
        public string OpeningMileageImage { get; set; }
        public double ClosingMileage { get; set; }
        public string ClosingMileageImage { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Purpose { get; set; }
        public string LoggedBy { get; set; }
    }
}
