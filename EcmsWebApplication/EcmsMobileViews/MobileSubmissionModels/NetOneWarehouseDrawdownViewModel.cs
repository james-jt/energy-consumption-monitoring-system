﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class NetOneWarehouseDrawdownViewModel : DrawdownViewModel
    {
        public string Warehouse { get; set; }
    }
}
