﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class GeneratorReadingsViewModel
    {
        public string LoggedBy { get; set; }
        public string MaintenanceCentre { get; set; }
        public virtual string Site { get; set; } 
        public double GenHoursRun { get; set; }
        public double GenFuelLevel { get; set; }
        public string GenHoursRunImage { get; set; }
        public string GenFuelLevelImage { get; set; }

    }
}
