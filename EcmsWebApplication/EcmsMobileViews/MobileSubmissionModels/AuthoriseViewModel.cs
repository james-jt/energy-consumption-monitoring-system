﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcmsModels.ViewModels
{
    public class AuthoriseViewModel
    {
        public string RequestNumber { get; set; }

        public DateTime DateActioned { get; set; }

        public string ActionedBy { get; set; }

        public string ActionerComment { get; set; }

        public bool Authorised { get; set; }
    }
}
