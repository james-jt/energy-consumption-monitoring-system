﻿using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EcmsBusinessLogic
{
    public class AuditLogItem<T> where T : class
    {
        public T Entity { get; set; }
        public string ActionerId { get; set; }
        public Type EntityType { get; set; }
        public string EntityId { get; set; }
        public AuditActionType ActionType { get; set; }
        public DateTime DateActioned { get; set; }

        public void Save()
        {
            var log = new AuditLog();
            EntityType = Entity.GetType();
            log.ActionerId = ActionerId;
            log.DateActioned = DateActioned.GetCondensedDateTime();
            var id = EntityType.GetProperty("Id");
            log.EntityId = id != null ? id.GetValue(Entity) as string : string.Empty;
            log.EntityType = EntityType.FullName;
            log.ActionType = ActionType.ToString();
            var db = new EcmsDbContext();
            db.AuditLog.Add(log);
            db.SaveChanges();
        }
    }
     
    public static class Logger
    {
        static string defaultApplicationLogFilePath = DefaultValues.DefaultApplicationLogFilePath;
        static int showBalloonDuration = 2000;

        public static void MakeAppLogEntry(string logMessage)
        {
            MakeAppLogEntry(logMessage, defaultApplicationLogFilePath);
        }

        public static void MakeAppLogEntry(string logMessage, NotifyIcon notifyIcon)
        {
            MakeAppLogEntry(logMessage, defaultApplicationLogFilePath);
            notifyIcon.BalloonTipText = logMessage;
            notifyIcon.ShowBalloonTip(showBalloonDuration);
        }

        public static void MakeAppLogEntry(string logMessage, string logFilePath)
        {
            if (string.IsNullOrEmpty(logMessage))
            {
                return;
            }
            if (string.IsNullOrEmpty(logFilePath))
            {
                logFilePath = defaultApplicationLogFilePath;
            }
            using (StreamWriter streamWriter = File.AppendText(logFilePath))
            {
                streamWriter.WriteLine();
                streamWriter.WriteLine($"{DateTime.Now} \t {logMessage}");
            }
        }

        public static void MakeAppLogEntry(string logMessage, string logFilePath, NotifyIcon notifyIcon)
        {
            if (string.IsNullOrEmpty(logMessage))
            {
                return;
            }
            if (string.IsNullOrEmpty(logFilePath))
            {
                logFilePath = defaultApplicationLogFilePath;
            }
            using (StreamWriter streamWriter = File.AppendText(logFilePath))
            {
                streamWriter.WriteLine();
                streamWriter.WriteLine($"{DateTime.Now} \t {logMessage}");
                notifyIcon.BalloonTipText = logMessage;
                notifyIcon.ShowBalloonTip(showBalloonDuration);
            }
        }


    }
}
