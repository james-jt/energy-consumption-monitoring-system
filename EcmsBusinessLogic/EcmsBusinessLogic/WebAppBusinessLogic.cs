﻿using EcmsModels.DataModels;
using EcmsModels.WebModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EcmsBusinessLogic
{
    public partial class BusinessLogic
    {

        #region Common Controller Helper Methods

        public static void RaiseReorderTrigger(FuelInventory inventory)
        {
            inventory.ReorderTriggered = true;

        }

        public static void ManageReorderTrigger(FuelInventory inventory)
        { 
            if (inventory.CurrentQuantity <= inventory.ReorderLevel)
            {
                if (!inventory.ReorderTriggered)
                    RaiseReorderTrigger(inventory);
            }
            else
            {
                inventory.ReorderTriggered = false;
            }
        }

        public static int GetNumericCouponNumber(string value)
        {
            var allowedChars = "0123456789";
            var stringCouponNumber =  new string(value.Where(c => allowedChars.Contains(c)).ToArray());
            var numericCouponNumber = Convert.ToInt32(stringCouponNumber);
            return numericCouponNumber;
        }

        public static string GetStringCouponNumber(string prototype, int numericCouponNumber)
        {
            var allowedChars = "01234567890";
            var temp = new string(prototype.Where(c => !allowedChars.Contains(c)).ToArray());
            var stringCouponNumber = temp + numericCouponNumber;
            return stringCouponNumber;
        }


        #endregion

        #region User Methods 

        public IEnumerable<ApplicationUser> GetUsersBySectionId(int id)
        { 
            var subsections = Context.Section.Find(id).SubSections;
            var users = new List<ApplicationUser>();
            subsections.ToList().ForEach(s => users.AddRange(s.Users));
            return users;
        }

        public IEnumerable<ApplicationUser> GetUsersBySubSectionId(int id)
        {
            var users = Context.SubSection.Find(id).Users;
            return users;
        }

        #endregion

        #region Department Methods

        public Department GetDepartmentById(int id)
        {
            try
            { 
                var department = Context.Department.Find(id);
                return department;
            }
            catch(Exception ex)
            {
                Logger.MakeAppLogEntry(ex.Message);
                throw;
            }
        }

        public IEnumerable<Department> GetAllDepartments()
        {
            var departments = Context.Department.ToList();
            return departments;
        }

        public int GetDepartmentId(string name)
        {
            try
            {
                var department = Context.Department.ToList().FirstOrDefault(d => d.Name == name.Trim()).Id;
                return department;
            }
            catch(Exception ex)
            {
                Logger.MakeAppLogEntry(ex.Message);
                throw;
            }
        }

        public bool CreateDepartment(Department model)
        {
            try
            {
                var result = Context.Department.SingleOrDefault(d => d.Name == model.Name);
                if (result == null)
                {
                    Context.Department.Add(model);
                    Context.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //EventLog.MakeLogEntry(ex.Message);
                return false;
            }
        }

        public bool UpdateDepartment(Department model)
        {
            if (model == null)
                return false;
            if (Context.Department.ToList().Where(e => e.Name == model.Name).Count() > 0)
                return false;
            try
            {
                var result = Context.Department.SingleOrDefault(d => d.Id == model.Id);
                if (result != null)
                {
                    result.Name = model.Name;
                    result.OverseerUserId = model.OverseerUserId;
                    result.Notes = model.Notes;
                    Context.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.MakeAppLogEntry(ex.Message);
                throw;
            }
        }

        #endregion
    }
}
