﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace EcmsBusinessLogic
{
    public class EmailService 
    {

        public async Task SendAsync(IdentityMessage message)
        {
            var emailService = new EcmsServices.EmailService();
            var mailMessage = new MailMessage();
            mailMessage.To.Add(message.Destination);
            mailMessage.Subject = message.Subject;
            mailMessage.Body = message.Body;
            try
            {
                await emailService.SendAsync(mailMessage);
            }
            catch(Exception e)
            {
                Logger.MakeAppLogEntry(e.Message);
            }
        }

        public static async Task SendAsync(string destination, string subject, string body)
        {
            var message = new IdentityMessage();
            message.Destination = destination;
            message.Body = body;
            message.Subject = subject;
            await new EmailService().SendAsync(message);
        }
    }

    public class SmsService 
    {
        public async Task SendAsync(IdentityMessage message)
        {

            try
            {
                
            }
            catch (Exception e)
            {
                Logger.MakeAppLogEntry(e.Message);
            }
        }

        public static async Task SendAsync(string destination, string subject, string body)
        {
            var message = new IdentityMessage();
            message.Destination = destination;
            message.Body = body;
            message.Subject = subject;
            await new SmsService().SendAsync(message);
        }

    }
}
