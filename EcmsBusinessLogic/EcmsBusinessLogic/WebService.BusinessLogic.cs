﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EcmsModels.ViewModels;
using EcmsModels.DataModels;
using EcmsBusinessLogic;
using EcmsModels.WebModels;
using System.Configuration;
using System.Net.Http;
using System.Net;
using EcmsModels.ViewModels.Enums;

namespace EcmsBusinessLogic
{
    public partial class BusinessLogic
    {
        public EcmsDbContext Context { get; set; }
        private static object padLock = new object();

        #region Constructors

        public BusinessLogic()
            :this(new EcmsDbContext())
        {
        }

        public BusinessLogic(EcmsDbContext context)
        {
            if (context != null)
                Context = context;
            else
                Context = new EcmsDbContext();
        }

        #endregion

        public List<DrawdownViewModel> GetFuelUp(string username, FuelUpTargets fuelUpTarget)
        {
            username = BusinessLogic.RestoreUsername(username);
            var requestsDetails = new List<DrawdownViewModel>();
            var requestor = Context.Users.FirstOrDefault(u => u.UserName == username);
            if (requestor != null)
            {
                switch (fuelUpTarget)
                {
                    case FuelUpTargets.Generator:
                        requestor.FuelRequests.Where(r => r.Drawdown != null && r.Drawdown.AvailableQuantity > 0).ToList().ForEach(e =>
                        {
                            var request = new DrawdownViewModel()
                            {
                                RequestNumber = e.Id,
                                AuthorisedBy = Context.Users.Find(e.ActionedBy).FullName,
                                Comment = e.RequestorComment,
                                DateAuthorised = e.DateActioned.ToString("dd/MM/yyyy HH:mm:ss"),
                                FuelType = Context.FuelType.Find(e.FuelTypeId).Name,
                                Quantity = e.Quantity
                            };
                            requestsDetails.Add(request);
                        });
                        break;
                    case FuelUpTargets.Drawdown:
                        requestor.FuelRequests.Where(r => r.ActionedBy != null && r.Drawdown == null).ToList().ForEach(e =>
                        {
                            var request = new DrawdownViewModel()
                            {
                                RequestNumber = e.Id,
                                AuthorisedBy = Context.Users.Find(e.ActionedBy).FullName,
                                Comment = e.RequestorComment,
                                DateAuthorised = e.DateActioned.ToString("dd/MM/yyyy HH:mm:ss"),
                                FuelType = Context.FuelType.Find(e.FuelTypeId).Name,
                                Quantity = e.Quantity
                            };
                            requestsDetails.Add(request);
                        });
                        break;
                    case FuelUpTargets.Vehicle:
                        requestor.FuelRequests.Where(r => (r.Drawdown != null && r.Drawdown.AvailableQuantity > 0) || (r.ActionedBy != null && r.Drawdown == null)).ToList().ForEach(e =>
                        {
                            var request = new DrawdownViewModel()
                            {
                                RequestNumber = e.Id,
                                AuthorisedBy = Context.Users.Find(e.ActionedBy).FullName,
                                Comment = e.RequestorComment,
                                DateAuthorised = e.DateActioned.ToString("dd/MM/yyyy HH:mm:ss"),
                                FuelType = Context.FuelType.Find(e.FuelTypeId).Name,
                                Quantity = e.Quantity
                            };
                            requestsDetails.Add(request);
                        });
                        break;
                    default:
                        break;
                }
            }
            return requestsDetails;
        }

        public async Task<HttpResponseMessage> PostFuelDrawdown(DrawdownViewModel viewModel)
        { 
            if (viewModel != null)
            {
                var request = await Context.FuelRequest.FindAsync(viewModel.RequestNumber);
                if (request != null)
                {
                    if (request.Drawdown == null)
                    {
                        var source = Context.FuelSource.Where(s => s.Name == viewModel.Source).FirstOrDefault();
                        if (source != null)
                        {
                            var inventory = source.FuelInventory.Where(i => i.FuelType.Id == request.FuelTypeId).FirstOrDefault();
                            if (inventory != null)
                            {
                                if (inventory.CurrentQuantity >= viewModel.Quantity)
                                {
                                    var fuelDrawdown = new Drawdown();
                                    fuelDrawdown.Id = request.Id;
                                    fuelDrawdown.FuelSourceId = source.Id;
                                    fuelDrawdown.Date = DateTime.Now;
                                    fuelDrawdown.AvailableQuantity = request.Quantity;
                                    fuelDrawdown.Notes = viewModel.Comment;
                                    fuelDrawdown.FuelRequest = request;
                                    request.Drawdown = fuelDrawdown;
                                    inventory.CurrentQuantity -= request.Quantity;
                                    if (inventory.CurrentQuantity <= inventory.ReorderLevel && !inventory.ReorderTriggered)
                                    {
                                        BusinessLogic.RaiseReorderTrigger(inventory);
                                    }
                                    await Context.SaveChangesAsync();
                                    return new HttpResponseMessage(HttpStatusCode.OK);
                                }
                                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                            }
                            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                        }
                        return new HttpResponseMessage(HttpStatusCode.NotFound);
                    }
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        #region Fuel Sources

        public string GetNextRequestNumber()
        {
            var sequence = string.Empty;
            lock (padLock)
            {
                sequence = ConfigurationManager.AppSettings["RequestSequence"];
                var numericSequence = Convert.ToInt32(sequence);
                numericSequence++;
                sequence = numericSequence.ToString();
                ConfigurationManager.AppSettings["RequestSequence"] = sequence;
            }
            if (sequence != null)
                return  sequence;
            return string.Empty;
        }

        #endregion

        #region Users

        public List<SiteViewModel> GetSiteNamesByMaintenanceCentre(string department)
        {
            var siteNames = new List<SiteViewModel>();
            Context.MaintenanceCentre.FirstOrDefault(d => d.Name == department).Sites.ToList().ForEach(s =>
            {
                var site = new SiteViewModel()
                {
                    Id = s.Id.ToString(),
                    Name = s.Name,
                    Location = s.Location
                };
                siteNames.Add(site);
            });
            return siteNames;
        }

        public IEnumerable<string> GetDepartmentForUser(string username)
        {
            username = BusinessLogic.RestoreUsername(username);
            var departmentName = new List<string>();
            var user = Context.Users.FirstOrDefault(u => u.UserName.Equals(username));
            if (user != null)
            {
                var dptId = Convert.ToInt16(user.SubSection.Section.DepartmentId);
                var departments = Context.Department.Where(m => m.Id.Equals(dptId));
                if (departments != null)
                {
                    departmentName.Add(departments.FirstOrDefault().Name);
                }
            }
            return departmentName; 
        }

        public IEnumerable<string> GetSectionForUser(string username)
        {
            username = BusinessLogic.RestoreUsername(username);
            var sectionName = new List<string>();
            var user = Context.Users.FirstOrDefault(u => u.UserName.Equals(username));
            if (user != null)
            {
                var sId = Convert.ToInt16(user.SubSection.SectionId);
                var sections = Context.Section.Where(m => m.Id.Equals(sId));
                if (sections != null)
                {
                    sectionName.Add(sections.FirstOrDefault().Name);
                }
            }
            return sectionName;
        }

        public IEnumerable<string> GetSubSectionForUser(string username)
        { 
            username = BusinessLogic.RestoreUsername(username);
            var subSectionName = new List<string>();
            var user = Context.Users.FirstOrDefault(u => u.UserName.Equals(username));
            if (user != null)
            {
                var ssId = Convert.ToInt16(user.SubSectionId);
                var subSections = Context.SubSection.Where(m => m.Id.Equals(ssId));
                if (subSections != null)
                {
                    subSectionName.Add(subSections.FirstOrDefault().Name + "|" + subSections.FirstOrDefault().Id);
                }
            }
            return subSectionName;
        }

        public static string RestoreUsername(string username)
        {
            if (username.EndsWith(DefaultValues.DefaultDomainName))
                return username;
            return username + DefaultValues.DefaultDomainName;
        }

        #endregion
    }
}
