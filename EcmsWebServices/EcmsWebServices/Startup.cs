﻿using System;
using System.Collections.Generic;
using System.Linq;
using EcmsModels.DataModels;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(EcmsWebServices.Startup))]

namespace EcmsWebServices
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
