﻿using EcmsBusinessLogic;
using EcmsModels;
using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/FuelSource")]
    public class FuelSourceController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/FuelSource/Warehouses
        [Route("Warehouses")]
        public Dictionary<string, string> GetWarehouses()
        { 
            var warehouseNames = new Dictionary<string, string>();
            Context.FuelSource.Where(s => s.FuelSourceType.Name == DefaultValues.WarehouseFuelSourceTypeName).ToList().ForEach(w =>
            {
                warehouseNames.Add(w.Name, w.Id.ToString());
            });
            return warehouseNames;
        }

        // GET: api/FuelSource/TpGarages
        [Route("TpGarages")]
        public Dictionary<string, string> GetTpGarages()
        { 
            var tpGarageNames = new Dictionary<string, string>();
            Context.FuelSource.Where(s => s.FuelSourceType.Name == DefaultValues.TpGarageFuelSourceTypeName).ToList().ForEach(t =>
            {
                tpGarageNames.Add(t.Name, t.Id.ToString());
            });
            return tpGarageNames;
        }

        //// GET: api/FuelSource/Coupons
        //[Route("Coupons")]
        //public IEnumerable<string> GetCoupons()
        //{
            
        //}

        // POST: api/FuelSource/Drawdown
        [Route("Drawdown")]
        public async Task<HttpResponseMessage> Drawdown()
        { 
            var viewModel = JsonConvert.DeserializeObject<DrawdownViewModel>(await Request.Content.ReadAsStringAsync());
            if (viewModel.SourceType == DefaultValues.CouponsFuelSourceTypeName)
            {
                //Call coupons method first
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return await new BusinessLogic().PostFuelDrawdown(viewModel);
            }
        }

    }
}
