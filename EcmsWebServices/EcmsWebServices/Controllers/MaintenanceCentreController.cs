﻿using EcmsModels.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/MaintenanceCentre")]
    public class MaintenanceCentreController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/MaintenanceCentre/Names/{subZoneId}
        [Route("Names/{subZoneId}")]
        public Dictionary<string, string> GetMaintenanceCentreNames(string subZoneId)
        {
            var maintenanceCentreNames = new Dictionary<string, string>();
            Context.MaintenanceCentre.Where(e => e.SubZoneId.ToString() == subZoneId).ToList().ForEach(d =>
            {
                maintenanceCentreNames.Add(d.Name, d.Id.ToString());
            });
            return maintenanceCentreNames;
        }

    }
}
