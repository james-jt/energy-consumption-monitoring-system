﻿using EcmsModels.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Section")]
    public class SectionController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Section/Names/{departmentId}
        [Route("Names/{departmentId}")]
        public Dictionary<string, string> GetSectionNames(string departmentId)
        { 
            var sectionNames = new Dictionary<string, string>();
            Context.Section.Where(e => e.DepartmentId.ToString() == departmentId).ToList().ForEach(d =>
            {
                sectionNames.Add(d.Name, d.Id.ToString());
            });
            return sectionNames;
        }
    }
}
