﻿using EcmsModels.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/SubZone")]
    public class SubZoneController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/SubZone/Names/{zoneId}
        [Route("Names/{zoneId}")]
        public Dictionary<string, string> GetSubZoneNames(string zoneId)
        {
            var subZoneNames = new Dictionary<string, string>();
            Context.SubZone.Where(e => e.SubSectionId.ToString() == zoneId).ToList().ForEach(d =>
            {
                subZoneNames.Add(d.Name, d.Id.ToString());
            });
            return subZoneNames;
        }
    }
}
