﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels.DataModels;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Department")]
    public class DepartmentController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Department/User/{username}
        [Route("User/{username}")]
        public IEnumerable<string> GetDepartment(string username)
        {
            return new BusinessLogic().GetDepartmentForUser(username);
        }

        // GET: api/Department/Names
        [Route("Names")]
        public Dictionary<string, string> GetDepartmentNames()
        {
            var departmentNames = new Dictionary<string, string>();
            Context.Department.ToList().ForEach(d => 
            {
                departmentNames.Add(d.Name, d.Id.ToString());
            });
            return departmentNames;
        }

    }
}
