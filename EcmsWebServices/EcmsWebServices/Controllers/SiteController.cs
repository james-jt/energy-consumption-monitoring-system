﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using EcmsModels.ViewModels.Enums;
using Newtonsoft.Json;
using static EcmsBusinessLogic.BusinessLogic;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Site")]
    public class SiteController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Site/Names/{maintenanceCentreId}
        [Route("Names/{maintenanceCentreId}")]
        public List<SiteViewModel> GetNames(string maintenanceCentreId)
        {
            var siteNames = new List<SiteViewModel>();
            Context.MaintenanceCentre.FirstOrDefault(e => e.Id.ToString() == maintenanceCentreId).Sites.ToList().ForEach(s =>
            {
                var site = new SiteViewModel()
                {
                    Id = s.Id.ToString(),
                    Name = s.Name,
                    Location = s.Location
                };
                siteNames.Add(site);
            });
            return siteNames;
        }

        // GET: api/Site/FuelUp/{username} 
        [Route("FuelUp/{username}")]
        public List<DrawdownViewModel> GetFuelUp(string username) //Get a fuelup
        {
            return new BusinessLogic().GetFuelUp(username, FuelUpTargets.Generator);
        }
         
        // POST: api/Site/FuelUp
        [HttpPost]
        [Route("FuelUp")]
        public async Task<HttpResponseMessage> FuelUp()
        {
            var viewModel = JsonConvert.DeserializeObject<FuelUpGeneratorViewModel>(await Request.Content.ReadAsStringAsync());
            if(viewModel != null)
            {
                var drawdown = await Context.Drawdown.FindAsync(Convert.ToInt32(viewModel.RequestNumber));
                if (drawdown != null)
                { 
                    var generator = Context.Generator.Where(s => s.Site.Name == viewModel.Site).FirstOrDefault();
                    if (generator != null)
                    {
                        if(drawdown.AvailableQuantity < viewModel.Quantity)
                            return new HttpResponseMessage(HttpStatusCode.Forbidden);
                        var fuelUp = new FuelUpGenerator();
                        fuelUp.Date = DateTime.Now;
                        fuelUp.DrawdownId = drawdown.Id;
                        fuelUp.FuelledBy = drawdown.FuelRequest.Requestor.Id;
                        fuelUp.GeneratorId = generator.Id;
                        fuelUp.Quantity = Convert.ToInt32(viewModel.Quantity);
                        fuelUp.HoursRun = Convert.ToInt32(viewModel.HoursRun);
                        fuelUp.LevelBefore = Convert.ToInt32(viewModel.LevelBefore);
                        fuelUp.LevelAfter = Convert.ToInt32(viewModel.LevelAfter);
                        fuelUp.Drawdown = drawdown;
                        fuelUp.Generator = generator;
                        Context.FuelUpGenerator.Add(fuelUp);
                        generator.GeneratorFuelUps.Add(fuelUp);
                        drawdown.GeneratorFuelUps.Add(fuelUp);
                        drawdown.AvailableQuantity -= fuelUp.Quantity;
                        await Context.SaveChangesAsync();
                        return new HttpResponseMessage(HttpStatusCode.OK);
                    }
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                return new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // POST: api/Site/GeneratorReadings
        [HttpPost]
        [Route("GeneratorReadings")]
        public async Task<HttpResponseMessage> GeneratorReadings()
        {
            var viewModel = JsonConvert.DeserializeObject<GeneratorReadingsViewModel>(await Request.Content.ReadAsStringAsync());
            if(viewModel != null)
            {
                var generator = Context.Generator.Where(g => g.Site.Name == viewModel.Site).FirstOrDefault();
                if(generator != null)
                {
                    viewModel.LoggedBy = RestoreUsername(viewModel.LoggedBy);
                    var loggedBy = Context.Users.Where(u => u.UserName == viewModel.LoggedBy).FirstOrDefault();

                    var fuelLevelCheck = new GenFuelLevelCheck();
                    fuelLevelCheck.Level = viewModel.GenFuelLevel;
                    fuelLevelCheck.DateChecked = DateTime.Now;
                    fuelLevelCheck.LoggedBy = loggedBy != null ? loggedBy.Id : string.Empty;
                    fuelLevelCheck.GeneratorId = generator.Id;
                    fuelLevelCheck.Generator = generator;
                    generator.GenFuelLevelChecks.Add(fuelLevelCheck);

                    var hoursRunCheck = new GenHoursRunCheck();
                    hoursRunCheck.HoursRun = viewModel.GenHoursRun;
                    hoursRunCheck.DateChecked = fuelLevelCheck.DateChecked;
                    hoursRunCheck.LoggedBy = fuelLevelCheck.LoggedBy;
                    hoursRunCheck.GeneratorId = fuelLevelCheck.GeneratorId;
                    hoursRunCheck.Generator = fuelLevelCheck.Generator;
                    generator.GenHoursRunChecks.Add(hoursRunCheck);

                    Context.GenFuelLevelCheck.Add(fuelLevelCheck);
                    Context.GenHoursRunCheck.Add(hoursRunCheck);
                    await Context.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("MeterReading")]
        public async Task<HttpResponseMessage> MeterReading()
        {
            var viewModel = JsonConvert.DeserializeObject<ElectricityMeterReadingViewModel>(await Request.Content.ReadAsStringAsync());
            if (viewModel != null)
            {
                var meter = Context.ElectrictyMeter.Where(m => m.Site.Name == viewModel.Site).FirstOrDefault();
                if (meter != null)
                {
                    viewModel.LoggedBy = RestoreUsername(viewModel.LoggedBy);
                    var loggedBy = Context.Users.Where(u => u.UserName == viewModel.LoggedBy).FirstOrDefault();
                    var electricityLevelCheck = new ElectricityLevelCheck();
                    electricityLevelCheck.Units = viewModel.MeterReading;
                    electricityLevelCheck.DateChecked = DateTime.Now;
                    electricityLevelCheck.LoggedBy = loggedBy != null? loggedBy.Id : string.Empty;
                    electricityLevelCheck.MeterId = meter.Id;
                    electricityLevelCheck.ElectrictyMeter = meter;
                    meter.ElectricityLevelChecks.Add(electricityLevelCheck);
                    var test = viewModel.MeterReadingImage;
                    Context.ElectricityLevelCheck.Add(electricityLevelCheck);
                    await Context.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // POST: api/Site/RechargeMeter
        [HttpPost]
        [Route("RechargeMeter")]
        public async Task<HttpResponseMessage> RechargeMeter()
        {
            var viewModel = JsonConvert.DeserializeObject<RechargeMeterViewModel>(await Request.Content.ReadAsStringAsync());
            if (viewModel != null)
            {
                var request = await Context.ElectricityRequest.FindAsync(Convert.ToInt32(viewModel.RequestNumber));
                var meter = await Context.ElectrictyMeter.FindAsync(viewModel.MeterNumber);
                if (request != null)
                {
                    if (request.ElectricityTokenIssue != null)
                    {
                        var electricityRecharge = new ElectricityRecharge();
                        electricityRecharge.Date = DateTime.Now;
                        electricityRecharge.Id = request.Id;
                        electricityRecharge.MeterId = meter.Id;
                        electricityRecharge.UnitsAfter = Convert.ToInt32(viewModel.UnitsAfter);
                        electricityRecharge.UnitsBefore = Convert.ToInt32(viewModel.UnitsBefore);
                        electricityRecharge.ElectricityTokenIssue = request.ElectricityTokenIssue;
                        electricityRecharge.ElectrictyMeter = meter;
                        request.ElectricityTokenIssue.ElectricityRecharge = electricityRecharge;
                        meter.ElectricityRecharges.Add(electricityRecharge);
                        Context.ElectricityRecharge.Add(electricityRecharge);
                        await Context.SaveChangesAsync();
                        return new HttpResponseMessage(HttpStatusCode.OK);
                    }
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);
                }
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
    }
}
