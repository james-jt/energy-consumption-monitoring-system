﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels;
using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using Newtonsoft.Json;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/TpGarage")]
    public class TpGarageController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/TpGarage/Names
        [Route("Names")]
        public Dictionary<string, string> GetNames()
        {
            var tpGarageNames = new Dictionary<string, string>();
            Context.FuelSource.Where(s => s.FuelSourceType.Description == DefaultValues.TpGarageFuelSourceTypeName).ToList().ForEach(t =>
            {
                tpGarageNames.Add(t.Description, t.Id.ToString());
            });
            return tpGarageNames;
        }

        // POST: api/TpGarage/FuelDrawdown
        [Route("FuelDrawdown")]
        public async Task<HttpResponseMessage> FuelDrawdown()
        {
            var viewModel = JsonConvert.DeserializeObject<DrawdownViewModel>(await Request.Content.ReadAsStringAsync());
            return await new BusinessLogic().PostFuelDrawdown(viewModel);
        }

    }
}
