﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels.DataModels;
using EcmsModels.WebModels;
using EcmsWebServices.Models;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix ("api/User")]
    public class UserController : ApiController
    {
        // GET: api/User/PasswordHash/{username}
        [Route("PasswordHash/{username}")]
        public IEnumerable<string> GetPasswordHash(string username)
        {
            var fullUsername = BusinessLogic.RestoreUsername(username);
            return new BusinessLogic().GetPasswordHashForUser(fullUsername);
        }

        // GET: api/User/Department/{username}
        [Route("Department/{username}")]
        public IEnumerable<string> GetDepartment(string username)
        {
            var context = new EcmsDbContext();
            var fullUsername = BusinessLogic.RestoreUsername(username);
            return new BusinessLogic().GetDepartmentForUser(fullUsername);
        }
    }
}
