﻿using EcmsBusinessLogic;
using EcmsModels.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/SubSection")]
    public class SubSectionController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/SubSection/User/{username}
        [Route("User/{username}")]
        public IEnumerable<string> GetSubSection(string username)
        { 
            return new BusinessLogic().GetSubSectionForUser(username);
        }

        // GET: api/SubSection/Names/{sectionId}
        [Route("Names/{sectionId}")]
        public Dictionary<string, string> GetSubSectionNames(string sectionId)
        {
            var subSectionNames = new Dictionary<string, string>();
            Context.SubSection.Where(e => e.SectionId.ToString() == sectionId).ToList().ForEach(d =>
            {
                subSectionNames.Add(d.Name, d.Id.ToString());
            });
            return subSectionNames;
        }

        // GET: api/SubSection/Zones/{sectionId}
        [Route("Zones/{sectionId}")]
        public Dictionary<string, string> GetZoneNames(string sectionId)
        {
            var zoneNames = new Dictionary<string, string>();
            Context.SubSection.Where(e => e.SectionId.ToString() == sectionId && e.SubZones.Count() > 0).ToList().ForEach(d =>
            {
                zoneNames.Add(d.Name, d.Id.ToString());
            });
            return zoneNames;
        }
    }
}
