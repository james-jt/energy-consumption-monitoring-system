﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using Newtonsoft.Json;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Coupon")]
    public class CouponController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Coupon/Numbers
        [Route("Numbers")]
        public IEnumerable<string> GetNames()
        {
            
        }

        // POST: api/Coupon/FuelDrawdown
        [Route("FuelDrawdown")]
        public async Task<HttpResponseMessage> FuelDrawdown() //Action might need customisation 
        {
            var viewModel = JsonConvert.DeserializeObject<DrawdownViewModel>(await Request.Content.ReadAsStringAsync());
            return await new BusinessLogic().PostFuelDrawdown(viewModel);
        }

    }
}
