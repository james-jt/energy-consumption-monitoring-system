﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels;
using EcmsModels.DataModels;
using EcmsModels.ViewModels;
using Newtonsoft.Json;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Warehouse")]
    public class WarehouseController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Warehouse/Names
        [Route("Names")]
        public Dictionary<string, string> GetNames()
        {
            var warehouseNames = new Dictionary<string, string>();
            Context.FuelSource.Where(s => s.FuelSourceType.Description == DefaultValues.WarehouseFuelSourceTypeName).ToList().ForEach(w =>
            {
                warehouseNames.Add(w.Description, w.Id.ToString());
            });
            return warehouseNames;
        }

        // POST: api/Warehouse/FuelDrawdown
        [Route("FuelDrawdown")]
        public async Task<HttpResponseMessage> FuelDrawdown()
        {
            var viewModel = JsonConvert.DeserializeObject<DrawdownViewModel>(await Request.Content.ReadAsStringAsync());
            if(viewModel.SourceType == DefaultValues.CouponsFuelSourceTypeName)
            {
                //Call coupons method first
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return await new BusinessLogic().PostFuelDrawdown(viewModel);
            }
        }
    }
}
