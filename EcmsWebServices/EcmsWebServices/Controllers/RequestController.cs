﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EcmsModels.ViewModels;
using Newtonsoft.Json;
using EcmsBusinessLogic;
using EcmsModels.DataModels;
using Microsoft.AspNet.Identity;
using EcmsModels.ViewModels.Enums;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Request")]
    public class RequestController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Request/FuelTypes
        [Route("FuelTypes")]
        public Dictionary<string, string> GetFuelTypes() //Get fuel types
        {
            var fuelTypes = new Dictionary<string, string>();
            Context.FuelType.ToList().ForEach(t => fuelTypes.Add(t.Name, t.Id.ToString()));
            return fuelTypes;
        }

        // GET: api/Request/NextRequestNumber/{isFuelRequest}
        [Route("NextRequestNumber/{isFuelRequest}")]
        public List<string> GetNextRequestNumber(bool isFuelRequest) //Get a fuel or electricity request
        {
            var requestSequence = new BusinessLogic().GetNextRequestNumber();
            if (isFuelRequest)
                requestSequence = "TR-" + requestSequence;
            else
                requestSequence = "ER-" + requestSequence;
            return new List<string>() { requestSequence };
        }

        // POST: api/Request/FuelRequest
        [HttpPost]
        [Route("FuelRequest")]
        public async Task<HttpResponseMessage> PostFuelRequest()  //Posting a fuel request
        {
            var viewModel = JsonConvert.DeserializeObject<RequestDrawdownSubmissionViewModel>(await Request.Content.ReadAsStringAsync());
            if(viewModel != null)
            {
                viewModel.Requestor = BusinessLogic.RestoreUsername(viewModel.Requestor);
                var requestor = Context.Users.Where(u => u.UserName == viewModel.Requestor).FirstOrDefault();
                FuelRequest fuelRequest = new FuelRequest()
                {
                    DateRequested = DateTime.MinValue.ConvertToDbMinDate(),
                    DateActioned = DateTime.MinValue.ConvertToDbMinDate()
                };
                fuelRequest.DateRequested = viewModel.RequestDate;
                fuelRequest.Quantity = Convert.ToInt32(viewModel.RequestQuantity);
                fuelRequest.FuelTypeId = Context.FuelType.Where(t => t.Name == viewModel.FuelType).FirstOrDefault().Id;
                fuelRequest.RequestorComment = viewModel.Comment;
                fuelRequest.Id = viewModel.RequestNumber;
                fuelRequest.RequestedBy = requestor.Id;
                fuelRequest.DepartmentId = requestor.GetDepartment().Id;
                fuelRequest.SectionId = requestor.GetSection().Id;
                fuelRequest.SubSectionId = requestor.SubSectionId;
                fuelRequest.Requestor = requestor;
                Context.FuelRequest.Add(fuelRequest);
                await Context.SaveChangesAsync();
                var superId = requestor.SubSection.OverseerUserId;
                var super = Context.Users.Find(superId);
                if(super != null)
                {
                    string body = $"{requestor.FullName} has sent you a request number {fuelRequest.Id} for {fuelRequest.Quantity} litres of fuel. Please review this request on your Ecms Mobile application.";
                    string subject = $"New Fuel Drawdown Request Number {fuelRequest.Id}";
                    if (!string.IsNullOrWhiteSpace(super.Email))
                        await EmailService.SendAsync(super.Email, subject, body);
                    if (!string.IsNullOrWhiteSpace(super.PhoneNumber))
                        await SmsService.SendAsync(super.PhoneNumber, subject, body);
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // POST: api/Request/ElectricityRequest
        [HttpPost]
        [Route("ElectricityRequest")]
        public async Task<HttpResponseMessage> PostElectricityRequest() //Posting an electricity request
        {
            var viewModel = JsonConvert.DeserializeObject<RequestTokenSubmissionViewModel>(await Request.Content.ReadAsStringAsync());
            if (viewModel != null)
            {
                viewModel.Requestor = BusinessLogic.RestoreUsername(viewModel.Requestor);
                var requestor = Context.Users.Where(u => u.UserName == viewModel.Requestor).FirstOrDefault();
                var site = Context.Site.Where(s => s.Name == viewModel.Site).FirstOrDefault();
                ElectricityRequest electricityRequest = new ElectricityRequest();
                electricityRequest.DateRequested = viewModel.RequestDate;
                electricityRequest.SiteId = site.Id;
                electricityRequest.RequestorComment = viewModel.Comment;
                electricityRequest.Id = viewModel.RequestNumber;
                electricityRequest.RequestedBy = requestor.Id;
                electricityRequest.DepartmentId = requestor.GetDepartment().Id;
                electricityRequest.SectionId = requestor.GetSection().Id;
                electricityRequest.SubSectionId = requestor.SubSectionId;
                electricityRequest.Requestor = requestor;
                Context.ElectricityRequest.Add(electricityRequest);
                await Context.SaveChangesAsync();
                var superId = requestor.SubSection.OverseerUserId;
                var super = Context.Users.Find(superId);
                if (super != null)
                {
                    string body = $"{requestor.FullName} has sent you an electricity token request number {electricityRequest.Id} for {site.Name} site. Please review this request on your Ecms Mobile application.";
                    string subject = $"New Electricity Token Request Number {electricityRequest.Id}";
                    if (!string.IsNullOrWhiteSpace(super.Email))
                        await EmailService.SendAsync(super.Email, subject, body);
                    if (!string.IsNullOrWhiteSpace(super.PhoneNumber))
                        await SmsService.SendAsync(super.PhoneNumber, subject, body);
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // GET: api/Request/FuelAuthorisation/{username} 
        [Route("FuelAuthorisation/{username}")]
        public List<RequestViewModel> GetFuelAuthorisation(string username) //Get fuel authorisation 
        {
            username = BusinessLogic.RestoreUsername(username);
            var requestsDetails = new List<RequestViewModel>();
            var authoriser = Context.Users.Where(u => u.UserName == username).FirstOrDefault();
            if (authoriser != null)
            {
                var requestsForAuthorisation = Context.FuelRequest.Where(r =>
                    r.ActionedBy == null &&
                    r.Requestor.SubSection.OverseerUserId == authoriser.Id
                    ).ToList();
                requestsForAuthorisation.ForEach(r =>
                {
                    var fuelType = Context.FuelType.Find(r.FuelTypeId);
                    var requestDetails = new RequestViewModel();
                    requestDetails.RequestNumber = r.Id;
                    requestDetails.DateRequested = r.DateRequested;
                    requestDetails.Department = r.Requestor.SubSection.Name;
                    requestDetails.FuelType = fuelType.Name;
                    requestDetails.Quantity = r.Quantity;
                    requestDetails.RequestedBy = r.Requestor.FullName;
                    requestDetails.RequestorComment = r.RequestorComment;
                    requestDetails.Site = string.Empty;
                    requestsDetails.Add(requestDetails);
                });
            }
            return requestsDetails;
        }

        // GET: api/Request/ElectricityAuthorisation/{username}
        [Route("ElectricityAuthorisation/{username}")]
        public List<RequestViewModel> GetElectricityAuthorisation(string username) //Get electricity authorisation
        {
            username = BusinessLogic.RestoreUsername(username);
            var requestsDetails = new List<RequestViewModel>();
            var authoriser = Context.Users.Where(u => u.UserName == username).FirstOrDefault();
            if (authoriser != null)
            {
                var requestsForAuthorisation = Context.ElectricityRequest.Where(r =>
                    r.ActionedBy == null &&
                    r.Requestor.SubSection.OverseerUserId == authoriser.Id
                    ).ToList();
                requestsForAuthorisation.ForEach(r =>
                {
                    var requestDetails = new RequestViewModel();
                    requestDetails.RequestNumber = r.Id;
                    requestDetails.DateRequested = r.DateRequested;
                    requestDetails.Department = r.Requestor.SubSection.Name;
                    requestDetails.FuelType = string.Empty;
                    requestDetails.Quantity = r.Units;
                    requestDetails.RequestedBy = r.Requestor.FullName;
                    requestDetails.RequestorComment = r.RequestorComment;
                    requestDetails.Site = Context.Site.Find(r.SiteId).Name;
                    requestsDetails.Add(requestDetails);

                });
            }
            return requestsDetails;
        }
         
        // POST: api/Request/Authorize
        [HttpPost]
        [Route("Authorize")]
        public async Task<HttpResponseMessage> Authorise() //Post fuel or electricity authorisation
        {
            var viewModel = JsonConvert.DeserializeObject<AuthoriseViewModel>(await Request.Content.ReadAsStringAsync());
            if(viewModel != null)
            {
                viewModel.ActionedBy = BusinessLogic.RestoreUsername(viewModel.ActionedBy);
                var actioner = Context.Users.Where(u => u.UserName == viewModel.ActionedBy).FirstOrDefault();
                if (viewModel.RequestNumber.StartsWith("TR"))
                {
                    var fuelRequest = await Context.FuelRequest.FindAsync(viewModel.RequestNumber);
                    if (fuelRequest == null)
                        return new HttpResponseMessage(HttpStatusCode.NotFound);
                    fuelRequest.ActionedBy = actioner.Id;
                    fuelRequest.ActionerComment = viewModel.ActionerComment;
                    fuelRequest.DateActioned = viewModel.DateActioned;
                    fuelRequest.Authorised = viewModel.Authorised;
                    await Context.SaveChangesAsync();
                    if (fuelRequest.Requestor != null)
                    {
                        string decision = fuelRequest.Authorised ? "APPROVED" : "REJECTED";
                        string body = $"{actioner.FullName} has {decision} your request number {fuelRequest.Id} for {fuelRequest.Quantity} litres of fuel.";
                        string subject = $"Fuel Drawdown Request Number {fuelRequest.Id} Response";
                        if (!string.IsNullOrWhiteSpace(fuelRequest.Requestor.Email))
                            await EmailService.SendAsync(fuelRequest.Requestor.Email, subject, body);
                        if (!string.IsNullOrWhiteSpace(fuelRequest.Requestor.PhoneNumber))
                            await SmsService.SendAsync(fuelRequest.Requestor.PhoneNumber, subject, body);
                    }
                }
                else if (viewModel.RequestNumber.StartsWith("ER"))
                {
                    var electricityRequest = await Context.ElectricityRequest.FindAsync(viewModel.RequestNumber);
                    if (electricityRequest == null)
                        return new HttpResponseMessage(HttpStatusCode.NotFound);
                    electricityRequest.ActionedBy = actioner.Id;
                    electricityRequest.ActionerComment = viewModel.ActionerComment;
                    electricityRequest.DateActioned = viewModel.DateActioned;
                    electricityRequest.Authorised = viewModel.Authorised;
                    await Context.SaveChangesAsync();
                    if (electricityRequest.Requestor != null)
                    {
                        string decision = electricityRequest.Authorised ? "APPROVED" : "REJECTED";
                        string body = $"{actioner.FullName} has {decision} your request number {electricityRequest.Id} for electricty for {(await Context.Site.FindAsync(electricityRequest.SiteId)).Name} site.";
                        string subject = $"Electricity Token Request Number {electricityRequest.Id} Response";
                        if (!string.IsNullOrWhiteSpace(electricityRequest.Requestor.Email))
                            await EmailService.SendAsync(electricityRequest.Requestor.Email, subject, body);
                        if (!string.IsNullOrWhiteSpace(electricityRequest.Requestor.PhoneNumber))
                            await SmsService.SendAsync(electricityRequest.Requestor.PhoneNumber, subject, body);
                    }
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // GET: api/Request/FuelDrawdown/{username} : See FuelSourceController for the attendant POSTs
        [Route("FuelDrawdown/{username}")]
        public List<DrawdownViewModel> GetFuelDrawdown(string username) //Get a fuel drawdown
        {
            return new BusinessLogic().GetFuelUp(username, FuelUpTargets.Drawdown);
        }

        // GET: api/Request/ElectricityRecharge/{username} : See SiteController for the attendant POST
        [Route("ElectricityRecharge/{username}")]
        public List<RechargeMeterViewModel> GetElectricityRecharge(string username) //Get an electricity recharge
        {
            username = BusinessLogic.RestoreUsername(username);
            var requestsDetails = new List<RechargeMeterViewModel>();
            var requestor = Context.Users.Where(u => u.UserName == username).FirstOrDefault();
            if (requestor != null)
            {
                var authorisedRequests = requestor.ElectricityRequests.Where(r => r.ElectricityTokenIssue != null && r.ElectricityTokenIssue.ElectricityRecharge == null).ToList();
                authorisedRequests.ForEach(r =>
                {
                    var meter = Context.ElectrictyMeter.Where(m => m.SiteId == r.SiteId).FirstOrDefault();
                    var requestDetails = new RechargeMeterViewModel();
                    requestDetails.RequestNumber = r.Id;
                    requestDetails.MaintenanceCentre = meter.Site.MaintenanceCentre.Name;
                    requestDetails.MeterNumber = meter.Id;
                    requestDetails.Units = r.ElectricityTokenIssue.ElectricityPurchases.EnergyBought.ToString();
                    requestDetails.Site = meter.Site.Name;
                    requestsDetails.Add(requestDetails);
                });
            }
            return requestsDetails;
        }
    }
}
