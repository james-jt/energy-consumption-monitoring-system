﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EcmsBusinessLogic;
using EcmsModels.ViewModels;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Threading.Tasks;
using EcmsModels.DataModels;
using static EcmsBusinessLogic.BusinessLogic;
using EcmsModels.ViewModels.Enums;

namespace EcmsWebServices.Controllers
{
    [RoutePrefix("api/Vehicle")]
    public class VehicleController : ApiController
    {
        EcmsDbContext Context = new EcmsDbContext();

        // GET: api/Vehicle/RegistrationNumbers/{subSectionId}
        [Route("RegistrationNumbers/{subSectionId}")]
        public IEnumerable<string> GetRegistrationNumbers(string subSectionId)
        {
            var vehicleRegNumbers = new List<string>();
            Context.Vehicle.Where(v => v.SubSectionId.ToString() == subSectionId).ToList().ForEach(v => vehicleRegNumbers.Add(v.Id));
            return vehicleRegNumbers;
        }

        // POST: api/Vehicle/RecordTrip
        [HttpPost]
        [Route("RecordTrip")]
        public async Task<HttpResponseMessage> RecordTrip()
        {
            var viewModel = JsonConvert.DeserializeObject<RecordTripViewModel>(await Request.Content.ReadAsStringAsync());
            if(viewModel != null)
            {
                var vehicle = await Context.Vehicle.FindAsync(viewModel.VehicleRegistrationNumber);
                if(vehicle != null)
                {
                    viewModel.LoggedBy = RestoreUsername(viewModel.LoggedBy);
                    var loggedBy = Context.Users.Where(u => u.UserName == viewModel.LoggedBy).FirstOrDefault();

                    var vehicleTrip = new VehicleTrip();
                    vehicleTrip.Origin = viewModel.Origin;
                    vehicleTrip.Destination = viewModel.Destination;
                    vehicleTrip.Purpose = viewModel.Purpose;
                    vehicleTrip.OpeningMileage = viewModel.OpeningMileage;
                    vehicleTrip.OpeningMileageImage = viewModel.OpeningMileageImage;
                    vehicleTrip.ClosingMileage = viewModel.ClosingMileage;
                    vehicleTrip.ClosingMileageImage = viewModel.ClosingMileageImage;
                    vehicleTrip.DateRecorded = DateTime.Now;
                    vehicleTrip.VehicleId = viewModel.VehicleRegistrationNumber;
                    vehicleTrip.Vehicle = vehicle;
                    vehicleTrip.LoggedBy = loggedBy != null ? loggedBy.Id : string.Empty;
                    Context.VehicleTrip.Add(vehicleTrip);
                    Context.VehicleTrip.Add(vehicleTrip);
                    await Context.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // POST: api/Vehicle/Readings
        [HttpPost]
        [Route("Readings")]
        public async Task<HttpResponseMessage> Readings()
        {
            var viewModel = JsonConvert.DeserializeObject<VehicleReadingsViewModel>(await Request.Content.ReadAsStringAsync());
            if (viewModel != null)
            {
                var vehicle = await Context.Vehicle.FindAsync(viewModel.VehicleRegistrationNumber);
                if (vehicle != null)
                {
                    viewModel.LoggedBy = RestoreUsername(viewModel.LoggedBy);
                    var loggedBy = Context.Users.Where(u => u.UserName == viewModel.LoggedBy).FirstOrDefault();

                    var fuelLevelCheck = new VehicleFuelLevelCheck();
                    fuelLevelCheck.Level = viewModel.FuelLevel;
                    fuelLevelCheck.DateChecked = DateTime.Now;
                    fuelLevelCheck.LoggedBy = loggedBy != null ? loggedBy.Id : string.Empty;
                    fuelLevelCheck.VehicleId = vehicle.Id;
                    fuelLevelCheck.Vehicle = vehicle;
                    vehicle.VehicleFuelLevelChecks.Add(fuelLevelCheck);

                    var mileageCheck = new VehicleMileageCheck();
                    mileageCheck.Mileage = viewModel.Mileage;
                    mileageCheck.DateChecked = fuelLevelCheck.DateChecked;
                    mileageCheck.LoggedBy = fuelLevelCheck.LoggedBy;
                    mileageCheck.VehicleId = fuelLevelCheck.VehicleId;
                    mileageCheck.Vehicle = fuelLevelCheck.Vehicle;
                    vehicle.VehicleMileageChecks.Add(mileageCheck);

                    Context.VehicleFuelLevelCheck.Add(fuelLevelCheck);
                    Context.VehicleMileageCheck.Add(mileageCheck);
                    await Context.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
         
        // GET: api/Vehicle/FuelUp/{username} 
        [Route("FuelUp/{username}")]
        public List<DrawdownViewModel> GetFuelUp(string username) //Get a fuelup
        {
            return new BusinessLogic().GetFuelUp(username, FuelUpTargets.Vehicle);
        }

        // POST: api/Vehicle/FuelUp
        [HttpPost]
        [Route("FuelUp")]
        public async Task<HttpResponseMessage> FuelUp()
        {
            var viewModel = JsonConvert.DeserializeObject<FuelUpVehicleViewModel>(await Request.Content.ReadAsStringAsync());
            if (viewModel != null)
            {
                var vehicle = await Context.Vehicle.FindAsync(viewModel.VehicleRegistrationNumber);
                if (vehicle != null)
                {
                    var drawdown = await Context.Drawdown.FindAsync(viewModel.RequestNumber);
                    if (drawdown == null)
                    {
                        var request = await Context.FuelRequest.FindAsync(viewModel.RequestNumber);
                        if (request != null)
                        {
                            var drawdownViewModel = new DrawdownViewModel();
                            drawdownViewModel.RequestNumber = request.Id;
                            drawdownViewModel.FuelType = request.FuelTypeId.ToString();
                            drawdownViewModel.Quantity = request.Quantity;
                            drawdownViewModel.AuthorisedBy = request.ActionedBy;
                            drawdownViewModel.DateAuthorised = request.DateActioned.ToString();
                            drawdownViewModel.Comment = request.RequestorComment;
                            drawdownViewModel.Source = viewModel.Source;
                            await new BusinessLogic().PostFuelDrawdown(drawdownViewModel);
                            drawdown = await Context.Drawdown.FindAsync(viewModel.RequestNumber);
                            if (drawdown == null)
                                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                        }
                        else
                        {
                            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                        }
                    }
                    var fuelUp = new FuelUpVehicle();
                    fuelUp.Date = DateTime.Now;
                    fuelUp.DrawdownId = drawdown.Id;
                    fuelUp.FuelledBy = drawdown.FuelRequest.Requestor.Id;
                    fuelUp.VehicleId = vehicle.Id;
                    fuelUp.Quantity = Convert.ToInt32(drawdown.AvailableQuantity);
                    fuelUp.Mileage = Convert.ToInt32(viewModel.Mileage);
                    fuelUp.LevelBefore = Convert.ToInt32(viewModel.LevelBefore);
                    fuelUp.LevelAfter = Convert.ToInt32(viewModel.LevelAfter);
                    fuelUp.Drawdown = drawdown;
                    fuelUp.Vehicle = vehicle;
                    Context.FuelUpVehicle.Add(fuelUp);
                    vehicle.VehicleFuelUps.Add(fuelUp);
                    drawdown.VehicleFuelUps.Add(fuelUp);
                    drawdown.AvailableQuantity -= fuelUp.Quantity;
                    await Context.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
    }
}
